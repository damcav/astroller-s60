# ============================================================================
#  Name	 : build_help.mk
#  Part of  : QGazer
# ============================================================================
#  Name	 : build_help.mk
#  Part of  : QGazer
#
#  Description: This make file will build the application help file (.hlp)
# 
# ============================================================================

do_nothing :
	@rem do_nothing

# build the help from the MAKMAKE step so the header file generated
# will be found by cpp.exe when calculating the dependency information
# in the mmp makefiles.

MAKMAKE : QGazer.hlp
QGazer.hlp : QGazer.xml QGazer.cshlp Custom.xml
	cshlpcmp QGazer.cshlp
ifeq (WINS,$(findstring WINS, $(PLATFORM)))
	copy QGazer.hlp $(EPOCROOT)epoc32\$(PLATFORM)\c\resource\help
endif

BLD : do_nothing

CLEAN :
	del QGazer.hlp
	del QGazer.hlp.hrh

LIB : do_nothing

CLEANLIB : do_nothing

RESOURCE : do_nothing
		
FREEZE : do_nothing

SAVESPACE : do_nothing

RELEASABLES :
	@echo QGazer.hlp

FINAL : do_nothing
