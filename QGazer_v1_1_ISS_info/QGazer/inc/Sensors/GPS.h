/*
 * GPS.h
 *
 *  Created on: 12-Apr-2010
 *      Author: Damien
 */

#ifndef GPS_H_
#define GPS_H_
#include <lbs.h>
#include <LbsSatellite.h> 
//LbsPositionInfo.h 
//lbsselflocate.lib 

// FORWARD DECLARATIONS
class MPositionObserver; 
class CLocation : public CActive    
	{    
	public:         
		static CLocation* NewL( const TTimeIntervalMicroSeconds& aInterval, MPositionObserver& aPositionListener, TBool aLastKnown ) ;        
		virtual ~CLocation();  
		void Start();    
	protected:  // from CActive        
		void DoCancel();        
		void RunL();        
		TInt RunError(TInt aError);     
	private:         
		void ConstructL(const TTimeIntervalMicroSeconds& aInterval);       
		CLocation(MPositionObserver& aPositionListener, TBool aLastKnown );        
		void DoInitialiseL();        
		void PositionUpdatedL();        
		void Wait();                
		void CancelWait();        
    
		static TInt PeriodicTick(TAny* aObject);        
		void PositionLost();     
	public:        
		void Pause();        
		void Continue();        
		TPositionInfoBase& CurrentPosition();    
		
		
	private:    // Data         
        
		TPositionModuleId    iUsedPsy;          
		RPositionServer             iPosServer;        
		RPositioner                 iPositioner;        
		TPositionInfo               iPositionInfo;           
		 
		TInt                        iUpdateTimeout;         
		MPositionObserver&          iPositionListener;             
		TPositionUpdateOptions      iUpdateops;              
		      
		// State variable used to mark if we are         
		// getting last known position        
		TBool                       iGettingLastknownPosition;         
		CPeriodic*                  iPeriodic;    
		
		};
	


#endif /* GPS_H_ */
