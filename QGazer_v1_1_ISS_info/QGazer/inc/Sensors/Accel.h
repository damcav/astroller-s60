/*
 * Accel.h
 *
 *  Created on: 28-Oct-2009
 *      Author: dacavana
 */

#ifndef ACCEL_H_
#define ACCEL_H_

#include <sensrvdatalistener.h>

#include "CQSensors.h"
#include "utils3d.h"

class CAccel : public CBase, public MSensrvDataListener
	{
public:
	static CAccel* NewL();
	
	void Read(TVector& aAccel);
	void Stop();
	void StartL();

	
	~CAccel();
private:
	CAccel();
	void ConstructL();
	void OpenChannelL( CSensrvChannel*& aChannel,
	                                        TSensrvChannelTypeId aChannelTypeId );
	
    virtual void DataReceived( CSensrvChannel& aChannel, 
                               TInt aCount, 
                               TInt aDataLost );
    
    virtual void DataError( CSensrvChannel& aChannel, 
                                TSensrvErrorSeverity aError );
        
	virtual void GetDataListenerInterfaceL( TUid aInterfaceUid, TAny*& aInterface );    

private:
	CSensrvChannel* iChannel;

	TInt iX;
	TInt iY;
	TInt iZ;
	TInt iLastiX;
	};

#endif /* ACCEL_H_ */
