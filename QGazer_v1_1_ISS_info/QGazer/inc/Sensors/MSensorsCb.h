/*
 * MSensorsCb.h
 *
 *  Created on: 27-Mar-2010
 *      Author: Damien
 */

#ifndef MSENSORSCB_H_
#define MSENSORSCB_H_

#include "CommonTypes.h"

class MSensorsCb 
	{
public:
	virtual void AccDataL(TReal aAngle) = 0;
	virtual void CompassData(TReal aBearing)  = 0;
	virtual void Updated(TBool aTime, TBool aAcc, TBool aCompass) = 0;
	virtual void GpsDataL(TGpsStatus aStatus, TReal aLong, TReal aLat) = 0;
	
	};

#endif /* MSENSORSCB_H_ */
