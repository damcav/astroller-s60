/*
 * GPSCb.h
 *
 *  Created on: 12-Apr-2010
 *      Author: Damien
 */

#ifndef GPSCB_H_
#define GPSCB_H_

#include <lbsSatellite.h> 
class MPositionObserver    
	{    
public:        
	virtual void PositionUpdatedL(TPositionInfo& aPosInfo, TBool aGettingLastknownPosition) = 0;        
	virtual void ErrorL(TInt aError) = 0;    
	};

#endif /* GPSCB_H_ */
