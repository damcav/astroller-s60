/*
 * CQSensors.h
 *
 *  Created on: 25-Mar-2010
 *      Author: Damien
 */

#ifndef CQSENSORS_H_
#define CQSENSORS_H_

#include <e32base.h>
#include "MSensorsCb.h"
#include "Gps.h"
#include "GpsCb.h"

class CAccel;
class CCompass;
class CFlipWatcher;

typedef float GLfloat;

const TInt KLookingDown = -999;

class CQSensors;

class CSmoothMove
	{
public:
	static CSmoothMove* NewL();
	void SetThreshold(TReal aThreshold) {iThreshold = aThreshold;}
	void SetCurrent(TReal aCurrent) {iCurrent = aCurrent; iTarget = aCurrent; iPreviousTarget = aCurrent;}
	void SetFast(TBool aFast) {iFast = aFast;}
	void SetMaxLag(TReal aMaxLag) { iMaxLag = aMaxLag;}

	TBool Move(TReal aTarget);
	~CSmoothMove();
	TReal Current() const {return iCurrent;}
	void CheckStop();

protected:
	virtual TReal Distance(TReal a1, TReal a2);
	virtual void Add();


	CSmoothMove();
	TBool MoveFast(TReal aTarget);
	TBool MoveSlow(TReal aTarget);


	TReal iThreshold;
	TReal iMaxLag;
	TReal iCurrentLag;

	TReal iPreviousTarget;

	TReal iTarget;
	TReal iCurrent;
	TReal iDelta;

	TBool iScrolling;
	TBool iStop;

	TBool iFast;
	};

class CSmoothCompass : public CSmoothMove
	{
public:
	static CSmoothCompass* NewL();
protected:
	TReal Distance(TReal a1, TReal a2);
	void Add();
	};

class CQSensors : public CBase, public MPositionObserver
	{
public:
	static CQSensors* NewL(MSensorsCb& aObserver);
	CQSensors(MSensorsCb& iObserver);
	virtual ~CQSensors();
	void ConstructL();
	static TInt Callback(TAny* aPtr);
	void Stop();
	void StopAccComp();
	void StartL();
	void MonitorGps(const TTimeIntervalMicroSeconds& aInterval, TBool aGetLastKnown);

	void Flip(TUid aCat, TInt aValue);
	void ReadFlip();
	
//GPS cb
	virtual void PositionUpdatedL(TPositionInfo& aPosInfo, TBool aGettingLastknownPosition);       
	virtual void ErrorL(TInt aError);
	
	void SetGpsInvalid(TBool aInvalid);
	TBool UsingSensors() { return iUsingSensors;}
	void DoSensorModeSlow();
	void DoSensorModeFast();
	
private:
	void DoCallbackL();
	TBool UpdateTime();
	void DoMonitorGpsL();
	TReal CompassThreshold();
	
	GLfloat AngleDiff(GLfloat a2, GLfloat a1);
	
	CPeriodic* iTimer;
    CAccel* iAccel;
	CCompass* iCompass;
	
	CFlipWatcher* iFlipWatcher;
	
	
	GLfloat iCurrentAngle;
	
	CLocation* iGps;
	TTimeIntervalMicroSeconds iInterval;
	TBool iGetLastKnown;
	TBool iStartMonitorGps;
	
	MSensorsCb& iObserver;
	TBool iTest_GpsInvalid;	
	
	TTime iSystemTime;
	TTime iLastTime;
	TInt iTimeFactor;
	
	TBool iUsingSensors;
	
	TVector iCurrentAccel;
	TInt iCurrentBearing;
	TInt iLastBearing;
	TInt iCurrentAccuracy;
	TBool iAccelChanged;
	TBool iCompassChanged;
	
	TInt iSamplesPerSecond;
	TReal iDeltaA;
	TReal iTargetAngle;
	TBool iScrollingA;
	
	CSmoothMove* iSmoothAccel;
	CSmoothCompass* iSmoothCompass;
	};



#endif /* CQSENSORS_H_ */
