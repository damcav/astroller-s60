/*
 * CCompass.h
 *
 *  Created on: 25-Mar-2010
 *      Author: Damien
 */

#ifndef CCOMPASS_H_
#define CCOMPASS_H_

#include <sensrvdatalistener.h>

class CCompass : public CBase, public MSensrvDataListener
	{
public:
	static CCompass* NewL();
	~CCompass();
	
	TInt Read(TInt& aAccuracy);
	void Stop();
	void StartL();
	
private:
	CCompass();
	void ConstructL();
	void OpenChannelL( CSensrvChannel*& aChannel, TSensrvChannelTypeId aChannelTypeId );
	
    virtual void DataReceived( CSensrvChannel& aChannel, 
                               TInt aCount, 
                               TInt aDataLost );
    
    virtual void DataError( CSensrvChannel& aChannel, 
                                TSensrvErrorSeverity aError );
        
	virtual void GetDataListenerInterfaceL( TUid aInterfaceUid, TAny*& aInterface );    

private:
	CSensrvChannel* iChannel;
	TInt iAngle;
	TInt iAccuracy;
	

	CCirBuf<TInt> iAngles;

	};

#endif /* CCOMPASS_H_ */
