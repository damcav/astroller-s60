/*
 * FlipWatcher.h
 *
 *  Created on: 23-Apr-2010
 *      Author: Damien
 */

#ifndef FLIPWATCHER_H_
#define FLIPWATCHER_H_

#include <e32base.h>
#include <e32property.h>
#include "CQSensors.h"

const TUint32 KHWRMFlipStatus         = 0x00000102;
enum EPSHWRMFlipStatus
    {
    EPSHWRMFlipStatusUninitialized = 0,
    EPSHWRMFlipOpen,
    EPSHWRMFlipClosed
    };
const TUid KPSUidHWRM = {0x10205047}; 

class CFlipWatcher : public CActive
	{
public:
    static CFlipWatcher* NewL( CQSensors& aPropertyObserver, 
                                      const TUid& aCategory, 
                                      const TUint aKey );
    ~CFlipWatcher();

public:
    void Subscribe();
	TInt Get( TInt& aValue );

private: // new methods
    CFlipWatcher( CQSensors& aPropertyObserver, 
                         const TUid& aCategory, 
                         const TUint aKey );
    void ConstructL();

public: // methods from base classes
    void RunL();

private:
    void DoCancel();
	
private:

	CQSensors& iPropertyObserver;   // observer informed of change events
    RProperty iProperty;                    // handle to the property
    TUid iCategory;                         // category of the property
    TUint iKey;                             // identifier of the property
	};


#endif /* FLIPWATCHER_H_ */
