/*
 *      This file, which is part of the project "QGazer", is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include "Utils3d.h"
#include "hardsums.h"
#include "CommonTypes.h"
#include "ccamera.h"
#include "qgstream.h"


//#define NM(x) x

enum {ELoc, EGrid, ELab, ECon, EView, EXSlider, EYSlider, EZoomSlider, EAtmos, EDateTime};

typedef enum {EPhoneTz, EUniversalTz, ENewLocationTz} TTimeZone;



const TRgb KCLabelTxt = TRgb(4,128,251);
const TRgb KCChosenLabelTxt = KRgbWhite;
const TRgb KCInfoText = KRgbWhite;
const TRgb KCLabelBg =  TRgb(32,32,32);
const TRgb KCChosenLabelBg = TRgb(32,32,32);
const TRgb KCInfoBg = TRgb(32,32,32);

class TCamera;
class TSkyObject;

class OUTSTREAM;

class TUserSettings
	{
public:
	void ExternalizeL(OUTSTREAM& aStream);
	void InternalizeL(INSTREAM& aStream);
	TUserSettings()
		{
		iLocationIndex = iNearestDistance = 0;
		iTimeZone = EPhoneTz;
		iShowIss = EFalse;
		};
	TLocation iLocation;
	TInt iLocationIndex;
	TTimeZone iTimeZone;
	TInt iNearestDistance;
	TTime iTime;	
	TReal32 iVisibleStars;
	TBool iShowAtmosphere;
	TBool iShowLabels;
	TBool iShowConstellations;
	TBool iLights;
	TBool iWaitingForGps;
	TBool iShowIss;
	};

class TChosenObject
	{
public:	
	const TSkyObject* iObject;
	TScreenCoords iScreen;
	THorizontal iHr;
	TEquatorial iEq;
	GLfloat iBearing;
	};

class Context : public CBase
{
public:
	static Context* NewL();
	void ConstructL();
	
	~Context();
	
	TCamera& Camera() {return iCamera;}

	void SetZoomEffectRunning(TBool aTf) {iZoomEffectRunning = aTf;}
	TBool ZoomEffectRunning() const {return iZoomEffectRunning;}
	
	void SetDragging(TBool aTf) { iDragging = aTf;}
	TBool Dragging() const {return iDragging;}
	
	void ToggleCrossHairMode() {iCrossHairMode = !iCrossHairMode;}
	void SetCrossHairMode(TBool aTf) {iCrossHairMode = aTf;}
	TBool  CrossHairMode() const {return iCrossHairMode;}
	
	void SetWaitingForGps(TBool aTf) { iUser.iWaitingForGps = aTf;}
	TBool WaitingForGps() const { return iUser.iWaitingForGps; }
	
	void SetDrawDot(TBool aTf) {iDrawDot = aTf;}
	TBool DrawDot() const {return iDrawDot;}
	
	void SetShowConstellations(TBool aShow) {iUser.iShowConstellations = aShow;}
	TBool ShowConstellations() { return iUser.iShowConstellations; }

	void ToggleInSpace();
//	void SetInSpace(TBool aMode) { iInSpace = aMode ;}
	TBool InSpace() {return iInSpace; }
	
	void SetShowLabels(TBool aShow) {	iUser.iShowLabels = aShow;}
	TBool ShowLabels() { return iUser.iShowLabels; }
	TBool ObjectQuery() { return iObjectQuery; }

	void SetOriginLongitude(GLfloat aOriginLongitude) {iOriginLongitude = aOriginLongitude;}
	GLfloat OriginLongitude() const { return iOriginLongitude;}

	void SetGreenwichSiderealTime(GLfloat aGreenwichSiderealTime) {iGreenwichSiderealTime = aGreenwichSiderealTime;}
	GLfloat GreenwichSiderealTime() const {return iGreenwichSiderealTime;}

	TBool UserTimeMode()  { return iUserTimeMode;}
	void SetUserTimeMode(TBool aTf) { iUserTimeMode = aTf;}	
	const TDateTime Time();			//time in use

	void SetShowIss(TBool aTf) {iUser.iShowIss = aTf;}
	TBool ShowIss() const {return iUser.iShowIss;}
	
public:
	void SetUserTimeToNow();
	void SetUserTime(const TDateTime& aTime, TInt aTimeZone);
	void SetTimeZone(TTimeZone aTimeZone) {iUser.iTimeZone = aTimeZone;}
	TTimeZone TimeZone() const { return iUser.iTimeZone;} 
	void UserTime(TDateTime& aTime, TInt& aTimeZone);
	const TDesC& TimeString();

	TTime UserTimeUtc();
	
public:
	void AddTime(const TInt64& aMs);

	TTimeIntervalMinutes PhoneTzMins() const { return iPhoneTzMins;}

	
	const TVector& EarthPosAndSize() { return iEarthPosAndSize;}
	void SetEarthPosAndSize(TVector& aVect) { iEarthPosAndSize = aVect ;}

	GLfloat iStarFactor; //TODO shouldn't be here!!!

	void ClearChosenObject(); 
	TChosenObject& ChosenObject() { return iChosenObject;}
	void SetChosenObject(const TSkyObject* aObject);
	TBool ObjectSelected() { return iChosenObject.iObject != NULL;}
	
	void SetSensorMode(TBool aMode) { iSensorMode = aMode ;}
	TBool SensorMode() const { return iSensorMode; } 
	
	void ToggleNightMode() { iNightMode = !iNightMode ;}
	TBool NightMode() const { return iNightMode; }
	
	void SetShowAtmosphere(TBool aMode) { iUser.iShowAtmosphere = aMode ;}
	TBool ShowAtmosphere() const { return iUser.iShowAtmosphere; } 

	void SetCompassHealth(TInt aHealth) { iCompassHealth = aHealth;}
	TInt CompassHealth() const { return iCompassHealth;}
	
	void SetGpsStatus(TGpsStatus aReady) { iGpsStatus = aReady; }
	TGpsStatus GpsStatus() const { return iGpsStatus;}
	
	
	void SetSatelliteVisible(TBool aVisible) { iSatelliteVisible = aVisible;}
	bool SatelliteVisible() const { return iSatelliteVisible;}
	
	void SetUserLocationToHome();
	
private:	
	TBool iSatelliteVisible;
	void SetUserNearestDistance(TInt aD) { iUser.iNearestDistance = aD; }
	void SetNearestDistance(TInt aD) { iNearestDistance = aD; }	
	TInt NearestDistance() const { return iNearestDistance;}
	TInt UserNearestDistance() const { return iUser.iNearestDistance;}
	
public:
	void SetFastOpenGL(TBool aTf) {iFastOpenGL = aTf;}
	const TBool FastOpenGL() {return iFastOpenGL;}
	
	void SetCurrentLocation(const TLocation& aLocation, TInt aNearestDistance);
	void SetUserLocation(const TLocation& aLocation, TInt aNearestDistance);
	
	const TLocation& UserLocation()	const	{ return iUser.iLocation; }
	const TLocation& CurrentLocation() const { return iCurrentLocation;}
	
	const TLocation& UserLocation(TInt& aNearestDistance) const;
	const TLocation& CurrentLocation(TInt& aNearestDistance) const;
	
	const TLocation& Location(TInt& aNearestDistance) const;		//location in use
	const TLocation& Location() const;		//location in use
	
	TBool UserLocationMode() const { return iUserLocMode;}
	void SetUserLocationMode(TBool aTf) { iUserLocMode = aTf;}
	
	void ToggleLights() { iUser.iLights = !iUser.iLights;}
	TBool Lights() const {return iUser.iLights;}
	
	void SetVisibleStars(TReal32 iPc) { iUser.iVisibleStars = iPc;}
	TReal32 VisibleStars() const {return iUser.iVisibleStars;}
	
	void SetKeyboardOpen(TBool aTf) { iKeyboardOpen = aTf;}
	TBool KeyboardOpen() const { return iKeyboardOpen;}
	
	TUserSettings& UserSettings() { return iUser;}
	const TDesC& TimeStringHome();
	const TDesC& TimeStringUserLocation();
	
private:
	const TDateTime HomeDateTime() const {return iCurrentTime.DateTime();}	//UTC
	const TDateTime UserDateTime() const {return iUser.iTime.DateTime();}	//UTC
	//void UserTimeUtc(TDateTime& aTime);
	void AdjustTimeToUtc(TTime& aTime, TInt aTimeZone);
	void AdjustTimeFromUtc(TTime& aTime, TInt aTimeZone);	
	
	Context();
//	void setColours(int aValue);

	
public:	
	void SaveUserDataL();
	const TDesC& DoTimeString(TTime aTime, TTimeZone aTz);
private:
	void LoadUserDataL();

	TTime iCurrentTime;
	TTimeIntervalMinutes iPhoneTzMins;
	TBool iFastOpenGL;
	
	
	
	TBool iInSpace;
	

	
	TBool iObjectQuery;
	
	TLocation iCurrentLocation;

	GLfloat iOriginLongitude;
	GLfloat iGreenwichSiderealTime;


	TVector iEarthPosAndSize;



	TInt iOffsetSecs;
	TBuf<64> iTimeString;

	TCamera iCamera;
	TChosenObject iChosenObject;
	
	TBool iSensorMode;
	
	
	TInt iCompassHealth;
	TGpsStatus iGpsStatus;
	
	TBool iTestLocation;
	TInt iNearestDistance;

	TBool iUserLocMode;
	TBool iUserTimeMode;
	

	TBool iNightMode;
	
	
	TBool iKeyboardOpen;
	
	
	
	TUserSettings iUser;
	

	
	TBool iDrawDot;
	
	TBool iCrossHairMode;
	TBool iDragging;
	TBool iZoomEffectRunning;
	};


#endif // SETTINGS_H
