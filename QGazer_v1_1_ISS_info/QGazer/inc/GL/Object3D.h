//#define DAMIEN_STICK2FLOAT

#pragma once

#include <e32base.h> // for CBase definition
#include <GLES/gl.h> // OpenGL ES header file
/*-----------------------------------------------------------------*/

class CObject3D : public CBase
	{
public:
	static CObject3D* NewL(unsigned int indexCount_, unsigned int vertexCount_);

    // Draws object
    void draw(void) const;
	~CObject3D();
	
private:
	void ConstructL();
	// Constructor and destructor
	CObject3D(unsigned int triCount, unsigned int vertexCount);
    

public:	
	unsigned int indexCount;
    unsigned int vertexCount;
    unsigned short *indices;
    
    GLfixed *vertices;
    GLfixed *normals;
    GLfixed *texCoords;
	};

class Utils
	{
public:
	static CObject3D* CreateSphereL(const GLfloat aRadius, const int aSlices, const int aStacks);
	};
