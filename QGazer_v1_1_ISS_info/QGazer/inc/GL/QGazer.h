/*
 * ==============================================================================
 *  Name        : SimpleCube.h
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */

#ifndef SIMPLECUBE_H
#define SIMPLECUBE_H

//  INCLUDES
#include <e32base.h> // for CBase definition
#include <GLES/gl.h> // OpenGL ES header file

#include "object3d.h"

#include "utils3d.h"
#include "CommonTypes.h"
/** Enumerated rendering mode for triangle and triangle fan based rendering. */
typedef enum { ETriangles, ETriangleFans } TRenderingMode;


// MACROS
#define FRUSTUM_LEFT   -1.f     //left vertical clipping plane
#define FRUSTUM_RIGHT   1.f     //right vertical clipping plane
#define FRUSTUM_BOTTOM -1.f     //bottom horizontal clipping plane
#define FRUSTUM_TOP     1.f     //top horizontal clipping plane
//#define FRUSTUM_NEAR    3.f     //near depth clipping plan//e
//#define FRUSTUM_FAR  1000.f     //far depth clipping plane
#define FRUSTUM_NEAR    0.f     //near depth clipping plane
#define FRUSTUM_FAR  1000.f     //far depth clipping plane

class MQGazerCb;
class CSkyModel;
class gglMatrix;

class TEquatorial;


// CLASS DECLARATION

/**
 * Class that does the actual OpenGL ES rendering.
 */

const TInt KNumBBArrays = 20;
class CQGazer : public CBase, public MTextureLoadingListener
    {
    public:  // Constructors and destructor

        /**
         * Factory method for creating a new CQGazer object.
         */
        static CQGazer* NewL( TUint aWidth, TUint aHeight, const CSkyModel& aModel, MQGazerCb& aParent );

        /**
         * Destructor. Does nothing.
         */
        virtual ~CQGazer();

        void Vector(const TEquatorial& aEq, TVector& aVector, GLfloat aLength );
        void ZoomIn();
        void ZoomOut();
        void DrawTheMoon();
        void BillBoard();
        void StartPointSprites(GLfloat* aVertices);
        void StartPoints(GLfloat* aVertices);
        void EndPointSprites();
        void DrawQuadCentred(GLfloat aWidth, GLfloat aHeight);
        void DrawQuadDown(GLfloat aWidth, GLfloat aHeight);
        void BeginQuadTex(GLuint aTexture);
        void EndTex();
        void DrawQuadColour(GLfloat aWidth, GLfloat aHeight, const TColour& aTop, const TColour& aBottom);
        void DrawQuad3D(GLfloat aSize);
        void Atmosphere();
        void DrawArrow();
        void ScreenCoords(const TVector& aVector, TScreenCoords& aScreen);
        void GetEarthRadiusL();
    public: // New functions

        /**
         * Initializes OpenGL ES, sets the vertex and color
         * arrays and pointers. Also selects the shading mode.
         */
        void AppInitL( void );

        /**
         * Called upon application exit. Does nothing.
         */
        void AppExit( void );

        /**
         * Draws a 3D box with triangles or triangle fans depending
         * on the current rendering mode.Scales the box to the given size using glScalef.
         * @param  aSizeX X-size of the box.
         * @param  aSizeY Y-size of the box.
         * @param  aSizeZ Z-size of the box.
         */
        void DrawBox( GLfloat aSizeX, GLfloat aSizeY, GLfloat aSizeZ );

        /**
         * Renders one frame.
         * @param aFrame Number of the frame to be rendered.
         */
        void AppCycle( TInt aFrame );

        /**
         * Sets the shading mode to flat.
         */
        void FlatShading( void );

        /**
         * Sets the shading to smooth (gradient).
         */
        void SmoothShading( void );

        /**
         * Sets the rendering to use triangles.
         */
        void TriangleMode( void );

        /**
         * Sets the rendering to use triangle fans.
         */
        void TriangleFanMode( void );

        /**
         * Notifies that the screen size has dynamically changed during execution of
         * this program. Resets the viewport to this new size.
         * @param aWidth New width of the screen.
         * @param aHeight New height of the screen.
         */
        void SetScreenSize( TUint aWidth, TUint aHeight );

        void DrawDot();

        void SetupView();
        
    protected: // New functions

        /**
         * Standard constructor that must never Leave.
         * Stores the given screen width and height.
         * @param aWidth Width of the screen.
         * @param aHeight Height of the screen.
         */
        CQGazer( TUint aWidth, TUint aHeight, const CSkyModel& aModel, MQGazerCb& aParent );

        /**
         * Second phase contructor. Does nothing.
         */
        void ConstructL( void );

        
        
    	/**
    	 * Called when the finite state machine enters a new state.
    	 * Does nothing in this implementation.
    	 * @param aState State that is about to be entered.
    	 */
    	void OnEnterStateL( TInt aState );

    	/**
    	 * Called when texture manager starts loading the textures.
    	 * Sets the current state to "loading textures".
    	 */
    	void OnStartLoadingTexturesL();

    	/**
    	 * Called when texture manager has completed texture loading.
    	 * Changes the current state to "running".
    	 */
    	void OnEndLoadingTexturesL();
        
    public:
    	void ChangeRot(const TPoint& aDelta);
    	void DirectionalLight(const TVector& aVect);
    	void InitCircle(GLfloat aRadius);
    	void DrawCircle(const TVector& aVector);
    	TInt Height() { return iHeight; }
    	const TColour& TopColour() {return iTop;}
    	const TColour& BottomColour() {return iBottom;}
    	
    private:
    	void Horizon();
    	void DrawTheEarth();
    	void SetRotations();
    	void SeppoTest();
    	
    	TBool iGotRadius;
    	
    	TColour iTop;
    	TColour iBottom;

    	GLfloat iCircleVertices[(360/5) * 3 + 6];
    	
    private: // Data
    	/** Texture manager that is used to load the used textures. */
    	CTextureManager * iTextureManager;
    	
        /** Width of the screen */
        TUint           iWidth;

        /** Height of the screen */
        TUint           iHeight;

        /** The current rendering mode */
        TRenderingMode  iDrawingMode;
        
        CObject3D* iGlobe;
        CObject3D* iMoon;
        TBool iReady;
        
        /** World texture. */
        TTexture iWorldTexture;
        TTexture iMoonTexture;
        TTexture iStarTexture;
        TTexture iGrassTexture;
        TTexture iArrowTexture;
        
        GLfloat iYRot;
        GLfloat iXRot;
        GLfloat iZRot;
        GLfloat iXRot2;
        MQGazerCb& iParent; 
        
        GLfloat iModelViewBillboard[16];
        
        TInt iBBArrayCount;
        GLfloat iQv[6*2];
        
        static const GLfloat iQt[6*2];
        GLfloat iQv3[6*3];
        GLfloat iQc4[6*4];
        GLfloat iQcT[6*4];
        
        const CSkyModel& iModel;
        float iZoom;
        gglMatrix* iDamProject;
    };
#endif // SIMPLECUBE_H

// End of File
