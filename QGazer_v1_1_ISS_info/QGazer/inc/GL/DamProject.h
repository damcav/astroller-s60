//#define DAMPROJECT

#ifndef DAMPROJECT_H
#define DAMPROJECT_H



// gglMatrix.h

#pragma once

/* gglMatrix provides translation from Pixels to OpenGL world coordinates.
It links the ModelView and Projection matrices and the ViewPort so you don't have to.
Once you've set the ViewPort,Projection and ModelView, you can do things like this:
  GLfloat x,y,z;
  gglMatrix Tx;
  Tx.Pixel2World(CPoint(2,3), x,y,z);
  CPoint Pixel;
  Tx.World2Pixel(x,y,z, Pixel);
It should be fairly quick to do many translations because the time-consuming bit is in the constructor which calls SetFromGL().
For Ortho projections, though, this class is overkill: I have developed much more efficient methods.
*/

#include "mathutils.h"
#include <GLES/gl.h> // OpenGL ES header file

class CPoint
{
public:
	GLfloat x;
	GLfloat y;
	GLfloat z;
	void SetPoint(GLfloat ax, GLfloat ay, GLfloat az) { x=ax; y=ay; z=az;}

};

struct gglMatrix { // Initialised using SetFromGL();
  GLint    V[ 4]; // V=ViewPort
  GLfloat M[16]; // M=Projection x ModelView
  GLfloat I[16]; // I=Inverse of M
  gglMatrix() {SetFromGL();}
  virtual ~gglMatrix() {}
  void Set(const GLfloat Mat[16]);
  void SetFromInverse(const GLfloat Inv[16]);
  void SetFromGL(); // Called from Constructor
  bool Pixel2World(const CPoint& Point, GLfloat& x, GLfloat& y, GLfloat& z); // MUST have SetFromGL(); first!!!
  bool World2Pixel(GLfloat x, GLfloat y, GLfloat z, CPoint& Point); // MUST have SetFromGL(); first!!!
  static void Transform(const GLfloat P[4], const GLfloat M[16], GLfloat PxM[4]);
  static void Multiply(const GLfloat A[16], const GLfloat B[16], GLfloat AxB[16]);
  static void GetInverse(const GLfloat M[16], GLfloat I[16]);
  static GLfloat GetDeterminant(const GLfloat M[16]);
};

#endif // DAMPROJECT_H
