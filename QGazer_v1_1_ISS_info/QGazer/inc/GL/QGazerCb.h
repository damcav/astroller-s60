/*
 * QGazerCb.h
 *
 *  Created on: 16-Mar-2010
 *      Author: Damien
 */

#ifndef QGAZERCB_H_
#define QGAZERCB_H_

class MQGazerCb
	{
public:
	virtual void GLReadyL() = 0;
	};

#endif /* QGAZERCB_H_ */
