/*
 * TSkyColour.h
 *
 *  Created on: 23-Mar-2010
 *      Author: Damien
 */

#ifndef TSKYCOLOUR_H_
#define TSKYCOLOUR_H_

#include "CommonTypes.h"

class TSkyColour
	{
public:
	void Colours(const THorizontal& aSun, TColour& aBottom, TColour& aTop, GLfloat& aStarFactor);
	
private:
	TColourTransform iTransformBottom;
	TColourTransform iTransformTop;
	};

#endif /* TSKYCOLOUR_H_ */
