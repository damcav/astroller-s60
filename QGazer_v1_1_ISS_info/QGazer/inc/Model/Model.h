#ifndef MODEL_H
#define MODEL_H

#include <e32base.h>
#include "objects.h"
#include "qgstream.h"

class INSTREAM;
class CHardSums;
class Context;

class CSkyModel : public CBase
{
public:
	~CSkyModel();
	static CSkyModel* NewL();
	void ConstructL();
	void InitL();
	
	void SetGL(CQGazer* aGL) const;
	void DrawGL() const;

	void UpdateCoords();
	
	const CHardSums& Sums() const {return *iHardSums;}

	const TSkyPlanet& Planet(TInt aIndex) const;
	const TNamedStar& NamedStar(TInt aIndex) const;
	const TSkyStar& NamedStarObject(TInt aIndex) const;
	const TSkyConstellation& Constellation(TInt aIndex) const;

	const TSkySun& Sun() const { return iSun;}
	const TSkyMoon& Moon() const { return iMoon;}
	const TLocation& Location(TInt aIndex) const;
	//const TSkySat& Sat() const {return iSat;}
	void SetObjectQuery() const;
	
	TInt ConstellationCount() const { return iConstellations.Count(); }
	TInt NamedStarCount() const { return iNamedStars.Count(); }
	TInt PlanetCount() const { return iPlanets.Count(); }
	TInt LocationCount() const { return iLocations.Count(); }

#ifdef SATELLITE
	TInt SatellitesCount() const { return iSatellites.Count(); }
	const TSkySat& Satellite(TInt aIndex) const {return iSatellites[aIndex];}
	void InternalizeSatellitesL();	
	const TSkySat& Iss() const {return iSatellites[0];}
	
	//TSkySat iSat;
	RArray<TSkySat> iSatellites;
#endif
	
#ifdef MESSIER
	const TSkyMessier& Messier(TInt aIndex) const;
	TInt MessierCount() const { return iMessier.Count(); }
#endif
	
	const TDesC& NearestCity();
	TInt FindNearesetCity(TLocation& aLocation);
	
	void CreateNewLocationDataL();
	
private:
	CSkyModel();
	
#ifdef CREATEDATA
	void ExternalizeCitiesL(OUTSTREAM& aStream);
	void ExternalizeStarsL(OUTSTREAM& aStream);
#endif
	void InternalizeCitiesL(INSTREAM& aStream);
	void InternalizeStarsL(INSTREAM& aStream);
#ifdef MESSIER
	void InternalizeMessierL(INSTREAM& aStream);
#ifdef CREATEDATA
	void ExternalizeMessierL(OUTSTREAM& aStream);
#endif
#endif
	
	void ObjectFoundL(const TSkyObject* aObject) const;
	
	RArray<TSkyConstellation> iConstellations;
	RArray<TSkyPlanet> iPlanets;
	RArray<TNamedStar> iNamedStars;
	//RArray<TLocation> iLocations;
	TFixedArray<TLocation, 1440> iLocations;	//TODO MAGIC!!!
#ifdef MESSIER
	TFixedArray<TSkyMessier, 111> iMessier;	//TODO MAGIC!!!
#endif	
	TSkySun iSun;
	TSkyMoon iMoon;

	TSkyStar* iStarPool;
	
	TReal iMostRed;
	TReal iMostBlue;
	
	mutable TBool iObjectQuery;
	mutable TRect iObjectQueryRect;
	
	CHardSums* iHardSums;
	INSTREAM* iStream;
	
	RBuf iNearestCity;
};

#endif // MODEL_H
