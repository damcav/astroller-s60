#ifndef OBJECTS_H
#define OBJECTS_H

#include "CreateData.h"
#include "commontypes.h"
#include "Utils3d.h"
#include "qgstream.h"

class TSkyObject;

class CQGazer;
class CSkyModel;

#define STOREVECTORS

TInt32 MakeInt(TReal aFloat);

class TNamedStar
	{
public:
	~TNamedStar();
	void Close();
	void InternalizeL(INSTREAM& ds);
	
#ifdef CREATEDATA
	void ExternalizeL(OUTSTREAM& aStream);
#endif

	const RBuf& Name() const {return iName;}
	
	RBuf iName;
	RBuf iNotes;
	TUint32 iIndex;
	};

class TSkyObject
{
public:
	virtual const RBuf& Name() const = 0;
	void InternalizeL(INSTREAM& aStream);
#ifdef CREATEDATA
	void ExternalizeL(OUTSTREAM& aStream);
#endif

	
	TEquatorial iEq;

	inline void Vector() const;
	inline void Vector(TVector& aVector) const;
	
	void HandleChosen() const;
	
	virtual void DescriptionL(CDesCArray& aLines) const;
	void DescriptionCoordsL(CDesCArray& aLines) const;
	virtual const TDesC& Article() const;
	
	void AddScreenCoords() const;
	
	
	
	virtual TBool Horizontal(THorizontal& aHor) const;
	void DetermineVisibility();
	virtual TBool IsVisible() const {return iVisible;}
	virtual TBool Show() const {return ETrue;}
public:

#ifdef STOREVECTORS
	virtual void StoreVector() const;
	mutable TVector iStoredVector;
#endif
	
	static CQGazer* iGL;
	static CSkyModel* iModel;
	static TVector iVector;
	
protected:
	TBool iVisible;		//TODO stick this in iEq in an efficient way
};

class TSkyPointObject : public TSkyObject
	{
public:	
	inline void DrawGL() const;
	static void Begin();
	static void End();
	TBool CheckObjectQuery(const TRect& aTarget) const;

	virtual void DescriptionL(CDesCArray& aLines) const;
	
	mutable TUint32 iColour;
	
	static void Reset();
	

	
	TReal32 iMagnitude;
protected:	

	static GLfloat iPointVertex[3];
	static TBool iPointSprites;
	};

class TSkyStar : public TSkyPointObject
{
public:
	const RBuf& Name() const;
	TSkyStar() : iNameIndex(9999) {}
	inline void DrawGL() const;
	void Init(TReal aMostRed, TReal aMostBlue, TReal aBv);
	TReal InternalizeL(INSTREAM& aStream);
	
#ifdef CREATEDATA
	void ExternalizeL(OUTSTREAM& aStream);
#endif

	TUint32 iNameIndex;
	static RBuf iHipName;
	
	virtual void DescriptionL(CDesCArray& aLines) const;
	virtual const TDesC& Article() const;
	
//private:
	TUint32 iHipNum;
	TReal32 iParallax;
//#ifdef CREATEDATA
	TReal iBv;
//#endif

};

class TSkyPlanet : public TSkyPointObject
{
public:
	void DrawGL() const;
	//NOTE! iNumber in TSkyPlanet is this enum +1
	enum {EMercury=0, EVenus, EEarth, EMars, EJupiter, ESaturn, EUranus, ENeptune, EPluto};
	TSkyPlanet(int aNumber);
	void Close();
	const RBuf& Name() const {return iName;}
	void UpdateCoords();
	virtual const TDesC& Article() const;
	
private:
	RBuf iName;
	RBuf iArticle;
	TInt iNumber;
};

class TSkySun : public TSkyPlanet
	{
public:
	TSkySun();
	void DrawGL() const;
	void UpdateCoords();
	virtual void DescriptionL(CDesCArray& aLines) const;
	TBool CheckObjectQuery(const TRect& aTarget) const;
	void AddScreenCoords() const;
	virtual const TDesC& Article() const;
	
	
	THorizontal iHr;
	TDateTime iSunrise;
	TDateTime iSunset;
	TBool iSunsetOk;
	TBool iSunriseOk;
	};

class TSkyConstellation : public TSkyObject
{
public:
	const RBuf& Name() const {return iName;}
	const RBuf& Notes() const {return iNotes;}
	virtual ~TSkyConstellation();
	void Close();
	inline void DrawGL() const;
	virtual void DescriptionL(CDesCArray& aLines) const;
	
	
	TInt InternalizeL(INSTREAM& aStream);
#ifdef CREATEDATA
	void ExternalizeL(OUTSTREAM& aStream);
#endif


private:
	RArray<TEquatorial> iEqList1;
	RArray<TEquatorial> iEqList2;
	
	RBuf iName;
	RBuf iNotes;
	
public:
	static GLfloat* iVertices;
};


class TSkyMoon : public TSkyObject
{
public:
	TSkyMoon();
	void Close();

	const RBuf& Name() const {return iName;}
	//static void initGL(GLWidget& aGL);
	
	virtual void DescriptionL(CDesCArray& aLines) const;
	void DrawGL(const TColour& aColour) const;
	void UpdateCoords();
	TBool GetSkyColour(TColour& aColour) const;	
	TBool CheckObjectQuery(const TRect& aTarget) const;
	virtual const TDesC& Article() const;
	
	
	void AddScreenCoords() const;
	//! Gets the sky colour 'behind' the moon
	//! Used to cover objects during eclipses
	//! @param	aColour	The resulting colour
	//!	@param  True	 if the moon is currently onscreen
	//bool GetSkyColour(TVector& aColour) const;
	THorizontal iHr;
private:
	mutable TScreenCoords iScreen;
	
	RBuf iName;
	//static quint32 iTexture;

	//! The radius of the moon
	GLfloat iRadius;
	
	TDateTime iLastFullMoon;
	TDateTime iNextFullMoon;

	//! The moon's vector length. Various between modes
	//qreal iVectorLength;
};

#ifdef MESSIER
class TSkyMessier : public TSkyPointObject
{
public:
	
	void InternalizeL(INSTREAM& aStream);
#ifdef CREATEDATA
	void ExternalizeL(OUTSTREAM& aStream);
#endif
	const RBuf& Name() const {return iName;}
	void Close();
	void DrawGL() const;
	virtual void DescriptionL(CDesCArray& aLines) const;
	virtual const TDesC& Article() const;
	RBuf iName;
	enum {EOpenCluster, EGlobularCluster, EBrightNebula, EPlanetaryNebula, EGalaxy, EAsterism};
	TInt iType;
	TReal32 iDist;
};
#endif

#ifdef SATELLITE

#include "Satellites.h"


class TSkySat : public TSkyPointObject
	{
public:
	TBool Show() const;
	void DrawGL() const;
	void SetName(const TDesC& aName);
	void SetColour(const TColour& aColour) {iColour = aColour.Int32();}
	void SetMagnitude(TReal aMag) { iMagnitude = aMag;}
	void SetTle1(const TDesC8& aLine1);
	void SetTle2(const TDesC8& aLine2);
	void SetTle3(const TDesC8& aLine3);
	const RBuf& Name() const {return iName;}
	void Close();
	TBool UpdateCoords();
	virtual const TDesC& Article() const;
	void InternalizeL(TFileText& aFt);
	virtual void DescriptionL(CDesCArray& aLines) const;
	TBool Horizontal(THorizontal& aHor) const;
	TBool UpToDate(TDateTime& aValidFrom, TDateTime& aValidUntil) const {return iTle.UpToDate(aValidFrom, aValidUntil);}
	TBool BeforeSatEpoch() const {return iTle.BeforeSatEpoch();}

#ifdef STOREVECTORS	
	virtual void StoreVector() const;
#endif	

	void VisibilityText(CDesCArray& aLines) const;
	void NextVisibleUtc(TRequestStatus* aStatus) const;
	void CancelNextVisibleUtc() const;
	
	TBool NextVisibleUtcCached(TDateTime& aVisible) const;
private:

	
	TLocation iLocation;	
	THorizontal iHr;

	
	
	
	RBuf iName;
	TBool iSunlit;
	TBool iAboveHorizon;

	TSatTle iTle;
	mutable /*ARSE!*/ TSatData iVisSat;
	mutable /*ARSE!*/ TBool iVisibilityIsCached;
	mutable /*ARSE!*/ TDateTime iWhenVisible;
	mutable /*ARSE!*/ TBool iIsVisible;
	

	};
#endif


#endif // OBJECTS_H
