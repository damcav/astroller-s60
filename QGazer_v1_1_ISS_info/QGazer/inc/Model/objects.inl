
#include "Context.h"
extern Context* gContext;
#include "HardSums.h"
extern CHardSums* gHardSums;
#include "QGazer.h"

inline void TSkyObject::Vector(TVector& aVector) const
	{
#ifdef STOREVECTORS
	aVector = iStoredVector;
#else		
	iGL->Vector(iEq, aVector, 1.0);
#endif
	}

inline void TSkyObject::Vector() const
	{
#ifdef STOREVECTORS
	iVector = iStoredVector;
#else		
	iGL->Vector(iEq, iVector, 1.0);
#endif
	}

inline void TSkyPointObject::DrawGL() const
	{
	Vector();

	GLfloat p;
	if (iMagnitude < -5)
		p = 24;
	else if (iMagnitude < 0)
		p = 11;
	else 
		p = 9.5 - (iMagnitude*3/2);

	p *= 1.6;
	
	p *= gContext->iStarFactor;
	p *= gContext->Camera().Zoom();
	
	if (!iPointSprites && (p > 2))
		{
		TSkyPointObject::Begin();
		iPointSprites = ETrue;
		}
	if ( p < 1)
		p = 1;//TODO Why now?
		{
		TColour c(iColour);
		c.GlColour();
		glPointSize(p);
		iPointVertex[0] = iVector.iX; iPointVertex[1] = iVector.iY; iPointVertex[2] = iVector.iZ;  
//		glVertexPointer (3, GL_FLOAT , 0, &iPointVertex);
		glDrawArrays (GL_POINTS, 0, 1);
		}
	}

inline void TSkyStar::DrawGL() const
	{
#ifdef HIDE_STARS_WHEN_DRAGGING	
	if (gContext->Dragging())
		{
		if (gContext->InSpace())
			{
			return;
			}
		else
			{
			if (iMagnitude > 3.5)
				return;
			}
		}
#endif	
	if (!gContext->InSpace())
		{
		if (!iVisible)
			return;
		}
	
	TSkyPointObject::DrawGL();
	 
	if ((gContext->ShowLabels()) &&(iNameIndex != 9999) && (iMagnitude < 1.5 ))
		{
		TScreenCoords sc;
		TSkyObject::iGL->ScreenCoords(iVector, sc);

		if (sc.OnScreen())
			{
			sc.Add(TPoint(4,4));
			gAppUiCb->Label(sc.Point(), Name(), TLabelType(EStarLabel));
			}
		}
	}

inline void TSkyConstellation::DrawGL() const
{
	Vector();
	
	if (gContext->ShowLabels())
		{
		TScreenCoords sc;
		iGL->ScreenCoords(iVector, sc);
		if (sc.OnScreen())
			{
			TBool add = ETrue;
			if (!gContext->InSpace())
				{
				THorizontal hr;
				gHardSums->ToHorizontal(iEq, hr);
				add = hr.iAltitude > 0;
				}
			if (add)
				gAppUiCb->Label(sc.Point(), Name(), TLabelType(EGeneral));
			}
		}

	TReal l = 1.0;
	TInt c=0;
	TInt count = iEqList1.Count();	//number of lines
	for (TInt j=0; j<count; j++)
	{
		iGL->Vector(iEqList1[j], iVector, l);
		iVertices[c++] = iVector.iX; iVertices[c++] = iVector.iY; iVertices[c++] = iVector.iZ;
		iGL->Vector(iEqList2[j], iVector, l);
		iVertices[c++] = iVector.iX; iVertices[c++] = iVector.iY; iVertices[c++] = iVector.iZ;
	}
	glColor4f(0.5f, 0.0f, 0.0f, 1.0f);
	glEnableClientState (GL_VERTEX_ARRAY);
	glVertexPointer (3, GL_FLOAT , 0, iVertices);	
	glDrawArrays (GL_LINES, 0, count*2);
	}
