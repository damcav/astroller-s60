/*
 * ListBoxWidget.h
 *
 *  Created on: 27-Apr-2010
 *      Author: Damien
 */

#ifndef LISTBOXWIDGET_H_
#define LISTBOXWIDGET_H_

#include <COECNTRL.H>

#include <eiklbo.h> // MEikListBoxObserver
#include <aknlists.h> // CAknSingleStyleListBox
#include "FloatingWindow.h"
#include "CommonTypes.h"

class CListBoxWidget;

class MListBoxCb
	{
public:
	virtual void ListBoxOkCancelL(TInt aId, TBool aOk) = 0;
	};

class CListBoxWidgetNM : public CFloatingWindow
	{
public:
	static CListBoxWidgetNM* NewL(CListBoxWidget* aListBox);
	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
private:
	CListBoxWidgetNM(CListBoxWidget* aListBox);
	void ConstructL();
	void Draw(const TRect& aRect) const;
	
	CListBoxWidget* iListBox;
	};

class CMyListItemDrawer: public CListItemDrawer	
	{
public:	
	CMyListItemDrawer(const CEikTextListBox& aListBox);
	
private: // CListItemDrawer	
	void DrawActualItem(TInt aItemIndex, const TRect& aActualItemRect,
			TBool aItemIsCurrent, TBool aViewIsEmphasized, 
			TBool aViewIsDimmed, TBool aItemIsSelected) const;
private:	
	const CEikTextListBox& iListBox;
	const CFont* iFont;
	};
	
class CMyListBox : public CEikTextListBox
	{
private: // from CEikTextListBox	
	virtual void CreateItemDrawerL() { iItemDrawer = new (ELeave) CMyListItemDrawer(*this);}	
public:
	TBool iHasScrollBar;
	CEikScrollBar* iScrollBar;
	};

class MListBoxWidgetObserver
	{
public:
	virtual void Selected(TInt aIndex) = 0;
	};

class CListBoxWidget : public CCoeControl, MEikListBoxObserver
	{
public:
	static CListBoxWidget* NewL(const TRect& aRect, CCoeControl* aParent, MListBoxWidgetObserver& aObserver);
	~CListBoxWidget();
	void ResetItems();
	void AddItemL(const TDesC& aItem);
	
	void ItemsAddedL(int aCurrent);  //aCurrent is the item to set as default
	void SetParentWindowRect(TRect aWindowRect) {iWindowRect = aWindowRect;}
	
	void HandleListBoxEventL(CEikListBox* aListBox, MEikListBoxObserver::TListBoxEvent aEventType);
	void KeyEvent(TUint aChar);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;	
	
	void SetVisible(TBool aTf);
	void SetExtentAndActivateL(const TRect& aRect);
	TInt ItemCount() const { return iItemList->MdcaCount();} 

	void Pointer(const TPointerEvent &aPointerEvent);
	void AddScrollBarL(const TRect& aRect);
	void ScrollBarNightModeL();
private:

	CListBoxWidget(const TRect& aRect, MListBoxWidgetObserver& aObserver);
	void ConstructL(CCoeControl* aParent);
	
	
	//CEikColumnListBox* iListBox;
	//CEikTextListBox* iListBox;
	CMyListBox* iListBox;
	CDesCArray *iItemList;
	MListBoxWidgetObserver& iObserver;
	TBuf<128> iBuf;
	TSize iSize;
	TBuf<256> iSearchString;
	TInt iShadow;
	TBool iParent;
	
	CListBoxWidgetNM* iNM;
	TRect iWindowRect;
	
	};

#endif /* LISTBOXWIDGET_H_ */
