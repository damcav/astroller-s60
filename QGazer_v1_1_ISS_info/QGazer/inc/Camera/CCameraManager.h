/*
 * CCameraManager.h
 *
 *  Created on: 27-Mar-2010
 *      Author: Damien
 */

#ifndef CCAMERAMANAGER_H_
#define CCAMERAMANAGER_H_

#include <e32base.h>
#include "CCamera.h"

class MAppUiCb;

#define ZOOM_EFFECT

class CCameraManager : public CBase
	{
public:
	CCameraManager();
	virtual ~CCameraManager();
	
	void Save();
	void Restore();
	void SetSpace(const TCamera& aCamera);
	void SetEarth(const TCamera& aCamera);
	void StopEffectsL();
	void StartObjectFoundEffect();
	
	static TInt Callback(TAny* aPtr);
	
#ifdef ZOOM_EFFECT
	void StartZoomIn();
	void StartZoomOut();
	void StopZoom();
	TBool iZoomingIn;
#endif
	TBool Panning() {return iMoveToRunning;}
	
private:
	void LaunchTimerL();
	void DoCallbackL();
	void InitMoveToEffectL(const TCamera& aToCamera);
	
private:
	CPeriodic* iTimer;
	
	TCamera iSpace;
	TCamera iEarth;

	TVector iMoveToDelta;
	GLfloat iMoveToTotalDistance;
	TBool iMoveToRunning;
	GLfloat iMoveToDuration;
	TTime iMoveToStartTime;
	GLfloat iMoveToVelocity;
	TVector iMoveToStartPos;
	};

#endif /* CCAMERAMANAGER_H_ */
