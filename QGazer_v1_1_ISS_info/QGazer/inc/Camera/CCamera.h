/*
 * TCamera.h
 *
 *  Created on: 22-Mar-2010
 *      Author: Damien
 */

#ifndef TCamera_H_
#define TCamera_H_

#include <e32base.h>
#include "utils3d.h"
#include "CommonTypes.h"

typedef float GLfloat;

class TCamera
	{
public:
	TCamera();
	TCamera(const TLocation& aLocation);
	void Set(const TVector& iVector);
	void Set(const THorizontal& aHr);
	void Set(const TEquatorial& aEq);
	void Set(const TLocation& aLocation);
	void Add(const TPoint& aDelta);
	void AddZoom(const GLfloat aDelta);
	void SetX(const GLfloat aX);
	void SetY(const GLfloat aX);
	void SetZoom(const GLfloat aZoom);
	TBool CheckEarth(const TPoint& aPos, TLocation &aLocation);
 
	GLfloat Bearing(const TCamera& aTo);
	void Location(TLocation& aLocation) const;
	
	inline GLfloat X() const { return iX; }
	inline GLfloat Y() const { return iY; }
	inline GLfloat Zoom() const { return iZ; }

private:
	GLfloat DoBearing(GLfloat lat1, GLfloat long1, GLfloat lat2, GLfloat long2);
	GLfloat iX;
	GLfloat iY;
	GLfloat iZ;
	GLfloat iXBase;
	};

#endif /* TCamera_H_ */
