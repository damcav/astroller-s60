#ifndef __CALLBACK_H__
#define __CALLBACK_H__

#include <e32base.h>

class MCallback
	{
public:
	virtual void Callback(TInt aId = 0) = 0;
	};

class CCallback : public CActive
	{
public:
	CCallback(MCallback& aListener);
	~CCallback();

	void RunL();
	void DoCancel();
	
	void CallMe(TInt aId = 0);

private:
	MCallback& iListener;
	TInt iId;
	};

#endif /* FILELOG_H_ */
