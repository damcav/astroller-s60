#ifndef __ALIGNEX_H__
#define __ALIGNEX_H__

typedef enum {ELeftEx, ERightEx, ECentreEx, ETopLeftEx, ETopCentreEx, ETopRightEx, EBottomLeftEx, EBottomCentreEx, EBottomRightEx, ETopEx, EBottomEx} TAlignEx;

#endif //__ALIGNEX_H__
