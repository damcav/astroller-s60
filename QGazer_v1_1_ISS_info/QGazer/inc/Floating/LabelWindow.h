/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LABELWINDOW_H_
#define LABELWINDOW_H_

#include <e32cmn.h> 

#include "FloatingWindow.h"
#include "MAppUiCb.h"

class CFbsBitmap;


class CLabelWindow : public CFloatingWindow
	{
public:
	TBool Flash(TInt aAlpha);
	virtual ~CLabelWindow();
	static CLabelWindow* NewL();
	virtual void Draw(const TRect& aRect) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);

	void Set(const TPoint& aPoint, TBool aCrossHair);
	void Move(const TPoint& aPoint);
	
	void Refresh();
	void Reset();
	
	const TPoint& Point() const { return iPoint;}
	
private:
	CLabelWindow();
	void ConstructL();
	TPoint iPoint;
	TInt iAlpha;
	TBool iCrossHair;
	};

#endif /* FLOATINGKEYPAD_H_ */

