/*
 * LabelContainer.h
 *
 *  Created on: 05-May-2010
 *      Author: Damien
 */

#ifndef LABELCONTAINER_H_
#define LABELCONTAINER_H_

#include "CommonTypes.h"

#include "FloatingWindow.h"

class TLabel
	{
public:
	TRect iRect;
	TBuf<32> iText;
	TLabelType iType;
	};

class CLabelContainer : public CFloatingWindow
	{
public:
	static CLabelContainer* NewL(const TRect& aRect);
	~CLabelContainer();
	void ConstructL();
	void Add(const TPoint& aPoint, const TDesC& aText, TLabelType aType);
	void Reset();
	void DrawLabels();
protected:
	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	
private:
	CLabelContainer(const TRect& aRect);
	void Draw(const TRect& aRect) const;
	void PrepareLabels();
	void UnIntersect(TRect& aRect1, TRect& aRect2);
	
private:
	RArray<TLabel> iLabels;
	};

#endif /* LABELCONTAINER_H_ */
