/*
 * FloatingKeypad.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LBBUTTON_H_
#define LBBUTTON_H_

#include <coecntrl.h> 

class CFbsBitmap;

class MFloatingButtonObserver
	{
public:
	virtual void ButtonPressedL() = 0;
	};

class CLBButton : public CCoeControl
	{
public:
	static CLBButton* NewL(const TRect& aRect, TInt aId);
	virtual void Draw(const TRect& aRect) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	//void SetObserver(MFloatingButtonObserver* aObserver);
	~CLBButton();
	void SetPercentage(TReal32 aPc);
	void MoveButton(const TPoint& aOffset);
	
private:
	CLBButton(const TRect& aRect);	
	void ConstructL(TInt aId);
	void LayoutL();
	void Percentage();
	
private:
	//MFloatingButtonObserver* iObserver;
	CFbsBitmap* iBm;
	TPoint iButtonDownPoint;
	TRect iParentRect;
	TInt iId;
	};

#endif /* FLOATINGKEYPAD_H_ */
