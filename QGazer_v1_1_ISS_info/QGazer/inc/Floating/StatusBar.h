/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LOWERBAR_H_
#define LOWERBAR_H_

#include <e32cmn.h> 

#include "FloatingWindow.h"
#include "MAppUiCb.h"

class CFbsBitmap;

enum {EStatusBarLocationPressed = -2};

const TInt KCompassBmWidth = 60;
const TInt KCompassBmHeight = 25;
const TInt KCompassPenSize = 3;
const TInt KCompassTotalHeight = KCompassBmHeight + KCompassPenSize*2;
const TInt KCompassTotalWidth = KCompassBmWidth + KCompassPenSize*2;
const TInt KBarHeight = KCompassTotalHeight;
const TInt KOptionsWidth = 40;
const TInt KInfoBmWidth = 32;
const TInt KInfoBmHeight = 25;


class CCompassDisplay : public CCoeControl
	{
public:
	static CCompassDisplay* NewL(const CFont* aFont);
	virtual void Draw(const TRect& aRect)  const;
	virtual ~CCompassDisplay();
	void SetShakePhone(TBool aTf) {iShakePhone = aTf;}
	void LoadBitmapsL();
private:
	CCompassDisplay(const CFont* aFont);
	void ConstructL();
	CFbsBitmap* iBm;
    TBool iShakePhone;
    const CFont* iFont;
	};

class CStatusBar : public CFloatingWindow
	{
public:
	
	
	virtual ~CStatusBar();
	static CStatusBar* NewL(const TRect& aRect, MAppUiCb& aObserver);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void UpdateBearing();
	void SetFlashing(TBool aTf);
	TBool Flash(TInt aAlpha);
//	TBool Intersects(const TRect& aRect);
	void BarLayoutL();
	void LoadBitmapsL();
	
private:
	
	CStatusBar(const TRect& aRect, MAppUiCb& aObserver);
	void DrawCompass(CWindowGc& aGc, const TRect& aRect) const;
	void DrawLocation(CWindowGc& aGc, const TRect& aRect) const;
	void DrawTime(CWindowGc& aGc, const TRect& aRect) const;
	void DrawOptions(CWindowGc& aGc, const TRect& aRect) const;
	void DrawGps(CWindowGc& aGc, const TRect& aRect) const;
	void DrawInfo(CWindowGc& aGc, const TRect& aRect) const;
	void ConstructL();
	void LayoutL();
	
private:
	MAppUiCb& iObserver;
    CCompassDisplay* iCompassDisplay;
    
    TRect iInfoRect;
    TRect iGpsRect;
    TRect iLocRect;
    TRect iTimeRect;
    TRect iCompassRect;
    TRect iMenuRect;
    
    TBool iFlashing;
    TInt iShakeCount;
    CFbsBitmap* iInfoBm;
    
	};

#endif /* FLOATINGKEYPAD_H_ */

