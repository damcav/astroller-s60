/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef INFOWINDOW_H_
#define INFOWINDOW_H_

#include <e32cmn.h> 

#include "FloatingWindow.h"
#include "MAppUiCb.h"
#include "TextButton.h"
#include "MButtonObserver.h"

const TInt KBorderThickness = 4;

#include "AlignEx.h"
 
class CInfoWindowBorder : public CFloatingWindow
	{
	
public:
	TBool HiddenByMenu() {return ETrue;}	
	static CInfoWindowBorder* NewL(const TRgb& aColour);
	TBool Flash(TInt aAlpha);
	void Draw(const TRect& aRect) const;
private:
	CInfoWindowBorder(const TRgb& aColour);
	void ConstructL();
	TInt iAlpha;
	mutable TRgb iColour;
	};

class CInfoWindow : public CFloatingWindow, public MButtonObserver
	{
public:
	TBool HiddenByMenu() {return ETrue;}
	TBool Flash(TInt aAlpha);
	virtual ~CInfoWindow();
	static CInfoWindow* NewL(TInt aId, CInfoWindowBorder* aBorder, TInt aFontEnum = ETinyFont);
	virtual void Draw(const TRect& aRect) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void Set(const TRect& aRect, TAlignEx aAlign);
	CDesCArray& StringArray();
	void SetTextAlignment(TAlign aTextAlignment) {iTextAlignment = aTextAlignment;}
	void SetTemporary(TBool aTf) { iTemporary = aTf;}
	virtual TBool Temporary() { return iTemporary; } //from floatingwindow
	void AddButtonL(const TDesC& aText, TInt aId);
	void AddButton2L(const TDesC& aText, TInt aId);
	virtual void KeyPressedL(TInt aId);
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	void SetBm(CFbsBitmap* aBm) {iBm = aBm;}
	void KillBorder();
	void SetThreeD(TBool aTf) {iThreeD = aTf;}
//	void SetFont(TInt aFontEnum) {iFontEnum = aFontEnum;}
private:
	CInfoWindow(TInt aId, CInfoWindowBorder* aBorder,  TInt aFontEnum);
	void ConstructL();
	void ThreeD(CWindowGc& aGc, const TRect& aRect) const;
	
	CInfoWindowBorder* iBorderWindow;
	TPoint iPoint;
	TRect iSaveRect;
	CDesCArray* iLines;
	TInt iId;
	TAlign iTextAlignment;
	TBool iTemporary;
	CTextButton* iButton;
	CTextButton* iButton2;
	CFbsBitmap* iBm;
	TRect iContainerRect;
	TAlignEx iAlign;
	
	TInt iFontEnum;
	TBool iThreeD;
	};

#endif /* FLOATINGKEYPAD_H_ */

