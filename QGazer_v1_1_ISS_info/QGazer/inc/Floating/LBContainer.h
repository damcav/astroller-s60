/*
 * FloatingKeypad.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef FLOATINGKEYPAD_H_
#define FLOATINGKEYPAD_H_

#include <e32cmn.h> 
 
#include "FloatingWindow.h"
#include "MButtonObserver.h"

class CFbsBitmap;
class CLBScroll;

const TInt KNumberOfBitmaps  = 1;


//class MFloatingKeypadObserver
//	{
//public:
//	virtual void KeyPressedL(TInt aId) = 0;
//	};

class CTextButton;

class CLBContainer : public CCoeControl, public MButtonObserver
	{
public:
	~CLBContainer();
	static CLBContainer* NewL(const TRect& aRect);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);

//	void SetObserver(MFloatingKeypadObserver* aObserver);
	
	void SetKeyText(TInt aKeyId, const TDesC& aString);
	void SetKeyBm(TInt aKeyId, const CFbsBitmap* aBm);
	void ToggleShowDigits(TBool aKeepPos = ETrue);
	void Reset();
	
	virtual void KeyPressedL(TInt aId);
	virtual void ChildPointerEvent(TInt aId);

private:
	CLBContainer(const TRect& aRect);
	void ConstructL();
	void CreateKeysL();
	void LayoutL();
	void NewBmKey(TInt aKeyId, TInt aBmId );
	
private:
	//MFloatingKeypadObserver* iObserver;
	RPointerArray<CTextButton> iKeys;
    //RPointerArray<CFbsBitmap> iBitmaps;
    CLBScroll* iScroll;

    TBool iChildPointerEvent;
    
    const CFont* iFont;
    TInt iCellWidth;
    TInt iCellHeight;
    TInt iNumberOfCells;
    TInt iWidth;
    TInt iBorder;
    
	};

#endif /* FLOATINGKEYPAD_H_ */
