/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef SLIDER_H
#define SLIDER_H

#include <e32cmn.h> 

#include "FloatingWindow.h"
#include "MAppUiCb.h"


class CLBScroll;
class CSliderWindow : public CFloatingWindow
	{
public:
	TBool Flash(TInt aAlpha);
	TBool Temporary() { return ETrue;}
	virtual ~CSliderWindow();
	static CSliderWindow* NewL(const TRect& aRect, TInt aId);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);

	void HandleListBoxEventL(CEikListBox* aListBox, MEikListBoxObserver::TListBoxEvent aEventType);

	void Reset();
	void Refresh();
	
private:
	CSliderWindow(const TRect& aRec);
	void ConstructL(TInt aId);
    void CreateSliderL();	
    //void InitListBoxL(); 
	void LayoutL();
	TPoint iPoint;
	TBool iSliding;
	TInt iSlidingCount;
	TRect iSlidingRect;

private:
//	CAknSlider* iSlider;
    TInt iShadow;

	CLBScroll* iScroll;
	TInt iId;
	};

#endif /* FLOATINGKEYPAD_H_ */

