/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef DROPDOWNINPUT_H_
#define DROPDOWNINPUT_H_

#include <e32cmn.h> 
#include "ListBoxWidget.h"
#include "FloatingWindow.h"
#include "MButtonObserver.h"
#include "MAppUiCb.h"
#include "model.h"


class CTextButton;
class CTextDoubleButton;

enum {EPopulateListBox, ESetFromListBox, ESetDefault};

class CDropDownInput : public CFloatingWindow, MListBoxWidgetObserver, MButtonObserver
	{
public:
	virtual ~CDropDownInput();
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	virtual void KeyPressedL(TInt aId);
	void KeyEvent(TUint aChar);
	void Selected(TInt aIndex); //MButtonObserver
	void HighlightCurrentButton(TBool aDrawNow);
	CTextButton* FindButtonById(TInt aId, TBool& aDouble);
	CTextButton* FindButtonById(TInt aId) const;
	TInt Id() {return iId;}
	void HighlightCurrentButton();
	virtual int InitialSelection();

protected:
	CDropDownInput(MListBoxCb& aObserver)  : iObserver(aObserver) {};
	CTextButton* AddButtonL(TInt aId);
	CTextDoubleButton* AddButtonL(TInt aId1, TInt aId2);
	void AddItemL(const TDesC& aItem);
	virtual void SetBoxL(TInt aIndex, TInt aAction) = 0;
	void ConstructL();
	void SetLabelText(TInt aLabel, const TDesC& aText);

	TInt iCurrentList;
	RArray<TInt> iSelections;
	TInt iSelected;
	RPointerArray<CTextButton> iButtons;
	TRect iAbsRect;
	TRect iAppRect;
	TInt iId;

private:
	
	
	void SetButtonTexts();
	
    void InitListBoxL(); 	
	void LayoutL();
	
private:
	MListBoxCb& iObserver;
	
	TPoint iPoint;
	CListBoxWidget* iListBoxWidget;

	TBuf<64> iBuf;
	};

#endif /* FLOATINGKEYPAD_H_ */

