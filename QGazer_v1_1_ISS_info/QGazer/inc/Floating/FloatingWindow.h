/*
 * FloatingWindow.h
 *
 *  Created on: 13-Jan-2010
 *      Author: Damien
 */

#ifndef FLOATINGWINDOW_H_
#define FLOATINGWINDOW_H_

#include <COECNTRL.H>
#include <GDI.H>

#include "TextControl.h"

class CAknAppUi;

#define MAX_LINES 20

class CFloatingWindow;

class CFloatingWindowGroup : public CBase
	{
public:
	static CFloatingWindowGroup* NewL();
	~CFloatingWindowGroup();
	void SetOnTop();
	void SetBehind();
	RWindowGroup* Group() { return iGroup;}
	
	const CFont* BigFont() {return iBigFont;}
private:
	CFloatingWindowGroup() {}
	void ConstructL();
	
	const CFont* iNormalFont;
	const CFont* iSmallFont;
	const CFont* iTinyFont;
	const CFont* iBigFont;

private:
	RWindowGroup* iGroup;
	friend class CFloatingWindow;
	friend class CTextControl;
	};

class CFloatingWindow : public CTextControl
	{
public:
	virtual TBool Flash(TInt /*aAlpha*/) {return EFalse;}
	virtual TBool Temporary() { return EFalse;}//gets killed when user clicks outside
	virtual TBool HiddenByMenu() {return EFalse;}
	virtual ~CFloatingWindow();
	
	void ConstructL(TInt aBorder, TInt aMargin);
	 
	virtual void Draw(const TRect& aRect) const = 0; 

	void BaseDraw(CWindowGc& aGc, const TRect& aRect) const;    
    void ZeroRect();
    void HandleResourceChange(TInt aType);
    void ForegroundL(TBool aFg);
    void MakeVisible(TBool aVisible);
    void SetColours(const TRgb& aFg, const TRgb& aBg, const TRgb& aBd=KRgbBlack);
    const TRect& SaveRect() {return iSaveRect;}
	void SetDeletePending() {iDeletePending = ETrue;}
	TBool DeletePending() { return iDeletePending;}
	
protected:
	TBool iDeletePending;
	TInt iBackgroundCount;
	
	void SetTransparent();
	void SetTransparentAlpha();
	TBool BackgroundTapped(const TPointerEvent &aPointerEvent);
	
public:
    TRgb iBgColor, iFgColor, iBorderColor;
    TInt iCurrentWidth, iCurrentLineCount, iLineHeight;
    TInt iMargin, iBorder;
 	TBuf<64> iText;
    
    TRect iClientRect;
    TPoint iButtonDownPoint;
    TBool iVisible;

    TRect iSaveRect;
	
    CFloatingWindowGroup* iGroup;
	};

#endif /* FLOATINGWINDOW_H_ */
