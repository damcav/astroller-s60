/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LOCATIONINPUT_H_
#define LOCATIONINPUT_H_

#include <e32cmn.h> 
#include "MAppUiCb.h"
#include "model.h"
#include "DropDownInput.h"

class CLocationInput : public CDropDownInput
	{
public:
	TBool Temporary() { return ETrue;}
	virtual ~CLocationInput();
	static CLocationInput* NewL(const TRect& aRect, TInt aId, MListBoxCb& aObserver);
	virtual void Draw(const TRect& aRect) const;
	virtual void KeyPressedL(TInt aId);
	virtual int InitialSelection();
	
protected:
	void SetBoxL(TInt aIndex, TInt aAction);
	
private:
	CLocationInput(const TRect& aRec, TInt aId,  MListBoxCb& aObserver);
	void ConstructL();
	void LayoutL();

	const TLocation& ThisLoc();

	void SetFirstAndLastCities();
	void FindUserLocation();
	
	void InitCountriesL(TInt aAction);
	void InitCitiesL(TInt aAction);
	void SetPlace();

private:
	
	TBuf<64> iBuf;

	//EOk MUST BE ZERO!
	enum {EOk, ECountry, ECity, EDay, EMonth, EYearHi, EYearLo, EHour, EMinute, ETimeZone};
				 
	RArray<TInt> iLocationsIndex;
	
	TInt iFirstCity;
	TInt iLastCity;
	
	TInt iCurrentLocation;
	};

#endif /* FLOATINGKEYPAD_H_ */

