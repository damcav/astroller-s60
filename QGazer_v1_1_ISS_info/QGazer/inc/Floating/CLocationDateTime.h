/*
 * CLocationDateTime.h
 *
 *  Created on: 10-May-2010
 *      Author: Damien
 */

#ifndef CLOCATIONDATETIME_H_
#define CLOCATIONDATETIME_H_

#include "FloatingWindow.h"
#include "TextButton.h"
#include "Grid.h"
#include "ListBoxWidget.h"
#include "MButtonObserver.h"


class CQueue;
class CDropDownInput;

class CLocationDateTime : public CFloatingWindow, public MButtonObserver, public MListBoxCb
	{
public:
	TBool Temporary() { return ETrue;}
	static CLocationDateTime* NewL(const TRect& aRect, TInt aId, MListBoxCb& aObserver, CQueue* aAllWindows);
	~CLocationDateTime();
	virtual void Draw(const TRect& aRect) const;
	void KeyEvent(TUint aChar);
	TBool LocationActive() {return iLocationActive;}
	TBool DateTimeActive() {return iDateTimeActive;}
	
protected:
	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void KeyPressedL(TInt aId);
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	void ListBoxOkCancelL(TInt aId, TBool aOk);
	
private:
	enum {ELocIcon, ETimeIcon, ELocButton, ETimeButton, EOk};
	enum {ELocationInput, EDateTimeInput};
	
	CLocationDateTime(const TRect& aRect, TInt aId, MListBoxCb& aObserver, CQueue* aAllWindows);
	void ConstructL();
	void LayoutL();
	void StartListBoxL(TInt aId);
	void SetLocString();
	void SetTimeString();
	
	MListBoxCb& iObserver;
	TInt iId;
	RPointerArray<CTextButton> iButtons;

	CFbsBitmap* iLocBm;
	CFbsBitmap* iTimeBm;
	CQueue* iAllWindows;
	
	TRect iApplicationRect;
	
	CDropDownInput* iDropDownWindow;
	TBool iLocationActive;
	TBool iDateTimeActive;
	
	
	TBool iButtonsHidden;
	};

#endif /* CLOCATIONDATETIME_H_ */
