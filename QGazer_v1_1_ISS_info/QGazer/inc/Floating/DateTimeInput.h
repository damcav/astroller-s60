/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef DATETIMEINPUT_H_
#define DATETIMEINPUT_H_

#include <e32cmn.h> 
#include "MAppUiCb.h"
#include "model.h"
#include "DropDownInput.h"

class CDateTimeInput : public CDropDownInput
	{
public:
	TBool Temporary() { return ETrue;}
	virtual ~CDateTimeInput();
	static CDateTimeInput* NewL(const TRect& aRect, TInt aId, MListBoxCb& aObserver);
	virtual void Draw(const TRect& aRect) const;
	virtual int InitialSelection();
	
protected:
	void SetBoxL(TInt aIndex, TInt aAction);
	
private:
	CDateTimeInput(const TRect& aRec, TInt aId,  MListBoxCb& aObserver);
	void ConstructL();
	void LayoutL();

	void SetLabelsFromUtc();
	TDateTime ReadTime();
	
	const TDesC&  TimeZoneString(TInt aType);
	
	void InitDaysL(TInt aAction);
	void InitMonthsL(TInt aAction);
	void InitYearLowL(TInt aAction);
	void InitYearHighL(TInt aAction);
	void InitHoursL(TInt aAction);
	void InitMinutesL(TInt aAction);
	void InitTimeZoneL(TInt aAction);

	void SetTime();

private:
	TBuf<64> iBuf;

	//EOk MUST BE ZERO!
	enum {EOk, EDay, EMonth, EYearHi, EYearLo, EHour, EMinute, ETimeZone, EResetToNow};
	
	TDateTime iTime;
	TDateTime iTimeUtc;
	};

#endif /* FLOATINGKEYPAD_H_ */

