/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef MENUWINDOW_H_
#define MENUWINDOW_H_

#include <e32cmn.h> 

#include "FloatingWindow.h"
#include "MButtonObserver.h"
#include "MAppUiCb.h"

#include "Callback.h"
#include "ArticleHelper.h"

class CTextButton;

const int KNumPages = 8;

const int KLayersPage = 1;
const int KLearnPage = 3;
const int KIssSettingsPage = 2;

const int KFirstArticlePage = KLearnPage;
const int KLastArticlePage = KNumPages-1;


const int KNoIcon = -1;
const int KArticlesStart = 100;
const int KTextStart = 200;
const int KTextEnd = 300;

//Indexes in the MBM file
enum {EConstIcon=0,ELabelsIcon , EGlobeIcon, ELocIcon, ENightModeIcon, 
		EFindIcon, EAtmosIcon,  EStarsIcon, 
		EVisitIcon, EQuitIcon, EMortarIcon, ELayersIcon, 
		EPreviousIcon, ENextIcon,  EIssSettingsIcon, EIssIcon,
		EIssUpdateIcon, EIssPreviewIcon,
		ELoadingIcon, EShowPleaseWait, EHidePleaseWait, EQueueKIssPage};

const TInt KNumIcons = ELoadingIcon + 1;

const TInt KColumns = 4; 
const TInt KRows = 2;
const TInt KNumButtons = KRows*KColumns;

class MGetVisCb
	{
public:
	virtual void GotVis() = 0;
	};

class CGetVis : public CActive
	{
public:
	CGetVis(MGetVisCb& aListener);
	~CGetVis();

	void RunL();
	void DoCancel();
	
	void Go();

private:
	MGetVisCb& iListener;
	};



class CMenuWindow : 
	public CFloatingWindow, MButtonObserver, MCallback, MArticleHelperObserver, MGetVisCb
	{
public:
	TBool Temporary() { return ETrue;}
	virtual ~CMenuWindow();
	static CMenuWindow* NewL(const TRect& aRect, TInt aPage);
	virtual void Draw(const TRect& aRect) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void KeyPressedL(TInt aId);
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	void LayoutL();
	void DoLayout(TInt aIndex, const TRect& aRect);

	//from MCallback
	void Callback(TInt aId = 0);
	
	//from MArticleHelperObserver
	virtual void BitmapReady(CFbsBitmap* aBitmap);
	const TSkyObject* ChosenArticle() const {return iChosenArticle;}
	
	virtual void GotVis();
	TInt CurrentPage() const {return iPage;}
	
private:
	CMenuWindow(const TRect& aRect, TInt aPage);
	void ConstructL();
	void PrepareArticlePages();
	void LoadPageBitmaps();
	
	void HandleTextArea(TBool aFirst, TInt aId, const TRect& aRect);
	void FillTextArray(TInt aId);
	TBool IsArticle(TInt aIcon);
	
	void AdjustIssPageLayout();
	
	CTextButton* FindButton(TInt aIcon);
	
	RPointerArray<CTextButton> iButtons;
	RPointerArray<CFbsBitmap> iBitmaps;
	
	
	TPoint iPoint;
	TInt iPage;
	
	CCallback* iCallback;
	
	TInt iIconIndex[KNumIcons];
	int iPages[10][KNumButtons];
	
	//---------------------
	RPointerArray<TSkyObject> iArticleObjects;
	
	RPointerArray<TSkyObject> iArticleObjectsThisPage;
	RPointerArray<CFbsBitmap> iArticleBitmapsThisPage;
	RArray<TInt> iArticleButtonsThisPage;
	RArray<TRect> iTextAreas;
	RPointerArray<CDesCArrayFlat> iTextArrays;

	TBool iShowIssPreview;
	TBool iShowDownloadButton;
	
	TInt iCurrentThumb;
	
	CArticleHelper* iArticleHelper;
	const TSkyObject* iChosenArticle;
	
	CGetVis* iGetVis;
	TInt iIssMargin;
	};

#endif /* FLOATINGKEYPAD_H_ */

