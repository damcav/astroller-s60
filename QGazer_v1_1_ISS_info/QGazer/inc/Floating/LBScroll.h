/*
 * LBControl.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LBSCROLL_H_
#define LBSCROLL_H_

#include <coecntrl.h>

class CFbsBitmap;
class CLBButton;

class CLBScroll : public CCoeControl
	{
public:
	static CLBScroll* NewL(const TRect& aRect, TInt aId);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	~CLBScroll();
	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	
private:
	CLBScroll(const TRect& aRect);
	void ConstructL(TInt aId);
	CLBButton* iButton;
	};



#endif /* LBControl_H_ */
