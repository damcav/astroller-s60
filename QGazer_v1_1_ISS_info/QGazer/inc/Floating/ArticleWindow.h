/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef ARTICLEWINDOW_H_
#define ARTICLEWINDOW_H_

#include <e32cmn.h> 

#include "FloatingWindow.h"
#include "MButtonObserver.h"
#include "MAppUiCb.h"

#include "ArticleHelper.h"

class CTextButton;

enum {EOkButton};
class CFbsBitmap;

class CArticleWindow : public CFloatingWindow, MButtonObserver,  MArticleHelperObserver
	{
public:
	TBool Temporary() { return ETrue;}
	virtual ~CArticleWindow();
	static CArticleWindow* NewL(const TRect& aRect, const TSkyObject* aObject, TBool aFindButton);
	virtual void Draw(const TRect& aRect) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void KeyPressedL(TInt aId);
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	void LayoutL();
	void DrawStar(CWindowGc& aGc, const TRect& aRect) const;
	void DrawBitmap(CWindowGc& aGc, const TRect& aRect) const;

	virtual void BitmapReady(CFbsBitmap* aBitmap);

private:
	CArticleWindow(const TRect& aRect, const TSkyObject* aObject, TBool aFindButton);
	void ConstructL();

	const TSkyObject* iObject;
	RPointerArray<CTextButton> iButtons;
	RPointerArray<CFbsBitmap> iBitmaps;
	TPoint iPoint;
	CDesCArray* iLines;
	CArticleHelper* iArticleHelper;
	TBool iReady;
	TRect iRightRect;
	TRect iLeftRect;
	TRect iTextRect;
	TInt iRectDiff;
	TInt iLinesDy;
	RBuf iLoadedArticle;
	CFbsBitmap* iBm;
	TBool iHasBitmap;
	TBool iHasFindButton;
	};

#endif /* FLOATINGKEYPAD_H_ */

