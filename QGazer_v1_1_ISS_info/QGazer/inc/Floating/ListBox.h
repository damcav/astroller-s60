/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LISTBOX_H_
#define LISTBOX_H_

#include <e32cmn.h> 
#include "ListBoxWidget.h"
#include "FloatingWindow.h"
#include "MButtonObserver.h"
#include "MAppUiCb.h"

class CSkyModel;
class TSkyObject;
class CTextButton;

class CListBoxWindow : public CFloatingWindow, MListBoxWidgetObserver, MButtonObserver
	{
public:
	TBool Temporary() { return ETrue;}
	virtual ~CListBoxWindow();
	static CListBoxWindow* NewL(const TRect& aRect, const CSkyModel& aModel, TBool aLocations, MListBoxCb& aObserver);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	virtual void KeyPressedL(TInt aId);
	void Reset();
	void Refresh();
	void KeyEvent(TUint aChar);
	void Selected(TInt aIndex); //observer

private:
	CListBoxWindow(const TRect& aRec, const CSkyModel& aModel,  TBool aLocations, MListBoxCb& aObserver);
	void ConstructL();
    void InitListBoxL(); 
	void LayoutL();
	void InitConstellations();
	void InitPlanets();
	void InitStars();
	void InitLocations();
#ifdef MESSIER
	void InitMessier();
#endif
	void SetChosenObject();
	void AddIfVisible(const TSkyObject& aObject);
	void HighlightCurrentButton(TBool aDrawNow);

private:
	TPoint iPoint;
	CListBoxWidget* iListBoxWidget;
    TPoint iListBoxPos;
    TSize iListBoxSize;
    MListBoxCb& iObserver;
	RPointerArray<CTextButton> iButtons;
	RPointerArray<TSkyObject> iObjects;
	const CSkyModel& iModel;
	TBuf<64> iBuf;

	typedef enum {EConstellations, EPlanets, EStars, EMessier/*, ELocations*/ } TLBButton;
	TLBButton iCurrentList;
	TBool iLocations;
	
	TRect iWindowRect;
	};

#endif /* FLOATINGKEYPAD_H_ */

