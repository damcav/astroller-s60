/*
 * LowerBar.h
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#ifndef LOCATIONDATETIME_H_
#define LOCATIONDATETIME_H_

#include <e32cmn.h> 
#include "ListBoxWidget.h"
#include "FloatingWindow.h"
#include "MButtonObserver.h"
#include "MAppUiCb.h"
#include "model.h"


class CTextButton;

class CLocationDateTime : public CFloatingWindow, MListBoxWidgetObserver, MButtonObserver
	{
public:
	TBool Temporary() { return ETrue;}
	virtual ~CLocationDateTime();
	static CLocationDateTime* NewL(const TRect& aRect, const CSkyModel& aModel, TInt aId, MAppUiCb& aObserver);
	virtual void Draw(const TRect& aRect) const;
	TInt CountComponentControls() const;
	CCoeControl* ComponentControl(TInt aIndex) const;
	virtual void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	virtual void KeyPressedL(TInt aId);
	void KeyEvent(TUint aChar);
	void Selected(TInt aIndex); //observer
	void HighlightCurrentButton(TBool aDrawNow);
	TInt FindButtonById(TInt aId);
	TInt Id() {return iId;}
	
private:
	const TLocation& ThisLoc() {return iModel.Location(iCurrentLocation);}
	
	void SetLabelsFromUtc();
	TDateTime ReadTime();
	
	void SetLabelText(TInt aLabel, const TDesC& aText);
	const TDesC&  TimeZoneString(TInt aType);
	void SetFirstAndLastCities();
	void FindUserLocation();
	
	void SetButtonTexts();
	void SetBoxL(TInt aIndex, TInt aAction);
	CLocationDateTime(const TRect& aRec, const CSkyModel& aModel, TInt aId,  MAppUiCb& aObserver);
	void ConstructL();
    void InitListBoxL(); 
	void LayoutL();
	
	void InitCountriesL(TInt aAction);
	void InitCitiesL(TInt aAction);
	void InitDaysL(TInt aAction);
	void InitMonthsL(TInt aAction);
	void InitYearLowL(TInt aAction);
	void InitYearHighL(TInt aAction);
	void InitHoursL(TInt aAction);
	void InitMinutesL(TInt aAction);
	void InitTimeZoneL(TInt aAction);

	void SetPlace();
	void SetTime();
private:
	TPoint iPoint;
	CListBoxWidget* iListBoxWidget;
	MAppUiCb& iObserver;
	RPointerArray<CTextButton> iButtons;

	TBuf<64> iBuf;

	enum {EPopulateListBox, ESetFromListBox, ESetDefault};
	typedef enum {ECountry, ECity, EDay, EMonth, EYearHi, EYearLo, EHour, EMinute, ETimeZone, EOk } TLDTButton;
				 
	TLDTButton iCurrentList;
	TRect iAbsRect;
	TRect iAppRect;
	const CSkyModel& iModel;
	RArray<TInt> iLocationsIndex;
	
	TInt iFirstCity;
	TInt iLastCity;
	
	TInt iCurrentLocation;
	TInt iSelections[EOk];
	TInt iSelected;
	TInt iId;
	TBool iLocationOnly;
	TDateTime iTime;
	TDateTime iTimeUtc;
	};

#endif /* FLOATINGKEYPAD_H_ */

