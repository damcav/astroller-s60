/*
 * colours.h
 *
 *  Created on: 29 Sep 2011
 *      Author: Damien
 */

#ifndef COLOURS_H_
#define COLOURS_H_

#define ALPHA 232

#include <gdi.h>

#define NM(x) gColours.NMColour(x)

const TRgb KScheme0 = KRgbWhite;
const TRgb KScheme1(64,128,128);
const TRgb KScheme2(192, 192, 192);
const TRgb KScheme3(26, 26, 26);
const TRgb KScheme4(251, 134, 4);	//orange
const TRgb KScheme5(255, 0, 0);
const TRgb KScheme6(0, 255, 0);


const TRgb KNightRed(196, 0, 0);

//---Splash screen
//const TRgb KBcSplash(46, 85, 158);
const TRgb KBcSplash(62, 123, 123);
const TRgb KFcSplash = KRgbWhite;

//Status bar
const TRgb KFcShakePhone = KRgbCyan;
//const TRgb KFcCityName(176, 176, 4);
const TRgb KFcCityName = KScheme4;



//Ground mode button
const TRgb KBcGroundModeUser(119, 58,71);
const TRgb KFcGroundModeUser = KScheme2;

const TRgb KBcGroundMode = KScheme1;
const TRgb KFcGroundMode = KRgbWhite;


//New location button
const TRgb KBcNewLocation = KScheme1;
const TRgb KFcNewLocation = KScheme2;

//Waiting for GPS button
const TRgb KBcWaitingForGps = KScheme5;
const TRgb KFcWaitingForGps = KRgbWhite;

//Lights button
const TRgb KBcLightsButton = KScheme3;

//"Must return" button
const TRgb KBcMustReturn = KScheme5;
const TRgb KFcMustReturn = KRgbWhite;

//"Close flip" button
const TRgb KBcCloseFlip = KScheme5;
const TRgb KFcCloseFlip = KRgbWhite;

//Crosshair button
const TRgb KBcCrossHairButton = KScheme3;

//Reset zoom button
const TRgb KBcResetZoom(0, 0, 96);
const TRgb KFcResetZoom = KRgbWhite;

//Standard buttons
//const TRgb KFcStandardButton(5,165,128);
const TRgb KFcStandardButton = KScheme1;
const TRgb KBcStandardButton(0, 32, 32);
const TRgb KBcSplitButton(0, 48, 48);
const TRgb KBcButtonHighlight(198, 117, 28);
const TRgb KFcButtonHighlight = KRgbWhite;

//Date/Time input
const TRgb KBcDtResetButton(96, 0, 0);
const TRgb KFcDtResetButton = KRgbWhite;
const TRgb KBcDtOkButton(0, 96, 0);
const TRgb KFcDtOkButton = KRgbWhite;

//Floating windows
const TRgb KBcFloating = KScheme3;
//const TRgb KFcFloating(5,165,128);
const TRgb KFcFloating = KScheme1;


//Labels
const TRgb KBcLabel(16, 16, 16);
const TRgb KFcLabel(16, 16, 16);
const TRgb KFcPlanetLabel(251, 134, 4);
const TRgb KFcStarLabel = KRgbWhite;
//const TRgb KFcConstellationLabel(80, 211, 97);
//const TRgb KFcConstellationLabel(181, 206, 0);
const TRgb KFcConstellationLabel(160, 183, 0);

//Star count
const TRgb KFcStarCountButton = KRgbWhite;
const TRgb KBcStarCountButton(0, 64, 0);
const TRgb KBcStarCountScroll = KRgbBlack;

//Cursor
const TRgb KBcCursorStandard(171, 72, 247);
const TRgb KBcCursorCrossHair = KScheme4;

//Listbox
const TRgb KBcCurrentItem = KBcButtonHighlight;
const TRgb KFcCurrentItem = KFcButtonHighlight;

//Menu window
const TRgb KFcIssTitle(5,165,128);

//Download
const TRgb KFcDownload = KFcIssTitle;
const TRgb KBcDownload = KBcFloating;

//Info box
//const TRgb KBcInfoBox(61, 4, 102);
const TRgb KBcInfoBox = KScheme3;
const TRgb KFcInfoBox = KBcCursorStandard;

class TColours
	{
public:	
	TColours();
	~TColours();
	const TRgb& GlobalColour(TInt aIndex) const;
	const TRgb& NMColour(const TRgb& aColour) const;
	
private:
	RArray<TRgb> iGlobalColours;
	mutable TRgb iNMColour;
	};
#endif /* COLOURS_H_ */
