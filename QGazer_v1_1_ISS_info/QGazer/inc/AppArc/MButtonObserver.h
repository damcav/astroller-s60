/*
 * MButtonObserver.h
 *
 *  Created on: 13-Apr-2010
 *      Author: Damien
 */

#ifndef MBUTTONOBSERVER_H_
#define MBUTTONOBSERVER_H_

class MButtonObserver
	{
public:
	virtual void KeyPressedL(TInt aId) = 0;
	};

#endif /* MBUTTONOBSERVER_H_ */
