/*
 * ==============================================================================
 *  Name        : SimpleCubeAppUi.h
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */

#ifndef SIMPLECUBEAPPUI_H
#define SIMPLECUBEAPPUI_H

#define NEWLABELS



// INCLUDES
#include <eikapp.h>
#include <eikdoc.h>
#include <e32std.h>
#include <coeccntx.h>
#include <aknappui.h>
#include <aknlongtapdetector.h> 

#include "colours.h"


#include "MSensorsCb.h"
#include "CommonTypes.h"

#include "MAppUiCb.h"
#include "CQueue.h"


// FORWARD DECLARATIONS
class CQGazerContainer;
class CQSensors;
class CFloatingWindowGroup;
class CStatusBar;
class CLabelWindow;
class CCameraManager;
class CLocationInput;
class CLocationDateTime;

//class CListBoxWindow;
class CSliderWindow;
class CFloatingWindow;
class CSkyModel;
class CInfoWindow;
class CInfoWindowBorder;
class CMenuWindow;
class CDateTimeInput;
class CArticleWindow;


#include "Callback.h"

#ifdef NEWLABELS
    class CLabelContainer;
#endif
    
#ifdef SATELLITE
#include "CGetUrl.h"
#endif
    
#include "listbox.h"

const TInt KMaxLabels = 1;

const TTimeIntervalMicroSeconds  KGapThreshold(1000000/3);
const TTimeIntervalMicroSeconds  KTapThreshold(1000000/3);
const TTimeIntervalMicroSeconds  KTapTimeOut(1000000);
const TTimeIntervalMicroSeconds  KDragThreshold(1000000/6);

const TInt KPointDiffThreshold = 25;


#include <remconcoreapitargetobserver.h>// link against RemConCoreApi.lib 
#include <remconcoreapitarget.h>           
#include <remconinterfaceselector.h>        // RemConInterfaceBase.lib

class CMediaKeysTestUi :  public MRemConCoreApiTargetObserver
	{
// From MRemConCoreApiTargetObserver
public:
	void ConstructL(MAppUiCb* aParent);

	void MrccatoCommand(TRemConCoreApiOperationId aOperationId,
			TRemConCoreApiButtonAction aButtonAct);
	virtual ~CMediaKeysTestUi();
	
private:
	CRemConInterfaceSelector* iInterfaceSelector;
	CRemConCoreApiTarget* iCoreTarget;
	MAppUiCb* iParent;
	};



class TOnScreenObject
	{
public:
	TScreenCoords iCoords;
	TBool iSunMoon;
	const TSkyObject* iObject;
	};

// CLASS DECLARATION

/**
 * Application UI class that contains the CQGazerContainer
 * (as required by the Symbian UI application architecture).
 */
class CQGazerAppUi : 
	public CAknAppUi, 
	public MSensorsCb, 
	public MAppUiCb, 
	public MListBoxCb
#ifdef SATELLITE
	, public MGetUrlObserver
	, public MCallback
#endif	
    {
    public: // Constructors and destructor
    	
        void ConstructL();
        virtual ~CQGazerAppUi();

        void SetViewAndStatusBarL();
    	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
    	void Label(const TPoint& aPoint, const TDesC& aText, TLabelType aType);
    	void DrawLabels();
    	void ResetLabels();
    	
    	virtual void GLReadyL();
    	
    	TBool SearchActive();
    	GLfloat SearchBearing();
    	void SliderPercentage(TInt aId, GLfloat aPercentage);
    	void ForegroundL(TBool aFg);
    	
    	void HandleForegroundEventL(TBool aForeground);

 
    	enum {EDeleteObjectQueue};
    	void QueueCallBackL(TInt aId);
    	CSkyModel& Model();
    	//void ChosenObjectInfoL(TBool aShow);
    	void InfoBoxL(TBool aShow);
    	 void InfoWindowCbL(TInt aId);
    	
    	void DoubleTapL(const TPoint& aPoint);
    	
    	CArrayFix<TCoeHelpContext>* HelpContextL() const;
    	
    	void DoHelpL(TBool aIndex);
    	
    	TBool Zoom(TBool aIn);
    	void ResetZoom();

    	virtual void FindObject(const TSkyObject* aObject);
    	
    	void Callback(TInt aId = 0);
    	
    private: // Functions from base classes
    	TBool FloatingWindowVisible();
    	void LoadBitmapsL();
    	void DestroyMenu();
    	enum {EStarsSlider};
    	void Redraw();
    	void ActivateIfReadyL(CFloatingWindow* aWindow);

    	void GroundModeButtonL(TBool aShow);
    	void NewLocationButtonL(TBool aShow, TBool aTemporary = ETrue);
    	void WaitingForGpsButtonL(TBool aShow);
    	void LightsButtonL(TBool aShow);
    	void ReturnButtonL(TBool aShow);
    	void SplashWindowL(TBool aShow, TBool aButton);
    	void MustReturnWindowL(TBool aShow);
    	void CloseFlipButtonL(TBool aShow);
    	void DownloadBoxL(TBool aShow);
    	void PleaseWaitWindowL(TBool aShow);
    	
    	
    	void ReturnToHomeLocationL();
    	void VisitLocationL(TBool aUserTime);

    	
    	void StarsSliderStartL();
    	void StarsSliderEnd();
    	void StarsSliderPercentage(GLfloat aPercentage);
    	void SliderOk(TInt aId);
    	void ListBoxOkCancelL(TInt aId, TBool aOk);

    	void LocationOnGlobe();
    	
    	enum {EMainInfoWindow=1, EGroundModeWindow, ENewLocationWindow, ENewLocationOkButon, 
    	EWaitingForGpsButton, EWaitingForGpsOkButton, ELightsButton, ESplashWindow,
    	ESplashHelpButton, EMustReturnWindow, EMustReturnButton, ENightModeButton, EReturnButton, 
    	ECloseFlipButton, ECrossHairButton, EResetZoomButton, EMoreInfoButton,
    	EDownloadWindow, EDownloadCancelButton, EDownloadOkButton, EPleaseWaitWindow};
    	
        /**
         * This method is called by the EIKON framework just before it displays
         * a menu pane. Does nothing in this implemenation.
         */
        void DynInitMenuPaneL(TInt aResourceId,CEikMenuPane* aMenuPane);

        /**
         * CEikAppUi method that is used to handle user commands.
         * Handles menu commands and application exit request.
         * @param aCommand Command to be handled.
         */
        void HandleCommandL(TInt aCommand);

        /**
         * CEikAppUi method that is used to handle key events.
         * Does nothing in this implemenation.
         * @param aKeyEvent Event to handled (ignored by this implementation).
         * @param aType Type of the key event (ignored by this implementation).
         * @return Response code. Always EKeyWasNotConsumed in this implementation.
         */
        virtual TKeyResponse HandleKeyEventL(
            const TKeyEvent& aKeyEvent,TEventCode aType);

        
        void FlipModeL();
        void DoFlipModeL();
        void RefreshL();
        void ResetCamera();
        void MenuWindowL(TInt aPage = 0);
        
        void ArticleWindowL(const TSkyObject* aObject, TBool aFindButton);
        void MenuCommandL(TInt aId);
        void StartListBoxL(TInt aId);
        
    	virtual void AccDataL(TReal aAngle);
    	virtual void CompassData(TReal aBearing);
    	virtual void Updated(TBool aTime, TBool aAcc, TBool aCompass);
        virtual void GpsDataL(TGpsStatus aValid, TReal aLong, TReal aLat);
    	
        const TRect& SkyRect() {return iSkyRect;}
        
        void MoveCrossHair(const TPoint& aDelta);
        void ToggleCrossHair();
        void AddObjectOnScreen(const TScreenCoords& aCoords, const TSkyObject* aObject, TBool aSunMoon);
        void EnableCrossHair();
        void ClearCrossHair();
        void ClearLabelWindows();
        TInt GetNearest(TReal& aDist);
        void CrossHairUp();
        void CrossHairButtonL(TBool aShow);
        void ResetZoomButtonL(TBool aShow);
        void FindFinishedL(TBool aZoom = EFalse);
        
    private: //Data

        /** GUI container that resides in this application UI. */
        CQGazerContainer* iAppContainer;
        CQSensors* iSensors;
        
        CFloatingWindowGroup* iFloatingWindowGroup;
        CStatusBar* iStatusBar;
   
        
        CCameraManager* iCameraManager;
    	TPoint iButtonDownPoint;

    	CQueue* iAllWindows;

    	
    	TBool iCursorVisible;
    	
    	
    	CListBoxWindow* iListBoxWindow;
    	CSliderWindow* iSliderWindow;
    	
    	CAknLongTapDetector* iLongTap;
    	
    	TBool iShowArrow;
    	TBool iShowLabels;
    	TBool iSensorValid;
    	
    	
    	CLabelWindow* iCursor;
    	
    	CInfoWindow* iInfoWindow;
    	CInfoWindowBorder* iInfoWindowBorder;
    	
    	CInfoWindow* iGroundModeButton;
    	CInfoWindowBorder* iGroundModeButtonBorder;
    	
    	CInfoWindow* iNewLocationButton;
    	CInfoWindowBorder* iNewLocationButtonBorder;
    	
    	CInfoWindow* iWaitingForGpsButton;
    	CInfoWindow* iLightsButton;
    	CInfoWindow* iSplashWindow;
    	CInfoWindow* iMustReturnWindow;
    	CInfoWindow* iReturnButton;
    	CInfoWindow* iCloseFlipButton;
    	CInfoWindow* iCrossHairButton;
    	CInfoWindow* iResetZoomButton;
    	CInfoWindow* iDownloadWindow;
    	CInfoWindow* iPleaseWaitWindow;
    	
    	CMenuWindow* iMenuWindow;
    	CArticleWindow* iArticleWindow;
    	
    	TRect iSkyRect;
    	
    	CFbsBitmap* iLightsOnBm;
    	CFbsBitmap* iLightsOffBm;
    	CFbsBitmap* iCrossHairOnBm;
    	CFbsBitmap* iCrossHairOffBm;
    	
    	TBool iReady;
    	
    	CLocationInput* iLocationInput;
    	
    	CLocationDateTime* iLDT;
    	
    	TBool iDontShowRealModeWarning;
    	CMediaKeysTestUi* iVolumeKeys;

    	TBool iDoFindAfterZoomEffect;
    	TBool iDoDownLoadAfterDeletingMenu;
    	
    	CCallback* iCallback;
    	
#ifdef SATELLITE
    	CGetUrl* iDownload;
    	void DownloadStatusL(WebClientStatus aStatus);
    	void DownloadCancel();
    	void DownloadOk();
    	void HandleDownloadL();
    	void HandleDownload2L();
    	
#endif

#ifdef NEWLABELS
    	CLabelContainer* iLabelContainer;
#endif
    	TBool iSensorMessageDisplayed;
    	RArray<TOnScreenObject> iOnScreenObjects;
    	TInt iLastShortestI;
    };

#endif

// End of File
