/*
 * TextControl.h
 *
 *  Created on: 13-Apr-2010
 *      Author: Damien
 */

#ifndef TEXTCONTROL_H_
#define TEXTCONTROL_H_

#include <COECNTRL.H>

enum {ENormalFont, ESmallFont, ETinyFont, EBigFont};

#include "AlignEx.h"

class CTextControl : public CCoeControl
	{
protected:	
    
    const CFont* NormalFont() const;
    const CFont* SmallFont() const;
    const CFont* TinyFont() const;
    const CFont* BigFont() const;
    
    TInt FontHeight() const {return iFontHeight;}
    TInt FontDescent() const {return iFontDescent;}
    TInt TextWidth(const TDesC& aText) const; 
    
    TRect TextRect(const CDesCArray* aLines) const;
    void UseFont(CWindowGc& aGc, TInt aFontId) const;
    void SetFont(TInt aFontId) const;

    void DrawText(const TRect& aRect, const TDesC& aText, TAlign aAlign=ELeft) const;
    void DrawText(const TPoint& aPoint, const TDesC& aText) const;
    void DrawTextMultiColour(const TPoint& aPoint, const TDesC& aText) const;
    void DrawText(TPoint aPoint, const CDesCArray* aLines) const;
    void DrawText(const TRect& aRect, const CDesCArray* aLines, TAlign aAlignX, TAlignEx aAlignY = ETopEx) const;
    TInt AlignX(TInt aWidth, TAlign aAlign, const TDesC& aText) const;
    void MakeLines(TInt aWidth, const TDesC& aText, CDesCArray& aLines);
    
private: 
    TInt ColourCodePos(const TDesC& aText) const;
    mutable TInt iFontHeight;
    mutable TInt iFontDescent;
    mutable const CFont* iFont;
    mutable CWindowGc* iGc;
    CWindowGc& Gc() const {return *iGc;}
	};

#endif /* TEXTCONTROL_H_ */
