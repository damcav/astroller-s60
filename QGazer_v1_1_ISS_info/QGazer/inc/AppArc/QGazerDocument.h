/*
 * ==============================================================================
 *  Name        : SimpleCubeDocument.h
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */

#ifndef SIMPLECUBEDOCUMENT_H
#define SIMPLECUBEDOCUMENT_H

// INCLUDES
#include <akndoc.h>

// FORWARD DECLARATIONS
class  CEikAppUi;
class CSkyModel;

// CLASS DECLARATION

/**
 * Document class that is just used as the container for the application
 * (as required by the Symbian UI application architecture).
 */
class CQGazerDocument : public CAknDocument
    {
    public: // Constructors and destructor

        /**
         * Factory method for creating a new CQGazerDocument object.
         */
        static CQGazerDocument* NewL(CEikApplication& aApp);

        /**
         * Destructor. Does nothing.
         */
        virtual ~CQGazerDocument();
        
        CSkyModel& Model() { return *iModel; }

    private:  // Functions from base classes

        /**
         * C++ constructor. Just passes the given application reference to the baseclass.
         */
        CQGazerDocument(CEikApplication& aApp);

        /**
         * Second phase constructor. Does nothing.
         */
        void ConstructL();

    private: // New functions

        /**
         * From CEikDocument, creates and returns CQGazerAppUi application UI object.
         */
        CEikAppUi* CreateAppUiL();
        
        CSkyModel* iModel;
    };

#endif

// End of File

