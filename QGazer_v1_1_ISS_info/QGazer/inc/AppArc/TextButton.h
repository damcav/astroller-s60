/*
 * TextButton.h
 *
 *  Created on: 13-Apr-2010
 *      Author: Damien
 */

#ifndef TEXTBUTTON_H_
#define TEXTBUTTON_H_

#include <e32std.h>
#include "TextControl.h"

class MButtonObserver;

#if(0)
class MQueueCallback
	{
public:
	virtual void QueueCallback() = 0;
	};

class CQueueCallback : public CActive
	{
public:	
	void Queue();
	static CQueueCallback* NewL(MQueueCallback& aObserver);
	virtual	void RunL();

private:
	void DoCancel() {}
	CQueueCallback(MQueueCallback& aObserver);
	
	MQueueCallback& iObserver;
	};

#endif

enum {EButtonStyleNone, EButtonStyleTime, EButtonStyleDate};

class CTextButton : public CTextControl//, public MQueueCallback
	{
public:	
	~CTextButton();
	static CTextButton* NewL(MButtonObserver& aObserver, TInt aId);
	virtual void Draw(const TRect& aRect) const;
	void DrawMultiButton(const TRect& aRect) const;
	void SetText(const TDesC& aString);
	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void SetColours(const TRgb& aFg, const TRgb& aBg, const TRgb& aBd=KRgbBlack);
	virtual TInt Id() const; 
	const TDesC& Text() {return iText;}
	void SetBm(CFbsBitmap* aBm) {iBm = aBm;}
	CFbsBitmap* Bm() const {return iBm;} 
	
	void SetShadow(TInt aShadow) {iShadow = aShadow;}
	void SetRound(TInt aRound) {iRound = aRound;}
	void SetCrossed(TBool aTf, TBool aFlippable) {iCrossed = aTf; iCrossable = aFlippable;}
	void SetFontId(TInt aId) { iFontId = aId; }
	void SetStyle(TInt aStyle) { iStyle = aStyle;}
	virtual void SetHighlighted(TBool aTf) {iHighlighted = aTf;}
	void SetUnderneath(const TRgb& aUnderneath) {iUnderneath = aUnderneath;}
	//virtual void QueueCallback() { DrawNow();}
	void SetAllowHighlighting(TBool aTf) {iAllowHighlighting = aTf;}
	void ChangeId(TInt aId) {iId = aId;} 
	TBool Crossed() const {return iCrossed;}
	
private:
	void ConstructL();
	
protected:
	CTextButton(MButtonObserver& aObserver, TInt aId);
	MButtonObserver& iObserver;
	TInt iShadow;
	TInt iRound;

	TInt iFontId;
    TRgb iBgColour, iFgColour, iBdColour, iHlColour, iUnderneath;
    TRgb iShadowColour;
	
	TBuf<64> iText;
	TInt iId;
	CFbsBitmap* iBm;
	
	TBool iCrossed;
	TBool iCrossable;
	
	TInt iStyle;
	TBool iAllowHighlighting;
	
	//CQueueCallback* iCb;
	
	mutable TBool iHighlighted; 
	};

class CTextDoubleButton : public CTextButton
	{
public:	
	static CTextDoubleButton* NewL(MButtonObserver& aObserver, TInt aId1, TInt aId2);
	virtual void Draw(const TRect& aRect) const;
	void HandlePointerEventL(const TPointerEvent &aPointerEvent);
	void SetText2(const TDesC& aString);
	void SetColour2(const TRgb& aColour) {iBgColour2 = aColour;}
	virtual void SetHighlighted(TBool aTf) {iHighlighted = aTf; iHighlighted2 = aTf;}
	virtual TInt Id() const; 
	
protected:
	CTextDoubleButton(MButtonObserver& aObserver, TInt aId1, TInt aId2);
	
private:
	TInt iId2;
	TBuf<32> iText1;
	TBuf<32> iText2;
	TRgb iBgColour2;
	mutable TBool iHighlighted2;
	};

#endif /* TEXTBUTTON_H_ */
