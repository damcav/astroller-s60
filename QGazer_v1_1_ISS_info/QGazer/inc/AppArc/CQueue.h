/*
 * CQueue.h
 *
 *  Created on: 02-Apr-2010
 *      Author: Damien
 */

#ifndef CQUEUE_H_
#define CQUEUE_H_

#include <e32base.h>

#include "FloatingWindow.h"

#define STOREWINDOW(z) iAllWindows->Store((CFloatingWindow**)&(z))
#define DELETEWINDOW(z) iAllWindows->QueueDelete((CFloatingWindow**)&(z))
#define DELETEWINDOWNOW(z) iAllWindows->Delete((CFloatingWindow**)&(z))

class TStoredWindow
	{
public:
	inline TStoredWindow(CFloatingWindow** aWindow) : iWindow(aWindow), iDelete(EFalse) {}
	inline void Delete() { delete *iWindow; *iWindow = NULL;}
	void MarkForDelete() {iDelete = ETrue;}
	inline TBool MarkedForDelete() { return iDelete;}
	inline CFloatingWindow** WindowPtr() {return iWindow;}
	inline CFloatingWindow* Window() {return *iWindow;}
	
private:
	CFloatingWindow** iWindow;
	TBool iDelete;
	};

class CPeriodic;

class CQueue : public CActive
	{
public:	
	static CQueue* NewL();
	virtual	void RunL();
	~CQueue();
	void QueueDelete(CFloatingWindow** aWindow);
	void Delete(CFloatingWindow** aWindow); 
	void Store(CFloatingWindow** aWindow);
	void Test() {};
	void DoCancel() {}
	void ForegroundL(TBool aFg);
	void HideL(TBool aTf);
	void QueueDeleteTemporaries(TBool aDeleteAll = EFalse);
	void QueueDeleteAll();
	static TInt Callback(TAny* aPtr);
	void StartFlashingL();	
	void StopFlashing();
	void ActivateL();
	void DeleteAll();
	
private:
	CQueue();
	
	void FlashingWindows();
	
	CPeriodic* iTimer;
  	RArray<TStoredWindow> iWindows;
  	
    TInt iAlphaDelta;
	TInt iAlpha;
  	
	};

#endif /* CQUEUE_H_ */
