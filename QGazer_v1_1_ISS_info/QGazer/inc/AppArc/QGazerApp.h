/*
 * ==============================================================================
 *  Name        : SimpleCubeApp.h
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2005-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */

#ifndef SIMPLECUBEAPP_H
#define SIMPLECUBEAPP_H

// INCLUDES
#include <aknapp.h>

// CONSTANTS
/** UID of the application. */
const TUid KUidSimpleCube = { 0x2003088B };
const TUid KUidQGazerHelp = {0x2003088C};  // From help

// CLASS DECLARATION

/**
 * Application class. Provides factory method to create a concrete document object.
 */
class CQGazerApp : public CAknApplication
    {
public:
	const TDesC& FullResourcePath(const TDesC& aFileName);
	const TDesC& SetPrivatePath(RFs& aSession, TBool aWriteable=EFalse);
	
    private: // Functions from base classes

        /**
         * From CApaApplication, creates and returns CQGazerDocument document object.
         * @return Pointer to the created document object.
         */
        CApaDocument* CreateDocumentL();

        /**
         * From CApaApplication, returns application's UID (KUidSimpleCube).
         * @return Value of KUidSimpleCube.
         */
        TUid AppDllUid() const;
        
        TBuf<256> iPath;
    };

// OTHER EXPORTED FUNCTIONS

/**
 * Factory method used by the E32Main method to create a new application instance.
 */
LOCAL_C CApaApplication* NewApplication();

/**
 * Entry point to the EXE application. Creates new application instance and
 * runs it by giving it as parameter to EikStart::RunApplication() method.
 */
GLDEF_C TInt E32Main();

#endif

// End of File
