/*
 * MAppUiCb.h
 *
 *  Created on: 29-Mar-2010
 *      Author: Damien
 */

#ifndef MAPPUICB_H_
#define MAPPUICB_H_

typedef float GLfloat;

#include "commontypes.h"
#include "objects.h"

enum {EFindListBox, ELocationListBox, EDateTimeListBox, ELocationDateTime};

class MAppUiCb
	{
public:
	virtual void Label(const TPoint& aPoint, const TDesC& aText, TLabelType aType) = 0;
	virtual void HandlePointerEventL(const TPointerEvent& aPointerEvent) = 0;
	virtual void DrawLabels() = 0;
	virtual void ResetLabels() = 0;
	virtual void SliderPercentage(TInt aId, GLfloat aPercentage) = 0;
	virtual void SliderOk(TInt aId) = 0;
	virtual void ForegroundL(TBool aFg) = 0;
	virtual void Redraw() = 0;
  	virtual void InfoBoxL(TBool aShow) = 0;
	virtual void InfoWindowCbL(TInt aId) = 0;
	virtual void MenuWindowL(TInt aPage = 0) = 0;
	virtual void MenuCommandL(TInt aId) = 0;
	virtual void DoubleTapL(const TPoint& aPoint) = 0;
	virtual void GLReadyL() = 0;
	virtual void SplashWindowL(TBool aShow, TBool aButton) = 0;
	virtual void RefreshL() = 0;
	virtual TBool Zoom(TBool aIn) = 0;	
	virtual void AddObjectOnScreen(const TScreenCoords& aCoords, const TSkyObject* aObject, TBool aSunMoon) = 0;
	virtual void FindFinishedL(TBool aZoom = EFalse) = 0;
	virtual void FindObject(const TSkyObject* aObject) = 0;
	};
#endif /* MAPPUICB_H_ */
