/*
 * ==============================================================================
 *  Name        : SimpleCubeContainer.h
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */

#ifndef SIMPLECUBECONTAINER_H
#define SIMPLECUBECONTAINER_H

// INCLUDES
#include <coecntrl.h>
#include <GLES/egl.h>
#include "QGazer.h"
#include <akndef.h>
#include <aknappui.h> 

#include "QGazerCb.h"
#include "MAppUiCb.h"

class CSkyModel;
// CLASS DECLARATION

/**
 * Container control class that handles the OpenGL ES initialization and deinitializations.
 * Also uses the CQGazer class to do the actual OpenGL ES rendering.
 */
class CQGazerContainer : public CCoeControl, MCoeControlObserver, MQGazerCb, MCoeForegroundObserver
    {
    public: // Constructors and destructor
    	CQGazerContainer(MAppUiCb& aParent) : iParent(aParent) {}

    	void GLReadyL();
        /**
         * EPOC default constructor. Initializes the OpenGL ES for rendering to the window surface.
         * @param aRect Screen rectangle for container.
         */
        void ConstructL(const TRect& aRect, const CSkyModel& aModel);

        /**
         * Destructor. Destroys the CPeriodic, CQGazer and uninitializes OpenGL ES.
         */
        virtual ~CQGazerContainer();
        
        CQGazer& QGazer() { return *iSimpleCube;} 

        void HandleGainingForeground();
        void HandleLosingForeground();
        
    public: // New functions

        /**
         * Callback function for the CPeriodic. Calculates the current frame, keeps the background
         * light from turning off and orders the CQGazer to do the rendering for each frame.
         *@param aInstance Pointer to this instance of CQGazerContainer.
         */
        static TInt DrawCallBack( TAny* aInstance );

        void HandlePointerEventL(const TPointerEvent &aPointerEvent);
        
    private: // Functions from base classes

        /**
         * Method from CoeControl that gets called when the display size changes.
         * If OpenGL has been initialized, notifies the renderer class that the screen
         * size has changed.
         */
        void SizeChanged();

        /**
         * Handles a change to the control's resources. This method
         * reacts to the KEikDynamicLayoutVariantSwitch event (that notifies of
         * screen size change) by calling the SetExtentToWholeScreen() again so that
         * this control fills the new screen size. This will then trigger a call to the
         * SizeChanged() method.
         * @param aType Message UID value, only KEikDynamicLayoutVariantSwitch is handled by this method.
         */
        void HandleResourceChange(TInt aType);

        /**
         * Method from CoeControl. Does nothing in this implementation.
         */
        TInt CountComponentControls() const;

        /**
         * Method from CCoeControl. Does nothing in this implementation.
         */
        CCoeControl* ComponentControl(TInt aIndex) const;

        
        TBool iStop;
        /**
         * Method from CCoeControl. Does nothing in this implementation.
         * All rendering is done in the DrawCallBack() method.
         */
        void Draw(const TRect& aRect) const;

        /**
         * Method from MCoeControlObserver that handles an event from the observed control.
         * Does nothing in this implementation.
		 * @param aControl   Control changing its state.
		 * @param aEventType Type of the control event.
         */
        void HandleControlEventL(CCoeControl* aControl,TCoeEvent aEventType);

    private: //data

        /** Display where the OpenGL ES window surface resides. */
        EGLDisplay  iEglDisplay;

        /** Window surface where the OpenGL ES rendering is blitted to. */
        EGLSurface  iEglSurface;

        /** OpenGL ES rendering context. */
        EGLContext  iEglContext;

        /** Active object that is the timing source for the animation. */
        CPeriodic*  iPeriodic;

        /**
         * Flag that indicates if OpenGL ES has been initialized or not.
         * Used to check if SizeChanged() can react to incoming notifications.
         */
        TBool iOpenGlInitialized;

        /** Frame counter variable, used in the animation. */
        TInt iFrame;

/** Application UI class for querying the client rectangle size. */ 
        CAknAppUi* iAppUi;

    public:  //data

        /** Used in DrawCallBack() method to do the actual OpenGL ES rendering.  */
        CQGazer* iSimpleCube;
        
private:
	MAppUiCb& iParent;
	TBool iGLReady;
    };

#endif

// End of File
