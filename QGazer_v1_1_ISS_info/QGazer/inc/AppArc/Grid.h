/*
 * Grid.h
 *
 *  Created on: 10-May-2010
 *      Author: Damien
 */

#ifndef GRID_H_
#define GRID_H_

#include <e32base.h>

typedef enum {ECentre, EShrink, EGrow} TGridPolicy;

class TGrid
	{
public:
	TGrid(const TSize& aSize);
	void SetBorders(TInt aVertical, TInt aHorizontal);
	void SetCellBorders(TInt aVertical, TInt aHorizontal);
	TSize SetDim(TInt aColumns, TInt aRows, TGridPolicy aPolicy);	//returns grow factor
	void SetCellExtent(TInt aWidth, TInt aHeight);
	TRect Rect(TInt aX, TInt aY);
	
private:
	TSize iSize;
	TInt iRows;
	TInt iColumns;
	TInt iHrBorder;
	TInt iVtBorder;
	TInt iButHrBorder;
	TInt iButVtBorder;
	
	
	TSize iButCellExtent;
	TSize iCellSize;
	};

#endif /* GRID_H_ */
