/*
 * CreateData.h
 *
 *  Created on: 16-Mar-2010
 *      Author: Damien
 */

#ifndef CREATEDATA_H_
#define CREATEDATA_H_

#include <e32base.h>
#include <f32file.h>
#include <s32file.h> 

#include "qgstream.h"


#ifdef CREATEDATA
class CTextStream : public CBase
	{
public:
	void ListCitiesL();
	CTextStream();
	virtual ~CTextStream();
	void OpenStreamL(const TDesC& aFileName);

	TInt ReadIntL();
	TReal ReadFloatL();
	const TDesC& ReadStringL();
	
private:
	void ReadLineL();
	RFs iSession;
	RFile iFile;
	TBuf<256> iLine;
	TFileText iFt;
	};



#endif

class CInStream : public CBase
	{
public:
	CInStream();
	virtual ~CInStream();
	TInt OpenStreamL(const TDesC& aFileName, TBool aWriteable=EFalse);
	TInt ReadIntL();
	TReal ReadFloatL();
	const TDesC& ReadStringL();

private:
	RFs iSession;
	RFileReadStream iStream;
	TBuf<256> iBuf;
	};

class COutStream : public CBase
	{
public:
	COutStream();
	virtual ~COutStream();
	TInt OpenStreamL(const TDesC& aFileName);
	void WriteIntL(TInt aInt);
	void WriteFloatL(TReal aReal);
	void WriteStringL(const TDesC& aString);
	
private:
	RFs iSession;
	RFileWriteStream iStream;
	TFileName iFn;
	};


#endif /* CREATEDATA_H_ */
