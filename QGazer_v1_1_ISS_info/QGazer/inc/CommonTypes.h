#ifndef COMMONTYPES_H
#define COMMONTYPES_H

//#define SATELLITE
#define MESSIER

//#define QT_NO_DEBUG_OUTPUT
#define GAZER_VERSION_1.01
#include <e32base.h>

#include "qgstream.h"
#include "Utils3D.h"

#include "qgstream.h"

//#define SUSHI(s) TDebug::Print(_L(s))
#define SUSHI(s)

typedef float GLfloat;
#define  DEG_IN_RADIAN     57.2957795130823

class INSTREAM;
class OUTSTREAM;

class SkyObject;

typedef enum {EGeneral, EChosen, EInfoBoxWindow, EPlanetLabel, EStarLabel} TLabelType;

#define ACCEL_THRESHOLD_FAST 1.5
#define TIMER_FREQUENCY_FAST 50000
#define ACCEL_THRESHOLD_SLOW 3.0
#define TIMER_FREQUENCY_SLOW 300000

class CFileLog : public CBase
	{
public:
	void StartL();
	void AddL(const TDesC& aText);
	~CFileLog();
	RFile iFile;
	RFs iSession;
	};

class TDebug
	{
public:
	static void FileLog(TBool aTf);
	static void CloseFileLog();
	static void Print(TDateTime aDateTime);
	static void NoteL(const TDesC& aNote);
	static void Print(const TRect& aRect);
	static void Print(const TRgb& aColour);
	static void Print(const TDesC& aDes);
	static void Print(const TPoint& aPoint);
	static void Print(TInt aNum);
	static void Print(TReal aNum);
	static void Print(TReal aNum1, TReal aNum2);
	static void Print(TReal aNum1, TReal aNum2, TReal aNum3);
	static void Count();
	static TInt BreakAt(TInt aStop);
	static CFileLog* iFileLog;
	};

class TBldStr
	{
public:
	static void Clear();
	static void Set(const TDesC& aString);
	static void Add(const TDesC& aString);
	static void AddNum(TInt aNumber);
	static void AddNum(TReal aNumber);
	static void AddNum2(TInt aNum);
	static void Add(TChar aChar);
	static void Set(TChar aChar);
	static void Space();
	static void Comma();
	static void Colon();
	static const TDesC& String();
	static const TDesC& Num(TInt aNum);
	static const TDesC& Num2(TInt aNum);
	static void AddDateTime(TDateTime aT, TBool aSecs = EFalse);
	static void AddDate(TDateTime aT);
	static void AddTime(TDateTime aT, TBool aSecs = EFalse);	
	
private:
	static TBuf<512> iString;
	static TBuf<32> iNumString;
	};

class TGeographic
{
public:
	//void Set(TInt aDeg, TInt aMin, TInt aSec, TBool aPositive);
	void Set(TReal aDeg, TReal aMin, TReal aSec);
	void SetHours(TReal aHours, TReal aMin, TReal aSec);
	void Set(TReal aDecDegrees);
	TReal32 DecDegrees() const {return iDecDegrees;}
	TInt Deg() const { return iDeg; }
	TInt Min() const { return iMin; }
	TInt Sec() const { return iSec; }
	bool Positive() const { return iPositive; }

	const TDesC& String() const;
	
private:
	void DegreesToDecimalDegrees();
	void DecimalDegreesToDegrees();

	TInt iDeg;
	TInt iMin;
	TInt iSec;
	bool iPositive;

	TReal32 iDecDegrees;
	static TBuf<16> iBuf;
};

typedef enum  {EGpsValid, EGpsNotValid, EGpsLastKnown} TGpsStatus;

class TLocation
{
public:
	TLocation();
	TBool IsSet();
	TLocation(TReal32 aLong, TReal32 aLat);
	void Set(TReal32 aLong, TReal32 aLat);
	void Set(const TGeographic& aLong, const TGeographic& aLat);
	void Set(const TDesC& aCountry, const TDesC& aCity);
	~TLocation();	//it's ok for T classes to have a d'tor from 9.1
	void Close();
	//! @returns	the longitude
	const TGeographic& Longitude() const { return iLongitude; }

	//! @returns	the latitude
	const TGeographic& Latitude() const { return iLatitude; }
	
	TLocation&  operator = (const TLocation& aOther);
	
	
	void InternalizeL(INSTREAM& aStream);
	void ExternalizeL(OUTSTREAM& aStream);

	const TDesC& String() const;
	
	const TDesC& City() const {return iCity;}
	const TDesC& Country() const {return iCountry;}
	
	//TReal32 TzOffset() const {return iTzOffset;}
private:


private:
	RBuf iCountry;
	RBuf iCity;
	TGeographic iLongitude;
	TGeographic iLatitude;
	TReal32 iTzOffset;
	TReal32 iDstOffset;
	
	static TBuf<32> iBuf;
};

class TEquatorial
{
public:
	bool operator==(const TEquatorial &other) const
	{
		return ((other.iRA == iRA)
				&& (other.iDec == iDec));
	}

	void InternalizeL(INSTREAM& aStream);
#ifdef CREATEDATA
	void ExternalizeL(OUTSTREAM& aStream);
#endif

	TReal iRA;
	TReal iDec;
};

//! Represents a horizontal coordinate pair
//! An object's horizontal position is dependent time and the observer's location
class THorizontal
{
public:
	//! The azimuth in decimal degrees
	TReal iAzimuth;

	//! The altutude in decimal degrees
	TReal iAltitude;
};


class TScreenCoords
{
public:
	TScreenCoords() : iOnScreen(EFalse) {}

	//!	@param aX The x coord
	//!	@param aY The y coord. 0 is at bottom of the screen
	//!	@param aZ The z coord. Range varies with projection mode.
	void Set(TInt aX, TInt aY, TInt aZ, TBool aOnScreen);

	//! @returns True if these coordinate represent a visible location on the screen
	bool OnScreen() const {return iOnScreen;}

	//!	@returns The x, y coordinates as a QPoint
	const TPoint& Point() const {return iPoint;}
	void Add(const TPoint& aOffset) {iPoint += aOffset;}
	
private:
	TPoint iPoint;
	TInt iZ;
	TBool iOnScreen;
};


class TColour	 
	{
public:
	TColour() {} 
	TColour(TInt aRed, TInt aGreen, TInt aBlue, TInt aAlpha=255);
	TColour(TUint32 aInt);
	void Set(TInt aRed, TInt aGreen, TInt aBlue, TInt aAlpha=255);
	void Set(TUint32 aInt);
	void SetClamped(GLfloat aRed, GLfloat aGreen, GLfloat aBlue, GLfloat aAlpha=1.0);
	void GlColour() const;
	void GLClearColour() const;
	void Copy4(GLfloat* aPtr) const;
	const TInt inline Red() const {return iRed;}
	const TInt inline Green() const {return iGreen;}
	const TInt inline Blue() const {return iBlue;}
	const TInt inline Lum() const {return iLum;}
	TUint32 Int32() const;
	
private:
	void Clamp();
	
	TInt iRed;
	TInt iGreen;
	TInt iBlue;
	TInt iAlpha;
	TInt iLum;
	
	GLfloat iRedC;
	GLfloat iGreenC;
	GLfloat iBlueC;
	GLfloat iAlphaC;
	GLfloat iLumC;
	};

class TColourTransform
{
public:
	//! Sets the two colours
	//! @param	aFrom	The colour to fade from
	//! @param	aTo		The colour to fade to
	void Set(const TColour& aFrom, const TColour& aTo);

	//!	Returns a colour between the from and to colours
	//! @param	aPercentage	The percentage of fade
	//!	@returns	A colour faded from the 'from' colour to the 'to' colour by aPercentage percent
	const TColour& Colour(GLfloat aPercentage);

private:
	TVector iFrom;
	TVector iTo;
	TVector iDelta;
	TColour iColour;

	float iLength;
};




#endif // COMMONTYPES_H
