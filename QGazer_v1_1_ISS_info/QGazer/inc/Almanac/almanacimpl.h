#ifndef ALMANACIMPL_H
#define ALMANACIMPL_H

#include "almanac.h"

class AlmanacImpl : public Almanac
{
public:
	AlmanacImpl();
	~AlmanacImpl() {};
	bool SetJulianDateTime(const date_time& aDate);
	double GetJulianDateTime(const date_time& aDate);
	double CurrentJulianDateTime() const;
	bool SetLongLat(double aLong, double aLat);
	bool SiderealTime(double& aSiderealTime);
	bool PlanetPosition(int aPlanetNumber, double& aRA, double& aDec);
	bool MoonPosition(double& aRA, double& aDec, double& aDistanc);
	bool NextMoonPhase(int aPhase, date_time& aDate);
	bool LastMoonPhase(int aPhase, date_time& aDate);
	bool ToHorizontal(double aRa, double aDec, double& aAzimuth, double& aAltitude);


private:
	void SetLocalSiderealTime();

private:
//earth coods along ecliptic at time iJulianDay
	double iEarthX;
	double iEarthY;
	double iEarthZ;

//right ascension and declination of the planets
	double iRA[10];
	double iDec[10];

	double iJulianDateTime;
	double iGreenwichSiderealTime;
	double iLocalSiderealTime;

	double iLongitude;
	double iLatitude;

	bool iDateTimeSet;
	bool iLongLatSet;
};

#endif // ALMANACIMPL_H
