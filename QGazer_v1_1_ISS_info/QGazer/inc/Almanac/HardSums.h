/*
 *      This file, which is part of the project "QGazer", is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef CHardSums_H
#define CHardSums_H

#include "CommonTypes.h"
#include <GLES/gl.h> // OpenGL ES header file

class Almanac;

class date_time;
class CSatellites;

class CHardSums : public CBase
{
public:
	static CHardSums* NewL();
	void ConstructL();
	
	~CHardSums();

	//! Calculates planet, moon etc positions given location and datetime
	//! @param aLocation
	//! @param aDateTime *in UT*
	void Reset(const TLocation& aLocation, const TDateTime aDateTime);

	//! Converts equatorial coords (RA, dec) to horizontal (azimuth, inc) at the current location
	//! @param e	equatorial coords
	//! @param h	horizontal coords
	void ToHorizontal(const TEquatorial e, THorizontal& h) const;

	//! Gets the position of a planet in equatorial coords
	//! @param aPlanet	the planet number (0 = the sun(!), 3 = earth etc)
	//! @param e		equatorial coords
	void PlanetPos(TInt aPlanet, TEquatorial& e) const;

	//! Gets the position of the moon in horizontal coords at the current location
	//! @param h	horizontal coords
	void MoonPos(TEquatorial& e) const;

	//! Gets a string describing the current phase of the moon
	//! @param aString	the string to be filled
	void MoonPhase(RBuf& aString) const;

	void NextMoonPhase(TInt aPhase, TDateTime& aDate);
	void LastMoonPhase(TInt aPhase, TDateTime& aDate);
	
	//! Returns the current sidereal time at Greenwich
	GLfloat GreenwichSiderealTime() const { return iGreenwichSiderealTime;}

	double ArcInRadians(const TLocation& from, const TLocation& to);
	double DistanceInMeters(const TLocation& from, const TLocation& to);
	TInt DistanceBetween(const TPoint& aPoint1, const TPoint& aPoint2);
	TBool SunriseSunset(TBool aSunrise, TDateTime& aDateTime);
	TBool SunriseSunset(TBool aSunrise, TDateTime& aDateTime, const TDateTime& aNow, TBool aPhoneTime = ETrue);
	
	GLfloat AngleDiff(GLfloat a2, GLfloat a1);
	
	void UtcToPhoneTime(TDateTime& aDt);
	
	CSatellites& SatMaths() {return *iSatMaths;}
	
	double GetJulianDate(const TDateTime& aDateTime);
	double CurrentJulianDateTime() const;
	
private:
	void SetJulianDate(const TDateTime& aDateTime);
	
	void ToDateTime(const date_time& aDt, TDateTime& aDateTime);

	
	CHardSums();
	//! The current sidereal time at Greenwich
	GLfloat iGreenwichSiderealTime;
	TDateTime iDateTime;
	Almanac* iAlmanac;
	CSatellites* iSatMaths;
};

#endif // CHardSums_H
