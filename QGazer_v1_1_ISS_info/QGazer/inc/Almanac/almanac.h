#ifndef ALMANAC_H
#define ALMANAC_H

#include "datetime.h"
#include "CommonTypes.h"

class Almanac
{
public:
	static Almanac* New();
	virtual ~Almanac(){}
	virtual bool SetJulianDateTime(const date_time& aDate) = 0;
	virtual double GetJulianDateTime(const date_time& aDate) = 0;	
	virtual double CurrentJulianDateTime() const  = 0;
	virtual bool SetLongLat(double aLong, double aLat) = 0;
	virtual bool SiderealTime(double& aSiderealTime) = 0;
	virtual bool PlanetPosition(int aPlanetNumber, double& aRA, double& aDec) = 0;
	virtual bool MoonPosition(double& aRA, double& aDec, double& aDistance) = 0;
	virtual bool NextMoonPhase(int aPhase, date_time& aDate) = 0;
	virtual bool LastMoonPhase(int aPhase, date_time& aDate) = 0;
	virtual bool ToHorizontal(double aRa, double aDec, double& aAzimuth, double& aAltitude) = 0;
};

#endif // ALMANAC_H
