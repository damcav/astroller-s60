#ifndef ASTROMATHS_H
#define ASTROMATHS_H

#include <time.h>
#include <math.h>
#include "datetime.h"



void oprntf(char* s);
void oprntf(char* s, float f);

#define  J2000             2451545.        /* Julian date at standard epoch */
#define  SEC_IN_DAY        86400.
#define  DEG_IN_RADIAN     57.2957795130823
#define  HRS_IN_RADIAN     3.819718634205
#define  PI_OVER_2         1.57079632679490  /* From Abramowitz & Stegun */
#define  TWOPI             6.28318530717959

double date_to_jd( const date_time& date);
void jd_to_date(double jd, date_time& date);
int get_sys_date(date_time *date);
double adj_time(double x);
double altit(double dec, double ha, double lat, double& az);
void sidereal_time (double jd, double& gst);

#endif // ASTROMATHS_H
