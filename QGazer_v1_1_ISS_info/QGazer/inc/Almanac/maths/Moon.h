#ifndef MOON_H
#define MOON_H

void lpmoon(double jd, double lat, double sid, double* ra, double *dec, double *dist);
void flmoon(int n, int nph, double& jdout);
void print_phase(double	jd, int* phase, double* x);

#endif // MOON_H
