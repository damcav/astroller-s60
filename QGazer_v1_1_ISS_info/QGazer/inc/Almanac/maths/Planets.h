#ifndef PLANETS_H
#define PLANETS_H

void planetaryOrbitalElements(double jd);
void planetxyz(int p, double jd, double* x, double* y, double* z);
void eclrot(double jd, double* x, double* y, double* z);
double atan_circ(double x, double y);
void earthview(double x, double y, double z, double ex, double ey, double ez, double* ra, double* dec);
void xyz_cel(double x, double y, double z, double* ra, double* dec);

#endif // PLANETS_H
