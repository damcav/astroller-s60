/*
 * datetime.h
 *
 *  Created on: 18-Mar-2010
 *      Author: Damien
 */

#ifndef DATETIME_H_
#define DATETIME_H_

struct date_time
   {
	short y;
	short mo;
	short d;
	short h;
	short mn;
	float s;
   };

#endif /* DATETIME_H_ */
