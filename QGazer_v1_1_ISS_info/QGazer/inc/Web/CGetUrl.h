#ifndef __CGETURL_H__
#define __CGETURL_H__

#include <e32std.h>

#include "WebClientEngine.h"

class MGetUrlObserver
	{
public:
		virtual void DownloadStatusL(WebClientStatus aStatus)=0;
	};

class CGetUrl : public CBase, public MWebClientObserver
	{
public:
	static CGetUrl* NewL(const TDesC8& aUrl, MGetUrlObserver& aObs);
	void SaveL(const TDesC& aName);
	~CGetUrl();

    virtual void ClientEventL( WebClientStatus aStatus );
    virtual void ClientHeaderReceived( const TDesC& /*aHeaderData*/ ){}
    virtual void ClientBodyReceived( const TDesC8& aBodyData );
    
private:
	void ConstructL(const TDesC8& aUrl);
	CGetUrl(MGetUrlObserver& aObs);
	
	CWebClientEngine* iEngine;	
	MGetUrlObserver& iObs;
	RBuf8 iUrl;
	RBuf8 iBuffer;
	};
#endif
