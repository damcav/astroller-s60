#ifndef __ARCTICLEHELPER_H__
#define __ARCTICLEHELPER_H__

#include <e32cmn.h> 
#include <f32file.h>

#include "Callback.h"
#include "model.h"

class MArticleHelperObserver
	{
public:
	virtual void BitmapReady(CFbsBitmap* aBitmap)=0;
	};

class MArticleBitmapObserver
	{
public:
	virtual void BitmapReady(TInt aErr, CFbsBitmap* aBitmap) = 0;
	};

class CImageDecoder;
class CFbsBitmap;
class CWindowGc;

class CArticleBitmap : public CActive
	{
public:
	static CArticleBitmap* NewL(MArticleBitmapObserver& aObserver, const TDesC& aFilename);
	static CArticleBitmap* NewL(MArticleBitmapObserver& aObserver, const TDesC& aFilename, const TSize& aNewSize);
	~CArticleBitmap();
	void RunL();
	CFbsBitmap* Bitmap() const {return iBitmap;}

private:
	CArticleBitmap(MArticleBitmapObserver& aObserver);
	CArticleBitmap(MArticleBitmapObserver& aObserver, const TSize& aNewSize);
	void ConstructL(const TDesC& aFilename);
    void DoCancel();
    CFbsBitmap* iBitmap;
    CImageDecoder* iDecoder;
    RFs iFs;
    TBool iReady;
    MArticleBitmapObserver& iObserver;
    TBuf<256> iFullPath;
    const TSize& iNewSize;
    TBool iThumbnail;
	};

class CArticleHelper : public CBase, public MArticleBitmapObserver, public MCallback
	{
public:	
	static CArticleHelper* NewL(MArticleHelperObserver& aObserver);
	~CArticleHelper();
	static const TDesC& ArticleFileName(const TDesC& aName, TBool aText);
	void AllArticleObjects(RPointerArray<TSkyObject>& aObjects);
	void AllThumbnails(RPointerArray<TSkyObject>& aObjects, const TSize& aSize);
	TInt Bitmap(const TSkyObject* aObject);
	TInt Thumbnail(const TSkyObject* aObject, const TSize& aSize);
	void LoadArticleL(const TSkyObject* aObject, RBuf& aArticle);
	
private:
	void ConstructL();
	CArticleHelper(MArticleHelperObserver& aObserver);
	void NextThumb();
	
//From MArticleBitmapObserver
protected:
	virtual void BitmapReady(TInt aErr, CFbsBitmap* aBitmap);
	
//From MCallback
	virtual void Callback(TInt aId = 0);
	
	
private:
	static TBuf<64> iFn;
	MArticleHelperObserver& iObserver;
	TInt iCurrentThumb;
	CArticleBitmap* iArticleBitmap;
	RPointerArray<TSkyObject>* iAllThumbnailsObjects;
	TSize iThumbnailSize;
	TBool iGettingThumbs;
	CCallback* iCallback;
	};
#endif
