The Dumbell Nebula is a planetary nebula in the constellation Vulpecula, at a distance of about 1,360 light years.
This object was the first planetary nebula to be discovered; by Charles Messier in 1764. At its brightness of visual magnitude 7.5 and its diameter of about 8 arcminutes, it is easily visible in binoculars, and a popular observing target in amateur telescopes.
It appears to be shaped like an prolate spheroid and is viewed from our perspective along the plane of its equator.
