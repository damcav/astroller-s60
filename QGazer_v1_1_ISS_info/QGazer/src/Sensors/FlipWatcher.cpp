/*
 * FlipWatcher.cpp
 *
 *  Created on: 23-Apr-2010
 *      Author: Damien
 */
#include <E32BAse.h>
#include "FlipWatcher.h"

 CFlipWatcher* CFlipWatcher::NewL( CQSensors& aPropertyObserver, 
                                                 const TUid& aCategory, 
                                                 const TUint aKey )
     {
     CFlipWatcher* self = new(ELeave) CFlipWatcher( aPropertyObserver, 
                                                                  aCategory, 
                                                                  aKey );
     CleanupStack::PushL( self );
     self->ConstructL();
     CleanupStack::Pop( self );
     return self;
     }

 CFlipWatcher::~CFlipWatcher()
   {
   Cancel();
   iProperty.Close();
   }

 void CFlipWatcher::Subscribe()
     {
     iProperty.Subscribe( iStatus );
     SetActive();
     }

 TInt CFlipWatcher::Get( TInt& aValue )
 	{
 	return iProperty.Get( aValue );
 	}

 CFlipWatcher::CFlipWatcher( CQSensors& aPropertyObserver, 
                                           const TUid& aCategory, 
                                           const TUint aKey ) :
     CActive( EPriorityStandard ),
     iPropertyObserver( aPropertyObserver ),
     iCategory( aCategory),
     iKey( aKey )
     {
     }

 void CFlipWatcher::ConstructL()
     {
     CActiveScheduler::Add( this );
 	User::LeaveIfError( iProperty.Attach( iCategory, iKey ) );
     }

 void CFlipWatcher::RunL()
     {
     const TInt error ( iStatus.Int() );

     Subscribe();
     if ( error == KErrNone )
         {
    	iPropertyObserver.Flip( iCategory, iKey );
 		}
     }

 void CFlipWatcher::DoCancel()
     {
     iProperty.Cancel();
     }
