/*
 * CCompass.cpp
 *
 *  Created on: 25-Mar-2010
 *      Author: Damien
 */

#include <sensrvmagnetometersensor.h>	
#include <sensrvchannelfinder.h>
#include <sensrvchannel.h>
#include <sensrvaccelerometersensor.h>
#include <sensrvmagneticnorthsensor.h>

#include "CCompass.h"
#include "Commontypes.h"
#include "Context.h"

extern Context* gContext;

CCompass* CCompass::NewL()
	{
	CCompass* self = new (ELeave) CCompass();
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}

CCompass::CCompass()
	{
	}

TInt CCompass::Read(TInt& aAccuracy)
	{
	aAccuracy = iAccuracy;
	
	if (0)//gContext->FastOpenGL())
		{
		if (iAngles.Count())
			iAngles.Remove(&iAngle);
		}
	
	return iAngle;
	}

void CCompass::ConstructL()
	{
	if (0)//gContext->FastOpenGL())
		iAngles.SetLengthL(1000);
	
	OpenChannelL(iChannel, KSensrvChannelTypeIdMagneticNorthData);
	}

void CCompass::OpenChannelL( CSensrvChannel*& aChannel,
                                        TSensrvChannelTypeId aChannelTypeId )
    {
    CSensrvChannelFinder* finder = CSensrvChannelFinder::NewLC();
    RSensrvChannelInfoList channelList;
    CleanupClosePushL( channelList );

    TSensrvChannelInfo info;
    info.iChannelType = aChannelTypeId;
    finder->FindChannelsL( channelList, info );

    if ( channelList.Count() > 0 )
        {
        info = channelList[0];
        aChannel = CSensrvChannel::NewL( info );
        aChannel->OpenChannelL();
        }
    
    channelList.Close();
    CleanupStack::Pop( &channelList );
    CleanupStack::PopAndDestroy( finder );
    }

void CCompass::GetDataListenerInterfaceL( 
TUid /*aInterfaceUid*/, TAny*& aInterface )
	{
	aInterface = NULL;
	}

void CCompass::DataError( CSensrvChannel& /*aChannel*/,
                                            TSensrvErrorSeverity aError )
    {
    TBldStr::Set(_L("Magnetometer error: "));
    TBldStr::AddNum((TInt)aError);
    TDebug::Print(TBldStr::String());
    }

void CCompass::DataReceived( CSensrvChannel& aChannel,
                                               TInt aCount,
                                               TInt /*aDataLost*/ )
    {
    TSensrvChannelInfo info = aChannel.GetChannelInfo();

    if( info.iChannelType == KSensrvChannelTypeIdMagneticNorthData )
        {
        TSensrvMagneticNorthData magData;
        TPckg<TSensrvMagneticNorthData> magPackage( magData );
    
        if (0)//gContext->FastOpenGL())
        	{
        	for (TInt i=0; i<aCount; i++)
        		{
        		aChannel.GetData( magPackage );
        		TInt angle(magPackage().iAngleFromMagneticNorth);
        		iAngles.Add(&angle);
        		}
        	}
		else
			{
			for (TInt i=0; i<aCount; i++)
				{
				aChannel.GetData( magPackage );
				iAngle = magPackage().iAngleFromMagneticNorth;
				}
			}
        
        TSensrvProperty calibration;
        aChannel.GetPropertyL(KSensrvPropCalibrationLevel, 0, calibration);
        calibration.GetValue(iAccuracy);
        }
    }

CCompass::~CCompass()
    {
    if( iChannel )
        {
        Stop();
        iChannel->CloseChannel();
        delete iChannel;
        iChannel = NULL;
        }
    }

void CCompass::Stop()
	{
	if (iChannel)
		iChannel->StopDataListening();
	}

void CCompass::StartL()
	{
	if (!iChannel)
		User::Leave(KErrNotFound);
	iChannel->StartDataListeningL( this, 5, 5, 0 );//5,5
	}
