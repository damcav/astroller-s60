/*
 * CQSensors.cpp
 *
 *  Created on: 25-Mar-2010
 *      Author: Damien
 */

//#define SHOWWILLY




#include "CQSensors.h"
#include "Accel.h"
#include "CCompass.h"
#include "Context.h"
#include "CommonTypes.h"
#include "FlipWatcher.h"


extern Context* gContext;

CQSensors::CQSensors(MSensorsCb& aObserver) : iObserver(aObserver)
	{
	iCurrentAngle = 60;
	iCurrentBearing = iLastBearing = 180;	//south
	
	
#if defined(__WINS__) // *sigh*
	iTest_GpsInvalid = ETrue;
#endif
	iTimeFactor = 1;
	iUsingSensors = EFalse;
	}

void CQSensors::Flip(TUid /*aCat*/, TInt aValue)
	{
	TInt value;
	iFlipWatcher->Get(value);
	gContext->SetKeyboardOpen(value == EPSHWRMFlipOpen);
//	if (!gContext->InSpace() && !gContext->UserLocationMode())
//		gAppUiCb->RefreshL();
	}

CQSensors::~CQSensors()
	{
	delete iAccel; iAccel = NULL;
	delete iCompass; iCompass = NULL;
	delete iGps; iGps = NULL;
	delete iTimer; iTimer = NULL;
	delete iFlipWatcher; iFlipWatcher = NULL;
	
	delete iSmoothAccel; iSmoothAccel = NULL;
	delete iSmoothCompass; iSmoothCompass = NULL;
	}

CQSensors* CQSensors::NewL(MSensorsCb& aObserver)
	{
	CQSensors* self = new (ELeave) CQSensors(aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}

void CQSensors::ConstructL()
	{
	iLastTime.UniversalTime();
	
	iSmoothAccel = CSmoothMove::NewL();
	iSmoothCompass = CSmoothCompass::NewL();
	iSmoothCompass->SetMaxLag(3.0);
	iSmoothAccel->SetMaxLag(3.0);
	
	
	
SUSHI("CQSensors::ConstructL() 1");

	TInt err = KErrNone;
	TRAP(err, iAccel = CAccel::NewL());

SUSHI("CQSensors::ConstructL() 2");
	
    TRAP(err, iCompass = CCompass::NewL());

SUSHI("CQSensors::ConstructL() 3");
    
    TRAP(err, iFlipWatcher = CFlipWatcher::NewL( *this, KPSUidHWRM, KHWRMFlipStatus ));
    if (iFlipWatcher)
    	{
    	//iFlipWatcher->Subscribe();
    	ReadFlip();
    	}
    else
    	{
    	gContext->SetKeyboardOpen(EFalse);
    	}

SUSHI("CQSensors::ConstructL() 4");
    MonitorGps(10000000, ETrue);
	}

void CQSensors::ReadFlip()
	{
	if (iFlipWatcher)
		Flip(TUid::Uid(0), 0);
	}

void CQSensors::SetGpsInvalid(TBool aInvalid)
	{
#if defined(__WINS__) // *sigh*	
	iTest_GpsInvalid = aInvalid;
	if (!aInvalid)
		iObserver.GpsDataL(EGpsLastKnown, 0, 0);
#endif
	}

void CQSensors::DoMonitorGpsL()
	{
	iStartMonitorGps = EFalse;
	
	if (iGps)
		{
		delete iGps;
		iGps = NULL;
		}
	
    TRAPD(err, iGps = CLocation::NewL(iInterval, *this, iGetLastKnown));
    if (iGps)
    	iGps->Start();
	}
	
void CQSensors::MonitorGps(const TTimeIntervalMicroSeconds& aInterval, TBool aGetLastKnown)
	{
	iInterval = aInterval;
	iGetLastKnown = aGetLastKnown;
	iStartMonitorGps = ETrue;
	}

GLfloat CQSensors::AngleDiff(GLfloat a2, GLfloat a1)
	{
	GLfloat diff = a2 - a1;
	while (diff < -180) diff  += 360;
	while (diff > 180) diff -= 360;
	return Abs(diff);
	}

TReal CQSensors::CompassThreshold()
	{
	//threshold range 1 to 20
	//depending on iCurrentAngle (60 = horizon, 0 = overhead)
	TReal a = 60.0 - float(iCurrentAngle);
	TReal t1 = 1.0;
	TReal t2 = gContext->FastOpenGL() ? 60.0 : 20.0;//20
	//TReal t2 = 20.0;
	//scale a = (0..60) to (t1..t2)
	TReal dt = t2 - t1;
	TReal threshold = a * (dt / 60.0);
	threshold += t1;
	return threshold;
	}

	
void CQSensors::DoCallbackL()
	{
	TBool updateTime = UpdateTime();
	iAccelChanged = EFalse;
	iCompassChanged = EFalse;

//TODO only need to do these once	
	
	iSmoothAccel->SetFast(gContext->FastOpenGL());
	iSmoothCompass->SetFast(gContext->FastOpenGL());

	if (iStartMonitorGps)
		DoMonitorGpsL();
	
	if (!iUsingSensors)
		{
		iObserver.Updated(updateTime, iAccelChanged, iCompassChanged);
		return;
		}

	iAccel->Read(iCurrentAccel);

	TInt currentBearing = iCompass->Read(iCurrentAccuracy); 
	 
	gContext->SetCompassHealth(iCurrentAccuracy);

	if (gContext->InSpace() || iCurrentAccel.iZ >= 0 || iCurrentAccel.iX < 0 || iCurrentAccel.iX >= 60.0)	//looking down
		{//we are in space looking down
		TBool updateAcc = gContext->SensorMode();
		gContext->SetSensorMode(EFalse);
		
		if (updateAcc)	//sensors were on, now turned off. So stop the "Shake phone" message
			iObserver.AccDataL(KLookingDown);
		}
	else
		{
		iCurrentBearing = currentBearing;
		
		if (gContext->SensorMode())
			{
			iSmoothAccel->SetThreshold(gContext->FastOpenGL() ? ACCEL_THRESHOLD_FAST : ACCEL_THRESHOLD_SLOW);
			iAccelChanged = iSmoothAccel->Move(iCurrentAccel.iX);
			
			if (iCurrentAccuracy)
				{
				iSmoothCompass->SetThreshold(CompassThreshold());
				iCompassChanged = iSmoothCompass->Move(iCurrentBearing);
				}
			}
		else	//switched to sensor mode, so set current accel value
			{
			iAccelChanged = ETrue;
			iSmoothAccel->SetCurrent(iCurrentAccel.iX);
//			if (iCurrentAccuracy)
//				{
//				iCompassChanged = ETrue;
//				iSmoothCompass->SetCurrent(iCurrentBearing);
//				}
			}

		gContext->SetSensorMode(ETrue);
		
		if (iAccelChanged)
			{
			//scale angle from 60->0 to 15->90
			iSmoothAccel->CheckStop();
			TReal angle = 60.0 - iSmoothAccel->Current();
			angle *= 1.2; //(90.0 / 75.0);
			angle += 15.0;

			iObserver.AccDataL(angle);
			}
		
		if (iCompassChanged)
			{
			iSmoothCompass->CheckStop();
			iObserver.CompassData(iSmoothCompass->Current());
			}
		}

	iObserver.Updated(updateTime, iAccelChanged, iCompassChanged);
	}

TInt CQSensors::Callback(TAny* aPtr )
	{
	CQSensors* me=(CQSensors*)aPtr;
	me->DoCallbackL();
	return 0;
	}

void CQSensors::Stop()
	{
	if (iUsingSensors)
		{
		iAccel->Stop();
		iCompass->Stop();
		}
    
    delete iTimer;
    iTimer = NULL;
	}

void CQSensors::StartL()
	{
	if (!iTimer)
		{
		iTimer = CPeriodic::NewL(EPriorityHigh);

		//TTimeIntervalMicroSeconds32 one(300000);//TODO DMC
		//TInt timerFrequency = gContext->FastOpenGL() ? TIMER_FREQUENCY_FAST : TIMER_FREQUENCY_SLOW;
		TInt timerFrequency = TIMER_FREQUENCY_FAST;
		TTimeIntervalMicroSeconds32 one(timerFrequency); 
		iSamplesPerSecond = 1000000 / timerFrequency;
		iTimer->Start(one, one, TCallBack(Callback, this));

#if defined(__WINS__) // *sigh*
	return;
#endif
	
		TInt err(KErrNone);
		if (!iAccel)
			{
			err = KErrNotFound;	//there was some problem creating the accel
			}
		else
			{
			TRAP(err, iAccel->StartL());
			}

		if (err == KErrNone)
			{
			if (!iCompass)
				{	
				err = KErrNotFound;	//there was some problem creating the compass
				}
			else
				{
				TRAP(err, iCompass->StartL());
				}
			
			if (err == KErrNone)
				iUsingSensors = ETrue;
			}
		}
	}

void CQSensors::PositionUpdatedL(TPositionInfo& aPosInfo, TBool aGettingLastknownPosition)
	{
	TBool invalid = iTest_GpsInvalid;
	
	if (aPosInfo.UpdateType() != EPositionUpdateUnknown)
		{
		TPosition pos;
		aPosInfo.GetPosition(pos);
		TReal64 lat = pos.Latitude();
		TReal64 lon = pos.Longitude();
		TGpsStatus status = EGpsNotValid;
		
		if (!invalid && !Math::IsNaN(lat) && !Math::IsNaN(lon))
			{
			status = aGettingLastknownPosition ? EGpsLastKnown : EGpsValid;
			}
		else
			status = EGpsNotValid;

		iObserver.GpsDataL(status, lon, lat);
		}
	}

void CQSensors::ErrorL(TInt aError)
	{
	TBuf<64> n;
	n.Append(_L("GPS ERROR: "));
	n.AppendNum(aError);
	TDebug::NoteL(n);
	iObserver.GpsDataL(EGpsNotValid, 0, 0);
	}

TBool CQSensors::UpdateTime()
	{
	iSystemTime.UniversalTime();
	TInt64 dT = iSystemTime.MicroSecondsFrom(iLastTime).Int64();
	dT *= iTimeFactor;
#ifdef SATELLITE
	TInt64 limit = 10000000;
	if (gContext->SatelliteVisible())
		limit /= 4;
	if (dT > limit)
#else
	if (dT > 60000000)
#endif
		{
		gContext->AddTime(dT);
		iLastTime.UniversalTime();
		return ETrue;
		}
	return EFalse;
	}





CSmoothMove* CSmoothMove::NewL()
	{
	CSmoothMove* self = new /*(ELeave)*/ CSmoothMove();
	return self;
	}

CSmoothMove::CSmoothMove()
	{
	
	iFast = ETrue;
	iScrolling = EFalse;
	}

CSmoothMove::~CSmoothMove()
	{
	}

TBool CSmoothMove::Move(TReal aTarget)
	{
	return iFast ? MoveFast(aTarget) : MoveSlow(aTarget);
	}



TBool CSmoothMove::MoveSlow(TReal aTarget)
	{
	if (Abs(Distance(aTarget, iCurrent)) > iThreshold)
		{
		iCurrent = aTarget;
		return ETrue;
		}
	return EFalse;
	}

TReal CSmoothMove::Distance(TReal a1, TReal a2)
	{
	return a1 - a2;
	}

void CSmoothMove::Add()
	{
	iCurrent += iDelta;
	}


TBool CSmoothMove::MoveFast(TReal aTarget)
	{
	TBool change = EFalse;
	if (!iScrolling)
	{
		iCurrentLag = iMaxLag;
		change =  Abs(Distance(aTarget, iPreviousTarget)) > iThreshold;
	}
	else
	{
		change = aTarget != iPreviousTarget;
	}


	if (change)
		{
		iTarget = aTarget;
		iPreviousTarget = aTarget;

		TReal distance = Distance(iTarget, iCurrent);
		if (Abs(distance) > 0.0)
			{
			iScrolling = ETrue;
			if (Abs(distance) > iCurrentLag)
			{
				//----------- safety net
				if (iCurrentLag<1)
					{
					TDebug::Print(iCurrentLag);
					iCurrent = iTarget;
					return ETrue;
					}
				//-----------------------
				
				iDelta = distance / iCurrentLag;
			}
			else
				{
				if (distance < 0.0)
					iDelta = -1.0;
				else
					iDelta = 1.0;
				}
			}
		}

	if (iScrolling)
		{
		Add();
		iCurrentLag--;
		return ETrue;
		}

	iScrolling = EFalse;
	return EFalse;
	}

void CSmoothMove::CheckStop()
	{
	if (Abs(Distance(iCurrent, iTarget)) < 1.0)
		{
		iScrolling = EFalse;
		iCurrent = iTarget;
		}
	}

CSmoothCompass* CSmoothCompass::NewL()
	{
	CSmoothCompass* self = new (ELeave) CSmoothCompass();
	return self;
	}

TReal CSmoothCompass::Distance(TReal a1, TReal a2)
	{
	float diff = a1 - a2;
	while (diff < -180) diff  += 360;
	while (diff > 180) diff -= 360;

	return diff;
	}

void CSmoothCompass::Add()
	{
	iCurrent += iDelta;

	if (iCurrent >= 360.0)
		iCurrent -= 360.0;
	else if (iCurrent < 0.0)
		iCurrent += 360.0;
	}
