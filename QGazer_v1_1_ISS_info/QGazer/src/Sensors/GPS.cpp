/*
 * GPS.cpp
 *
 *  Created on: 12-Apr-2010
 *      Author: Damien
 */

#include <Lbs.h>
#include <eikenv.h>

#include "gps.h"
#include "gpscb.h"

// CONSTANTS
const TInt KSecond = 1000000;
const TInt KMaxAge = KSecond;
const TInt KErrBuffer = 100;
// GPS sleeping time after error
const TInt KGPSSleepingMinor = KSecond*10;

//The name of the requestor
_LIT(KRequestor,"MyLocationApp");

CLocation::CLocation(MPositionObserver& aPositionListener, TBool aLastKnown )
	: CActive(CActive::EPriorityStandard),
	iPositionListener( aPositionListener ),
	iGettingLastknownPosition( aLastKnown )
{
}

void CLocation::ConstructL(const TTimeIntervalMicroSeconds& aInterval)
{
	// Set update interval to one second to receive one position data
	// per second
	iUpdateops.SetUpdateInterval(aInterval);

	// If position server could not get position
	// In two minutes it will terminate the position request
	iUpdateops.SetUpdateTimeOut(TTimeIntervalMicroSeconds(iUpdateTimeout));

	// Positions which have time stamp below KMaxAge can be reused
	iUpdateops.SetMaxUpdateAge(TTimeIntervalMicroSeconds(KMaxAge));

	// Enables location framework to send partial position data
	iUpdateops.SetAcceptPartialUpdates(EFalse);

	// Add this position requestor to the active scheduler
	CActiveScheduler::Add( this );

	// Initialise the position request sequence
	DoInitialiseL();
}

CLocation* CLocation::NewL(const TTimeIntervalMicroSeconds& aInterval, MPositionObserver& aPositionListener, TBool aLastKnown )
{
	//Create the object
	CLocation* self = new( ELeave ) CLocation(aPositionListener, aLastKnown);

	//Push to the cleanup stack
	CleanupStack::PushL( self );

	//Construct the object
	self->ConstructL(aInterval);

	//Remove from cleanup stack
	CleanupStack::Pop( self );

	//Return pointer to the created object
	return self;
}

CLocation::~CLocation()
{
	// Cancel active object (will call DoCancel() if the active object is active)
	Cancel();

	// The timer may be running, cancel it
	CancelWait();

	// Close the positioner
	iPositioner.Close();

	// Close the session to the position server
	iPosServer.Close();
}

void CLocation::DoCancel()
{


	//If we are getting the last known position
	if ( iGettingLastknownPosition )
	{
		//Cancel the last known position request
		iPositioner.CancelRequest(EPositionerGetLastKnownPosition);
	}
	else
	{
		iPositioner.CancelRequest(EPositionerNotifyPositionUpdate);
	}

	
}

void CLocation::RunL()
{
	
	
	TBuf<KPositionMaxModuleName> buffer;
	switch ( iStatus.Int() )
	{
		// The fix is valid
	case KErrNone:
		{
			// Pre process the position information
			PositionUpdatedL();
			break;
		}
		// The fix has only partially valid information.
		// It is guaranteed to only have a valid timestamp
	case KPositionPartialUpdate:
		{
			// Send partial data to registered listener
			//iPositionListener.PositionUpdatedL(iPositionInfo);
			// and wait a while after askin position again
			Wait();
			break;
		}
		// The position data could not be delivered
	case KPositionQualityLoss:
		{
			PositionLost();
			break;
		}
		// Access is denied
	case KErrAccessDenied:
		{
			// Send error to position listener
			iPositionListener.ErrorL(KErrAccessDenied);
			break;
		}
		// Request timed out
	case KErrTimedOut:
		{
			PositionLost();
			break;
		}
		// The request was canceled
	case KErrCancel:
		{
			break;
		}
		// There is no last known position
	case KErrUnknown:
		{
			PositionLost();
			break;
		}
		// Unrecoverable errors.
	default:
		{
			// Send error to position listener
			iPositionListener.ErrorL(iStatus.Int());
			break;
		}
	}
}


void CLocation::DoInitialiseL()
{
	// Connect to the position server
	TInt error = iPosServer.Connect( );
	TBuf<KErrBuffer> buffer;

	// The connection failed
	if ( KErrNone != error )
	{
		iPositionListener.ErrorL( error );
		return;
	}

	// Open subsession to the position server
	error = iPositioner.Open(iPosServer);

	// The opening of a subsession failed
	if ( KErrNone != error )
	{
		iPositionListener.ErrorL( error );
		iPosServer.Close();
		return;
	}

	// Set position requestor
	error = iPositioner.SetRequestor( CRequestor::ERequestorService,
	CRequestor::EFormatApplication , KRequestor );

	// The requestor could not be set
	if ( KErrNone != error )
	{
		iPositionListener.ErrorL( error );
		iPositioner.Close();
		iPosServer.Close();
		return;
	}

	// Set update options
	error = iPositioner.SetUpdateOptions( iUpdateops );

	// The options could not be updated
	if ( KErrNone != error )
	{
		iPositionListener.ErrorL( error );
		iPositioner.Close();
		iPosServer.Close();
		return;
	}

	// Get last known position. The processing of the result
	// is done in RunL method
//	Start();
}

TInt CLocation::RunError(TInt /*aError*/)
{
	return KErrNone;
}

void CLocation::Pause()
{
	if (IsActive())
	{
		Cancel();
	}
}

void CLocation::Continue()
{
	if (!IsActive())
	{
		Start();
	}
}

TPositionInfoBase& CLocation::CurrentPosition()
{
	return iPositionInfo; 
}

void CLocation::PositionUpdatedL()
{
	TPositionUpdateType update = iPositionInfo.UpdateType();
	
	// Send GPS position
	if (update!=EPositionUpdateUnknown)
	{
		// Send position information to registered listener
		iPositionListener.PositionUpdatedL(iPositionInfo, iGettingLastknownPosition);
		iGettingLastknownPosition = EFalse;
		Start();
	}
	else
	{
		Wait();
	}
}

void CLocation::Wait()
{
	if (!iPeriodic)
	{
		// Close GPS handles
		Cancel();

		// Sleep
		iPeriodic = CPeriodic::NewL(CActive::EPriorityIdle);
		iPeriodic->Start(KGPSSleepingMinor,KGPSSleepingMinor,
		TCallBack(PeriodicTick, this));
	}
}

TInt CLocation::PeriodicTick(TAny* aObject)
{
	CLocation* gpslistener = (CLocation*)aObject;
	if (gpslistener)
	{
		// Cancel timer running
		gpslistener->CancelWait();

		// Start listening GPS again after waiting a while
		gpslistener->Start();
	}

	// Does not continue again
	return EFalse; 
}

void CLocation::Start()
{
	if (!IsActive())
	{
		// Get last known position. The processing of the result
		// is done in RunL method
		if (iGettingLastknownPosition)
		{
			iPositioner.GetLastKnownPosition(iPositionInfo,iStatus);
		}
		else
		{
			iPositioner.NotifyPositionUpdate(iPositionInfo,iStatus);
		}

		// Set this active object active
		SetActive();
	}
}

void CLocation::CancelWait()
{
	if (iPeriodic)
	{
		iPeriodic->Cancel();
		delete iPeriodic;
		iPeriodic = NULL;
	}
}

void CLocation::PositionLost()
{
	// Wait and then request again
	Wait();
}
