/*
 * Accel.cpp
 *
 *  Created on: 28-Oct-2009
 *      Author: dacavana
 */

#include <sensrvchannelfinder.h>
#include <sensrvchannel.h>
#include <sensrvaccelerometersensor.h>

#include <e32math.h> 
#include <AknGlobalNote.h>

#include "Accel.h"
#include "utils3d.h"

#include "CCompass.h"

static TVector lastAcc(0,0,0);

CAccel* CAccel::NewL()
	{
	CAccel* self = new (ELeave) CAccel();
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}

CAccel::CAccel() 
	{
	}

void CAccel::ConstructL()
	{
	OpenChannelL(iChannel, KSensrvChannelTypeIdAccelerometerXYZAxisData);
	}

void CAccel::Read(TVector& aAccel)
	{
#if defined(__WINS__) // *sigh*
	aAccel.Set(0,0,0);
	return;
#endif
	aAccel = lastAcc;
	}

void CAccel::OpenChannelL( CSensrvChannel*& aChannel,
                                        TSensrvChannelTypeId aChannelTypeId )
    {
    CSensrvChannelFinder* finder = CSensrvChannelFinder::NewLC();
    RSensrvChannelInfoList channelList;
    CleanupClosePushL( channelList );

    TSensrvChannelInfo info;
    info.iChannelType = aChannelTypeId;
    finder->FindChannelsL( channelList, info );

    if ( channelList.Count() > 0 )
        {
        info = channelList[0];

        aChannel = CSensrvChannel::NewL( info );
        aChannel->OpenChannelL();
        }

    channelList.Close();
    CleanupStack::Pop( &channelList );
    CleanupStack::PopAndDestroy( finder );
    }

void CAccel::GetDataListenerInterfaceL( 
TUid /*aInterfaceUid*/, TAny*& aInterface )
	{
	aInterface = NULL;
	}

void CAccel::DataError( CSensrvChannel& /*aChannel*/,
                                            TSensrvErrorSeverity /*aError*/ )
    {
    }

void CAccel::DataReceived( CSensrvChannel& aChannel,
                                               TInt aCount,
                                               TInt /*aDataLost*/ )
    {
    TSensrvChannelInfo info = aChannel.GetChannelInfo();
    if( info.iChannelType == KSensrvChannelTypeIdAccelerometerXYZAxisData )
        {
        TSensrvAccelerometerAxisData accData;
        TPckg<TSensrvAccelerometerAxisData> accPackage( accData );
        
        for (TInt i=0; i<aCount; i++)
        	{
        	aChannel.GetData( accPackage );
        	iX=accPackage().iAxisX;
        	iY=accPackage().iAxisY;
        	iZ=accPackage().iAxisZ;
            TVector acc(iX,iY,iZ);
            lastAcc=(lastAcc*3 + acc) / 4;
        	}
        }
    }

CAccel::~CAccel()
    {
    if( iChannel )
        {
        Stop();
        iChannel->CloseChannel();
        delete iChannel;
        iChannel = NULL;
        }
    }

void CAccel::Stop()
	{
	if (iChannel)
		iChannel->StopDataListening();
	}

void CAccel::StartL()
	{
	if (!iChannel)
		User::Leave(KErrNotFound);	
	iChannel->StartDataListeningL( this, 1, 1, 0 );
	}

