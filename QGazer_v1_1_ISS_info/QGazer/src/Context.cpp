/*
 *      This file, which is part of the project "QGazer", is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include <t32wld.h>
#include "context.h"
#include "CCamera.h"
#include "CreateData.h"
#include "Objects.h"

extern CHardSums* gHardSums;

void Context::SetChosenObject(const TSkyObject* aObject)
	{
	iChosenObject.iObject = aObject;
	iChosenObject.iEq = iChosenObject.iObject->iEq;
	aObject->Horizontal(iChosenObject.iHr);	
	}

Context* Context::NewL()
	{
	Context* self = new (ELeave) Context();
	CleanupStack::PushL(self);
    self->ConstructL();
	CleanupStack::Pop(self);    
    return self;
	}

void Context::ConstructL()
	{
	LoadUserDataL();
	}

Context::Context()
	:iCurrentLocation(0,0)//iCurrentLocation(139.7, 35.4)
	{
	iStarFactor = 1.0;
	iInSpace = ETrue;
	iDrawDot = ETrue;
	
	iCurrentTime.UniversalTime();
	iUser.iTime = iCurrentTime;
	TTime phoneTime;
	phoneTime.HomeTime();
	phoneTime.MinutesFrom(iCurrentTime, iPhoneTzMins);
	

	SetShowAtmosphere(ETrue);
	SetShowConstellations(ETrue);
	SetShowLabels(ETrue);

	iGpsStatus = EGpsNotValid;
	
	TGeographic surbitonLong;
	surbitonLong.Set(-0.3013);
	TGeographic surbitonLat;
	surbitonLat.Set(51.38786);
	iCurrentLocation.Set(surbitonLong, surbitonLat);
	iCurrentLocation.Set(_L("England"), _L("Surbiton"));

//	TGeographic londonLong;
//	londonLong.Set(-0.3013);
//	TGeographic londonLat;
//	londonLat.Set(51.38786);
//	iCurrentLocation.Set(londonLong, londonLat);
//	iCurrentLocation.Set(_L("United Kingdom"), _L("London"));

	iUser.iLocation = iCurrentLocation;

	iUser.iLights = EFalse;
	iUser.iWaitingForGps = ETrue;
	
	SetVisibleStars(100.0);
	}

const TLocation& Context::UserLocation(TInt& aNearestDistance) const	
	{
	aNearestDistance = iUser.iNearestDistance;
	return iUser.iLocation; 
	}

const TLocation& Context::CurrentLocation(TInt& aNearestDistance) const
	{
	aNearestDistance = iNearestDistance;
	return iCurrentLocation;
	}
	
const TLocation& Context::Location(TInt& aNearestDistance) const
	{
	return UserLocationMode() ? UserLocation(aNearestDistance) : CurrentLocation(aNearestDistance);
	}

const TLocation& Context::Location() const
	{
	return UserLocationMode() ? UserLocation() : CurrentLocation();
	}

void Context::SetCurrentLocation(const TLocation& aLocation, TInt aNearestDistance)
	{
	iCurrentLocation = aLocation;
	iNearestDistance = aNearestDistance;
	}

void Context::SetUserLocation(const TLocation& aLocation, TInt aNearestDistance)
	{
	iUser.iLocation = aLocation;
	iUser.iNearestDistance = aNearestDistance;
	}

void Context::SetUserLocationToHome()
	{
	iUser.iLocation = iCurrentLocation;
	iUser.iNearestDistance = iNearestDistance;
	}

void Context::SetUserTimeToNow()
	{
	iUser.iTime.UniversalTime();
	}

void Context::SetUserTime(const TDateTime& aTime, TInt aTimeZone)
	{
	//iUser.iTimeZone = aTimeZone;
	iUser.iTime = TTime(aTime);
	AdjustTimeToUtc(iUser.iTime, aTimeZone);
	}

TTime Context::UserTimeUtc()
	{
	TDateTime userDt;
	TInt userTz;
	UserTime(userDt, userTz);
	TTime t(userDt);
	AdjustTimeToUtc(t, userTz);
	return t;
	}
/*
void Context::UserTimeUtc(TDateTime& aTime)
	{
	TTime t(iUser.iTime);
	if (t.Int64() == 0)
		{
		t.UniversalTime();
		iUser.iTimeZone = EPhoneTz;
		}
	aTime = t.DateTime();
	}
*/
void Context::UserTime(TDateTime& aTime, TInt& aTimeZone)
	{
	TTime t(iUser.iTime);
	if (t.Int64() == 0)
		{
		t.UniversalTime();
		iUser.iTimeZone = EPhoneTz;
		}
	AdjustTimeFromUtc(t, iUser.iTimeZone);
	aTime = t.DateTime();
	aTimeZone = iUser.iTimeZone;
	}

void Context::AdjustTimeToUtc(TTime& aTime, TInt aTimeZone)	//to UTC
	{
	if (aTimeZone == ENewLocationTz)
		{
		TInt hour = TInt(UserLocation().Longitude().DecDegrees()) / 15;
		aTime -= TTimeIntervalMinutes(hour * 60);
		}
	else if (aTimeZone == EPhoneTz)
		{
		aTime -= iPhoneTzMins;
		}
	}

void Context::AdjustTimeFromUtc(TTime& aTime, TInt aTimeZone)
	{
	if (aTimeZone == ENewLocationTz)
		{
		TInt hour = TInt(UserLocation().Longitude().DecDegrees()) / 15;
		aTime += TTimeIntervalMinutes(hour * 60);
		}
	else if (aTimeZone == EPhoneTz)
		{
		aTime += iPhoneTzMins;
		}
	}

const TDateTime Context::Time()
	{
	return UserTimeMode() ? UserDateTime() : HomeDateTime();
	}

const TDesC& Context::TimeString()
	{
	return UserTimeMode() ? TimeStringUserLocation() : TimeStringHome();
	}

const TDesC& Context::TimeStringUserLocation()
	{
	return DoTimeString(iUser.iTime, iUser.iTimeZone);
	}

const TDesC& Context::TimeStringHome()
	{

	return DoTimeString(iCurrentTime,  iUser.iTimeZone);
	}

const TDesC& Context::DoTimeString(TTime aTime, TTimeZone aTz)
	{
	//_LIT(KDateTimeString, "%*E%*D%X%*N%*Y %1 %2 '%3 %H%:1%T%:2%S");
_LIT(KMonths,"JanFebMarAprMayJunJulAugSepOctNovDec");

	AdjustTimeFromUtc(aTime, aTz);

	TDateTime dt = aTime.DateTime();
	

	
	iTimeString.Zero();
	

	
	iTimeString.AppendNum(dt.Day()+1);
	iTimeString.Append(_L(" "));
	iTimeString.Append(KMonths().Mid(dt.Month()*3,3));
	iTimeString.Append(_L(" "));
	iTimeString.AppendNum(dt.Year());
	iTimeString.Append(_L(" "));
	
	if (iUser.iTimeZone == ENewLocationTz)
		iTimeString.Append(_L("~"));
	
	if (dt.Hour() < 10)
		iTimeString.AppendNum(0);
	iTimeString.AppendNum(dt.Hour());
	iTimeString.Append(_L(":"));
	if (dt.Minute() < 10)
			iTimeString.AppendNum(0);
	iTimeString.AppendNum(dt.Minute());
	

	if (aTz == EUniversalTz)
		iTimeString.Append(_L("(UT)"));
	
	return iTimeString;
	}

void Context::AddTime(const TInt64& aMs)
	{
	iCurrentTime += TTimeIntervalMicroSeconds(aMs);
	iUser.iTime += TTimeIntervalMicroSeconds(aMs);
	}

Context::~Context()
	{
	}

void Context::ToggleInSpace()
	{
	iInSpace = !iInSpace;
	if (iInSpace)
		iStarFactor = 1.0;
	}

void Context::ClearChosenObject()
	{ 
	iChosenObject.iObject = NULL;
	iChosenObject.iScreen.Set(-1, -1, -1, EFalse);
	}

_LIT(KUser,"user");

void Context::SaveUserDataL()
	{
#ifndef CREATEDATA	
	OUTSTREAM* stream = new (ELeave) OUTSTREAM();
	CleanupStack::PushL(stream);
	if (stream->OpenStreamL(KUser) == KErrNone)
		{
		iCurrentLocation.ExternalizeL(*stream);	//for legacy
		iUser.iLocation = iCurrentLocation;
		iUser.iNearestDistance = iNearestDistance;
		iUser.ExternalizeL(*stream);
		}
	CleanupStack::PopAndDestroy(stream);
#endif	
	}

void Context::LoadUserDataL()
	{
#ifndef CREATEDATA	
	SUSHI("Context::LoadUserDataL() 1");
	INSTREAM* stream = new (ELeave) INSTREAM();
	CleanupStack::PushL(stream);
	if (stream->OpenStreamL(KUser, ETrue) == KErrNone)
		{
		iCurrentLocation.InternalizeL(*stream);	//for legacy
		iUser.InternalizeL(*stream);
		SetCurrentLocation(iUser.iLocation, iUser.iNearestDistance);
		}
	CleanupStack::PopAndDestroy(stream);
	SUSHI("Context::LoadUserDataL() 2");
#endif	
	}

void TUserSettings::ExternalizeL(OUTSTREAM& aStream)
	{
	iLocation.ExternalizeL(aStream);
	
	aStream.WriteIntL(iLocationIndex);	//NOT USED
	
	aStream.WriteIntL(iNearestDistance);
	aStream.WriteIntL(I64LOW(iTime.Int64()));
	aStream.WriteIntL(I64HIGH(iTime.Int64()));
	
	aStream.WriteIntL(iShowAtmosphere);
	aStream.WriteIntL(iShowLabels);
	aStream.WriteIntL(iShowConstellations);
	aStream.WriteFloatL(iVisibleStars);
	aStream.WriteIntL(iLights);
	aStream.WriteIntL(iWaitingForGps);
	aStream.WriteIntL(iShowIss);
	}

void TUserSettings::InternalizeL(INSTREAM& aStream)
	{
	
	iLocation.InternalizeL(aStream);
	
	iLocationIndex = aStream.ReadIntL();	//NOT USED
	
	iNearestDistance = aStream.ReadIntL();
	
	TUint low = aStream.ReadIntL();
	TUint high = aStream.ReadIntL();
	iTime = TTime(MAKE_TINT64(high, low));
	iShowAtmosphere = aStream.ReadIntL();
	iShowLabels = aStream.ReadIntL();
	iShowConstellations = aStream.ReadIntL();
	iVisibleStars = aStream.ReadFloatL();
	iLights = aStream.ReadIntL();
	iWaitingForGps = aStream.ReadIntL(); 
	TRAP_IGNORE(iShowIss = aStream.ReadIntL());
	}

