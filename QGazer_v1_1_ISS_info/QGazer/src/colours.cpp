/*
 * colours.cpp
 *
 *  Created on: 3 Oct 2011
 *      Author: Damien
 */
#include <e32std.h>
#include "colours.h"
#include "context.h"
extern Context* gContext;

TColours gColours;

TColours::TColours()
	{
	iGlobalColours.Append(KScheme0);
	iGlobalColours.Append(KScheme1);
	iGlobalColours.Append(KScheme2);
	iGlobalColours.Append(KScheme3);
	iGlobalColours.Append(KScheme4);
	iGlobalColours.Append(KScheme5);
	iGlobalColours.Append(KScheme6);
	}

TColours::~TColours()
	{
	iGlobalColours.Reset();
	}

const TRgb& TColours::GlobalColour(TInt aIndex) const 
	{
	return iGlobalColours[aIndex];
	}

const TRgb& TColours::NMColour(const TRgb& aColour) const
	{
	iNMColour = aColour;
	if (gContext->NightMode())
		{
		TInt lum = iNMColour.Red() + iNMColour.Green() + iNMColour.Blue();
		lum /= 3;
		iNMColour.SetRed(lum);
		iNMColour.SetGreen(0);
		iNMColour.SetBlue(0);
		}
	return iNMColour;
	}
