/*
 * FloatingWindow.cpp
 *
 *  Created on: 13-Jan-2010
 *      Author: Damien
 */

#include <EIKENV.H>
#include <aknutils.h>
#include <FBS.H>

#include "CommonTypes.h"
#include "FloatingWindow.h"

#include "colours.h"
extern TColours gColours;

#include "context.h"
extern Context* gContext;

CFloatingWindowGroup* gGroup;

CFloatingWindowGroup* CFloatingWindowGroup::NewL()
	{	
	CFloatingWindowGroup* self = new (ELeave) CFloatingWindowGroup();
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

void CFloatingWindowGroup::ConstructL()
	{
	//iGroup = new (ELeave) RWindowGroup(CCoeEnv::Static()->WsSession());
	//User::LeaveIfError(iGroup->Construct((TUint32)iGroup));
	//iGroup->SetOrdinalPosition(0, 1);//ECoeWinPriorityAlwaysAtFront);
//	iGroup->EnableReceiptOfFocus(EFalse);
	
	iNormalFont = CEikonEnv::Static()->DenseFont();
	iSmallFont = CEikonEnv::Static()->LegendFont();
	
	TFontSpec tinyFontSpec = iSmallFont->FontSpecInTwips();
	CGraphicsDevice* screenDevice = CEikonEnv::Static()->ScreenDevice();
	tinyFontSpec.iHeight = tinyFontSpec.iHeight * 30 / 40; 
	CFont* tinyFont;
	screenDevice->GetNearestFontInTwips(tinyFont, tinyFontSpec);
	iTinyFont = tinyFont;
	
	TFontSpec bigFontSpec = iNormalFont->FontSpecInTwips();
	bigFontSpec.iHeight = bigFontSpec.iHeight * 40 / 30; 
	CFont* bigFont;
	screenDevice->GetNearestFontInTwips(bigFont, bigFontSpec);
	iBigFont = bigFont;

	gGroup = this;
	}

CFloatingWindowGroup::~CFloatingWindowGroup()
	{
//	iGroup->Close();
	//delete iGroup;
	//iGroup = NULL;
	}

void CFloatingWindowGroup::SetOnTop()
	{
	//iGroup->SetOrdinalPosition(0, 1);//ECoeWinPriorityAlwaysAtFront)
	}

void CFloatingWindowGroup::SetBehind()
	{
	//iGroup->SetOrdinalPosition(-1, ECoeWinPriorityAlwaysAtFront);
	}

CFloatingWindow::~CFloatingWindow()
	{
	}

void CFloatingWindow::ZeroRect() 
	{
       //SetRect( TRect(TPoint(0, 0), TSize(0, 0)) );
    }

void CFloatingWindow::HandleResourceChange(TInt aType)
	{
	}

void CFloatingWindow::SetTransparent()
	{
	Window().SetTransparencyFactor(TRgb(196,196,196));
	}

void CFloatingWindow::SetTransparentAlpha()
	{
	Window().SetTransparencyFactor(KRgbWhite);
	Window().SetTransparencyAlphaChannel();
	}

void CFloatingWindow::ConstructL(TInt aBorder, TInt aMargin) 
	{
	//CreateWindowL(gGroup->Group());
	CreateWindowL();
	
	iGroup = gGroup;
	SetColours(KFcFloating, KBcFloating);
	
	SetTransparentAlpha();
	Window().SetOrdinalPosition(0, ECoeWinPriorityAlwaysAtFront);
	
	ZeroRect();
	
	iMargin=aMargin;
	iBorder=aBorder;
    }

void CFloatingWindow::ForegroundL(TBool aFg)
	{
	CCoeControl::MakeVisible(aFg);
	}

void CFloatingWindow::MakeVisible(TBool aVisible)
	{
	iVisible = aVisible;
	CCoeControl::MakeVisible(iVisible);
	}

void CFloatingWindow::SetColours(const TRgb& aFg, const TRgb& aBg, const TRgb& /*aBd*/)
	{
	iFgColor = aFg;
	iBgColor = aBg;
	iBgColor.SetAlpha(ALPHA);
	iBorderColor = aBg;//aBd;
	iBorderColor.SetAlpha(ALPHA);
	}

void CFloatingWindow::BaseDraw(CWindowGc& aGc, const TRect& aRect) const
	{
	aGc.SetPenStyle(CGraphicsContext::ESolidPen);
	aGc.SetPenSize(TSize(iBorder, iBorder));
	aGc.SetPenColor(NM(iBorderColor));	
	aGc.SetBrushColor(NM(iBgColor));
	aGc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	aGc.DrawRect(Rect());
	aGc.SetPenColor(NM(iFgColor));
	}	

TBool CFloatingWindow::BackgroundTapped(const TPointerEvent &aPointerEvent)
	{
	if (aPointerEvent.iType == TPointerEvent::EButton1Down)
		{
		for (TInt i=0; i<CountComponentControls(); i++)
			{
			if (ComponentControl(i)->Rect().Contains(aPointerEvent.iPosition))
				{
				return EFalse;
				}
			}
		return ETrue;
		}
	return EFalse;
	}



