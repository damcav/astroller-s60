/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#include <e32std.h>
#include <gdi.h>

#include "ListBox.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "LBContainer.h"

#include "model.h"
#include "objects.h"
#include "hardsums.h"

#include "TextButton.h"

extern CHardSums* gHardSums;
extern MAppUiCb* gAppUiCb;
extern Context* gContext;

CListBoxWindow* CListBoxWindow::NewL(const TRect& aRect, const CSkyModel& aModel, TBool aLocations, MListBoxCb& aObserver)
	{	
	CListBoxWindow* self = new (ELeave) CListBoxWindow(aRect, aModel, aLocations, aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CListBoxWindow::CListBoxWindow(const TRect& aRect, const CSkyModel& aModel, TBool aLocations, MListBoxCb& aObserver)
	: iObserver(aObserver), iModel(aModel), iLocations(aLocations)
	{
	TRect rect = aRect;
	rect.Shrink(rect.Width()/10, rect.Height()/12);
	SetRect(rect);
	iWindowRect = rect;
	}

void CListBoxWindow::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
    MakeVisible(EFalse);
    LayoutL();
    iCurrentList = EConstellations;
   	InitListBoxL();
	}

const TPtrC KLabels[]=
	{
	_S("Constellations"),
	_S("Planets, Moon, Sun"), 
	_S("Stars"),    
	_S("Other")
	};

void CListBoxWindow::LayoutL()
	{
	TInt width = Rect().Width() / 2;
	TInt height = Rect().Height();
	
	iListBoxPos = TPoint(width,0);    
	iListBoxSize =  TSize(width, height); 
	
#ifdef MESSIER	
	TInt numGroups = 4;
#else
	TInt numGroups = 3;
#endif
	
	TInt buttonGap = 12;
	TInt buttonHeight = (Rect().Height() - ((numGroups+1) * buttonGap)) / numGroups;
	TInt buttonAreaWidth = iListBoxPos.iX;
	TInt buttonWidth = buttonAreaWidth * 2 / 3;
	TInt buttonX = buttonAreaWidth - buttonWidth;
	buttonX /= 2;


	
	for (TInt i=0; i<numGroups; i++)
		{
		TPoint p(buttonX, (buttonGap*(i+1)) + (buttonHeight*i));
		TSize s(buttonWidth, buttonHeight);
		CTextButton* button = CTextButton::NewL(*this, i);
		iButtons.Append(button);
		button->SetContainerWindowL(*this);
		button->SetRect(TRect(p,s));
		button->SetText(KLabels[i]);
		}
	
	iButtons[0]->SetHighlighted(ETrue);
	}
	
CListBoxWindow::~CListBoxWindow()
	{
	iButtons.ResetAndDestroy();
	iButtons.Close();
	
	iObjects.Reset();
	iObjects.Close();
	
	delete iListBoxWidget;
	iListBoxWidget = NULL;
	}

void CListBoxWindow::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);
	}

void CListBoxWindow::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	if (BackgroundTapped(aPointerEvent))
		iObserver.ListBoxOkCancelL(EFindListBox, EFalse);
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

TInt CListBoxWindow::CountComponentControls() const
	{
	TInt n = iButtons.Count();
	if (iListBoxWidget)
		n++;
	return n;
	}

CCoeControl* CListBoxWindow::ComponentControl(TInt aIndex) const
	{
	if (aIndex < iButtons.Count())
		return iButtons[aIndex];
	else
		return iListBoxWidget;
	}

void CListBoxWindow::Refresh()
	{
	MakeVisible(ETrue);
	DrawNow();
	}
	
void CListBoxWindow::Reset()
	{
	MakeVisible(EFalse);
	}

void CListBoxWindow::AddIfVisible(const TSkyObject& aObject)
	{
	if (gContext->InSpace() || aObject.IsVisible() )
		{
		iListBoxWidget->AddItemL(aObject.Name());
		iObjects.Append(&aObject);
		}
	}

void CListBoxWindow::InitConstellations()
	{
	iObjects.Reset();
	for (TInt i=0; i<iModel.ConstellationCount(); i++)
		AddIfVisible(iModel.Constellation(i));
	}

void CListBoxWindow::InitPlanets()
	{
	iObjects.Reset();
	AddIfVisible(iModel.Moon());
	AddIfVisible(iModel.Sun());
	for (TInt i=0; i<iModel.PlanetCount(); i++)
		if (i != TSkyPlanet::EEarth)
			AddIfVisible(iModel.Planet(i));
	}

void CListBoxWindow::InitStars()
	{
	iObjects.Reset();
	for (TInt i=0; i<iModel.NamedStarCount(); i++)
		AddIfVisible(iModel.NamedStarObject(i));
	}

#ifdef MESSIER
void CListBoxWindow::InitMessier()
	{
	iObjects.Reset();
#ifdef SATELLITE
//	for (TInt i=0; i<iModel.SatellitesCount(); i++)
//		AddIfVisible(iModel.Satellite(i));
	if (iModel.Iss().Show())
		AddIfVisible(iModel.Iss());		
#endif
	for (TInt i=0; i<iModel.MessierCount(); i++)
		AddIfVisible(iModel.Messier(i));
	}
#endif


void CListBoxWindow::Selected(TInt aIndex)
	{
	TChosenObject& chosen = gContext->ChosenObject();
	chosen.iObject = iObjects[aIndex];
	chosen.iEq = chosen.iObject->iEq;
	chosen.iObject->Horizontal(chosen.iHr);
	
	iObserver.ListBoxOkCancelL(EFindListBox, ETrue);	
	}

void CListBoxWindow::HighlightCurrentButton(TBool /*aDrawNow TODO remove*/)
	{
	for (TInt i=0; i<iButtons.Count(); i++)
		{
		iButtons[i]->SetHighlighted(EFalse);
		iButtons[i]->DrawDeferred();
		}
/*	
		{
		if (i == iCurrentList)
			iButtons[i]->SetColours(KRgbYellow, KButtonBg, KRgbYellow);
		else
			iButtons[i]->SetColours(KButtonFg, KButtonBg, KButtonBd);
		if (aDrawNow)
			iButtons[i]->DrawDeferred();
		}
*/		
	}

void CListBoxWindow::KeyPressedL(TInt aId)
	{
	TLBButton b = (TLBButton)aId;
	iCurrentList = b;
	HighlightCurrentButton(ETrue);
	InitListBoxL();
	}
	
void CListBoxWindow::InitListBoxL()    
	{
	// construct listbox item array
	if (iListBoxWidget)
		iListBoxWidget->MakeVisible(EFalse);
	delete iListBoxWidget;
	iListBoxWidget = NULL;
	
    iListBoxWidget = CListBoxWidget::NewL(TRect(iListBoxPos, iListBoxSize), this, *this);
    iListBoxWidget->SetParentWindowRect(iWindowRect);
   	

   	
	iListBoxWidget->ResetItems();
	switch (iCurrentList)
		{
	
	case EConstellations:
		InitConstellations();
		break;
	
	case EPlanets:
		InitPlanets();
		break;
	
	case EStars:
		InitStars();
		break;
		
#ifdef MESSIER
	case EMessier:
		InitMessier();
		break;
#endif
	default:;
	//	TDebug::Print(668);
		//User::Invariant();
		}

	int current = iListBoxWidget->ItemCount()/2;
	iListBoxWidget->ItemsAddedL(current);
	iListBoxWidget->SetExtentAndActivateL(TRect(iListBoxPos, iListBoxSize));
	iListBoxWidget->DrawNow();
	}

void CListBoxWindow::KeyEvent(TUint aChar)
	{
	iListBoxWidget->KeyEvent(aChar);
	}
