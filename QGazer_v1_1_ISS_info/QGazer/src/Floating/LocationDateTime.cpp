/*
 * LocationDateTime.cpp
 *
 *  Created on: 10-May-2010
 *      Author: Damien
 */

#include "CLocationDateTime.h"
#include "TextButton.h"
#include "CQueue.h"
#include "LocationInput.h"
#include "DateTimeInput.h"
#include "Context.h"
#include "QGazerapp.h"

#include "colours.h"

extern CQGazerApp* gApp;

extern Context* gContext;

#include "colours.h"

CLocationDateTime* CLocationDateTime::NewL(const TRect& aRect, TInt aId, MListBoxCb& aObserver, CQueue* aAllWindows)
	{
	CLocationDateTime* self = new (ELeave) CLocationDateTime(aRect, aId, aObserver, aAllWindows);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CLocationDateTime::CLocationDateTime(const TRect& aRect, TInt aId, MListBoxCb& aObserver, CQueue* aAllWindows)
	: iId(aId), iObserver(aObserver), iAllWindows(aAllWindows)
	{
	iApplicationRect = aRect;
	TRect rect = aRect;
	rect.Shrink(rect.Width()/12, rect.Height()/11);
	SetRect(rect);
	}

void CLocationDateTime::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
	
	iLocBm = new (ELeave) CFbsBitmap();
	iTimeBm = new (ELeave) CFbsBitmap();
  		
	if (!gContext->NightMode())
		{
		User::LeaveIfError(iLocBm->Load(gApp->FullResourcePath(_L("qgazermisc.mbm")), 4));
		User::LeaveIfError(iTimeBm->Load(gApp->FullResourcePath(_L("qgazermisc.mbm")), 5));
		}
	else
		{
		User::LeaveIfError(iLocBm->Load(gApp->FullResourcePath(_L("qgazermiscred.mbm")), 4));
		User::LeaveIfError(iTimeBm->Load(gApp->FullResourcePath(_L("qgazermiscred.mbm")), 5));
		}
	
	if (!gContext->UserLocationMode())
		{
		TInt distance;
		const TLocation& loc = gContext->CurrentLocation(distance);
		gContext->SetUserLocation(loc, distance);
		gContext->SetUserTimeToNow();
		}
	
	LayoutL();
	}

void CLocationDateTime::LayoutL()
	{
	TGrid grid(Rect().Size());
	grid.SetBorders(20, 20);
	grid.SetCellBorders(0, 4);
	grid.SetDim(14, 3, ECentre);

	grid.SetCellExtent(2, 1);
	CTextButton* b = CTextButton::NewL(*this, ELocIcon);
	iButtons.Append(b);
	b->SetRect(grid.Rect(0,0));
	b->SetBm(iLocBm);
	b->SetShadow(-1);
	
	b = CTextButton::NewL(*this, ETimeIcon);
	iButtons.Append(b);
	b->SetRect(grid.Rect(0,1));
	b->SetBm(iTimeBm);
	b->SetShadow(-1);
	
	grid.SetCellExtent(11, 1);
	b = CTextButton::NewL(*this, ELocButton);
	iButtons.Append(b);
	b->SetRect(grid.Rect(3, 0));
	b->SetFontId(ESmallFont);
	
	b = CTextButton::NewL(*this, ETimeButton);
	iButtons.Append(b);
	b->SetRect(grid.Rect(3, 1));
	b->SetColours(KFcStandardButton, KBcStandardButton);
	b->SetFontId(ESmallFont);
	
	grid.SetCellExtent(2, 1);
	b = CTextButton::NewL(*this, EOk);
	iButtons.Append(b);
	b->SetRect(grid.Rect(12, 2));
	b->SetText(_L("Ok"));
	b->SetFontId(ENormalFont);
	
	for (TInt i=0; i<iButtons.Count(); i++)
		{
		iButtons[i]->SetContainerWindowL(*this);
		iButtons[i]->SetColours(KFcStandardButton, KBcStandardButton); //DADA
		}
	//ok button is green
	b->SetColours(KFcDtOkButton, KBcDtOkButton);
	
	SetLocString();
	SetTimeString();
	}

CLocationDateTime::~CLocationDateTime()
	{
	//TDebug::Print(_L("~LOCATION DATE TIME"));
	iButtons.ResetAndDestroy();
	delete iLocBm;
	iLocBm = NULL;
	delete iTimeBm;
	iTimeBm = NULL;
	}
	
void CLocationDateTime::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);
	}

void CLocationDateTime::StartListBoxL(TInt aId)
	{
	if (aId == ELocationInput)
		{
		iDropDownWindow = CLocationInput::NewL(iApplicationRect, aId, *this);
		iLocationActive = ETrue;
		}
	else
		{
		iDropDownWindow  = CDateTimeInput::NewL(iApplicationRect, aId, *this);
		iDateTimeActive = ETrue;
		}
	
	STOREWINDOW(iDropDownWindow);
	iDropDownWindow->MakeVisible(ETrue);
	iDropDownWindow->ActivateL();
	MakeVisible(EFalse);
	}
	
void CLocationDateTime::ListBoxOkCancelL(TInt aId, TBool /*aOk*/)
	{
	DELETEWINDOW(iDropDownWindow);
	iLocationActive = EFalse;
	iDateTimeActive = EFalse;
	
	if (aId == ELocationInput)
		SetLocString();
	//else
		SetTimeString();

		
//	iButtonsHidden = EFalse;
	MakeVisible(ETrue);
	DrawNow();
	}
	
void CLocationDateTime::KeyPressedL(TInt aId)
	{
	//TDebug::Count();
	switch (aId)
		{
		case EOk:
			iObserver.ListBoxOkCancelL(iId, ETrue);
			break;
			
		case ELocIcon:
		case ELocButton:
			//iButtonsHidden = ETrue;
			//DrawNow();
			MakeVisible(EFalse);
			StartListBoxL(ELocationInput);
			break;

		case ETimeIcon:
		case ETimeButton:
			//DrawNow();
			//MakeVisible(EFalse);
			StartListBoxL(EDateTimeInput);
			break;
			
		default:;
		}
	}

TInt CLocationDateTime::CountComponentControls() const
	{
	return 5;
	}

CCoeControl* CLocationDateTime::ComponentControl(TInt aIndex) const
	{
	return iButtons[aIndex];
	}

void CLocationDateTime::SetLocString()
	{

	TInt dist;
	const TLocation& loc = gContext->UserLocation(dist);
	dist /= 1000;
	TBldStr::Set(loc.Country());
	TBldStr::Comma();
	TBldStr::Add(loc.City());

	if (dist > 5)
		{
		TBldStr::Add(_L(" ("));
		TBldStr::AddNum(dist);
		TBldStr::Add(_L("km)"));
		}
	iButtons[ELocButton]->SetText(TBldStr::String());
	iButtons[ELocButton]->SetHighlighted(EFalse);
	iButtons[ELocButton]->DrawNow();
	}

void CLocationDateTime::SetTimeString()
	{
	iButtons[ETimeButton]->SetText(gContext->TimeStringUserLocation());
	iButtons[ETimeButton]->SetHighlighted(EFalse);
	iButtons[ETimeButton]->DrawNow();
	}

void CLocationDateTime::KeyEvent(TUint aChar)
	{
	if (iDropDownWindow)
		iDropDownWindow->KeyEvent(aChar);
	}

void CLocationDateTime::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	if (BackgroundTapped(aPointerEvent))
		iObserver.ListBoxOkCancelL(iId, EFalse);
	
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}
