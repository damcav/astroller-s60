#include <e32std.h>
#include <gdi.h>
#include <FBS.H>


#include "ArticleWindow.h"

#include "MAppUiCb.h"
#include "Context.h"
#include "TextButton.h"
#include "MenuWindow.h"


//#include "QGazerapp.h"
//extern CQGazerApp* gApp;

extern Context* gContext;
extern MAppUiCb* gAppUiCb;


const TInt KFindButtonId = 1;

/*
 * Convert a JPG using CImageDecode
 * image/jpeg
 * static IMPORT_C CImageDecoder *FileNewL(RFs &aFs, const TDesC &aSourceFilename, const TDesC8 &aMIMEType, const TOptions aOptions=EOptionNone);
 * IMPORT_C const TFrameInfo &FrameInfo(TInt aFrameNumber=0) const;
 CFbsBitmap* IMPORT_C TInt Create(const TSize &aSizeInPixels, TDisplayMode aDispMode);
 * virtual IMPORT_C void Convert(TRequestStatus *aRequestStatus, CFbsBitmap &aDestination, TInt aFrameNumber=0);
 * 
 * 
 * if Article == "_" load it from a file (a_name.txt)
 * if Article == "" we should not have this extra screen
 * if Article == "*" we should have this screen (eg, star) but show no article
 * if it's a Messier and it starts with "M" there is no article or pic
 * 
 * 
 * To get an image or thumbnail:
 * call name=CArticleWindow::ArticleFileName()
 * then create a CArticleBitmap::NewL(name)
 */
const TPtrC KLabels[]=
	{
	_S("Ok")	
	};


CArticleWindow* CArticleWindow::NewL(const TRect& aRect, const TSkyObject* aObject, TBool aFindButton)
	{	
	CArticleWindow* self = new (ELeave) CArticleWindow(aRect, aObject, aFindButton);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CArticleWindow::CArticleWindow(const TRect& aRect, const TSkyObject* aObject, TBool aFindButton)
	: iObject(aObject), iHasFindButton(aFindButton)
	{
	TRect rect = aRect;
	rect.Shrink(rect.Width()/17, rect.Height()/13);
	SetRect(rect);
	if (!iObject->Show())
		iHasFindButton = EFalse;
	}

void CArticleWindow::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
    MakeVisible(EFalse);
    
    iLines = new (ELeave) CDesCArrayFlat(32);  
    

    iLines->Reset();
    iObject->DescriptionL(*iLines);
	iLines->AppendL(_L(""));

	LayoutL();

	SetFont(ESmallFont);

	iHasBitmap = EFalse;
//Of TSkyObject::Article() returns "_" we should load the article from a file
	if (iObject->Article().Length())
    	{
	
		iArticleHelper = CArticleHelper::NewL(*this);
		iArticleHelper->Bitmap(iObject);
		
		if (iObject->Article()[0]=='_')	//load if from a file
			{
			iHasBitmap = ETrue;
			TRAPD(err, iArticleHelper->LoadArticleL(iObject, iLoadedArticle));
			
			if (err == KErrNone)
				MakeLines(iRightRect.Width(), iLoadedArticle, *iLines);
			}
		else
			{
			if (iObject->Article()[0] != '*')	//no actual article to show
				MakeLines(iRightRect.Width(), iObject->Article(), *iLines);
			}
    	}
    
	iTextRect = TextRect(iLines);
	iRectDiff = iTextRect.Height() - iRightRect.Height();
	}

void CArticleWindow::LayoutL()
	{
	TRect rect(TPoint(0,0), TSize(Rect().Height(), Rect().Height()));
	 
	TInt s=Rect().Height()/9;
	rect.Shrink(TSize(s,s));
	iLeftRect = rect;
	

	if (iHasFindButton)
		{
		TInt butHeight = s;
		TInt butWidth = Rect().Width() / 5;
		
		iLeftRect.Move(0, -s);	//move the left rect to the top
		TRect findRect(iLeftRect.iTl, TSize(butWidth, butHeight));
		
		findRect.Move(208/2 - butWidth/2, Rect().Height() - butHeight - s);		//move right and up
		CTextButton* findBut = CTextButton::NewL(*this, KFindButtonId);
		findBut->SetText(_L("Find"));
		findBut->SetRect(findRect);
		iButtons.AppendL(findBut);
		}
	
	rect.SetRect(TPoint(Rect().Width()/2, 0), TSize(Rect().Width()/2, Rect().Height()));
	s=Rect().Height()/15;
	rect.Shrink(TSize(s,s));
	iRightRect = rect;
	
	EnableDragEvents();
	}

void CArticleWindow::KeyPressedL(TInt aId)
	{
	if (aId == KFindButtonId)
		gAppUiCb->FindObject(iObject);
	}

CArticleWindow::~CArticleWindow()
	{
	iButtons.ResetAndDestroy();
	iButtons.Close();
	delete iArticleHelper;
	iArticleHelper = NULL;

	if (iLines)
		iLines->Reset();
	delete iLines;
	iLines = NULL;
	
	delete iBm;
	iBm = NULL;
	
	iLoadedArticle.Close();
	}

void CArticleWindow::BitmapReady(CFbsBitmap* aBitmap)
	{
	iBm = aBitmap;
	DrawNow();
	}

void CArticleWindow::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	TPoint p;
	
	BaseDraw(gc, aRect);

	if (iHasBitmap)
		DrawBitmap(gc, iLeftRect);
	else
		DrawStar(gc, iLeftRect);

	UseFont(gc, ESmallFont);
	gc.SetClippingRect(iRightRect);
//	BaseDraw(gc, aRect);
	
	TRect rect(iTextRect);
	rect.SetRect(iRightRect.iTl, rect.Size());
	rect.Move(0, iLinesDy);

	DrawText(rect, iLines, TAlign(ELeft));
	}

TInt CArticleWindow::CountComponentControls() const
	{
	return iButtons.Count();
	}

CCoeControl* CArticleWindow::ComponentControl(TInt aIndex) const
	{
	return iButtons[aIndex];
	}

void CArticleWindow::DrawStar(CWindowGc& aGc, const TRect& aRect) const
	{
	const TSkyPointObject* op = (TSkyPointObject*)gContext->ChosenObject().iObject;
	TColour c(op->iColour);
	TRgb colour(c.Red(), c.Green(), c.Blue());

	TRect rect(aRect);
	
	TInt numCircles = 30;
	TInt dx = numCircles * 2;
	dx = rect.Width() / dx;

	aGc.SetBrushStyle(CGraphicsContext::ESolidBrush);

	TInt loAlpha = 0;
	TInt hiAlpha = 255;
	TInt dAlpha = (hiAlpha - loAlpha) / numCircles;
	
	colour.SetAlpha(loAlpha);

	for (TInt i=0; i<numCircles; i++)
		{
		aGc.SetPenColor(colour);
		aGc.SetBrushColor(colour);		
		aGc.DrawEllipse(rect);
		rect.Shrink(TSize(dx, dx));
		colour.SetAlpha(loAlpha + i*dAlpha);
		}
	}
	
void CArticleWindow::DrawBitmap(CWindowGc& aGc, const TRect& aRect) const
	{
	if (iBm)
		{
		TRect rect(aRect);
		TSize s(iBm->SizeInPixels());
		rect.Shrink(0, (rect.Height() - s.iHeight) / 2);
		iBm->SetSizeInTwips(aGc.Device());
		aGc.DrawBitmap(rect.iTl, iBm);
		}
	}

void CArticleWindow::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	if (iRightRect.Contains(aPointerEvent.iPosition))
		{
		switch (aPointerEvent.iType)
			{
			case TPointerEvent::EButton1Down:
				iButtonDownPoint = aPointerEvent.iPosition;
				break;
				
			case TPointerEvent::EDrag:
				if (iRectDiff > 0)
					{
					TPoint delta = aPointerEvent.iPosition;
					delta -= iButtonDownPoint;
					iLinesDy += delta.iY;
					if (iLinesDy < -iRectDiff)
						iLinesDy = -iRectDiff;
					if (iLinesDy > 0)
						iLinesDy = 0;
					iButtonDownPoint = aPointerEvent.iPosition;
					DrawNow();
					}
				break;
			default:;
			};
		}
	else if (iLeftRect.Contains(aPointerEvent.iPosition))
		{
		if (iObject->Name().Compare(_L("ISS")) == 0)
			gAppUiCb->MenuCommandL(EQueueKIssPage);
		}
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}




