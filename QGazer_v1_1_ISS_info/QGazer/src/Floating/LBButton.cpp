/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */
#include <e32std.h>
#include <gdi.h>
#include <AknGlobalNote.h> 
#include "LBButton.h"
#include "MAppUiCb.h"
extern MAppUiCb* gAppUiCb;
#include "context.h"
extern Context* gContext;

#include "colours.h"
extern TColours gColours;


typedef float GLfloat;

CLBButton* CLBButton::NewL(const TRect& aRect, TInt aId)
	{	
	
	CLBButton* self = new (ELeave) CLBButton(aRect);
	CleanupStack::PushL(self);
	self->ConstructL(aId);
	CleanupStack::Pop(self);    
	return self;
	}

CLBButton::CLBButton(const TRect& aRect)
	{
	iParentRect = aRect;
	TRect rect(aRect.iTl, TSize(aRect.Width(), 30));
	SetRect(rect);
	}

CLBButton::~CLBButton()
	{
	delete iBm;
	iBm = NULL;
	}

void CLBButton::ConstructL(TInt aId)
	{
	iId = aId;
    LayoutL();
    SetPercentage(gContext->VisibleStars());
    }

void CLBButton::LayoutL()
	{
	if (iBm)	
		SetRect(TRect(TPoint(0,0), iBm->SizeInPixels()));
	}

void CLBButton::Percentage()
	{
	GLfloat slidingArea = GLfloat(iParentRect.Height() - Rect().Height());
	GLfloat pos = GLfloat(Rect().iTl.iY - iParentRect.iTl.iY);
	GLfloat pc = (pos / slidingArea) * 100.0;
	pc = 100.0 - pc;

	gAppUiCb->SliderPercentage(iId, pc);
	}

void CLBButton::SetPercentage(GLfloat aPc)
	{
	GLfloat slidingArea = GLfloat(iParentRect.Height() - Rect().Height());
	GLfloat pos = slidingArea * ((100.0-aPc) / 100.0);
	//TDebug::Print(pos);
	TPoint p(iParentRect.iTl);
	p += TPoint(0, TInt(pos));
	SetRect(TRect(p, Rect().Size()));
	}

void CLBButton::Draw(const TRect& /*aRect*/) const
	{
	CWindowGc& gc = SystemGc();
	gc.SetPenStyle(CGraphicsContext::ESolidPen);
	gc.SetPenSize(TSize(1, 1));
	gc.SetPenColor(NM(KFcStarCountButton));
	gc.SetBrushColor(NM(KBcStarCountButton));
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	gc.DrawRect(Rect());
	}

void CLBButton::MoveButton(const TPoint& aOffset)
	{
	TPoint pos = Position();
	pos += aOffset;
	
	if (pos.iY < iParentRect.iTl.iY)
		pos.iY = iParentRect.iTl.iY;
	else if (pos.iY + Rect().Height() >= iParentRect.iBr.iY)
		pos.iY = iParentRect.iBr.iY - 1 - Rect().Height();
		
	SetPosition(pos);
	Percentage();	
	Parent()->DrawNow();
	}

void CLBButton::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{//not draggable!
	
	switch (aPointerEvent.iType)
		{
		case TPointerEvent::EButton1Down:
			iButtonDownPoint = aPointerEvent.iPosition;
			
		//	iObserver->ButtonPressedL();
			break;
			
		case TPointerEvent::EDrag:
			{
		
			TPoint pPos = aPointerEvent.iPosition;
			pPos -= iButtonDownPoint;
			pPos.iX = 0;

			MoveButton(pPos);
			
			iButtonDownPoint = aPointerEvent.iPosition;
			break;
			}
		default:;
		}
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

