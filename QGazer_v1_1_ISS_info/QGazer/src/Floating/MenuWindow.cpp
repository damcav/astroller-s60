/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */
/*
 List of objects
 100 101 102 103
 EPrevious
 */
#include <e32std.h>
#include <gdi.h>

#include "MenuWindow.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "TextButton.h"
#include "QGazerapp.h"

#include "ArticleWindow.h"

#include "AlignEx.h"

#include "colours.h"
extern TColours gColours;

extern CQGazerApp* gApp;
extern Context* gContext;
extern MAppUiCb* gAppUiCb;

#include "model.h"
extern CSkyModel* gModel;

//These correlate with the icon positions in the MBM file
const TPtrC KLabels[]=
	{
	_S("Constellations"),	
	_S("Labels"),			
	_S("Globe view"),		
	_S("Home location"),			
	_S("Night mode"),				
	_S("Find"),		
	_S("Atmosphere"),	
	_S("Star count"),
	_S("Visit"),
	_S("Exit"),
	_S("Learn"),
	_S("Layers"),
	_S("Previous"),
	_S("More"),
	_S("ISS"),
	_S("Show/Hide"),
	_S(""),
	_S("")
	};





const int KPages[KNumPages][KNumButtons]={
		ELayersIcon, EGlobeIcon, ELocIcon, ENightModeIcon, 
		EFindIcon, EMortarIcon, EVisitIcon, EQuitIcon,

		KNoIcon, EConstIcon, ELabelsIcon, EIssSettingsIcon,
		EPreviousIcon, EAtmosIcon, EStarsIcon,  EQuitIcon,

		KNoIcon, KTextStart, KTextEnd, EIssUpdateIcon,
		EIssIcon, KTextStart+1, KTextEnd+1, EIssPreviewIcon,
		
		100, 101, 102, 103,
		104, 105, 106, ENextIcon,
		
		107, 108, 109, 110,
		EPreviousIcon, 111, 112, ENextIcon,
		
		113, 114, 115, 116,
		EPreviousIcon, 117, 118, ENextIcon,
		
		119, 120, 121, 122,
		EPreviousIcon, 123, 124, 125
	};

TBool CMenuWindow::IsArticle(TInt aIcon)
	{
	return (aIcon >= KArticlesStart) && (aIcon < KTextStart);
	}

void CMenuWindow::LoadPageBitmaps()
	{
	iArticleObjectsThisPage.Close();
	iArticleButtonsThisPage.Close();
	iArticleBitmapsThisPage.ResetAndDestroy();
	iArticleBitmapsThisPage.Close();
	
	for (TInt i=0; i<KNumButtons; i++)
		{
		TInt c = iPages[iPage][i]; 
		if (IsArticle(c))
			{
			iArticleObjectsThisPage.Append(iArticleObjects[c - KArticlesStart]);
			iArticleButtonsThisPage.Append(i);
			}
		}

	iCurrentThumb = 0;
	iArticleHelper->AllThumbnails(iArticleObjectsThisPage, TSize(104, 104));
	}

void CMenuWindow::BitmapReady(CFbsBitmap* aBitmap)
	{
	TInt but = iArticleButtonsThisPage[iCurrentThumb++];
	iButtons[but]->SetBm(aBitmap);
	iArticleBitmapsThisPage.Append(aBitmap);
	DrawNow();
	}

CMenuWindow* CMenuWindow::NewL(const TRect& aRect, TInt aPage)
	{	
	CMenuWindow* self = new (ELeave) CMenuWindow(aRect, aPage);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CMenuWindow::CMenuWindow(const TRect& aRect, TInt aPage)
	{
	iPage = aPage;
	TRect rect = aRect;
	rect.Shrink(rect.Width()/10, rect.Height()/13);
	SetRect(rect);
	}

void CMenuWindow::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
    MakeVisible(EFalse);

    iGetVis = new CGetVis(*this);
    
    iArticleHelper = CArticleHelper::NewL(*this);
	iArticleHelper->AllArticleObjects(iArticleObjects);
//	iArticleHelper->AllThumbnails(iArticleObjects, TSize(104, 104));


  	for (TInt i = EConstIcon; i<KNumIcons; i++)
  		{
  		CFbsBitmap* bm = new (ELeave) CFbsBitmap();
  		CleanupStack::PushL(bm);
  		
  		if (!gContext->NightMode())
  			User::LeaveIfError(bm->Load(gApp->FullResourcePath(_L("qgazermenu.mbm")), i));
  		else
  			User::LeaveIfError(bm->Load(gApp->FullResourcePath(_L("qgazermenured.mbm")), i));
  		iBitmaps.Append(bm);
  		CleanupStack::Pop(bm);
  		}
  	
  	for (TInt i=0; i<KNumPages; i++)
		for (TInt j=0; j<KNumButtons; j++)
			{
			TInt p= KPages[i][j];
			if (IsArticle(p))
				if ((p - KArticlesStart) >= iArticleObjects.Count())
					{
					iPages[i][j] = KNoIcon;
					continue;
					}
			iPages[i][j] = p;
			}

  	iIssMargin = Rect().Width() / 50;
  	
  	LayoutL();

  	iCallback = new CCallback(*this);
	}

void CMenuWindow::DoLayout(TInt aIndex, const TRect& aRect)
	{
	TInt icon = iPages[iPage][aIndex];
	
	if (icon == KNoIcon)
		return;

	if ((icon >= KTextStart) && (icon < KTextEnd))
		{
		HandleTextArea(ETrue, icon-KTextStart, aRect);
		return;
		}

	if (icon >= KTextEnd)
		{
		HandleTextArea(EFalse, icon-KTextEnd, aRect);
		return;
		}


	CTextButton* button = CTextButton::NewL(*this, icon);
	if (IsArticle(icon))
		{
		//icon += 5;
		button->SetBm(iBitmaps[ELoadingIcon]);
		
		TBuf<64> buf = iArticleObjects[icon-KArticlesStart]->Name();
		TInt bracket = buf.Locate(TChar('('));
		if (bracket != KErrNotFound)
			buf.SetLength(bracket);
		while (buf.Right(1) == _L(" "))
			buf.SetLength(buf.Length()-1);
		
		button->SetText(buf);
		}
	else
		{
		button->SetBm(iBitmaps[icon]);
		button->SetText(KLabels[icon]);
		iIconIndex[icon] = iButtons.Count();
		}
	
	iButtons.Append(button);
	button->SetContainerWindowL(*this);
	
	button->SetRect(aRect);
	button->SetShadow(-1);
	button->SetContainerWindowL(*this);

	//ISS hacks
	if ((icon == EIssPreviewIcon) && !iShowIssPreview)
		button->MakeVisible(EFalse);

	}
	
void CMenuWindow::LayoutL()
	{
	iShowDownloadButton = ETrue;
	iTextAreas.Close();
	iTextArrays.ResetAndDestroy();
	iTextArrays.Close();

	TInt buttonGapV = 12;	//TODO MAGIC!!!
	TInt buttonHeight = (Rect().Height() - ((KRows+1) * buttonGapV)) / KRows;
	TInt buttonAreaWidth = Rect().Width();
	TInt buttonGapH = 4;	//TODO MAGIC!!!
	TInt buttonWidth = (buttonAreaWidth - ((KColumns+1) * buttonGapH)) / KColumns;
	TInt buttonX = buttonGapH;

	TSize s(buttonWidth, buttonHeight);
	
	for (TInt i=0; i<KNumIcons; i++)
		iIconIndex[i] = KNoIcon;
	
	TInt xPos = 0;
	
//First row -----------	
	for (TInt i=0; i<KColumns; i++)
		{
		TPoint p(buttonX + xPos++ *(buttonGapH+buttonWidth), buttonGapV);
		DoLayout(i, TRect(p, s));
		}

//Second row -----------
	xPos = 0;
	for (TInt i=KColumns; i<KNumButtons; i++)
		{
		TPoint p(buttonX + xPos++ *(buttonGapH+buttonWidth), buttonGapV + buttonHeight);
		DoLayout(i, TRect(p, s));
		}

	CTextButton* b = NULL;
	b = FindButton(EConstIcon);
	if (b) b->SetCrossed(gContext->ShowConstellations(), ETrue);
	
	b = FindButton(EIssIcon);
	if (b) b->SetCrossed(gContext->ShowIss(), ETrue);

	CTextButton* globe = FindButton(EGlobeIcon);
	CTextButton* atmos = FindButton(EAtmosIcon);
	if (!gContext->InSpace())
		{
		if (globe) globe->SetCrossed(EFalse, EFalse);
		if (atmos) atmos->SetCrossed(gContext->ShowAtmosphere(), ETrue);
		}
	else
		{
		if (atmos) atmos->MakeVisible(EFalse);
		if (globe) globe->SetCrossed(ETrue, EFalse);
		}
	
	b = FindButton(EStarsIcon);
	if(b) b->MakeVisible(!gContext->InSpace());
	
	b = FindButton(EVisitIcon);
	if(b) b->MakeVisible(!gContext->WaitingForGps());
	
	b = FindButton(EGlobeIcon);
	if(b) b->MakeVisible(!gContext->WaitingForGps());

	b = FindButton(ELocIcon);
	if(b) b->MakeVisible(gContext->GpsStatus() != EGpsValid);
	
	b = FindButton(ENightModeIcon);
	if(b) b->SetCrossed(gContext->NightMode(), EFalse);
	
	b = FindButton(ELabelsIcon);
	if(b) b->SetCrossed(gContext->ShowLabels(), ETrue);
		
	//iButtons[EVisitBut]->SetCrossed(gContext->UserLocationMode() || gContext->UserTimeMode(), ETrue);
	
	TRgb under(NM(iBgColor));
	for (TInt i=0; i<iButtons.Count(); i++)
		iButtons[i]->SetUnderneath(under);
	
	AdjustIssPageLayout();
	}

CTextButton* CMenuWindow::FindButton(TInt aIcon)
	{
	TInt b = iIconIndex[aIcon];
	if (b == KNoIcon)
		return NULL;
	else
		return iButtons[b];
	}

void CMenuWindow::KeyPressedL(TInt aId)
	{
	
	if (IsArticle(aId))
		{
		iChosenArticle = iArticleObjects[aId-KArticlesStart];
		if (!iDeletePending)
			gAppUiCb->MenuCommandL(KArticlesStart);
		}
	
	switch (aId)
		{
		case ELayersIcon:
			iPage = KLayersPage;
			iCallback->CallMe();
			break;
		
		case EMortarIcon:
			iPage = KLearnPage;
			iCallback->CallMe();
			break;

		case EPreviousIcon:
			iPage--;
			iCallback->CallMe();
			break;

		case ENextIcon:
			iPage++;
			iCallback->CallMe();
			break;

		case EIssSettingsIcon:
			iPage = KIssSettingsPage;
		//	gAppUiCb->MenuCommandL(EShowPleaseWait);
			iCallback->CallMe();
			break;
			
		case EIssPreviewIcon:
			{
			TDateTime vis;
			if (gModel->Iss().NextVisibleUtcCached(vis))
				{
				TTime visTime(vis);
				visTime -= TTimeIntervalSeconds(10);
				vis = visTime.DateTime();
				gContext->SetUserTime(vis, EUniversalTz);
				if (!gContext->UserLocationMode())
					gContext->SetUserLocationToHome();
				gContext->SetChosenObject(&gModel->Iss());
				gAppUiCb->MenuCommandL(EIssPreviewIcon);
				}
			break;
			}
		default:
			if (!iDeletePending)
				{
				gAppUiCb->MenuCommandL(aId);
				AdjustIssPageLayout();
				}
		}
	DrawNow();
	}



void CMenuWindow::AdjustIssPageLayout()
	{
	TBool show = EFalse;
	if (iPage == KIssSettingsPage)
		{
		TInt w = Rect().Width();
		CTextButton* b = FindButton(EIssIcon);
		if (b)
			{
			if (gContext->ShowIss())
				{
				b->SetText(_L("Don't show ISS"));
				show = ETrue;
				}
			else
				{
				b->SetText(_L("Show ISS"));
				}
			SetFont(ESmallFont);
			TSize s(b->Bm()->SizeInPixels());
			s.iHeight += (FontHeight() + FontDescent());
			TRect br(TPoint(0,0), s);
			br.Move(Rect().iTl);
			
			if (show)
				{
				br.Move(iIssMargin, Rect().Height() - br.Height() - iIssMargin*3);
				}
			else
				{
				br.Move(Rect().Width()/2 - br.Width()/2, Rect().Height() - br.Height() - iIssMargin*3);
				}
			b->SetRect(br);
			
			b = FindButton(EIssPreviewIcon);
			if (b) b->MakeVisible(show && iShowIssPreview);
			b = FindButton(EIssUpdateIcon);
			if (b) b->MakeVisible(show && iShowDownloadButton);
			}
		}
	}

void CMenuWindow::Callback(TInt /*aId*/)
	{
	iButtons.ResetAndDestroy();
	iButtons.Close();
	if (iPage>=KFirstArticlePage && iPage<=KLastArticlePage)
		LoadPageBitmaps();
	LayoutL();
	DrawNow();
	//if (iPage == KIssSettingsPage)
	//	gAppUiCb->MenuCommandL(EHidePleaseWait);
	
	}

CMenuWindow::~CMenuWindow()
	{
	iButtons.ResetAndDestroy();
	iButtons.Close();
	
	iArticleObjectsThisPage.Close();
	iArticleButtonsThisPage.Close();
	iArticleBitmapsThisPage.ResetAndDestroy();
	iArticleBitmapsThisPage.Close();
	
	iBitmaps.ResetAndDestroy();
	iBitmaps.Close();

	iArticleObjects.Close();
	delete iArticleHelper;
	iArticleHelper = NULL;
	delete iCallback;
	iCallback = NULL;

	iTextAreas.Close();
	iTextArrays.ResetAndDestroy();
	iTextArrays.Close();
	
	delete iGetVis;
	}

_LIT(KSettingsTitle, "ISS Settings");

void CMenuWindow::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);

	
	TInt n = iTextAreas.Count();
	
	if (!gContext->ShowIss())
		n = 0;
	
	if (n)
		{
		UseFont(gc, ETinyFont);

		for (TInt i=0; i<n; i++)
			{
			DrawText(iTextAreas[i], iTextArrays[i], ERight, ECentreEx);
			}
		gc.DiscardFont();
		}	
	
	if (iPage == KIssSettingsPage)
		{
		TRect r;
		UseFont(gc, EBigFont);
		r.SetWidth(TextWidth(KSettingsTitle));
		r.SetHeight(FontHeight()+FontDescent());
		r.Move(Rect().iTl);
		r.Move(iIssMargin, iIssMargin);
		gc.SetPenColor(NM(KFcIssTitle));
		DrawText(r, KSettingsTitle);
		gc.DiscardFont();
		}
	}

void CMenuWindow::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
//	if (BackgroundTapped(aPointerEvent))
//		gAppUiCb->MenuCommandL(-1);
	
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

TInt CMenuWindow::CountComponentControls() const
	{
	return iButtons.Count();
	}

CCoeControl* CMenuWindow::ComponentControl(TInt aIndex) const
	{
	return iButtons[aIndex];
	}

void CMenuWindow::FillTextArray(TInt aId)
	{
	CDesCArray* c = iTextArrays[aId];
	if (aId == 0)	//satellite data
		{
		TDateTime validFrom;
		TDateTime validUntil;
		
		if (!gModel->SatellitesCount())
			{
			c->AppendL(_L("^5No orbit data."));
			c->AppendL(_L("Please download (3Kb)"));
			}
		else
			{
			TBldStr::Set(_L("^1Orbit data quality: "));
			TBool valid = gModel->Iss().UpToDate(validFrom, validUntil);
			if (!valid)
				{
				TBldStr::Add(_L("^5Poor^1"));
				c->AppendL(TBldStr::String());
				c->AppendL(_L("Please download (3Kb)"));
				}
			else
				{
				TTime now;
				now.UniversalTime();
				if (now.DaysFrom(TTime(validFrom)) > TTimeIntervalDays(2))
					{
					TBldStr::Add(_L("^4Fair^1"));
					c->AppendL(TBldStr::String());
					c->AppendL(_L("For maximum accuracy,"));
					c->AppendL(_L("please download (~2Kb)"));
					}
				else
					{
					TBldStr::Add(_L("^6Good"));
					c->AppendL(TBldStr::String());
					iShowDownloadButton = EFalse;
					}
				}
			}
		}
	else	//visibility
		{
		iShowIssPreview = EFalse;
		if (gModel->SatellitesCount())
			{
			iGetVis->Go();
			c->AppendL(_L("^1Calculating..."));
			}
		}
	}

void CMenuWindow::HandleTextArea(TBool aStart, TInt aId, const TRect& aRect)
	{
	if (aStart)
		{
		//Start of text area, so set the topleft
		iTextAreas.Append(TRect());
		iTextArrays.Append(new (ELeave) CDesCArrayFlat(32));
		FillTextArray(aId);
		iTextAreas[aId].Move(aRect.iTl);
		}
	else
		{
		//now set the botright
		TRect& r = iTextAreas[aId];
		r.SetWidth(aRect.iBr.iX - r.iTl.iX);
		r.SetHeight(aRect.iBr.iY - r.iTl.iY);
		}
	}


/*
ISS orbit data last updated ttt.
Data is out of date and should be downloaded from the internet (~2Kb).
The ISS will next be visible from xxx at ttt.
*/

void CMenuWindow::GotVis()
	{
	TDateTime vis;
	iShowIssPreview = gModel->Iss().NextVisibleUtcCached(vis);
	if (iShowIssPreview)
		{
		CTextButton* b = NULL;
		b = FindButton(EIssPreviewIcon);
		if (b) b->MakeVisible(ETrue && gContext->ShowIss());		
		}
	
	CDesCArray* c = iTextArrays[1];
	c->Reset();
	gModel->Iss().VisibilityText(*c);
	DrawNow();
	}

CGetVis::CGetVis(MGetVisCb& aListener)
	: CActive(-200), iListener(aListener)
	{
	CActiveScheduler::Add(this);
	}

void CGetVis::RunL()
	{
	if (iStatus.Int() == KErrNone)
		iListener.GotVis();
	}

void CGetVis::Go()
	{
	if (!IsActive())
		{
		SetActive();
		gModel->Iss().NextVisibleUtc(&iStatus);
		}
	}

void CGetVis::DoCancel()
	{
	gModel->Iss().CancelNextVisibleUtc();
	}

CGetVis::~CGetVis()
	{
	Cancel();
	}
