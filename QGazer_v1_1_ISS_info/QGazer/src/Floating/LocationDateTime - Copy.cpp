/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#include <e32std.h>
#include <gdi.h>

#include "LocationDateTime.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "LBContainer.h"

#include "TextButton.h"

extern MAppUiCb* gAppUiCb;
extern Context* gContext;

const TRgb KButtonFg = KRgbWhite;
const TRgb KButtonBg = TRgb(0,32,32);
const TRgb KButtonBg2 = TRgb(0,48,48);
const TRgb KButtonBd = TRgb(0,32,32);

CLocationDateTime* CLocationDateTime::NewL(const TRect& aRect, const CSkyModel& aModel, TInt aId, MAppUiCb& aObserver)
	{	
	CLocationDateTime* self = new (ELeave) CLocationDateTime(aRect, aModel, aId, aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CLocationDateTime::CLocationDateTime(const TRect& aRect, const CSkyModel& aModel,  TInt aId, MAppUiCb& aObserver)
	: iObserver(aObserver), iModel(aModel), iId(aId)
	{
	iAppRect = aRect;
	TRect rect = aRect;
	rect.Shrink(rect.Width()/10, rect.Height()/12);
	iAbsRect = rect;
	SetRect(rect);
	iLocationOnly = iId == ELocationListBox;
	}

void CLocationDateTime::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
	Window().SetTransparencyFactor(TRgb(232,232,232));
    MakeVisible(EFalse);

    TRect rect(Rect());
	rect.Shrink(100,50);
	
    LayoutL();
    iCurrentList = ECountry;

    if (!iLocationOnly)
    	{
    	//if (!gContext->UserLocation().IsSet())
    	//	gContext->UserLocation() = gContext->CurrentLocation();
    	
    	SetFirstAndLastCities();
    	FindUserLocation();
    	
    	gContext->UserTime(iTime, iSelections[ETimeZone]);
	
    	for (TInt i=ECountry; i<=EHour; i++)
    		SetBoxL(i, ESetDefault);
    	
    	SetBoxL(ETimeZone, ESetDefault); //sets hour and minutes
    	}
    else
    	{
    	SetFirstAndLastCities();
		SetBoxL(ECountry, ESetDefault);
		SetBoxL(ECity, ESetDefault);
    	}
    HighlightCurrentButton(ETrue);
	SetPlace();
	}

void CLocationDateTime::LayoutL()
	{
	TInt buttonGapX = 10;
	TInt buttonGapY = 35;
	TInt borderLeft = 5;//70;
	TInt borderRight  = 5;
	TInt borderY = 5;
	TInt width = Rect().Width() - borderLeft - borderRight;
	TInt height = Rect().Height();
	TInt buttonHeight = (height - (2 *  buttonGapY) - (2 * borderY)) / 3;
	
//first line
	TInt buttonWidth = width / 2;
	TInt timeWidth = buttonWidth;
	
	TInt buttonX = borderLeft + buttonGapX / 2;
	TInt buttonY = borderY;


	if (iLocationOnly)
		buttonY = (height / 2) - (buttonHeight / 2);
		
	for (TInt i=ECountry; i<=ECity; i++)
		{
		TPoint p(buttonX, buttonY);
		TSize s(buttonWidth-buttonGapX, buttonHeight);
		CTextButton* button = CTextButton::NewL(*this, i);
		button->SetContainerWindowL(*this);		
		button->SetRect(TRect(p,s));
		button->SetFontId(ENormalFont);
		iButtons.Append(button);
		buttonX += buttonWidth;
		}

	if (iLocationOnly)	
		buttonY = borderY;


//second line
		buttonWidth = width / 3;
		buttonX = borderLeft + buttonGapX / 2;
		buttonY += (buttonHeight + buttonGapY);

	for (TInt i=EDay; i<=EMonth; i++)
		{
		TInt gap = buttonGapX;
		TPoint p(buttonX, buttonY);

		TSize s(buttonWidth-gap, buttonHeight);
		CTextButton* button = CTextButton::NewL(*this, i);
		button->SetContainerWindowL(*this);		
		button->SetRect(TRect(p,s));
		button->SetFontId(ENormalFont);
		iButtons.Append(button);
		buttonX += buttonWidth;
		
		if (i == EDay)
			{
			button->SetStyle(EButtonStyleDate);
			button->SetFontId(EBigFont);
			}
		}

		{
		TPoint p(buttonX, buttonY);
		TSize s((buttonWidth-buttonGapX), buttonHeight);
		CTextDoubleButton* button = CTextDoubleButton::NewL(*this, EYearHi, EYearLo);
		button->SetContainerWindowL(*this);
		button->SetColour2(KButtonBg2);
		button->SetFontId(EBigFont);
		
		TRect rect(TRect(p,s));
		button->SetRect(rect);
		iButtons.Append(button);
		buttonX += buttonWidth;
		}
		
	buttonY += (buttonHeight + buttonGapY);
	buttonX = borderLeft + buttonGapX / 2;

	
		{
		TPoint p(buttonX, buttonY);
		TSize s(buttonWidth-buttonGapX, buttonHeight);
		CTextDoubleButton* button = CTextDoubleButton::NewL(*this, EHour, EMinute);
		button->SetContainerWindowL(*this);		
		button->SetFontId(EBigFont);
		button->SetColour2(KButtonBg2);
		button->SetStyle(EButtonStyleTime);
		TRect rect(TRect(p,s));
		//rect.Shrink(buttonGapX*2, 0);
		button->SetRect(rect);
		iButtons.Append(button);
		buttonX += buttonWidth;
		}
	
	buttonWidth = width / 3;
	//local time button
		{
		TPoint p(buttonX, buttonY);
		TSize s(buttonWidth-buttonGapX, buttonHeight);
		CTextButton* button = CTextButton::NewL(*this, ETimeZone);
		button->SetContainerWindowL(*this);		
		button->SetRect(TRect(p,s));
		iButtons.Append(button);
		buttonX += buttonWidth;
		}

		buttonWidth /= 2;
		buttonX += buttonWidth;
		
		//ok button
		{
		TPoint p(buttonX, buttonY);
		TSize s(buttonWidth-buttonGapX, buttonHeight);
		CTextButton* button = CTextButton::NewL(*this, EOk);
		button->SetContainerWindowL(*this);		
		button->SetRect(TRect(p,s));
		button->SetText(_L("Ok"));
		button->SetFontId(ENormalFont);
		iButtons.Append(button);
		}
	
	}
	
CLocationDateTime::~CLocationDateTime()
	{
	iButtons.ResetAndDestroy();
	iButtons.Close();
	
	delete iListBoxWidget;
	iListBoxWidget = NULL;
	
	iLocationsIndex.Reset();

	}

void CLocationDateTime::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);
	
	if (iLocationOnly)
		{
		UseFont(gc, ETinyFont);
		gc.SetPenColor(NM(KRgbWhite));
		TPoint p(iButtons[0]->Rect().iTl);
		p -= TPoint(0, iButtons[0]->Rect().Height()/4);
		gc.DrawText(_L("Country"), p);
		p = iButtons[1]->Rect().iTl;
		p -= TPoint(0, iButtons[1]->Rect().Height()/4);
		gc.DrawText(_L("City"), p);
		gc.DiscardFont();
		}
		
	}

void CLocationDateTime::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	if (aPointerEvent.iType == TPointerEvent::EButton1Down)
		if (iListBoxWidget)
			{
			delete iListBoxWidget;
			iListBoxWidget = NULL;
			}

		
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

TInt CLocationDateTime::CountComponentControls() const
	{
	TInt n = iButtons.Count();
	if (iListBoxWidget)
		n++;
	return n;
	}

CCoeControl* CLocationDateTime::ComponentControl(TInt aIndex) const
	{
	if (aIndex < iButtons.Count())
		return iButtons[aIndex];
	else return iListBoxWidget;
	}



void CLocationDateTime::Selected(TInt aIndex)
	{
	iListBoxWidget->MakeVisible(EFalse);
	iSelected = aIndex;
	iSelections[iCurrentList] = iSelected;
	SetBoxL(iCurrentList, ESetFromListBox);
	
	HighlightCurrentButton(ETrue);
	}

TInt CLocationDateTime::FindButtonById(TInt aId)
	{
	if (aId == EYearLo)
		aId = EYearHi;
	if (aId == EMinute)
		aId = EHour	;
	
	for (TInt i=0; i<iButtons.Count(); i++)
		if (iButtons[i]->Id() == aId)
			return i;
	User::Invariant();
	return -1;
	}

void CLocationDateTime::SetLabelText(TInt aLabel, const TDesC& aText)
	{
	if (aLabel == EYearLo)
		((CTextDoubleButton*)iButtons[FindButtonById(EYearHi)])->SetText2(aText);
	else if (aLabel == EMinute)
		((CTextDoubleButton*)iButtons[FindButtonById(EHour)])->SetText2(aText);
	else
		iButtons[FindButtonById(aLabel)]->SetText(aText);
	}

void CLocationDateTime::HighlightCurrentButton(TBool aDrawNow)
	{
	if (iLocationOnly)
		{
		for (TInt i=EDay;  i<iButtons.Count()-1; i++)
			iButtons[i]->MakeVisible(EFalse);
		}
	
	for (TInt i=0; i<iButtons.Count(); i++)
		{
		iButtons[i]->SetColours(KButtonFg, KButtonBg, KButtonBd);
		iButtons[i]->SetHighlighted(EFalse);
		iButtons[i]->DrawDeferred();
		}
	}

TDateTime CLocationDateTime::ReadTime()
	{
	TDateTime time;
	time.SetYear(iSelections[EYearHi] * 100 + iSelections[EYearLo]);
	time.SetMonth(TMonth(iSelections[EMonth]));
	time.SetDay(iSelections[EDay]);
	time.SetHour(iSelections[EHour]);
	time.SetMinute(iSelections[EMinute]);
	return time;
	}


void CLocationDateTime::SetLabelsFromUtc()
	{
/*	
	gContext->UserTimeUtc(iTime);
	TTime t(iTime);
	gContext->AdjustTimeFromUtc(t, iSelections[ETimeZone]);
	iTime = t.DateTime();
	for (TInt i=EDay; i<ETimeZone; i++)
    	SetBoxL(i, ESetDefault);
 */
	TInt tz;
	gContext->UserTime(iTime, tz);
	for (TInt i=EDay; i<ETimeZone; i++)
    	SetBoxL(i, ESetDefault);
	}

void CLocationDateTime::SetPlace()
	{
	if (iId == ELocationListBox)	
		{
		gContext->CurrentLocation() = ThisLoc();
		}
	else
		{
		TUserSettings& s = gContext->UserSettings();
		s.iLocation = ThisLoc(); 
		s.iNearestDistance = 0;		
		}
	}

void CLocationDateTime::SetTime()
	{
	gContext->SetUserTime(ReadTime(), iSelections[ETimeZone]);
	}

void CLocationDateTime::KeyPressedL(TInt aId)
	{
	TLDTButton b = (TLDTButton)aId;
	if (aId == EOk)
		{
		gAppUiCb->ListBoxOkCancelL(iId, ETrue);
		}
	else
		{
		iCurrentList = b;
		HighlightCurrentButton(ETrue);
		InitListBoxL();
		}
	}


void CLocationDateTime::SetBoxL(TInt aIndex, TInt aAction)
	{
	switch (aIndex)
		{
		case ECountry:
			InitCountriesL(aAction);
			break;
		case ECity:
			InitCitiesL(aAction);
			break;
		case EDay:
			InitDaysL(aAction);
			break;
		case EMonth:
			InitMonthsL(aAction);
			break;
		case EYearHi:
			InitYearHighL(aAction);
			break;
		case EYearLo:
			InitYearLowL(aAction);
			break;
		case EHour:
			InitHoursL(aAction);
			break;
		case EMinute:
			InitMinutesL(aAction);
			break;
		case ETimeZone:
			InitTimeZoneL(aAction);
			break;
		case EOk:
			break;
		default:;
		}
	}

void CLocationDateTime::InitListBoxL()    
	{    
	// construct listbox item array
	delete iListBoxWidget;
	iListBoxWidget = NULL;
	
	iListBoxWidget = CListBoxWidget::NewL(iAppRect, NULL, *this);
	
	iListBoxWidget->ResetItems();
	SetBoxL(iCurrentList, EPopulateListBox);
	
	if (iListBoxWidget->ItemCount() < 2)
		return;
	
	iListBoxWidget->MakeVisible(ETrue);
	iListBoxWidget->ActivateL();
	
	TInt height = Rect().Height();
	TRect butRect = iButtons[FindButtonById(iCurrentList)]->Rect();
	TInt width = butRect.Width();

	TPoint tl;
	
	if (iCurrentList == ETimeZone)		//special case
		{
		width *= 2;
		tl = TPoint(0, 0);
		}
	else
		{
		tl = TPoint(butRect.iTl.iX, 0);

		if (tl.iX + butRect.Width() >= iAppRect.iBr.iX / 2) 
			tl -= TPoint(width, 0);
		else
			tl += TPoint(butRect.Width(), 0);
		}
	
	tl += iAbsRect.iTl;
	TInt dx = tl.iX + width - iAppRect.iBr.iX;
	if (dx > 0)
		{
		tl -= TPoint(dx, 0);
		}
		


	iListBoxWidget->ItemsAddedL();
	iListBoxWidget->AddScrollBarL(TRect(tl, TSize(width, height)));
	iListBoxWidget->SetExtent(TRect(tl, TSize(width, height)));
	
	}

void CLocationDateTime::KeyEvent(TUint aChar)
	{
	iListBoxWidget->KeyEvent(aChar);
	}

void CLocationDateTime::FindUserLocation()
	{
	
	TInt country = 0;
	
	const TDesC& userCountry(gContext->UserLocation().Country());
//	TDebug::Print(userCountry);
	for (TInt i=0; i<iLocationsIndex.Count()-1; i++)
		{
		if ( iModel.Location(iLocationsIndex[i]).Country().Compare(userCountry) == 0)
			{
			country = i;
			break;
			}
		}
		
//	ASSERT(country > -1);

	iFirstCity = iLocationsIndex[country];
	iLastCity = iLocationsIndex[country+1] - 1;
	
	TInt count = iLocationsIndex[country+1] - iFirstCity;
	
	TInt city = 0;
	const TDesC& userCity(gContext->UserLocation().City());
	
	for (TInt i = 0; i < count; i++)
		{
		if ( iModel.Location(iFirstCity + i).City().Compare(userCity) == 0)
			{
			city = i;
			break;
			}
		}
	//ASSERT(city > -1);
	iSelections[ECountry] = country;
	iSelections[ECity] = city;
	iCurrentLocation = iFirstCity + city;
	}


void CLocationDateTime::SetFirstAndLastCities()
	{
	iLocationsIndex.Reset();
	iBuf.Zero();
	
	for (TInt i=0; i<iModel.LocationCount(); i++)
		{
		const TLocation& loc = iModel.Location(i);
		if (iBuf.Compare(loc.Country()) != 0)
			{
			iLocationsIndex.Append(i);
			iBuf = loc.Country();
			}
		}
	iLocationsIndex.Append(iModel.LocationCount());

	SetLabelText(ECountry, iModel.Location(iFirstCity).Country());
	}

void CLocationDateTime::InitCountriesL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<iLocationsIndex.Count()-1; i++)
			{
			const TLocation& loc = iModel.Location(iLocationsIndex[i]);
			iListBoxWidget->AddItemL(loc.Country());
			}
		}
	
	else if (aAction == ESetFromListBox)
		{
		iFirstCity = iLocationsIndex[iSelected];
		iCurrentLocation = iFirstCity;
		iSelections[ECity] = 0;
		SetLabelText(ECountry, ThisLoc().Country());
		iLastCity = iLocationsIndex[iSelected+1] - 1;
		InitCitiesL(ESetFromListBox);
		}
	
	else if (aAction == ESetDefault)
		{
		SetLabelText(ECountry, ThisLoc().Country());
		}
	}

void CLocationDateTime::InitCitiesL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=iFirstCity; i<=iLastCity; i++)
			{
			const TLocation& loc = iModel.Location(i);
			iListBoxWidget->AddItemL(loc.City());
			}
		}
	
	else if (aAction == ESetFromListBox)
		{
		iCurrentLocation = iFirstCity + iSelections[ECity];
		SetTime();	//make sure the current user time is stored
		SetPlace();

		SetLabelText(ECity, ThisLoc().City());
		if (!iLocationOnly)
			InitTimeZoneL(ESetFromListBox);
		}
	
	else if (aAction == ESetDefault)
		{
		SetLabelText(ECity, ThisLoc().City());
		}
	}

	
TInt KMonthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 31, 31};

void CLocationDateTime::InitDaysL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=1; i<=KMonthDays[iSelections[EMonth]]; i++)
			{
			iBuf.Zero();
			iBuf.AppendNum(i);
			iListBoxWidget->AddItemL(iBuf);
			}
		}
	
	else if (aAction == ESetFromListBox)
		{
		iBuf.Zero();
		iBuf.AppendNum(iSelections[EDay]+1);
		SetLabelText(EDay, iBuf);
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iBuf.Zero();
		iBuf.AppendNum(iTime.Day()+1);
		SetLabelText(EDay, iBuf);
		iSelections[EDay] = iTime.Day();
		}
	}

TPtrC KMonths[] = 
	{
		_S("January"),
		_S("February"),
		_S("March"),
		_S("April"),
		_S("May"),
		_S("June"),
		_S("July"),
		_S("August"),
		_S("September"),
		_S("October"),
		_S("November"),
		_S("December")
	};



void CLocationDateTime::InitMonthsL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<12; i++)
			iListBoxWidget->AddItemL(KMonths[i]);
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EMonth, KMonths[iSelected]);

		if (iSelections[EDay]+1 > KMonthDays[iSelected] )
			{
			iSelections[EDay] =  KMonthDays[iSelected]-1;
			InitDaysL(ESetFromListBox);
			}
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		SetLabelText(EMonth, KMonths[iTime.Month()]);
		iSelections[EMonth] = iTime.Month();
		}
	
	}

void CLocationDateTime::InitYearLowL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<=99; i++)
			iListBoxWidget->AddItemL(TBldStr::Num(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EYearLo, TBldStr::Num2(iSelected));
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iSelections[EYearLo] = iTime.Year() % 100;
		SetLabelText(EYearLo, TBldStr::Num2(iSelections[EYearLo]));
		}
	}

void CLocationDateTime::InitYearHighL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<30; i++)
			iListBoxWidget->AddItemL(TBldStr::Num(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EYearHi, TBldStr::Num(iSelected));
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iSelections[EYearHi] = iTime.Year() / 100;
		SetLabelText(EYearHi, TBldStr::Num(iSelections[EYearHi]));		
		}
	}


void CLocationDateTime::InitHoursL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<24; i++)
			iListBoxWidget->AddItemL(TBldStr::Num2(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EHour, TBldStr::Num2(iSelected));
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iSelections[EHour] = iTime.Hour();	//UTC;
		SetLabelText(EHour, TBldStr::Num2(iSelections[EHour]));
		}	
	}

void CLocationDateTime::InitMinutesL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<60; i++)
			iListBoxWidget->AddItemL(TBldStr::Num2(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetTime();
		SetLabelText(EMinute, TBldStr::Num2(iSelected));
		}
	
	else if (aAction == ESetDefault)
		{
		TInt m = iTime.Minute();
		iSelections[EMinute] = m;
		SetLabelText(EMinute, TBldStr::Num2(m));		
		}	
	}

const TDesC& CLocationDateTime::TimeZoneString(TInt aType)
	{
	iBuf.Zero();
	if (aType == ENewLocationTime)
		{
		iBuf.Append(ThisLoc().City());
		iBuf.Append(_L(" Time"));
		}
	else if (aType == EHomeLocationTime)
		{
		iBuf.Append(_L("Home("));
		iBuf.Append(gContext->CurrentLocation().City());
		iBuf.Append(_L(") Time"));
		}
	else
		{
		iBuf.Append(_L("Universal Time"));
		}
	return iBuf;
	}

void CLocationDateTime::InitTimeZoneL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		iListBoxWidget->AddItemL(TimeZoneString(0));
		iListBoxWidget->AddItemL(TimeZoneString(1));
		iListBoxWidget->AddItemL(TimeZoneString(2));
		}
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(ETimeZone, TimeZoneString(iSelections[ETimeZone]));
		gContext->SetTimeZone(iSelections[ETimeZone]);
		SetLabelsFromUtc();	//doesn't change the time
		}
	else if (aAction == ESetDefault)
		{
		SetLabelText(ETimeZone, TimeZoneString(iSelections[ETimeZone]));
		gContext->SetTimeZone(iSelections[ETimeZone]);
		SetLabelsFromUtc(); //doesn't change the time
		}
	}
