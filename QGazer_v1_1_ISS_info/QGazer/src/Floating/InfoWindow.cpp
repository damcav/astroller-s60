/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */
#include <e32std.h>
#include <gdi.h>

#include "InfoWindow.h"

#include "Context.h"
#include "colours.h"
extern TColours gColours;

extern Context* gContext;
#include "MAppUiCb.h"
extern MAppUiCb* gAppUiCb;


CInfoWindow* CInfoWindow::NewL(TInt aId, CInfoWindowBorder* aBorder, TInt aFontEnum)
	{	
	CInfoWindow* self = new (ELeave) CInfoWindow(aId, aBorder, aFontEnum);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CInfoWindow::CInfoWindow(TInt aId, CInfoWindowBorder* aBorder,  TInt aFontEnum)
	: iId(aId),iBorderWindow(aBorder), iFontEnum(aFontEnum)
	{
	iThreeD = ETrue;
	}

void CInfoWindow::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 6);
    MakeVisible(EFalse);
    iLines = new (ELeave) CDesCArrayFlat(32);   
    SetTextAlignment(ELeft);
    SetFont(iFontEnum);
	}

CInfoWindow::~CInfoWindow()
	{
	if (iLines)
		iLines->Reset();
	delete iLines;
	iLines = NULL;
	delete iButton;
	iButton = NULL;
	delete iButton2;
	iButton2 = NULL;	
	}

void CInfoWindow::ThreeD(CWindowGc& aGc, const TRect& aRect) const
	{
	
	TColour fg(iBgColor.Red(), iBgColor.Green(), iBgColor.Blue());
	TColourTransform c;
	
	c.Set(fg, TColour(255,255,255));
	const TColour& l = c.Colour(25.0);
	TRgb lighter(l.Red(), l.Green(), l.Blue());
	c.Set(fg, TColour(1,1,1));
	const TColour& d = c.Colour(50.0);
	TRgb darker(d.Red(), d.Green(), d.Blue());
	
	darker.SetAlpha(ALPHA);
	lighter.SetAlpha(ALPHA);
	
	TRect rect(aRect);	
	for (TInt i=0; i<3; i++)
		{
		aGc.SetPenColor(NM(lighter));
		aGc.MoveTo(rect.iTl);
		aGc.DrawLineBy(TPoint(rect.Width(), 0));
		aGc.MoveTo(rect.iTl);
		aGc.DrawLineBy(TPoint(0, rect.Height()));
		
		TPoint br(rect.iBr);
		br+=TPoint(-1, -1);
		aGc.SetPenColor(NM(darker));
		aGc.MoveTo(br);
		aGc.DrawLineBy(TPoint(-rect.Width(), 0));
		aGc.MoveTo(br);
		aGc.DrawLineBy(TPoint(0, -rect.Height()));
		
		rect.Shrink(1,1);
		};
	}

void CInfoWindow::Draw(const TRect& /*aRect*/) const
	{
	Window().SetOrdinalPosition(0);	
	CWindowGc& gc = SystemGc();

	BaseDraw(gc, Rect());

	TRect rect(Rect());

	if (iThreeD)
		ThreeD(gc, rect);
	
	rect.Shrink(iMargin, iMargin);
	
	gc.SetPenColor(NM(iFgColor));
		UseFont(gc, iFontEnum);
		
	if (iBm)
		gc.BitBlt(rect.iTl, iBm);
	else
		DrawText(rect, iLines, iTextAlignment);
	
	gc.DiscardFont();
	}

void CInfoWindow::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{

	if (iDeletePending)
		return;
	
	
		
	TBool check = ETrue;
	
	if (iButton)
		if (iButton->Rect().Contains(aPointerEvent.iPosition))
			check = EFalse;
	
	if (iButton2)
		if (iButton2->Rect().Contains(aPointerEvent.iPosition))
			check = EFalse;
	
	if (check)
		if (aPointerEvent.iType == TPointerEvent::EButton1Up)
			gAppUiCb->InfoWindowCbL(iId);
	
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

void CInfoWindow::Set(const TRect& aRect, TAlignEx aAlign)
	{
	iContainerRect = aRect;
	iAlign = aAlign;
	TRect rect(aRect);
	
	if (iBm)
		{
		rect = TRect(TPoint(0,0), iBm->SizeInPixels());
		iMargin = 3;
		}
	else
		{
		rect = TextRect(iLines);
		}
	
	if (iButton)
		{
		TInt w = iButton->Rect().Width();
		if (w > rect.Width())
			rect.SetWidth(w + iMargin * 2);
		}
	
	if (iButton2)
		{
		TInt w = iButton->Rect().Width();
		if (w > rect.Width())
			rect.SetWidth(w + iMargin * 2);
		}

	rect.Grow(iMargin, iMargin);
	rect.Move(iMargin, iMargin);	//move to 0,0
	
	if (iButton)
		{
		TRect bRect(iButton->Rect());
		bRect.Move((rect.Width() - bRect.Width())/2, rect.Height() - iMargin);
		iButton->SetRect(bRect);
		rect.SetHeight(rect.Height() + bRect.Height());
		}

	if (iButton2)
		{
		TRect bRect(iButton2->Rect());
		bRect.Move((rect.Width() - bRect.Width())/2, rect.Height());
		iButton2->SetRect(bRect);
		rect.SetHeight(rect.Height() + bRect.Height());
		}

	TInt b = iBorderWindow ? KBorderThickness : 0;
	
	switch (aAlign)
		{
	case ETopLeftEx:
	case ELeftEx:
	case EBottomLeftEx:	
		rect.Move(b, 0);
		break;

	case ETopCentreEx:
	case ECentreEx:
	case EBottomCentreEx:	
		rect.Move(aRect.Width()/2 - rect.Width()/2, 0);
		break;
	
	case ETopRightEx:
	case ERightEx:
	case EBottomRightEx:
		rect.Move(aRect.Width() - rect.Width() - b, 0);
		break;
	
	default:
		TDebug::Print(670);
		User::Invariant();
		}
		
	switch (aAlign)
		{
	case ETopLeftEx:
	case ETopCentreEx:
	case ETopRightEx:
		rect.Move(0, b);
		break;

	case ELeftEx:
	case ECentreEx:
	case ERightEx:
		rect.Move(0, aRect.Height()/2 - rect.Height()/2);
		break;
		
	case EBottomLeftEx:		
	case EBottomCentreEx:	
	case EBottomRightEx:
		rect.Move(b, aRect.Height() - rect.Height());
		break;
		
	default:
		TDebug::Print(669);
		User::Invariant();
		}

	if (iBorderWindow)
		{
		TRect borderRect(rect);
		borderRect.Shrink(-KBorderThickness, -KBorderThickness);
		iBorderWindow->SetRect(borderRect);
		iBorderWindow->MakeVisible(ETrue);
//		iBorderWindow->ActivateL();
		}
	
	
	SetRect(rect);
	
	MakeVisible(ETrue);
    DrawNow();
	}

CDesCArray& CInfoWindow::StringArray()
	{
	return *iLines;
	}

void CInfoWindow::AddButtonL(const TDesC& aText, TInt aId)
	{
	if (!iButton)
		iButton = CTextButton::NewL(*this, aId);
	else
		iButton->ChangeId(aId);
	iButton->SetText(aText);
	iButton->SetContainerWindowL(*this);
	iButton->SetShadow(0);
	iButton->SetRound(5);
	iButton->SetUnderneath(iBgColor);
	iButton->SetAllowHighlighting(EFalse);
		
	TRect rect(TPoint(0,0), TSize(TextWidth(aText), FontHeight()*2)); //TODO put in base class
	rect.Grow(4, 4);	//TODO MAGIC!!!
	rect.Move(4, 4);	

	iButton->SetRect(rect);
	iButton->ActivateL();
	}

void CInfoWindow::AddButton2L(const TDesC& aText, TInt aId)
	{
	if (!iButton2)
		iButton2 = CTextButton::NewL(*this, aId);
	else
		iButton2->ChangeId(aId);
		
	iButton2->SetText(aText);
	iButton2->SetContainerWindowL(*this);
	iButton2->SetShadow(0);
	iButton2->SetRound(5);
	iButton2->SetUnderneath(iBgColor);
	iButton2->SetAllowHighlighting(EFalse);
		
	TRect rect(TPoint(0,0), TSize(TextWidth(aText), FontHeight()*2)); //TODO put in base class
	rect.Grow(4, 4);	//TODO MAGIC!!!
	rect.Move(4, 4);	

	iButton2->SetRect(rect);
	iButton2->ActivateL();
	}

void CInfoWindow::KeyPressedL(TInt aId)
	{
	gAppUiCb->InfoWindowCbL(aId);//iButton->Id());
	}

TInt CInfoWindow::CountComponentControls() const
	{
	if (iButton2)
		return 2;
	if (iButton)
		return 1;
	return 0;
	}

CCoeControl* CInfoWindow::ComponentControl(TInt aIndex) const
	{
	if (aIndex == 0)
		return iButton;
	else
		return iButton2;
	}

TBool CInfoWindow::Flash(TInt /*aAlpha*/)
	{
	return EFalse;
	}

void CInfoWindow::KillBorder()
	{
	iBorderWindow = NULL;
	Set(iContainerRect, iAlign);
	}
