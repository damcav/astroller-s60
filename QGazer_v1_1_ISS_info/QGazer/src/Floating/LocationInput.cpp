/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#include <e32std.h>
#include <gdi.h>

#include "LocationInput.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "LBContainer.h"
#include "Grid.h"

#include "TextButton.h"

#include "colours.h"
extern TColours gColours;

extern MAppUiCb* gAppUiCb;
extern Context* gContext;
extern CSkyModel* gModel;

CLocationInput* CLocationInput::NewL(const TRect& aRect, TInt aId, MListBoxCb& aObserver)
	{	
	CLocationInput* self = new (ELeave) CLocationInput(aRect, aId, aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CLocationInput::CLocationInput(const TRect& aRect, TInt aId, MListBoxCb& aObserver)
	: CDropDownInput(aObserver)
	{
	TRect rect = aRect;
	rect.Shrink(rect.Width()/12, rect.Height()/11);
	
	SetRect(rect);

	iAppRect = aRect;
	iAbsRect = rect;
	iId = aId;
	}

void CLocationInput::ConstructL()
	{
	//------------test 
	
	
	
	//------------test
	
	CFloatingWindow::ConstructL(1, 3);
	Window().SetTransparencyFactor(TRgb(232,232,232));
    MakeVisible(EFalse);

    TRect rect(Rect());
	rect.Shrink(100,50);
	
    LayoutL();
    iCurrentList = ECountry;

   	SetFirstAndLastCities();
   	FindUserLocation();
	SetBoxL(ECountry, ESetDefault);
	SetBoxL(ECity, ESetDefault);

    HighlightCurrentButton();
	//SetPlace();
	}

void CLocationInput::LayoutL()
	{
	TGrid grid(Rect().Size());
	//TDebug::Print(Rect());
	grid.SetBorders(20, 20);
	grid.SetCellBorders(0, 4);
	grid.SetDim(7, 3, ECentre);

	grid.SetCellExtent(3, 1);
	
	CTextButton* button = AddButtonL(ECountry);
	button->SetRect(grid.Rect(0, 1));
	button->SetFontId(ESmallFont);
	
	button = AddButtonL(ECity);
	button->SetRect(grid.Rect(4, 1));
	button->SetFontId(ESmallFont);

	grid.SetCellExtent(1, 1);

	button = AddButtonL(EOk);
	button->SetRect(grid.Rect(6, 2));
	button->SetFontId(ENormalFont);
	button->SetText(_L("Ok"));
	button->SetColours(KRgbWhite, KRgbDarkGreen);
	}
	
CLocationInput::~CLocationInput()
	{
	iLocationsIndex.Reset();
	}

void CLocationInput::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);
	
	UseFont(gc, ETinyFont);
	gc.SetPenColor(NM(KRgbWhite));

	CTextButton* button = FindButtonById(ECountry);
	TPoint p(button->Rect().iTl);
	p -= TPoint(0, button->Rect().Height()/4);
	gc.DrawText(_L("Country"), p);
	
	button = FindButtonById(ECity);
	p = button->Rect().iTl;
	p -= TPoint(0, button->Rect().Height()/4);
	gc.DrawText(_L("City"), p);

	gc.DiscardFont();
		
	}

void CLocationInput::KeyPressedL(TInt aId)
	{
	if (aId == 0)
		SetPlace();	//OK

	CDropDownInput::KeyPressedL(aId);
	}

void CLocationInput::SetPlace()
	{
	if (iId == ELocationListBox)	
		{
		gContext->SetCurrentLocation(ThisLoc(), 0);
		}
	else
		{
		TUserSettings& s = gContext->UserSettings();
		s.iLocation = ThisLoc(); 
		s.iNearestDistance = 0;		
		}
	}

int CLocationInput::InitialSelection()
	{
	return iSelections[iCurrentList];
	}

void CLocationInput::SetBoxL(TInt aIndex, TInt aAction)
	{
	switch (aIndex)
		{
		case ECountry:
			InitCountriesL(aAction);
			break;
		case ECity:
			InitCitiesL(aAction);
			break;
		case EOk:
			break;
		default:;
		}
	}

void CLocationInput::FindUserLocation()
	{
	TInt country = 0;
	
	const TDesC& userCountry(gContext->UserLocation().Country());
//	TDebug::Print(userCountry);
	for (TInt i=0; i<iLocationsIndex.Count()-1; i++)
		{
		if ( gModel->Location(iLocationsIndex[i]).Country().Compare(userCountry) == 0)
			{
			country = i;
			break;
			}
		}
		
//	ASSERT(country > -1);

	iFirstCity = iLocationsIndex[country];
	iLastCity = iLocationsIndex[country+1] - 1;
	
	TInt count = iLocationsIndex[country+1] - iFirstCity;
	
	TInt city = 0;
	const TDesC& userCity(gContext->UserLocation().City());
	
	for (TInt i = 0; i < count; i++)
		{
		if ( gModel->Location(iFirstCity + i).City().Compare(userCity) == 0)
			{
			city = i;
			break;
			}
		}
	//ASSERT(city > -1);
	iSelections[ECountry] = country;
	iSelections[ECity] = city;
	iCurrentLocation = iFirstCity + city;
	}


void CLocationInput::SetFirstAndLastCities()
	{
	iLocationsIndex.Reset();
	iBuf.Zero();
	
	for (TInt i=0; i<gModel->LocationCount(); i++)
		{
		const TLocation& loc = gModel->Location(i);
		if (iBuf.Compare(loc.Country()) != 0)
			{
			iLocationsIndex.Append(i);
			iBuf = loc.Country();
			}
		}
	iLocationsIndex.Append(gModel->LocationCount());
	SetLabelText(ECountry, gModel->Location(iFirstCity).Country());
	}

void CLocationInput::InitCountriesL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<iLocationsIndex.Count()-1; i++)
			{
			const TLocation& loc = gModel->Location(iLocationsIndex[i]);
			AddItemL(loc.Country());
			}
		}
	
	else if (aAction == ESetFromListBox)
		{
		iFirstCity = iLocationsIndex[iSelected];
		iCurrentLocation = iFirstCity;
		iSelections[ECity] = 0;
		SetLabelText(ECountry, ThisLoc().Country());
		iLastCity = iLocationsIndex[iSelected+1] - 1;
		InitCitiesL(ESetFromListBox);
		}
	
	else if (aAction == ESetDefault)
		{
		SetLabelText(ECountry, ThisLoc().Country());
		}
	}

void CLocationInput::InitCitiesL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=iFirstCity; i<=iLastCity; i++)
			{
			const TLocation& loc = gModel->Location(i);
			AddItemL(loc.City());
			}
		}
	
	else if (aAction == ESetFromListBox)
		{
		iCurrentLocation = iFirstCity + iSelections[ECity];
		//SetPlace();

		SetLabelText(ECity, ThisLoc().City());
		}
	
	else if (aAction == ESetDefault)
		{
		SetLabelText(ECity, ThisLoc().City());
		}
	}

const TLocation& CLocationInput::ThisLoc()
	{
	return gModel->Location(iCurrentLocation);
	}


