/*
 * LabelContainer.cpp
 *
 *  Created on: 06-May-2010
 *      Author: Damien
 */

#include "LabelContainer.h"
#include "StatusBar.h"
#include "MAppUiCb.h"

#include "colours.h"
extern TColours gColours;

#include "context.h"
extern Context* gContext;

extern MAppUiCb* gAppUiCb;

CLabelContainer* CLabelContainer::NewL(const TRect& aRect)
	{
	CLabelContainer* self = new (ELeave) CLabelContainer(aRect);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;	
	}

CLabelContainer::CLabelContainer(const TRect& aRect)
	{
	TRect rect(aRect);
	rect.SetHeight(aRect.Height() - KBarHeight);
	SetRect(rect);
	}

CLabelContainer::~CLabelContainer()
	{
	
	Reset();
	}

void CLabelContainer::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
	SetTransparentAlpha();
	EnableDragEvents();
	ActivateL();
	}

void CLabelContainer::Reset()
	{
	iLabels.Reset();
	}

void CLabelContainer::DrawLabels()
	{
	if (iLabels.Count() == 0 && !gContext->InSpace())
		{
		MakeVisible(EFalse);
		}
	else
		{
		PrepareLabels();
		MakeVisible(ETrue);
		DrawNow();
		}
	}

void CLabelContainer::PrepareLabels()
	{
	TInt safe = 0;
again:
	if (++safe == 100)
		return;
	
	for (TInt i=0; i<iLabels.Count()-1; i++)
		{
		for (TInt j=i+1; j<iLabels.Count(); j++)
			{
			if (iLabels[i].iRect.Intersects(iLabels[j].iRect))
				{
				UnIntersect(iLabels[i].iRect, iLabels[j].iRect);
				goto again; //YAY!! Goto!!
				}
			}
		}
	}

void CLabelContainer::UnIntersect(TRect& aRect1, TRect& aRect2)
	{
	TRect intersect(aRect1);
	intersect.Intersection(aRect2);
	if (intersect.Height()>intersect.Width())
		{
		//move LR
		TInt dx = intersect.Width()/2 + 1;
		if (aRect1.iTl.iX > aRect2.iTl.iX)
			dx =- dx;
		aRect1.Move(-dx,0);
		aRect2.Move(dx,0);
		}
	else
		{
		//move UD
		TInt dy = intersect.Height()/2 + 1;
		if (aRect1.iTl.iY > aRect2.iTl.iY)
			dy =- dy;
		aRect1.Move(0,-dy);
		aRect2.Move(0,dy);
		}
	}
	
void CLabelContainer::Add(const TPoint& aPoint, const TDesC& aText, TLabelType aType)
	{
    TLabel label;
    
    if (aType == EStarLabel)
    	{
    	int p = aText.Locate('(');
    	if (p != KErrNotFound)
    		label.iText = aText.Left(p);
    	else
    		label.iText = aText;
    	}
    else
    	{
    	label.iText = aText;
    	}
    
	SetFont(ETinyFont);
    TInt height = FontHeight();
    TInt width = TextWidth(label.iText);
    TSize size(width, height);
	
    label.iRect.SetRect(aPoint, size);
    label.iType = aType;
    
    iLabels.Append(label);
	}

void CLabelContainer::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	
	//clear all the old labels
	TRgb black(KRgbBlack);
	black.SetAlpha(0);
	gc.SetBrushColor(NM(black));
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	gc.Clear();
	

	//draw the cross 
	if (gContext->InSpace())
		{
		gc.SetPenColor(gContext->DrawDot() ? NM(KRgbWhite) : NM(KRgbCyan));
		TInt s=5;	//length of one "arm" of the cross
		TPoint p(aRect.Width()/2, (aRect.Height()+KBarHeight)/2);	//mid point
		gc.MoveTo(p);
		gc.MoveBy(TPoint(-s, 0));
		gc.DrawLineBy(TPoint(s*2+1, 0));
		gc.MoveBy(TPoint(-s-1, -s));
		gc.DrawLineBy(TPoint(0, s*2+1));
		}

	//background for each label
	TRgb darkGrey(KBcLabel);
	darkGrey.SetAlpha(100);
	gc.SetBrushColor(NM(darkGrey));

	UseFont(gc, ETinyFont);

	//draw them
	for (TInt i=0; i<iLabels.Count(); i++)
		{
		const TLabel& l = iLabels[i];
		gc.SetPenColor(NM(darkGrey));
		gc.DrawRect(l.iRect);
		if (l.iType == EPlanetLabel)
			gc.SetPenColor(NM(KFcPlanetLabel));
		else if (l.iType == EStarLabel)
				gc.SetPenColor(NM(KFcStarLabel));
		else
			//gc.SetPenColor(NM(TRgb(0,180,180)));
			gc.SetPenColor(NM(KFcConstellationLabel));
		DrawText(l.iRect, l.iText, ECenter);
		}
	
	gc.DiscardFont();
	}

void CLabelContainer::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	gAppUiCb->HandlePointerEventL(aPointerEvent);
	}
