/*
 * ListBoxWidget.cpp
 *
 *  Created on: 27-Apr-2010
 *      Author: Damien
 */

#include "ListBoxWidget.h"
#include "CommonTypes.h"

#include "context.h"
#include "colours.h"
extern TColours gColours;

extern Context* gContext;
extern CFloatingWindowGroup* gGroup;

const TInt KShadow = 6;

CListBoxWidgetNM* CListBoxWidgetNM::NewL(CListBoxWidget* aListBox)
	{
	CListBoxWidgetNM* self = new (ELeave) CListBoxWidgetNM(aListBox);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

void CListBoxWidgetNM::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
	Window().SetOrdinalPosition(0, ECoeWinPriorityAlwaysAtFront+2);
	Window().SetTransparencyAlphaChannel();
	}

CListBoxWidgetNM::CListBoxWidgetNM(CListBoxWidget* aListBox)
	: iListBox(aListBox)
	{
	}

void CListBoxWidgetNM::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	gc.SetPenStyle(CGraphicsContext::ESolidPen);
	TRgb red(196,0,0,ALPHA);
	gc.SetBrushColor(red);
	gc.SetPenColor(red);
	gc.DrawRoundRect(aRect, TSize(12, 12));
	}

void CListBoxWidgetNM::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	iListBox->Pointer(aPointerEvent);
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

CListBoxWidget* CListBoxWidget::NewL(const TRect& aRect, CCoeControl* aParent, MListBoxWidgetObserver& aObserver)
	{
	CListBoxWidget* self = new (ELeave) CListBoxWidget(aRect, aObserver);
	CleanupStack::PushL(self);
	self->ConstructL(aParent);
	CleanupStack::Pop(self);    
	return self;
	}

CListBoxWidget::~CListBoxWidget()
	{
	
	delete iListBox;
	iListBox = NULL;

	delete iNM;
	iNM = NULL;
	
	if (iItemList)
		iItemList->Reset();
	delete iItemList;
	iItemList = NULL;
	}

void CListBoxWidget::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	
	gc.SetPenStyle(CGraphicsContext::ESolidPen);
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);

	if (iShadow)
		{
		TRgb shadow(0,0,0,ALPHA);
		gc.SetPenColor(NM(shadow));	
		gc.SetBrushColor(NM(shadow));
		TPoint p(aRect.iTl);
		p += TPoint(iShadow, iShadow);
		gc.DrawRect(TRect(p, iSize));
		}
	}
	
TInt CListBoxWidget::CountComponentControls() const
	{
	return 1;
	}

CCoeControl* CListBoxWidget::ComponentControl(TInt /*aIndex*/) const
	{
	return iListBox;
	}
	
void CListBoxWidget::ResetItems()
	{
	iItemList->Reset();
	}

void CListBoxWidget::AddItemL(const TDesC& aItem)
	{
	iItemList->AppendL(aItem);	
	}

void CListBoxWidget::HandleListBoxEventL(CEikListBox* aListBox, MEikListBoxObserver::TListBoxEvent aEventType)
	{
	if (aEventType == MEikListBoxObserver::EEventItemClicked)
		{
		iObserver.Selected(aListBox->CurrentItemIndex());
		}
	}

CListBoxWidget::CListBoxWidget(const TRect& aRect, MListBoxWidgetObserver& aObserver)
	: iObserver(aObserver)
	{
	SetRect(aRect);
	}

void CListBoxWidget::SetExtentAndActivateL(const TRect& aRect)
	{
	iListBox->View()->SetItemHeight(CEikonEnv::Static()->NormalFont()->HeightInPixels()*2);
	AddScrollBarL(aRect);
	
	iListBox->SetExtent(TPoint(0,0), aRect.Size());
	
	
	TRect rect(aRect);
	
	if (iParent)	//has a parent window. Size is fixed.
		{
		iSize = TSize(aRect.Width() - iShadow, aRect.Height() - iShadow);
		iListBox->SetExtent(aRect.iTl, iSize);
		}
	else
		{
		TInt h = iListBox->ItemHeight() * (iItemList->Count()+1);
		if (h < aRect.Height())
			rect.SetHeight(h);
		SetRect(rect);
		iSize = TSize(rect.Width(), rect.Height());
		iListBox->SetExtent(TPoint(0,0), iSize);
		}
	
	TPoint p(iWindowRect.iTl);	//the parent window's abs TL
	p += rect.iTl;
	
	for (TInt i =0; i<EColorNumberOfLogicalColors; i++)
		iListBox->OverrideColorL(i, gContext->NightMode() ? KRgbBlack : KRgbWhite);
	
	iListBox->View()->SetBackColor(KRgbBlack);
	iListBox->HandleResourceChange(KEikColorResourceChange);
	
	iListBox->ActivateL();
	
	ScrollBarNightModeL();
	
	if (iListBox->iHasScrollBar)
		iListBox->iScrollBar = iListBox->ScrollBarFrame()->GetScrollBarHandle(CEikScrollBar::EVertical);
	
	SetVisible(ETrue);
	iListBox->DrawNow();
	}

void CListBoxWidget::Pointer(const TPointerEvent &aPointerEvent)
	{
	CEikScrollBar* sb = iListBox->ScrollBarFrame()->GetScrollBarHandle(CEikScrollBar::EVertical);
	sb->HandlePointerEventL(aPointerEvent);
	}

void CListBoxWidget::ScrollBarNightModeL()
	{
	if (iListBox->iHasScrollBar && gContext->NightMode())
		{
		CEikScrollBar* sb = iListBox->ScrollBarFrame()->GetScrollBarHandle(CEikScrollBar::EVertical);
		
		TPoint p(0,0);
		p += Window().AbsPosition();
		p += iListBox->Rect().iTl;
		p += TPoint(iListBox->Rect().Width(), 0);
		p -= TPoint(sb->Rect().Width(), 0);
		 
		TRect rect(p, sb->Rect().Size());
		iNM->SetRect(rect);	
		iNM->MakeVisible(ETrue);
		iNM->ActivateL();
		iNM->DrawNow();
		}
	else
		{
		if (iNM)
			iNM->MakeVisible(EFalse);
		}
	}

void CListBoxWidget::AddScrollBarL(const TRect& aRect)
	{
	TInt h = iListBox->View()->ItemHeight() * (iItemList->Count()+1);
	
	if (h > aRect.Height())
		{
		iListBox->CreateScrollBarFrameL(ETrue);
		iListBox->ScrollBarFrame()->SetScrollBarVisibilityL( CEikScrollBarFrame::EOff, CEikScrollBarFrame::EOn );
		iListBox->iHasScrollBar = ETrue;
		}
	}

void CListBoxWidget::ConstructL(CCoeControl* aParent)
	{
	iParent = aParent != NULL;
	if (aParent)
		{
		iShadow = KShadow;
		SetContainerWindowL(*aParent);
		}
	else
		{
		iShadow = 0;
		CreateWindowL(gGroup->Group());
		Window().SetOrdinalPosition(0, ECoeWinPriorityAlwaysAtFront+1);
		}
	
	iItemList = new (ELeave) CDesCArrayFlat(32);      
	iListBox = new (ELeave) CMyListBox();
	iListBox->ConstructL(this);     
	iListBox->SetContainerWindowL(*this);
	iListBox->SetListBoxObserver(this);
	
	if (gContext->NightMode())
		iNM = CListBoxWidgetNM::NewL(this);
	}

void CListBoxWidget::ItemsAddedL(int aCurrent)
	{
	iListBox->HandleItemRemovalL();
	iListBox->Model()->SetItemTextArray(iItemList);     
	iListBox->Model()->SetOwnershipType(ELbmDoesNotOwnItemArray);      
	iListBox->SetCurrentItemIndex(0);    
	iListBox->SetFocus(ETrue);    
	iListBox->HandleItemAdditionL();
	iListBox->SetCurrentItemIndex(aCurrent);
	}

void CListBoxWidget::KeyEvent(TUint aChar)
	{
	TInt index = iListBox->CurrentItemIndex();
	TInt count = iItemList->Count();
	
	TInt  key(aChar);
	
	TBool update = EFalse;

	if (key == 5)	//turn space key code into 32
		key = 32;
	
	if (key == 1)	//delete
			{
			if (iSearchString.Length())
				{
				iSearchString.SetLength(iSearchString.Length()-1);
				if (iSearchString.Length() == 0)
					{
					iListBox->SetCurrentItemIndex(0);
					update = ETrue;
					key = 0;
					}
				}
			}

	TInt l = iSearchString.Length();
	
	if ((TChar(key).IsAlpha() || key == 32) && l < 256)
		{
		iSearchString.Append(TChar(key));
		l++;
		}
	
	if (TChar(key).IsAlpha() || key == 32 || key == 1 )
		{
		for (TInt i=0; i<count; i++)
			{
			TPtrC d = iItemList->MdcaPoint(i);
			TInt ll = l;
			if (ll>d.Length()-1)
				ll = d.Length()-1;
		
			
			if (iSearchString.Left(ll).CompareF(d.Left(ll)) == 0)
				{
				iListBox->SetCurrentItemIndex(i);
				update = ETrue;
				break;
				}
			}
		if (!update)
			iSearchString.SetLength(iSearchString.Length()-1);
		}

	else if (key == 17)	//down
		{
		if (index < count-1)
			{
			iListBox->SetCurrentItemIndex(++index);
			update = ETrue;
			}
		}
	
	else if (key == 16)	//up
		{
		if (index > 0)
			{
			iListBox->SetCurrentItemIndex(--index);
			update = ETrue;
			}
		}
	
	else if (key == 3 || key == 167)		//return
		{
		iObserver.Selected(iListBox->CurrentItemIndex());
		}
	
	if (update)
		iListBox->HandleItemAdditionL();
	}


CMyListItemDrawer::CMyListItemDrawer(const CEikTextListBox& aListBox)
 :CListItemDrawer(), iListBox(aListBox)	
	 {	// Store a GC for later use
	 iGc = &CCoeEnv::Static()->SystemGc();	
	 SetGc(iGc);
	iFont = CEikonEnv::Static()->NormalFont();
	// iFont = gGroup->BigFont();
	 }

void CMyListItemDrawer::DrawActualItem(TInt aItemIndex, 
		const TRect& aActualItemRect,	  
		TBool aItemIsCurrent, 
		TBool /*aViewIsEmphasized*/, 
		TBool /*aViewIsDimmed*/, 
		TBool /*aItemIsSelected*/) const
	{
	TRect rect(aActualItemRect);
	
	CMyListBox* lb = (CMyListBox*)&iListBox;
	if (lb->iScrollBar)
		rect.SetWidth(rect.Width() - lb->iScrollBar->Rect().Width());

	iGc->UseFont(iFont);
	iGc->SetBrushStyle(CGraphicsContext::ESolidBrush);
	iGc->SetPenStyle(CGraphicsContext::ESolidPen);
	if (gContext->NightMode())
		{
		iGc->SetPenColor(KRgbBlack);
		iGc->SetBrushColor(KRgbBlack);
		iGc->DrawRect(aActualItemRect);
		
		if (!aItemIsCurrent)
			{
			iGc->SetPenColor(KNightRed);
			iGc->SetBrushColor(KRgbBlack);
			}
		else
			{
			iGc->SetPenColor(KRgbBlack);
			iGc->SetBrushColor(KNightRed);
			}
		}
	else
		{
		iGc->SetPenColor(KRgbWhite);
		iGc->SetBrushColor((KRgbWhite));
		iGc->DrawRect(aActualItemRect);

		if (!aItemIsCurrent)
			{
			iGc->SetPenColor(KRgbBlack);
			iGc->SetBrushColor(KRgbWhite);
			}
		else
			{
			iGc->SetPenColor(KFcCurrentItem);
			iGc->SetBrushColor(KBcCurrentItem);
			}
		}
	
	TPtrC itemText = iListBox.Model()->ItemText(aItemIndex);	
	TInt baseline = (rect.Height() - iFont->HeightInPixels()) / 2	 + iFont->AscentInPixels();
	iGc->DrawText(itemText, rect, baseline);
	}

void CListBoxWidget::SetVisible(TBool aTf)
	{
	MakeVisible(aTf);
	if (iNM)
		iNM->MakeVisible(aTf);
	}
	
