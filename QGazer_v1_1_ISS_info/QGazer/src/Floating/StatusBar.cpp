/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */
#include <e32std.h>
#include <gdi.h>


#include "StatusBar.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "CommonTypes.h"
#include "QGazerApp.h"

#include "colours.h"
extern TColours gColours;

extern CQGazerApp* gApp;
extern MAppUiCb* gAppUiCb;
extern Context* gContext;

//#include "model.h"
//extern CSkyModel* gModel;
//int bollox = 0;
void CCompassDisplay::Draw(const TRect& aRect) const
	{
	if (gContext->InSpace())
		return;
	
	CWindowGc& gc = SystemGc();
	TRect rect(aRect);
	
	if (iShakePhone)
		{
		TInt w = iFont->TextWidthInPixels(_L("Shake phone")) + 4;
		TInt h = iFont->HeightInPixels() + 4;
		rect.Shrink((rect.Width() - w)/2, (rect.Height() - h)/2);
		gc.SetPenColor(NM(KFcShakePhone));
		gc.UseFont(iFont);
		TPoint p(rect.iTl);
		p += TPoint(0, iFont->HeightInPixels() - iFont->DescentInPixels());
		gc.DrawText(_L("Shake phone"), p);
		gc.DiscardFont();
		return;
		}
	
	rect.Shrink((rect.Width() - KCompassTotalWidth)/2, 0);
	
	gc.SetBrushStyle(CGraphicsContext::ENullBrush);
	TInt health = gContext->CompassHealth();
	if (health == 0)
		gc.SetPenColor(NM(KRgbRed));
	else if (health == 1)
		gc.SetPenColor(NM(TRgb(0x0080ff)));
	else if (health == 2)
		gc.SetPenColor(NM(KRgbYellow));
	else
		gc.SetPenColor(NM(KRgbDarkGreen));

	//gc.SetPenColor(NM(KRgbDarkGreen)); //SCREENSHOTS!
	
	TInt p=KCompassPenSize;
	rect.Shrink(1, 1);
	gc.SetPenSize(TSize(p, p));
	gc.DrawRect(rect);
	p--;
	rect.Shrink(p, p);
	
	TInt xPos = TInt(gContext->Camera().Y() * 4.0 / 9.0);
	
//if (++bollox%10 == 0)
//	TDebug::Print(gContext->Camera().Y(), gModel->Sun().iHr.iAzimuth);
	TSize compassBmSize(KCompassBmWidth, KCompassBmHeight);
	rect.SetSize(compassBmSize);
	TRect source(TPoint(xPos, 0), compassBmSize);
	gc.BitBlt(rect.iTl, iBm, source);

	gc.SetPenSize(TSize(1,1));
	gc.SetPenColor(NM(KRgbBlack));
	TInt lineLength = 9;
	xPos = rect.iTl.iX + compassBmSize.iWidth / 2 - lineLength / 2;
	for (TInt i=0; i<5; i++)
		{
		gc.MoveTo(TPoint(xPos++, rect.iTl.iY + i));
		gc.DrawLineBy(TPoint(lineLength, 0));
		lineLength -= 2;
		}
	}

CCompassDisplay* CCompassDisplay::NewL(const CFont* aFont)
	{
	CCompassDisplay* self=new (ELeave) CCompassDisplay(aFont);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CCompassDisplay::CCompassDisplay(const CFont* aFont)
	: iFont(aFont)
	{
	
	}

CCompassDisplay::~CCompassDisplay()
	{
	
	delete iBm;
	}

void CCompassDisplay::ConstructL()
	{
	LoadBitmapsL();
	}


void CCompassDisplay::LoadBitmapsL()
	{
	delete iBm;
	iBm = NULL;
	iBm = new (ELeave) CFbsBitmap();
	if (gContext->NightMode())
		User::LeaveIfError(iBm->Load(gApp->FullResourcePath(_L("qgazermiscred.mbm")), 0));
	else
		User::LeaveIfError(iBm->Load(gApp->FullResourcePath(_L("qgazermisc.mbm")), 0));
	}

CStatusBar* CStatusBar::NewL(const TRect& aRect, MAppUiCb& aObserver)
	{	
	CStatusBar* self = new (ELeave) CStatusBar(aRect, aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CStatusBar::CStatusBar(const TRect& aRect,  MAppUiCb& aObserver)
	: iObserver(aObserver)
	{
	iClientRect = aRect;
	}

void CStatusBar::LoadBitmapsL()
	{
	delete iInfoBm;
	iInfoBm = NULL;
	iInfoBm = new (ELeave) CFbsBitmap();
	if (gContext->NightMode())
		User::LeaveIfError(iInfoBm->Load(gApp->FullResourcePath(_L("qgazermiscred.mbm")), 3));
	else
		User::LeaveIfError(iInfoBm->Load(gApp->FullResourcePath(_L("qgazermisc.mbm")), 3));
	
	iCompassDisplay->LoadBitmapsL();
	}

void CStatusBar::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
    MakeVisible(EFalse);
    iCompassDisplay = CCompassDisplay::NewL(TinyFont());
    LayoutL();
  	LoadBitmapsL();
	}

void CStatusBar::LayoutL()
	{
    TInt width = iClientRect.Width();	
    TInt height = iClientRect.Height();
    
    TRect rect(TPoint(0,0), TSize(width, KBarHeight));
    rect.Move(0, height - KBarHeight);
    SetRect(rect);
	iSaveRect = rect;
	
	BarLayoutL();
	
    iCompassDisplay->SetContainerWindowL(*this);
    iCompassDisplay->MakeVisible(ETrue);
    iCompassDisplay->ActivateL();
    }

CStatusBar::~CStatusBar()
	{
	
	delete iCompassDisplay;
	iCompassDisplay = NULL;
	delete iInfoBm;
	iInfoBm = NULL;
	}

void CStatusBar::BarLayoutL()
	{
	TInt width = Rect().Width();
	TInt height = Rect().Height();
	
	TSize size(KOptionsWidth, height);
	iInfoRect = TRect(TPoint(0,0), size);
	
	iGpsRect = iInfoRect;
	iGpsRect.Move(iInfoRect.Width(), 0);
	
	iMenuRect = iInfoRect;
	iMenuRect.Move(width - KOptionsWidth, 0);
	
	TInt remWidth = width - KOptionsWidth * 3;
	TInt x = KOptionsWidth * 2;
	
	size.SetSize(remWidth / 2, height);
	iLocRect = TRect(TPoint(x, 0), size);
	
	TInt tw = gContext->InSpace() ? 5 : 3;
		
	x = KOptionsWidth*2 + iLocRect.Width();
	size.SetSize(remWidth * tw / 10, height);
	iTimeRect = TRect(TPoint(x, 0), size);
	
	if (!gContext->InSpace())
		{
		x = KOptionsWidth * 2 + iLocRect.Width() + iTimeRect.Width();
		size.SetSize(remWidth * 2 / 10, height);
		iCompassRect = TRect(TPoint(x, 0), size);
		}

	iGpsRect.Move(Rect().iTl); 
	iLocRect.Move(Rect().iTl);
	iTimeRect.Move(Rect().iTl);
	iCompassRect.Move(Rect().iTl);
	iMenuRect.Move(Rect().iTl);
	
	//Must make compass rect exact
//	iCompassRect.Shrink((iCompassRect.Width() - KCompassTotalWidth)/2, 0);
	iCompassDisplay->SetRect(iCompassRect);
	}

void CStatusBar::DrawLocation(CWindowGc& aGc, const TRect& aRect) const
	{
	TRect r(aRect);
	
	TInt dist;
	const TLocation& loc = gContext->Location(dist);
	dist /= 1000;
	
	TBldStr::Set(loc.String());
	TBldStr::Add(_L(" ("));
	TBldStr::Add(loc.City());
	if (dist > 5)
		{
		TBldStr::Add(':');
		TBldStr::AddNum(dist);
		TBldStr::Add(_L("km"));
		}
	TBldStr::Add(')');
	TInt width = r.Width() - TextWidth(TBldStr::String());
	r.Shrink(width/2, 0);
	
	TBldStr::Set(loc.String());
	TBldStr::Add(_L(" ("));
	aGc.SetPenColor(NM(iFgColor));
	DrawText(r, TBldStr::String());
	r.Move(TextWidth(TBldStr::String()), 0);
	TBldStr::Set(loc.City());
	aGc.SetPenColor(NM(KFcCityName));
	if (dist>5)
		{
		TBldStr::Add(TChar(':'));
		TBldStr::AddNum(dist);
		TBldStr::Add(_L("km"));
		}
	DrawText(r, TBldStr::String());
	r.Move(TextWidth(TBldStr::String()), 0);
	TBldStr::Set(')');
	aGc.SetPenColor(NM(iFgColor));
	DrawText(r, TBldStr::String());
	}

void CStatusBar::DrawTime(CWindowGc& aGc, const TRect& aRect) const
	{
	TRect r(aRect);
	
	TBldStr::Set(gContext->TimeString());
	
	TInt width = r.Width() - TextWidth(TBldStr::String());
	r.Shrink(width/2, 0);

	aGc.SetPenColor(NM(iFgColor));
	DrawText(r, TBldStr::String());
	}

void CStatusBar::DrawOptions(CWindowGc& aGc, const TRect& aRect) const
	{
	aGc.SetPenSize(TSize(3, 3));
	aGc.SetPenColor(NM(iFgColor));

	TInt dy = aRect.Height() /4;
	TInt x = aRect.Width() / 5;
	TInt y = 0;
	TInt lineLength = x * 3;

	for (TInt i=0; i<3; i++)
		{
		y += dy;
		TPoint p1(x, y);
		TPoint p2(x+lineLength-1, y);
		p1 += aRect.iTl;
		p2 += aRect.iTl;
		aGc.DrawLine(p1, p2);
		}
	}

void CStatusBar::DrawGps(CWindowGc& aGc, const TRect& aRect) const
	{
	TGpsStatus gps(gContext->GpsStatus());
	
	if (gps == EGpsNotValid)
		aGc.SetPenColor(NM(KRgbRed));
	else if (gps == EGpsValid)
		aGc.SetPenColor(NM(KRgbDarkGreen));
	else
		aGc.SetPenColor(NM(KRgbYellow));

	//aGc.SetPenColor(NM(KRgbDarkGreen));//TODO //SCREENSHOTS!
	
	_LIT(KGps,"GPS");
	DrawText(aRect, KGps(), ECenter);
	}

//void Text()
void CStatusBar::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);

	UseFont(gc, ETinyFont);
	
	DrawInfo(gc, iInfoRect);
	DrawGps(gc, iGpsRect);
	DrawLocation(gc, iLocRect);
	DrawTime(gc, iTimeRect);
	DrawOptions(gc, iMenuRect);
	
	gc.DiscardFont();	
	}




void CStatusBar::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	if (aPointerEvent.iType == TPointerEvent::EButton1Down)
		{
		if (gContext->InSpace() && iLocRect.Contains(aPointerEvent.iPosition))
			gAppUiCb->MenuCommandL(EStatusBarLocationPressed);
		else if (iInfoRect.Contains(aPointerEvent.iPosition))
			gAppUiCb->SplashWindowL(ETrue, ETrue);
		else
			gAppUiCb->MenuWindowL();
		}
	}

TInt CStatusBar::CountComponentControls() const
	{

	return 1;
	}


CCoeControl* CStatusBar::ComponentControl(TInt aIndex) const
	{
	
	return iCompassDisplay;
	}

void CStatusBar::UpdateBearing()
	{
	iCompassDisplay->DrawNow();
	}

void CStatusBar::SetFlashing(TBool aTf)
	{
	iFlashing = aTf;
	if (!iFlashing)
		{
		iCompassDisplay->SetShakePhone(EFalse);
		iCompassDisplay->DrawNow();
		}
	}

TBool CStatusBar::Flash(TInt /*aAlpha*/)
	{
	if (iFlashing)
		{
		TInt health = gContext->CompassHealth();
		if (health >= 2)
			{
			iFlashing = EFalse;
			iCompassDisplay->SetShakePhone(EFalse);
			iCompassDisplay->DrawNow();
			return EFalse;
			}
		else
			{
			TInt rate = health < 2 ? 10 : 15;
			TBool shake = (iShakeCount++ % rate < 5);
			iCompassDisplay->SetShakePhone(shake);
			iCompassDisplay->DrawNow();
			return ETrue;
			}
		}
	return EFalse;
	}

void CStatusBar::DrawInfo(CWindowGc& aGc, const TRect& aRect) const
	{
	TPoint p((aRect.Width()-KInfoBmWidth)/2, (aRect.Height()-KInfoBmHeight)/2);
	aGc.BitBlt(aRect.iTl, iInfoBm);
	}
