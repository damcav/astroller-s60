/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#include <e32std.h>
#include <gdi.h>

#include "DropDownInput.h"
#include "LBContainer.h"

#include "TextButton.h"

#include "colours.h"

extern MAppUiCb* gAppUiCb;
extern Context* gContext;

void CDropDownInput::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
	Window().SetTransparencyFactor(TRgb(232,232,232));
    MakeVisible(EFalse);
	}

CDropDownInput::~CDropDownInput()
	{
	iButtons.ResetAndDestroy();
	iButtons.Close();
	
	iSelections.Reset();
	
	delete iListBoxWidget;
	iListBoxWidget = NULL;
	}

void CDropDownInput::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	/*
	if (BackgroundTapped(aPointerEvent))
		{
		if (iListBoxWidget)
			{
			delete iListBoxWidget;
			iListBoxWidget = NULL;
			}
		else
			{
			iObserver.ListBoxOkCancelL(iId, EFalse);
			}
		}
	*/
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

TInt CDropDownInput::CountComponentControls() const
	{

	TInt n = iButtons.Count();
	if (iListBoxWidget)
		n++;
	return n;
	}

CCoeControl* CDropDownInput::ComponentControl(TInt aIndex) const
	{
	if (aIndex < iButtons.Count())
		return iButtons[aIndex];
	else return iListBoxWidget;
	}

void CDropDownInput::Selected(TInt aIndex)
	{
	iListBoxWidget->SetVisible(EFalse);

	iSelected = aIndex;
	iSelections[iCurrentList] = iSelected;
	SetBoxL(iCurrentList, ESetFromListBox);
	
	HighlightCurrentButton();
	}

void CDropDownInput::InitListBoxL()    
	{    
	// construct listbox item array
	delete iListBoxWidget;
	iListBoxWidget = NULL;
	
	iListBoxWidget = CListBoxWidget::NewL(iAppRect, NULL, *this);
	
	iListBoxWidget->ResetItems();
	SetBoxL(iCurrentList, EPopulateListBox);
	
	if (iListBoxWidget->ItemCount() < 2)
		{
		//DrawNow();	//get rid of highlight
		delete iListBoxWidget;
		iListBoxWidget = NULL;
		return;
		}
	
	iListBoxWidget->SetVisible(ETrue);
	iListBoxWidget->ActivateL();	//TODO if I don't do this here, it doesn't appear ever. Why?
	
	TInt height = Rect().Height();
	
	TRect butRect = FindButtonById(iCurrentList)->Rect();
	
	TInt width = butRect.Width();
	TPoint tl;
	tl = TPoint(butRect.iTl.iX, 0);

	if (tl.iX + butRect.Width() >= iAppRect.iBr.iX / 2) 
		tl -= TPoint(width, 0);
	else
		tl += TPoint(butRect.Width(), 0);

	
	tl += iAbsRect.iTl;
	TInt dx = tl.iX + width - iAppRect.iBr.iX;
	if (dx > 0)
		{
		tl -= TPoint(dx, 0);
		}

	int current = InitialSelection(); //iListBoxWidget->ItemCount()/2;
	
	iListBoxWidget->ItemsAddedL(current);
	iListBoxWidget->SetExtentAndActivateL(TRect(tl, TSize(width, height)));
	}

int CDropDownInput::InitialSelection()
	{
	return iListBoxWidget->ItemCount()/2;
	}

void CDropDownInput::KeyEvent(TUint aChar)
	{
	if (iListBoxWidget)
		iListBoxWidget->KeyEvent(aChar);
	}

void CDropDownInput::KeyPressedL(TInt aId)
	{
	TInt b = aId;
	if (aId == 0)
		{
		TBool cancel = !iListBoxWidget;	//True if no widget
		if (!cancel)
			cancel = !iListBoxWidget->IsVisible();
		
		if (cancel)
			iObserver.ListBoxOkCancelL(iId, ETrue);
		}
	else
		{
		TBool display = !iListBoxWidget;
		if (!display)
			display = !iListBoxWidget->IsVisible();
		
		if (display)
			{
			iCurrentList = b;
			HighlightCurrentButton();
			InitListBoxL();
			}
		}
	}

void CDropDownInput::HighlightCurrentButton()
	{
	for (TInt i=0; i<iButtons.Count(); i++)
		{
		iButtons[i]->SetHighlighted(EFalse);
		iButtons[i]->DrawDeferred();
		}
	}

void CDropDownInput::AddItemL(const TDesC& aItem)
	{
	iListBoxWidget->AddItemL(aItem);	
	}

CTextButton* CDropDownInput::AddButtonL(TInt aId)
	{
	//TDebug::Print(iButtons[i]->Id());
	
	CTextButton* button = CTextButton::NewL(*this, aId);
	iButtons.Append(button);	
	button->SetContainerWindowL(*this);		
	iSelections.Append(0);
	button->SetColours(KFcStandardButton, KBcStandardButton); //DADA
	return button;
	}

CTextDoubleButton* CDropDownInput::AddButtonL(TInt aId1, TInt aId2)
	{
	CTextDoubleButton* button = CTextDoubleButton::NewL(*this, aId1, aId2);
	iButtons.Append(button);	
	button->SetContainerWindowL(*this);		
	iSelections.Append(0);
	iSelections.Append(0);
	button->SetColours(KFcStandardButton, KBcStandardButton); //DADA
	button->SetColour2(KBcSplitButton);
	return button;
	}

void CDropDownInput::SetLabelText(TInt aLabel, const TDesC& aText)
	{
	TBool dubble;
	CTextButton* button = FindButtonById(aLabel, dubble);
	if (dubble)
		((CTextDoubleButton*)button)->SetText2(aText);
	else 
		button->SetText(aText);
	}


CTextButton* CDropDownInput::FindButtonById(TInt aId) const
	{
	for (TInt i=0; i<iButtons.Count(); i++)
		if (((iButtons[i]->Id() & 255) == aId)
			|| ((iButtons[i]->Id() >> 8) == aId))
			return iButtons[i];
	User::Invariant();
	return NULL;
	}

CTextButton* CDropDownInput::FindButtonById(TInt aId, TBool& aDouble)
	{
	
	for (TInt i=0; i<iButtons.Count(); i++)
		{

		if ((iButtons[i]->Id() & 255) == aId) //id1
			{
			aDouble = EFalse;
			return iButtons[i];
			}
		else if ((iButtons[i]->Id() >> 8) == aId) //id2
			{
			aDouble = ETrue;
			return iButtons[i];
			}
		}
	User::Invariant();
	return NULL;
	}

