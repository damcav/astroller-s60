/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#include <e32std.h>
#include <gdi.h>

#include <eikclb.h>   // for CEikColumnListBox 


#include "Slider.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "LBContainer.h"
#include "LBScroll.h"
#include "MAppUiCb.h"

#include "TextButton.h"

#include "colours.h"
extern TColours gColours;

extern MAppUiCb* gAppUiCb;


extern Context* gContext;

CSliderWindow* CSliderWindow::NewL(const TRect& aRect, TInt aId)
	{	
	CSliderWindow* self = new (ELeave) CSliderWindow(aRect);
	CleanupStack::PushL(self);
	self->ConstructL(aId);
	CleanupStack::Pop(self);    
	return self;
	}

CSliderWindow::CSliderWindow(const TRect& aRect)
	{
	TRect rect = aRect;
	TSize s(aRect.Width() / 7, aRect.Height());
	rect.SetSize(s);
	//TPoint p(aRect.Width() - rect.Width(), 0);
	TPoint p(TPoint(aRect.iBr.iX, aRect.iTl.iY));
	rect.SetRect(p, s);
	SetRect(rect);
	iSlidingRect = rect;
	iSliding = ETrue;
	}

void CSliderWindow::ConstructL(TInt aId)
	{
	iId = aId;
	CFloatingWindow::ConstructL(1, 3);
    MakeVisible(EFalse);

	TRect rect(Rect());
	rect.Shrink(Rect().Width()/4, Rect().Height()/10);
	//TInt x = rect.iTl.iX;
	//TPoint pos(x, Rect().Height()/10);
	//rect.SetRect(pos, rect.Size());
	iScroll = CLBScroll::NewL(rect, aId);
	iScroll->SetContainerWindowL(*this);
	
	/*
	rect = Rect();
	rect.Shrink(Rect().Width()/3, 0);
	rect.SetHeight(Rect().Height()/12);
	pos.SetXY(rect.iTl.iX, Rect().iBr.iY - Rect().Height()/12 - rect.Height());
	rect = TRect(pos, rect.Size());
	*/
	
	CreateSliderL();
    EnableDragEvents();
	}

void CSliderWindow::LayoutL()
	{
	}

CSliderWindow::~CSliderWindow()
	{
	delete iScroll;
	iScroll = 0;
	}

void CSliderWindow::Draw(const TRect& /*aRect*/) const
	{
	CWindowGc& gc = SystemGc();
	gc.SetPenStyle(CGraphicsContext::ESolidPen);
	gc.SetPenSize(TSize(iBorder, iBorder));
	gc.SetPenColor(NM(iBgColor));	
	gc.SetBrushColor(NM(iBgColor));
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	gc.DrawRect(Rect());
	}

void CSliderWindow::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	//if (BackgroundTapped(aPointerEvent))
	//		gAppUiCb->SliderOk(iId);
	
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

TInt CSliderWindow::CountComponentControls() const
	{
	return 1;
	}

CCoeControl* CSliderWindow::ComponentControl(TInt /*aIndex*/) const
	{
	return iScroll;
	}

void CSliderWindow::Refresh()
	{
	MakeVisible(ETrue);
	DrawNow();
	}
	
void CSliderWindow::Reset()
	{
	MakeVisible(EFalse);
	}

void CSliderWindow::CreateSliderL()
	{    
	}
	
TBool CSliderWindow::Flash(TInt /*aAlpha*/)
	{
	TInt dx = Rect().Width() / 3;
	if (iSliding && iSlidingCount++ < 3)
		{
		iSlidingRect.Move(-dx, 0);
		SetRect(iSlidingRect);
		
		DrawNow();
		return ETrue;
		}
	iSliding = EFalse;
	return EFalse;
	}

