/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */
#include <e32std.h>
#include <gdi.h>

#include "LabelWindow.h"
#include "MAppUiCb.h"
#include "Context.h"

#include "colours.h"
extern TColours gColours;

extern Context* gContext;
extern MAppUiCb* gAppUiCb;

const TInt KRingRadius = 10;
const TInt KRingThickness = 4;

CLabelWindow* CLabelWindow::NewL()
	{	
	CLabelWindow* self = new (ELeave) CLabelWindow();
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CLabelWindow::CLabelWindow()
	{
	}

void CLabelWindow::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
    MakeVisible(EFalse);
	}

CLabelWindow::~CLabelWindow()
	{
	}

void CLabelWindow::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	
	TRgb black(0,0,0);
	black.SetAlpha(0);
	gc.SetBrushColor(NM(black));
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	gc.Clear();
	
	TRect rect(aRect);

	TRgb colour(KBcCursorStandard);
	
	//if (gContext->FastOpenGL())
	//	colour = KRgbWhite;

	if (iCrossHair)
		{
		colour = KBcCursorCrossHair;
		if (iAlpha > 80)
			colour.SetAlpha(0);
		else
			colour.SetAlpha(255);
		}
	else	
		{
		colour.SetAlpha(iAlpha);
		}
	
	gc.SetPenColor(NM(colour));
	gc.SetPenSize(TSize(KRingThickness,KRingThickness));
	
	rect.Shrink(KRingThickness/2, KRingThickness/2);
	gc.DrawEllipse(rect);
	
	//gc.SetPenSize(TSize(1,1));
	//gc.MoveTo(rect.iTl);
	//gc.MoveBy(TPoint(rect.Width()/2, 0));
	//gc.DrawLineBy(TPoint(0, rect.Height()));
	//gc.MoveBy(TPoint(-rect.Width()/2, -rect.Height()/2));
	//gc.DrawLineBy(TPoint(rect.Width(), 0));
	}

void CLabelWindow::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	gAppUiCb->HandlePointerEventL(aPointerEvent);
	}

void CLabelWindow::Set(const TPoint& aPoint, TBool aCrossHair)
	{
	iPoint = aPoint;
	//SetTransparentAlpha();
	iCrossHair = aCrossHair;

	iSaveRect.SetRect(iPoint, TSize(1,1));
	iSaveRect.Move(-1, -1); //why?
	iSaveRect.Shrink(-KRingRadius, -KRingRadius);
	}

void CLabelWindow::Move(const TPoint& aDelta)
	{
	iPoint += aDelta;
	
	iSaveRect.SetRect(iPoint, TSize(1,1));
	iSaveRect.Move(-1, -1); //why?
	iSaveRect.Shrink(-KRingRadius, -KRingRadius);
	}

void CLabelWindow::Refresh()
	{
	SetRect(iSaveRect);
	MakeVisible(ETrue);
	iAlpha = 0;
	DrawNow();
	}
	
void CLabelWindow::Reset()
	{
	MakeVisible(EFalse);
	SetRect(TRect(0,0,0,0));
	}

TBool CLabelWindow::Flash(TInt aAlpha)
	{
	if (IsVisible())
		{
		TReal32 a(aAlpha);	//0..100
		a *= 2.55;
		iAlpha = TInt(a);//0..255
		
		DrawNow();
		return ETrue;
		}
	else
		{
		return EFalse;
		}
	}
