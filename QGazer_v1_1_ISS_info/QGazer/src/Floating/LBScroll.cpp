/*
 * LBControl.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */
#include <AknGlobalNote.h> 
#include "LBScroll.h"
#include "LBButton.h"

#include "colours.h"
extern TColours gColours;

#include "context.h"
extern Context* gContext;

CLBScroll* CLBScroll::NewL(const TRect& aRect, TInt aId)
	{
	CLBScroll* self = new (ELeave) CLBScroll(aRect);
	CleanupStack::PushL(self);
	self->ConstructL(aId);
	CleanupStack::Pop(self);
	return self;
	}

void CLBScroll::ConstructL(TInt aId)
	{
	iButton = CLBButton::NewL(Rect(), aId); 
	}

CLBScroll::CLBScroll(const TRect& aRect)
	//: CLBControlBm(aParent, aId, aBm)
	{
	SetRect(aRect);
	}

void CLBScroll::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	if (aPointerEvent.iType == TPointerEvent::EButton1Down)
		{
		if (!iButton->Rect().Contains(aPointerEvent.iPosition))
			{
			TInt h = iButton->Rect().Height();
			if (iButton->Rect().iBr.iY > aPointerEvent.iPosition.iY)
				h = -h;
			TPoint offset(0, h);
			iButton->MoveButton(offset);
			}
		}
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

void CLBScroll::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	TRect rect(aRect);
	rect.Shrink(rect.Width()*2/5, 0);
	gc.SetBrushColor(NM(KBcStarCountScroll));
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	gc.DrawRect(rect);
	}

TInt CLBScroll::CountComponentControls() const
	{
	return 1;
	}

CCoeControl* CLBScroll::ComponentControl(TInt aIndex) const
	{
	return iButton;
	}

CLBScroll::~CLBScroll()
	{
	delete iButton;
	}


