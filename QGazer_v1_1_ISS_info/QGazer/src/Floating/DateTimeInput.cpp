/*
 * FloatingKeypad.cpp
 *
 *  Created on: 14-Jan-2010
 *      Author: Damien
 */

#include <e32std.h>
#include <gdi.h>

#include "DateTimeInput.h"
#include "MAppUiCb.h"
#include "Context.h"
#include "LBContainer.h"
#include "Grid.h"

#include "colours.h"
#include "TextButton.h"

extern MAppUiCb* gAppUiCb;
extern Context* gContext;

CDateTimeInput* CDateTimeInput::NewL(const TRect& aRect, TInt aId, MListBoxCb& aObserver)
	{	
	CDateTimeInput* self = new (ELeave) CDateTimeInput(aRect, aId, aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);    
	return self;
	}

CDateTimeInput::CDateTimeInput(const TRect& aRect,  TInt aId, MListBoxCb& aObserver)
	: CDropDownInput(aObserver)
	{
	TRect rect = aRect;
	rect.Shrink(rect.Width()/12, rect.Height()/11);
	
	SetRect(rect);

	iAppRect = aRect;
	iAbsRect = rect;
	iId = aId;
	}

void CDateTimeInput::ConstructL()
	{
	CFloatingWindow::ConstructL(1, 3);
	Window().SetTransparencyFactor(TRgb(232,232,232));
    MakeVisible(EFalse);

    TRect rect(Rect());
	rect.Shrink(100,50);	//MAGIC
	
    LayoutL();
    iCurrentList = EDay;

    SetLabelsFromUtc();
    
    HighlightCurrentButton();
	}

void CDateTimeInput::LayoutL()
	{
	TGrid grid(Rect().Size());
	//TDebug::Print(Rect());
	grid.SetBorders(20, 20);
	grid.SetCellBorders(0, 4);
	grid.SetDim(7, 3, ECentre);

	grid.SetCellExtent(2, 1);

	CTextButton* button = AddButtonL(EDay);
	button->SetRect(grid.Rect(0, 1));
	button->SetStyle(EButtonStyleDate);
	button->SetFontId(EBigFont);
	
	grid.SetCellExtent(3, 1);	
	button = AddButtonL(EMonth);
	button->SetRect(grid.Rect(2, 1));
	button->SetFontId(ENormalFont);
	
	grid.SetCellExtent(2, 1);	
	button = AddButtonL(EYearHi, EYearLo);
	button->SetRect(grid.Rect(5, 1));
	button->SetFontId(EBigFont);
	
	button = AddButtonL(EHour, EMinute);
	button->SetRect(grid.Rect(4, 0));
	button->SetFontId(EBigFont);
	button->SetStyle(EButtonStyleTime);
	
	grid.SetCellExtent(3, 1);
	button = AddButtonL(ETimeZone);
	button->SetRect(grid.Rect(0, 0));
	button->SetFontId(ETinyFont);
	
	grid.SetCellExtent(2, 1);
	button = AddButtonL(EResetToNow);
	button->SetRect(grid.Rect(0, 2));
	button->SetFontId(ETinyFont);
	button->SetText(_L("Reset"));
	button->SetColours(KFcDtResetButton, KBcDtResetButton);
	button->SetAllowHighlighting(EFalse);
	
	grid.SetCellExtent(1, 1);	
	button = AddButtonL(EOk);
	button->SetColours(KFcDtOkButton, KBcDtOkButton);
	button->SetRect(grid.Rect(6, 2));
	button->SetFontId(ENormalFont);
	button->SetText(_L("Ok"));

	
	
#if(0)
	CTextButton* button = AddButtonL(ECountry);
	button->SetRect(grid.Rect(0, 1));
	button->SetFontId(ENormalFont);
	
	button = AddButtonL(ECity);
	button->SetRect(grid.Rect(3, 1));
	button->SetFontId(ENormalFont);

	grid.SetCellExtent(1, 1);

	button = AddButtonL(EOk);
	button->SetRect(grid.Rect(4, 3));
	button->SetFontId(ENormalFont);
	button->SetText(_L("Ok"));
#endif
	}
	
CDateTimeInput::~CDateTimeInput()
	{
	}

void CDateTimeInput::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	BaseDraw(gc, aRect);
	}

TDateTime CDateTimeInput::ReadTime()
	{
	TDateTime time;
	time.SetYear(iSelections[EYearHi] * 100 + iSelections[EYearLo]);
	time.SetMonth(TMonth(iSelections[EMonth]));
	time.SetDay(iSelections[EDay]);
	time.SetHour(iSelections[EHour]);
	time.SetMinute(iSelections[EMinute]);
	return time;
	}

void CDateTimeInput::SetLabelsFromUtc()
	{
	TInt tz;
	gContext->UserTime(iTime, tz);
	
	for (TInt i=EDay; i<=ETimeZone; i++)
		SetBoxL(i, ESetDefault);
	}

void CDateTimeInput::SetTime()
	{
	gContext->SetUserTime(ReadTime(), iSelections[ETimeZone]);
	}

void CDateTimeInput::SetBoxL(TInt aIndex, TInt aAction)	//called from InitListBoxL
	{
	if (aIndex == EResetToNow)
		{
		gContext->SetUserTimeToNow();
		SetLabelsFromUtc();
		return;
		}
		
	switch (aIndex)
		{
		case EDay:
			InitDaysL(aAction);
			break;
		case EMonth:
			InitMonthsL(aAction);
			break;
		case EYearHi:
			InitYearHighL(aAction);
			break;
		case EYearLo:
			InitYearLowL(aAction);
			break;
		case EHour:
			InitHoursL(aAction);
			break;
		case EMinute:
			InitMinutesL(aAction);
			break;
		case ETimeZone:
			InitTimeZoneL(aAction);
			break;
		case EOk:
			break;
		default:;
		}
	}


int CDateTimeInput::InitialSelection()
	{
	return iSelections[iCurrentList];
	}
	
TInt KMonthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 31, 31};

void CDateTimeInput::InitDaysL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=1; i<=KMonthDays[iSelections[EMonth]]; i++)
			{
			iBuf.Zero();
			iBuf.AppendNum(i);
			AddItemL(iBuf);
			}
		}
	
	else if (aAction == ESetFromListBox)
		{
		iBuf.Zero();
		
		iBuf.AppendNum(iSelections[EDay]+1);
		
		SetLabelText(EDay, iBuf);
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iBuf.Zero();
		iBuf.AppendNum(iTime.Day()+1);
		SetLabelText(EDay, iBuf);
		iSelections[EDay] = iTime.Day();
		}
	}

TPtrC KMonths[] = 
	{
		_S("January"),
		_S("February"),
		_S("March"),
		_S("April"),
		_S("May"),
		_S("June"),
		_S("July"),
		_S("August"),
		_S("September"),
		_S("October"),
		_S("November"),
		_S("December")
	};



void CDateTimeInput::InitMonthsL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<12; i++)
			AddItemL(KMonths[i]);
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EMonth, KMonths[iSelected]);

		if (iSelections[EDay]+1 > KMonthDays[iSelected] )
			{
			iSelections[EDay] =  KMonthDays[iSelected]-1;
			InitDaysL(ESetFromListBox);
			}
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		SetLabelText(EMonth, KMonths[iTime.Month()]);
		iSelections[EMonth] = iTime.Month();
		}
	}

void CDateTimeInput::InitYearLowL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<=99; i++)
			AddItemL(TBldStr::Num(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EYearLo, TBldStr::Num2(iSelected));
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iSelections[EYearLo] = iTime.Year() % 100;
		SetLabelText(EYearLo, TBldStr::Num2(iSelections[EYearLo]));
		}
	}

void CDateTimeInput::InitYearHighL(TInt aAction)
	{
	
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<30; i++)
			AddItemL(TBldStr::Num(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EYearHi, TBldStr::Num(iSelected));
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iSelections[EYearHi] = iTime.Year() / 100;
		SetLabelText(EYearHi, TBldStr::Num(iSelections[EYearHi]));		
		}
	}


void CDateTimeInput::InitHoursL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<24; i++)
			AddItemL(TBldStr::Num2(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(EHour, TBldStr::Num2(iSelected));
		SetTime();
		}
	
	else if (aAction == ESetDefault)
		{
		iSelections[EHour] = iTime.Hour();	//UTC;
		SetLabelText(EHour, TBldStr::Num2(iSelections[EHour]));
		}	
	}

void CDateTimeInput::InitMinutesL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		for (TInt i=0; i<60; i++)
			AddItemL(TBldStr::Num2(i));
		}
	
	else if (aAction == ESetFromListBox)
		{
		SetTime();
		SetLabelText(EMinute, TBldStr::Num2(iSelected));
		}
	
	else if (aAction == ESetDefault)
		{
		TInt m = iTime.Minute();
		iSelections[EMinute] = m;
		SetLabelText(EMinute, TBldStr::Num2(m));		
		}	
	}

const TDesC& CDateTimeInput::TimeZoneString(TInt aType)
	{
	iBuf.Zero();
	if (aType == ENewLocationTz)
		{
		iBuf.Append(_L("~"));
		iBuf.Append(gContext->UserLocation().City());
		iBuf.Append(_L(" time"));
		}
	else if (aType == EPhoneTz)
		{
		iBuf.Append(_L("Phone time"));
		}
	else
		{
		iBuf.Append(_L("Universal Time"));
		}
	return iBuf;
	}

void CDateTimeInput::InitTimeZoneL(TInt aAction)
	{
	if (aAction == EPopulateListBox)
		{
		AddItemL(TimeZoneString(EPhoneTz));
		AddItemL(TimeZoneString(EUniversalTz));
		AddItemL(TimeZoneString(ENewLocationTz));
		}
	else if (aAction == ESetFromListBox)
		{
		SetLabelText(ETimeZone, TimeZoneString(iSelections[ETimeZone]));
		gContext->SetTimeZone(TTimeZone(iSelections[ETimeZone]));
		
		SetLabelsFromUtc();	//doesn't change the time
		}
	else if (aAction == ESetDefault)
		{
		iSelections[ETimeZone] = gContext->TimeZone(); 
		SetLabelText(ETimeZone, TimeZoneString(iSelections[ETimeZone]));
		}
	}
