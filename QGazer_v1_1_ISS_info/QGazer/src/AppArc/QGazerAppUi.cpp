#include "QGazerAppUi.h"
#include "QGazerApp.h"
#include "QGazerContainer.h"
#include "QGazerDocument.h"
//#include <SimpleCube.rsg>
#include "QGazer.hrh"	
#include <EIKBTGPC.H>

#include <HLPLCH.H>
#include "QGazer.hlp.hrh"

#include "Context.h"
#include "StatusBar.h"
#include "LabelWindow.h"
#include "CCameraManager.h"
#include "CQSensors.h"
#include "InfoWindow.h"
#include "MenuWindow.h"
#include "ArticleWindow.h"

#include "model.h"
#include "ListBox.h"
#include "Slider.h"

#include "DateTimeInput.h"

#include "LocationInput.h"
#include "CLocationDateTime.h"

#include "HardSums.h"


#ifdef NEWLABELS
#include "LabelContainer.h"
#endif


extern CSkyModel* gModel;
extern CHardSums* gHardSums;

extern Context* gContext;
MAppUiCb* gAppUiCb;

#include <avkon.hrh>

extern CQGazerApp* gApp;

#define SHOWING(window) (window && !window->DeletePending())

void CMediaKeysTestUi::ConstructL(MAppUiCb* aParent)
	{
	iParent = aParent;
	iInterfaceSelector = CRemConInterfaceSelector::NewL();
	iCoreTarget = CRemConCoreApiTarget::NewL(*iInterfaceSelector, *this);
	iInterfaceSelector->OpenTargetL();
	}

 void CMediaKeysTestUi::MrccatoCommand(TRemConCoreApiOperationId aOperationId,
		 TRemConCoreApiButtonAction aButtonAct)
 {
	 switch( aOperationId )
	 {
	 case ERemConCoreApiVolumeUp:
		 iParent->Zoom(ETrue); 
		 break;
 
	 case ERemConCoreApiVolumeDown:
		 iParent->Zoom(EFalse);
		 break;
		 
	 default:
		 break;
	 }
 }

 CMediaKeysTestUi::~CMediaKeysTestUi()
 	{
 	delete iInterfaceSelector;
 	iInterfaceSelector = NULL;
 	}

 
void CQGazerAppUi::GLReadyL()
	{
	SUSHI("CQGazerAppUi::GLReadyL() 1");
	
	iReady = ETrue;
	SplashWindowL(EFalse, EFalse);
#ifdef NEWLABELS
	iLabelContainer = CLabelContainer::NewL(ApplicationRect());
	STOREWINDOW(iLabelContainer);
#endif

	iAllWindows->ActivateL();
	iCameraManager->SetSpace(TCamera(gContext->CurrentLocation()));
	gContext->SetDrawDot(ETrue);
	RefreshL();
	ResetCamera();
	Redraw();
	}

void CQGazerAppUi::ActivateIfReadyL(CFloatingWindow* aWindow)
	{
	if (iReady)
		aWindow->ActivateL();
	}

void CQGazerAppUi::ConstructL()
    {
    SUSHI("CQGazerAppUi::ConstructL() 1");
    
    gAppUiCb = this;
    BaseConstructL(0);
    iAllWindows = CQueue::NewL();

    SetOrientationL(EAppUiOrientationLandscape);
    
    SUSHI("CQGazerAppUi::ConstructL() 2");
    
    CEikButtonGroupContainer* cba = CEikButtonGroupContainer::Current();
    cba->DrawableWindow()->SetVisible(EFalse);
    
    iAppContainer = new (ELeave) CQGazerContainer(*this);
    iAppContainer->SetMopParent(this);
    iAppContainer->ConstructL( ClientRect(), ((CQGazerDocument*)Document())->Model() );
    AddToStackL( iAppContainer );
    
    SUSHI("CQGazerAppUi::ConstructL() 3");

    iSkyRect.SetRect(ApplicationRect().iTl, 
    	TSize(ApplicationRect().Width(), ApplicationRect().Height() - KBarHeight));

    iFloatingWindowGroup = CFloatingWindowGroup::NewL();

    SUSHI("CQGazerAppUi::ConstructL() 4");
   	
    iSensors = CQSensors::NewL(*this);
    
    SUSHI("CQGazerAppUi::ConstructL() 5");
    
    SplashWindowL(ETrue, EFalse);
    
    Model().InitL();
    
    SUSHI("CQGazerAppUi::ConstructL() 6");

    iStatusBar = CStatusBar::NewL(ApplicationRect(), *this);
    STOREWINDOW(iStatusBar);
    iStatusBar->MakeVisible(ETrue);
    
    SUSHI("CQGazerAppUi::ConstructL() 7");
    
	iCursor =  CLabelWindow::NewL();
	iCursor->Reset();
	iCursor->ActivateL();
	STOREWINDOW(iCursor);
    
    iCameraManager = new (ELeave) CCameraManager();

    SUSHI("CQGazerAppUi::ConstructL() 8");
    
    LoadBitmapsL();

    SUSHI("CQGazerAppUi::ConstructL() 9");
    
	SetViewAndStatusBarL();
    
	SUSHI("CQGazerAppUi::ConstructL() 10");

	iVolumeKeys = new (ELeave) CMediaKeysTestUi();
	iVolumeKeys->ConstructL(this);
	
	iCallback = new CCallback(*this);
	}

// ----------------------------------------------------
// CQGazerAppUi::~CQGazerAppUi()
// Destructor
// Frees reserved resources
// ----------------------------------------------------
//
void CQGazerAppUi::LoadBitmapsL()
	{
	delete iLightsOnBm;
	iLightsOnBm = NULL;
    iLightsOnBm = new (ELeave) CFbsBitmap();

    delete iLightsOffBm;
    iLightsOffBm = NULL;
    iLightsOffBm = new (ELeave) CFbsBitmap();
    
	delete iCrossHairOnBm;
	iCrossHairOnBm = NULL;
	iCrossHairOnBm = new (ELeave) CFbsBitmap();

    delete iCrossHairOffBm;
    iCrossHairOffBm = NULL;
    iCrossHairOffBm = new (ELeave) CFbsBitmap();
    
   	const TDesC& path(gContext->NightMode() ? 
			gApp->FullResourcePath(_L("qgazermiscred.mbm")) :
   			gApp->FullResourcePath(_L("qgazermisc.mbm"))); 
   	
   	User::LeaveIfError(iLightsOnBm->Load(path, 1));
   	User::LeaveIfError(iLightsOffBm->Load(path, 2));
   	User::LeaveIfError(iCrossHairOnBm->Load(path, 7));
   	User::LeaveIfError(iCrossHairOffBm->Load(path, 6));
	}

CQGazerAppUi::~CQGazerAppUi()
	{
#ifdef SATELLITE
	delete iDownload;
	iDownload = NULL;
#endif
	
	delete iAllWindows;
	iAllWindows = NULL;

    if ( iAppContainer )
        {
        RemoveFromStack( iAppContainer );
        delete iAppContainer;
        }
    
    delete iFloatingWindowGroup;
    iFloatingWindowGroup = NULL;
    
    delete iCameraManager;
    iCameraManager = NULL;
    
    delete iSensors;
    iSensors = NULL;
    
   	delete iLightsOnBm;
	delete iLightsOffBm;
	delete iCrossHairOnBm;
	delete iCrossHairOffBm; 	
	
	delete iVolumeKeys;
    iVolumeKeys = NULL;

	iOnScreenObjects.Reset();
	
	delete iDownload;
	iDownload = NULL;
	
	delete iCallback;
	iCallback = NULL;
	}

// ------------------------------------------------------------------------------
// CQGazerAppUi::::DynInitMenuPaneL(TInt aResourceId,CEikMenuPane* aMenuPane)
//  This function is called by the EIKON framework just before it displays
//  a menu pane. Its default implementation is empty, and by overriding it,
//  the application can set the state of menu items dynamically according
//  to the state of application data.
// ------------------------------------------------------------------------------
//
void CQGazerAppUi::DynInitMenuPaneL(
    TInt /*aResourceId*/,CEikMenuPane* /*aMenuPane*/)
    {
    }

void CQGazerAppUi::DestroyMenu()
	{
	DELETEWINDOW(iMenuWindow);
	}

void CQGazerAppUi::MenuCommandL(TInt aId)
	{
	//TBool destroy = EFalse;
	switch(aId)
		{
		case -1:
			//destroy = ETrue;
			break;
	
		case 100:	//an article has been chosen
		//	DestroyMenu();
			ArticleWindowL(iMenuWindow->ChosenArticle(), ETrue);
			break;
		
		case EShowPleaseWait:
			PleaseWaitWindowL(ETrue);
			break;
			
		case EHidePleaseWait:
			PleaseWaitWindowL(EFalse);
			break;
			
		case EQueueKIssPage:	//from the article window
			DELETEWINDOW(iArticleWindow);
			iCallback->CallMe(3);
			break;
			
		case EGlobeIcon:
			DestroyMenu();
			FlipModeL();
			//destroy = ETrue;
			break;

		case EIssIcon:
			gContext->SetShowIss(!gContext->ShowIss());
			Redraw();
			break;
			
		case EConstIcon:
			gContext->SetShowConstellations(!gContext->ShowConstellations());
			Redraw();
			break;
			
		case ELabelsIcon:
			gContext->SetShowLabels(!gContext->ShowLabels());
			Redraw();
			break;

		case EIssUpdateIcon:
			DestroyMenu();
			HandleDownloadL();
			//destroy = ETrue;
			break;
			
		case EFindIcon:
			ResetZoom();
			gContext->ClearChosenObject();
			DELETEWINDOWNOW(iInfoWindow);
			if (!gContext->InSpace())
				{
				gContext->SetCrossHairMode(EFalse);
				ClearCrossHair();
				}
			DestroyMenu();
			StartListBoxL(EFindListBox);
			break;
			
		case EAtmosIcon:
			gContext->SetShowAtmosphere(!gContext->ShowAtmosphere());
			Redraw();
			break;
			
		case EStarsIcon:
			DestroyMenu();
			StarsSliderStartL();
			break;
			
		case ELocIcon:
			DestroyMenu();
			StartListBoxL(ELocationListBox);
			WaitingForGpsButtonL(EFalse);
			break;
			
		case EVisitIcon:
			DestroyMenu();
			StartListBoxL(ELocationDateTime);
			break;	
			
		case EQuitIcon:
			gContext->SaveUserDataL();
			Exit();
			break;
			
		case ENightModeIcon:
			DestroyMenu();
			gContext->ToggleNightMode();
			iStatusBar->LoadBitmapsL();
			LoadBitmapsL();
			RefreshL();
			Redraw();
			break;
	
		case EStatusBarLocationPressed:
			iCameraManager->SetSpace(TCamera(gContext->Location()));
			gContext->SetDrawDot(ETrue);
			iCameraManager->Restore();
			gContext->SetDrawDot(ETrue);
			Redraw();
			break;
		
		case EIssPreviewIcon:
			DestroyMenu();
			ListBoxOkCancelL(ELocationDateTime, ETrue);
			break;
			
		default:
//			DestroyMenu();
			break;
		}
	}

void CQGazerAppUi::ReturnToHomeLocationL()
	{
	gContext->ClearChosenObject();
	ResetZoom();
	ReturnButtonL(EFalse);
	gContext->SetUserLocationMode(EFalse);
	gContext->SetUserTimeMode(EFalse);
	gContext->SetTimeZone(EPhoneTz);
	//gContext->UserLocation() = gContext->CurrentLocation();
	Model().UpdateCoords();
	//iCameraManager->SetSpace(TCamera(gContext->CurrentLocation()));
	gContext->SetDrawDot(ETrue);
	RefreshL();
	ResetCamera();
	Redraw();
	}

void CQGazerAppUi::VisitLocationL(TBool aUserTime)
	{
	gContext->SetUserLocationMode(ETrue);
	gContext->SetUserTimeMode(aUserTime);
	Model().UpdateCoords();
	iCameraManager->SetSpace(TCamera(gContext->UserLocation()));
	}

CArrayFix<TCoeHelpContext>* CQGazerAppUi::HelpContextL() const
	{
	return NULL;
	}

// ------
TBool CQGazerAppUi::Zoom(TBool aIn)
	{
	TBool changed = EFalse;
	TReal limit = 0;
	if (aIn)
		{
		if (gContext->InSpace())
			{
			if (gContext->ZoomEffectRunning())
				limit = 6.0;
			else
				limit = 4.4;
			}
		else
			{
			limit = 2.7;
			}
 
		if (gContext->Camera().Zoom() < limit)
			{
			changed = ETrue;
			gContext->Camera().AddZoom(0.30);
			}
		}
	else
		{
		if (gContext->Camera().Zoom() > 0.41)
			{
			gContext->Camera().AddZoom(-0.30);
			changed = ETrue;
			}
		}
		
	if (changed)
		{
		//TDebug::Print(gContext->Camera().Zoom());
		if (!gContext->InSpace())
				iAppContainer->QGazer().SetupView();
		RefreshL();
		Redraw();
		}
	return changed;
	}
//----------------------------------------------
// CQGazerAppUi::HandleKeyEventL(
//     const TKeyEvent& aKeyEvent,TEventCode /*aType*/)
// Key event handler
// ----------------------------------------------------
//
TKeyResponse CQGazerAppUi::HandleKeyEventL(
    const TKeyEvent& aKeyEvent ,TEventCode aType)
{
	if (aType != EEventKey)
		return EKeyWasNotConsumed;

	if (TInt(aKeyEvent.iCode) == 63577)
		{
		gContext->SetKeyboardOpen(ETrue);
		RefreshL();
		return EKeyWasNotConsumed;
		}
	
	if (TInt(aKeyEvent.iCode) == 63578)
		{
		gContext->SetKeyboardOpen(EFalse);
		RefreshL();
		return EKeyWasNotConsumed;
		}

	if (aKeyEvent.iCode == '1')
		Zoom(ETrue);

	if (aKeyEvent.iCode == '2')
		Zoom(EFalse);

#if defined(__WINS__) // *sigh*
	if (aKeyEvent.iCode == '3')
		{
		iSensors->SetGpsInvalid(ETrue);
		TDebug::Print(_L("GPS INVALID"));
		}

	if (aKeyEvent.iCode == '4')
		{
		iSensors->SetGpsInvalid(EFalse);
		TDebug::Print(_L("GPS VALID"));
		}

#endif
	
	if (iListBoxWindow)	//find
		iListBoxWindow->KeyEvent(aKeyEvent.iScanCode);
	else if (iLocationInput)	//home location
		iLocationInput->KeyEvent(aKeyEvent.iScanCode);
	else if (iLDT)
		iLDT->KeyEvent(aKeyEvent.iScanCode);
	return EKeyWasNotConsumed;
}

void CQGazerAppUi::GpsDataL(TGpsStatus aStatus, TReal aLong, TReal aLat)
	{
	TBool redrawGl = EFalse;
	TBool setMode = EFalse;
	
	TGpsStatus prevStatus = gContext->GpsStatus();
	
	if (aStatus == EGpsLastKnown || aStatus == EGpsValid)
		{
		gContext->SetWaitingForGps(EFalse);
		
		
//If status == yellow && city != surbit		
		if ((aStatus == EGpsLastKnown)
				&& (gContext->Location().City().Compare(_L("Surbiton")) != 0))
					goto skip;
		{
		TLocation l(aLong, aLat);
		TInt d = Model().FindNearesetCity(l);
		
#ifdef TESTING_GPS_STARTUP
		if (aStatus == EGpsLastKnown)
			{
			TGeographic surbitonLong;
			surbitonLong.Set(-0.3013);
			TGeographic surbitonLat;
			surbitonLat.Set(51.38786);
			l.Set(surbitonLong, surbitonLat);
			l.Set(_L("England"), _L("Surbiton"));
			d=0;
			aStatus = EGpsNotValid;
			}
#endif		
		
		gContext->SetCurrentLocation(l, d);
		}
skip:		
		iCameraManager->SetSpace(TCamera(gContext->CurrentLocation()));

		//Monitor every 30 mins if valid
		if (aStatus == EGpsValid && prevStatus!= EGpsValid)
			iSensors->MonitorGps(TTimeIntervalMicroSeconds(30*60*1000000), EFalse);
		
		//If changed from not valid, renew buttons etc
		setMode = (prevStatus != EGpsValid);
			
		if ((prevStatus == EGpsNotValid) && (iLocationInput))
			{
			TDebug::Print(_L("GPS active. No need to select Home Location!"));
			DELETEWINDOW(iLocationInput);
			}
		
		Model().UpdateCoords();
		redrawGl = ETrue;
		}
	else // EGpsNotValid)
		{
		if (prevStatus != EGpsNotValid)	
			{
			aStatus = EGpsLastKnown;	//go amber, because we previously had a location
			if (gContext->GpsStatus() != EGpsLastKnown)
				iSensors->MonitorGps(TTimeIntervalMicroSeconds(10*1000000), EFalse); //start monitoring frequently
			}
		}

	TBool redrawStatusBar = (aStatus != prevStatus);
	
	gContext->SetGpsStatus(aStatus);

	if (setMode)
		{
		RefreshL();
		//If the user has scrolled the earth, resest the camera
		if (gContext->DrawDot())
			ResetCamera();
		}
	else
		{
		if (redrawStatusBar)
			iStatusBar->DrawNow();
	
		if (redrawGl)
			{
			Redraw();
			}
		}
	}

void CQGazerAppUi::AccDataL(TReal aAngle)
	{
	if (aAngle == KLookingDown)
		{
		iStatusBar->SetFlashing(EFalse);
		MustReturnWindowL(EFalse);
		return;
		}
	
	//If the crosshair is active AND there's an infobox, clear cross hair
	if (gContext->CrossHairMode() && SHOWING(iInfoWindow))
		ToggleCrossHair();
	
	if (!gContext->UserLocationMode())
		{
		if (gContext->CompassHealth() < 3)
			{
			iStatusBar->SetFlashing(ETrue);
			iAllWindows->StartFlashingL();
			}
		
		gContext->Camera().SetX(aAngle-5);

		User::ResetInactivityTime();
		}
	else	//User mode
		{
		MustReturnWindowL(ETrue);
		}
	}

void CQGazerAppUi::CompassData(TReal aBearing)
	{
	//TDebug::Print(aBearing);
	if (!gContext->UserLocationMode())
		{
		TReal a = aBearing - 180.0;
		if (a < 0.0)
			a += 360.0;
		gContext->Camera().SetY(a);
		}
	}

void CQGazerAppUi::Updated(TBool aTime, TBool aAcc, TBool aCompass)
	{
	if (aTime)
		{
		Model().UpdateCoords();
		iStatusBar->DrawNow();
		}
	else
		{
		if (aCompass)
			iStatusBar->UpdateBearing();
		}

if (aAcc || aCompass || aTime)
		Redraw();
	}


#if(0)
N 0 
E 90
S 180
W 270
---
S 0
E 270
N 180
W 90

#endif

void CQGazerAppUi::HandleCommandL(TInt aCommand)
    {
    switch ( aCommand )
        {
        case EAknSoftkeyBack:
        case EEikCmdExit:
            {
            gContext->SaveUserDataL();
            Exit();
            break;
            }
        default:
            break;
        }
    }

void CQGazerAppUi::SetViewAndStatusBarL()
	{
	iAppContainer->QGazer().SetupView();
	iStatusBar->BarLayoutL();
	iStatusBar->DrawNow();	//DMC TODAY
	}

void CQGazerAppUi::DoFlipModeL()
	{
	gContext->ToggleInSpace();
	
#ifdef SATELLITE
	Model().UpdateCoords();//added to handle satellelites becoming invalid in ground mode
#endif
	
	gContext->Camera().SetZoom(1.0);
	SetViewAndStatusBarL();
	if (gContext->InSpace())
		{
	//	gContext->ClearChosenObject();
		gContext->SetDrawDot(ETrue);
		if (gContext->UserLocationMode())
			iCameraManager->SetSpace(TCamera(gContext->UserLocation()));
		}
	
	gContext->SetCrossHairMode(EFalse);
	RefreshL();
	ResetCamera();
//	Redraw();
	}

void CQGazerAppUi::FlipModeL()
	{
	NewLocationButtonL(EFalse);

//	iCameraManager->Save();
#ifdef ZOOM_EFFECT
	if (gContext->FastOpenGL() &&  gContext->InSpace())
		{
		gContext->SetCrossHairMode(EFalse);
		gContext->Camera().Set(gContext->Location());
		iCameraManager->StartZoomIn();
		iAllWindows->HideL(ETrue);
		return;
		}
#endif
	//no zoom if we get here
	DoFlipModeL();
	Redraw();
	}
	

void CQGazerAppUi::LocationOnGlobe()
	{
	//draw dot is false if the user has already moved the globe
	if (gContext->InSpace() && !FloatingWindowVisible() && !gContext->ZoomEffectRunning())
		{
		if (!gContext->DrawDot() && !gContext->CrossHairMode())
		{
			TLocation l;
			gContext->Camera().Location(l);
			TInt d = Model().FindNearesetCity(l);
			gContext->SetUserLocation(l, d);
			NewLocationButtonL(ETrue, EFalse);
			if (gContext->UserLocationMode())
				GroundModeButtonL(EFalse);
		}
		else
			{
			NewLocationButtonL(EFalse);
			}
		}
	
	}

TBool CQGazerAppUi::FloatingWindowVisible()
	{
		return (SHOWING(iMenuWindow))
			|| (SHOWING(iLDT))
			|| (SHOWING(iListBoxWindow))
			|| (SHOWING(iMustReturnWindow))
			|| (SHOWING(iArticleWindow))
			|| (SHOWING(iSliderWindow));
	}
/*			
void CQGazerAppUi::ResetCamera()
	{
	
//Set the earch came to the chosen object, or 0,0 if there is none	
	TCamera cam;	//set to 0,0
	
	//don't set the camera if we're going to do a find!
	if (gContext->ChosenObject().iObject && !iDoFindAfterZoomEffect)
		cam.Set(gContext->ChosenObject().iHr);
	iCameraManager->SetEarth(cam);

//Set the came to the Space camera or Earth camera
	iCameraManager->Restore();
	ResetZoomButtonL(gContext->Camera().Zoom() != 1.0);
	}
*/

void CQGazerAppUi::ResetCamera()
	{
	TCamera cam; //=(0, 0)
	
	if (gContext->InSpace())
		{
		if (gContext->ChosenObject().iObject && !iDoFindAfterZoomEffect)
			cam.Set(gContext->ChosenObject().iEq);
		else
			cam.Set(gContext->Location());
		iCameraManager->SetSpace(cam);		
		}
	else
		{
		//don't set the camera if we're going to do a find!
		if (gContext->ChosenObject().iObject && !iDoFindAfterZoomEffect)
			cam.Set(gContext->ChosenObject().iHr);
		
		iCameraManager->SetEarth(cam);
		}
	iCameraManager->Restore();
	ResetZoomButtonL(gContext->Camera().Zoom() != 1.0);
	}

void CQGazerAppUi::RefreshL()
	{
	TBool waitingForGps = gContext->WaitingForGps(); 
	WaitingForGpsButtonL(waitingForGps);

	if (!FloatingWindowVisible() && !gContext->ZoomEffectRunning())	//only reshow the buttons if there's no floating window
		{
		//Show the reset zoom button if the Zoom != 1
		ResetZoomButtonL(gContext->Camera().Zoom() != 1.0);
		
		//Show 'close flip' if flip is open in ground mode
		TBool showCloseFlip = (!gContext->InSpace()) && (!gContext->UserLocationMode()) && (gContext->KeyboardOpen());
		CloseFlipButtonL(showCloseFlip);
	
		//Refresh the info window
		InfoBoxL(gContext->ObjectSelected());//refresh

		//Refresh the cross-hair button
		//Don't show if the info box is visible and we're not in crosshair mode 
		CrossHairButtonL(!gContext->InSpace() && !(SHOWING(iInfoWindow) && !gContext->CrossHairMode()));

		//Refresh the gound mode button
		GroundModeButtonL(!waitingForGps);
	
		//Refresh the 'Return to xxx button;
		ReturnButtonL(gContext->UserLocationMode());

		//If in space, refresh  the 'lights on/off button
		LightsButtonL(gContext->InSpace());
		
		if (!gContext->InSpace())
			{
			//No location button in ground mode
			NewLocationButtonL(EFalse);

			//Check if chosen object is visible in ground mode. If not, remove info box
			if (gContext->ObjectSelected())
				{
				if (!gContext->ChosenObject().iObject->IsVisible())
					{
					ClearLabelWindows();	
					gContext->ClearChosenObject();
					InfoBoxL(EFalse);
					}
				}
			}
		iAllWindows->HideL(EFalse);
		}
	iStatusBar->DrawNow();
	}

void CQGazerAppUi::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	
	switch (aPointerEvent.iType)
		{
		case TPointerEvent::EButton1Down:
			iAllWindows->QueueDeleteTemporaries();
			iAllWindows->HideL(EFalse);
			iAllWindows->StartFlashingL();
			iButtonDownPoint = aPointerEvent.iPosition;
			//special case if the user has pressed the waiting for GPS button and then dismissed it
			if (gContext->WaitingForGps() && !iWaitingForGpsButton)
				RefreshL();
			
			break;
			
		case TPointerEvent::EDrag:
			{
			gContext->SetDragging(ETrue);
			TPoint delta = aPointerEvent.iPosition;
			
			delta -= iButtonDownPoint;
			iButtonDownPoint = aPointerEvent.iPosition;
			
			GLfloat zf = gContext->Camera().Zoom();
			if (zf > 1.0)
				{
				delta.iX /= zf;
				delta.iY /= zf;
				}
			
			if (gContext->CrossHairMode())
				{
				MoveCrossHair(delta);
				}
			else
				{
				gContext->Camera().Add(delta);
				iStatusBar->UpdateBearing();
				gContext->SetDrawDot(EFalse);
				Redraw();
				}
			break;
			}
			
		case TPointerEvent::EButton1Up:
			gContext->SetDragging(EFalse);
		
			if (gContext->CrossHairMode())
				{
				CrossHairUp();
				}
			else
				{
				Redraw();	//put all the stars back
				if (gContext->InSpace())
					if (gContext->UserLocationMode())
						iStatusBar->DrawNow();//
				}
		default:;
		}
	}

void CQGazerAppUi::HandleForegroundEventL(TBool aForeground)
	{
	ForegroundL(aForeground);
	}

void CQGazerAppUi::Label(const TPoint& aPoint, const TDesC& aText, TLabelType aType)
	{
	if (aType == EGeneral || aType == EPlanetLabel || aType == EStarLabel)
		{
		if (iLabelContainer)
			iLabelContainer->Add(aPoint, aText, aType);
		}
	else
		{
		iCursor->Set(aPoint, gContext->CrossHairMode());
		iCursor->Refresh();
		if (iStatusBar->SaveRect().Intersects(iCursor->SaveRect()))
			{
			iCursor->Reset();
			return;
			}
		}
	}

void CQGazerAppUi::DrawLabels()
	{
	//Has the crosshair been zoomed off screen?
	if (gContext->CrossHairMode())
		if (!iCursor->IsVisible())
			EnableCrossHair();
	
	
	if (iLabelContainer)	//not created till splash is dismissed
		iLabelContainer->DrawLabels();

	if (iCursor->IsVisible())
		{
		iAllWindows->StartFlashingL();
		iCursor->Refresh();
		}
	else
		{
		iCursor->Reset();
		}
	}

void CQGazerAppUi::ResetLabels() 
	{
	iOnScreenObjects.Reset();		
	if (iLabelContainer)
		iLabelContainer->Reset();
	iCursor->Reset();
	}

void CQGazerAppUi::SliderPercentage(TInt aId, GLfloat aPercentage)
	{
	if (aId == EStarsSlider)
		StarsSliderPercentage(aPercentage);
	}

void CQGazerAppUi::SliderOk(TInt aId)
	{
	if (aId == EStarsSlider)
		StarsSliderEnd();
	}

void CQGazerAppUi::StarsSliderStartL()
	{
	if (!iSliderWindow)
		{
		iSliderWindow = CSliderWindow::NewL(SkyRect(), EStarsSlider);
		iSliderWindow->ActivateL();
		STOREWINDOW(iSliderWindow);
		iSliderWindow->Refresh();
		iAllWindows->StartFlashingL();
		}
	}

void CQGazerAppUi::StarsSliderEnd()
	{
	iSliderWindow->Reset();
	DELETEWINDOW(iSliderWindow);
	}

void CQGazerAppUi::StarsSliderPercentage(GLfloat aPercentage)
	{
	gContext->SetVisibleStars(aPercentage);
	Redraw();
	}

void CQGazerAppUi::ForegroundL(TBool aFg)
	{
	if (aFg)
		{
		iSensors->StartL 	();
		
		iSensors->ReadFlip();
		
		if (!iSensors->UsingSensors() && !iSensorMessageDisplayed)
			{
			TDebug::Print(_L("This application requires both an accelerometer and a magnetometer. Some features will not be available."));
			iSensorMessageDisplayed = ETrue;
			}
		RefreshL();
		Redraw();
		}
	else
		{
		iCameraManager->Save();
		iSensors->Stop();
		}
	}


void CQGazerAppUi::DoubleTapL(const TPoint& aPoint)
	{
	}

void CQGazerAppUi::AddObjectOnScreen(const TScreenCoords& aCoords, const TSkyObject* aObject, TBool aSunMoon)
	{
	TOnScreenObject o;
	o.iSunMoon = aSunMoon;
	o.iCoords = aCoords;
	o.iObject = aObject;
	iOnScreenObjects.Append(o);
	}

void CQGazerAppUi::ToggleCrossHair() 
	{
	if (!gContext->CrossHairMode())
		{
		gContext->SetCrossHairMode(ETrue);
		Redraw();	//crosshair is added in ResetLabels
		RefreshL();
		}
	else
		{
		gContext->SetCrossHairMode(EFalse);
		Redraw();
		RefreshL();
		}
	}

void CQGazerAppUi::EnableCrossHair()
	{
	const TPoint p(ApplicationRect().Width()/2, ApplicationRect().Height() / 2);
	iCursor->Set(p, ETrue);
	iCursorVisible = ETrue;
	MoveCrossHair(TPoint(0,0));
	iLastShortestI = -1;
	}

void CQGazerAppUi::ClearCrossHair()
	{
	iCursorVisible = EFalse;
	iCursor->Reset();
	iOnScreenObjects.Reset();
	CrossHairButtonL(ETrue);
	if (gContext->ObjectSelected())
		Redraw();
	}

//Return the index of the nearest object to the cursor, and the distance in aDist
TInt CQGazerAppUi::GetNearest(TReal& aDist)
	{
	const TPoint& p(iCursor->Point());

	aDist = 99999.0;
	TInt shortestI = -1;
	
	for (TInt i=0; i<iOnScreenObjects.Count(); i++)
		{
		TInt dx = iOnScreenObjects[i].iCoords.Point().iX - p.iX;
		TInt dy = iOnScreenObjects[i].iCoords.Point().iY - p.iY;
		dx *= dx;
		dy *= dy;
		
		const TReal total(dx + dy);
		TReal dist = 0;
		Math::Sqrt(dist, total);

		if (dist < aDist)
			{
			aDist = dist;
			shortestI = i;
			if ((iOnScreenObjects[i].iSunMoon && aDist < 30.0))
				break;
			}
		}
	return shortestI;
	}

//User has lifted finger from the screen
void CQGazerAppUi::CrossHairUp()
	{
	if (iOnScreenObjects.Count() == 0)
		{
		EnableCrossHair();
		}
	else
		{
		TReal distance = 0;
		TInt shortestI = GetNearest(distance);
		gContext->SetChosenObject(iOnScreenObjects[shortestI].iObject);
		iCursor->Set(iOnScreenObjects[shortestI].iCoords.Point(), ETrue);
		iCursor->Refresh();
		InfoBoxL(ETrue);
		}
	}

void CQGazerAppUi::MoveCrossHair(const TPoint& aDelta)
	{
	TPoint d(aDelta);

	iCursor->Move(d);
	iCursor->Refresh();
	
	TReal distance = 0;
	TInt shortestI = GetNearest(distance);
	
	if (distance < 10.0)
		{
		if (iLastShortestI != shortestI)
			{
			gContext->SetChosenObject(iOnScreenObjects[shortestI].iObject);
			InfoBoxL(ETrue);
			}
		iLastShortestI = shortestI;
		}
	else
		{
		DELETEWINDOWNOW(iInfoWindow);
		gContext->ClearChosenObject();
		iLastShortestI = -1;
		}
	}

void CQGazerAppUi::Redraw()
	{
	iAppContainer->DrawCallBack(iAppContainer);
	LocationOnGlobe();
	}

CSkyModel& CQGazerAppUi::Model()
	{
	return ((CQGazerDocument*)Document())->Model();
	}

void CQGazerAppUi::ListBoxOkCancelL(TInt aId, TBool aOk)
	{
	if (aOk)
		{
		switch (aId)
			{
		case ELocationListBox:		//Home Location
			gContext->SetWaitingForGps(EFalse);
			DELETEWINDOW(iLocationInput);
			gContext->SetGpsStatus(EGpsLastKnown);
			Model().UpdateCoords();
			iCameraManager->SetSpace(TCamera(gContext->CurrentLocation()));
			gContext->SetDrawDot(ETrue);
			ResetCamera();
			RefreshL();
			Redraw();
			gContext->SaveUserDataL();
			break;
			
		case ELocationDateTime:	//Visit Location
			DELETEWINDOW(iLDT);
			VisitLocationL(ETrue);
			if (gContext->InSpace())
				{
				FlipModeL();
				}
			else
				{
				ResetCamera();
				RefreshL();
				Redraw();
				}
			break;
			
		case EFindListBox:
			DELETEWINDOW(iListBoxWindow);
			if (gContext->InSpace())
				NewLocationButtonL(EFalse);
			if (!gContext->SensorMode() || iDontShowRealModeWarning)
				{
				if (gContext->InSpace())
					{
						NewLocationButtonL(EFalse);
						gContext->SetDrawDot(ETrue);	//stops the location box being drawn (*sigh*)
					}
					iCameraManager->StartObjectFoundEffect();
				}
			InfoBoxL(ETrue);
			break;
		
		default:;
			User::Leave(KErrNotFound);
			User::Invariant();
			}
		}
	else
		{
		iAllWindows->QueueDeleteTemporaries();
		}
	}

void CQGazerAppUi::FindFinishedL(TBool aZoom)
	{
#ifdef ZOOM_EFFECT
//Yuk. FindFinished gets call when the zoom effect ends!
	if (aZoom)
		{
		DoFlipModeL();
	//	gContext->SetZoomEffectRunning(EFalse);//otherwise the stars won't get drawn
		Redraw();

		if (iDoFindAfterZoomEffect)
			{//from the "learn" screens
			iDoFindAfterZoomEffect = EFalse;
			ListBoxOkCancelL(EFindListBox, ETrue);
			}
		
		return;
		}
#endif		
	
	gContext->SetDrawDot(EFalse);
	LocationOnGlobe();
	}

void CQGazerAppUi::InfoWindowCbL(TInt aId)
	{
	
	switch (aId)
		{
	case EMoreInfoButton:
		ArticleWindowL(gContext->ChosenObject().iObject, EFalse);
		break;

	case EMainInfoWindow:
		gContext->ClearChosenObject();
		gContext->SetCrossHairMode(EFalse);
		ClearLabelWindows();
		RefreshL();
		Redraw();
		break;

	case EGroundModeWindow:	//User wants to return to Tokyo, or view Tokyo from the ground
		FlipModeL();
		break;

	case ENewLocationWindow:	//User has dismissed the double tap window
		NewLocationButtonL(EFalse);
		iAllWindows->HideL(EFalse);
		break;
	
	case ENewLocationOkButon:
		NewLocationButtonL(EFalse);
		WaitingForGpsButtonL(EFalse);	
		if (!gContext->UserTimeMode())
			{
			gContext->SetUserTimeToNow();//in case user wants to go to Visit screen later
		VisitLocationL(EFalse);	//visit location @ current time
			}
		else
			{
			VisitLocationL(ETrue);
			}
		FlipModeL();
		break;
	
	case EWaitingForGpsButton:
	case EWaitingForGpsOkButton:
		WaitingForGpsButtonL(EFalse);
		iAllWindows->HideL(ETrue);
		StartListBoxL(ELocationListBox);
		break;

	case ELightsButton:
		gContext->ToggleLights();
		LightsButtonL(ETrue); //toggle on/off
		Redraw();
		break;
		
	case ESplashWindow:	
	case ESplashHelpButton:
		if (iReady)
			{
			SplashWindowL(EFalse, EFalse);
			DoHelpL(EFalse);
			}
		break;		
	
	case EMustReturnWindow:		//dismiss must return window
		MustReturnWindowL(EFalse);
		iAllWindows->HideL(EFalse);
		break;
		
	case EMustReturnButton:
		MustReturnWindowL(EFalse);
		iDontShowRealModeWarning = ETrue;
		break;
		
	case EReturnButton:
		ReturnToHomeLocationL();
		break;
			
	case ECloseFlipButton:
		break;
		
	case ECrossHairButton:
		ToggleCrossHair();
		break;
		
	case EResetZoomButton:
		ResetZoom();
		ResetZoomButtonL(EFalse);
		Redraw();
		break;
		
	case EDownloadOkButton:
		DownloadOk();
		break;
		
	case EDownloadCancelButton:
		DownloadCancel();
		break;
		
	default:;
	User::Leave(KErrNotFound);
		User::Invariant();
		}
	}

void CQGazerAppUi::ResetZoom()
	{
	gContext->Camera().SetZoom(1.0);
	if (!gContext->InSpace())
			iAppContainer->QGazer().SetupView();
	}

void CQGazerAppUi::InfoBoxL(TBool aShow)
	{
	if (aShow)
		{
		if (!iInfoWindow)
			{
		    iInfoWindow = CInfoWindow::NewL(EMainInfoWindow, NULL, ESmallFont);
		    STOREWINDOW(iInfoWindow);
			}

		iInfoWindow->StringArray().Reset();
		iInfoWindow->SetTextAlignment(ECenter);
		if (gContext->ChosenObject().iObject->Article().Length())
			{
			iInfoWindow->AddButtonL(_L("     Info     "), EMoreInfoButton);
			iInfoWindow->StringArray().AppendL(_L(""));
			iInfoWindow->StringArray().AppendL(gContext->ChosenObject().iObject->Name());
			iInfoWindow->StringArray().AppendL(_L(""));
			}
		else
			{
			gContext->ChosenObject().iObject->DescriptionL(iInfoWindow->StringArray());
			}
				
		iInfoWindow->Set(SkyRect(), ETopLeftEx);

		if (gContext->CrossHairMode())
			iInfoWindow->SetColours(KBcCursorCrossHair, KBcInfoBox);
		else
			iInfoWindow->SetColours(KFcInfoBox, KBcInfoBox);
		
	    iAllWindows->StartFlashingL();
	    iInfoWindow->ActivateL();
	    iInfoWindow->DrawNow();//because sometimes the text changes colour	
		}
	else
		{
		DELETEWINDOW(iInfoWindow);
		}
	}

void CQGazerAppUi::ClearLabelWindows()
	{
	iCursor->Reset();
	}

void CQGazerAppUi::MenuWindowL(TInt aPage)
	{
	if (!iMenuWindow)
		{
		iAllWindows->HideL(ETrue);
		iMenuWindow = CMenuWindow::NewL(SkyRect(), aPage);
		iMenuWindow->ActivateL();
		iMenuWindow->MakeVisible(ETrue);
		iMenuWindow->DrawNow();
		iAllWindows->QueueDeleteTemporaries();
		STOREWINDOW(iMenuWindow);
		}
	else
		{
		DELETEWINDOW(iMenuWindow);
		}
	}

void CQGazerAppUi::StartListBoxL(TInt aId)
	{
	if (aId == ELocationDateTime)
		{
		iLDT = CLocationDateTime::NewL(ApplicationRect(), aId, *this, iAllWindows);
		STOREWINDOW(iLDT);
		iLDT->MakeVisible(ETrue);
		iLDT->ActivateL();
		}
		
	else if (aId == ELocationListBox)
		{
		iLocationInput  = CLocationInput::NewL(ApplicationRect(), aId, *this);
		STOREWINDOW(iLocationInput);
		iLocationInput->MakeVisible(ETrue);
		iLocationInput->ActivateL();
		}

	else if (!iListBoxWindow)
		{
		iAllWindows->HideL(ETrue);
		iListBoxWindow = CListBoxWindow::NewL(SkyRect(), Model(), EFalse, *this);
		STOREWINDOW(iListBoxWindow);
		iListBoxWindow->ActivateL();
		iListBoxWindow->Refresh();
		}
	}
	
void CQGazerAppUi::GroundModeButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iGroundModeButton);
		return;
		}
	
	if (!iGroundModeButton)
		{
		iGroundModeButton = CInfoWindow::NewL(EGroundModeWindow, NULL);
		STOREWINDOW(iGroundModeButton);
		}

	iGroundModeButton->StringArray().Reset();
	
	CInfoWindow* w = iGroundModeButton;
	w->SetTextAlignment(ERight);
	
	if (gContext->UserLocationMode())
		w->SetColours(KFcGroundModeUser, KBcGroundModeUser);
	else
		w->SetColours(KFcGroundMode, KBcGroundMode);		
	
	if (gContext->InSpace())// && !gContext->UserLocationMode())
		{
		if (gContext->UserLocationMode())
			{
			w->StringArray().AppendL(_L("View the sky"));
			TBldStr::Set(_L("from "));
			TBldStr::Add(gContext->Location().City());
			TBldStr::Space();
			TBldStr::Add(TChar(0x25BA));
			w->StringArray().AppendL(TBldStr::String());
			}
		else
			{
			TBldStr::Set(_L("Explore the sky from"));
			TBldStr::Add(TChar(0x25BA));
			w->StringArray().AppendL(TBldStr::String());
			w->StringArray().AppendL(_L("your home location"));
			}
		}	
	else
		{
		//ASSERT(gContext->UserLocationMode());
		w->StringArray().AppendL(_L("Globe"));
		TBldStr::Set(TChar(0x25BA));
		w->StringArray().AppendL(TBldStr::String());
		}
		
	w->Set(SkyRect(), ETopRightEx);		//causes redraw!!!
	ActivateIfReadyL(w);
	
	iAllWindows->StartFlashingL();
	}

void CQGazerAppUi::NewLocationButtonL(TBool aShow, TBool aTemporary)
	{
	if (!aShow)
		{
		DELETEWINDOW(iNewLocationButton);
		return;
		}
	
	if (!iNewLocationButton)
		{
		iNewLocationButton = CInfoWindow::NewL(ENewLocationWindow, NULL);
		STOREWINDOW(iNewLocationButton);
		}
	CInfoWindow* w = iNewLocationButton;
	w->AddButtonL(_L("   Visit this location   "), ENewLocationOkButon);
	w->SetTextAlignment(ECenter);
	
	TInt nearestDistance;
	const TLocation& loc = gContext->UserLocation(nearestDistance);
	w->StringArray().Reset();
//	w->StringArray().AppendL(_L(""));
	w->StringArray().AppendL(loc.String());
	RBuf line;
	TBuf<64> n;
	n.AppendNum(nearestDistance / 1000);
	n.Append(_L("km from"));
	w->StringArray().AppendL(n);

	w->StringArray().AppendL(loc.City());
	w->StringArray().AppendL(loc.Country());

//	w->StringArray().AppendL(_L(""));
			
	w->SetColours(KFcNewLocation, KBcNewLocation);

	w->Set(SkyRect(), aTemporary ? ECentreEx : EBottomLeftEx);
	

	w->SetTemporary(aTemporary);
	w->ActivateL();
	
	}

void CQGazerAppUi::WaitingForGpsButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iWaitingForGpsButton);
		return;
		}
	
	if (!iWaitingForGpsButton)
		{
		iWaitingForGpsButton = CInfoWindow::NewL(EWaitingForGpsButton, NULL);
		STOREWINDOW(iWaitingForGpsButton);
		
		CInfoWindow* w = iWaitingForGpsButton;
		w->SetTextAlignment(ECenter);
		w->StringArray().AppendL(_L("Waiting for GPS"));
		w->StringArray().AppendL(_L(""));
		w->SetColours(KFcWaitingForGps, KBcWaitingForGps);
		w->AddButtonL(_L("Select home location"), EWaitingForGpsOkButton);
		w->Set(SkyRect(), ETopRightEx);
		
		w->SetTemporary(EFalse);
		ActivateIfReadyL(w);
		}
	}


void CQGazerAppUi::LightsButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOWNOW(iLightsButton);
		return;
		}
	
	if (!iLightsButton)
		{
		iLightsButton = CInfoWindow::NewL(ELightsButton, NULL);
		STOREWINDOW(iLightsButton);
		}
	
	CInfoWindow* w = iLightsButton;
	
	if (!gContext->Lights())
		w->SetBm(iLightsOnBm);
	else
		w->SetBm(iLightsOffBm);
	
	w->Set(SkyRect(), ERightEx);
	w->SetColours(KBcLightsButton, KBcLightsButton);
	w->SetTemporary(EFalse);
	ActivateIfReadyL(w);
	}

void CQGazerAppUi::SplashWindowL(TBool aShow, TBool aButton)
	{
	if (!aShow)
		{
		DELETEWINDOW(iSplashWindow);
		return;
		}
	
	if (!iSplashWindow)
		{
		iSplashWindow = CInfoWindow::NewL(ESplashWindow, NULL, ESmallFont);
		STOREWINDOW(iSplashWindow);
		
		CInfoWindow* w = iSplashWindow;
		w->SetTextAlignment(ECenter);
		w->StringArray().AppendL(_L(""));
		w->StringArray().AppendL(_L("Astroller v4.0.0"));
		w->StringArray().AppendL(_L(""));
		TBldStr::Clear();
		TBldStr::Space();
		TBldStr::Add(TChar(0x00A9));
		TBldStr::Add(_L(" 2011 Heliotrope Software "));
		w->StringArray().AppendL(TBldStr::String());
		w->StringArray().AppendL(_L(""));

		w->SetColours(KFcSplash, KBcSplash);
		if (aButton)
			{
			w->StringArray().AppendL(_L("Press for help"));	
			w->StringArray().AppendL(_L(""));
			}

		w->Set(SkyRect(), ECentreEx);
		w->SetTemporary(ETrue);
		w->ActivateL();
		}
	}


void CQGazerAppUi::MustReturnWindowL(TBool aShow)
	{
	if (!aShow)
		{
		if (iMustReturnWindow)
			{
			DELETEWINDOW(iMustReturnWindow);
			iAllWindows->HideL(EFalse);
			}
		return;
		}
	
	if (iDontShowRealModeWarning)
		return;
	
	if (!iMustReturnWindow)
		{
		iAllWindows->HideL(ETrue);
		iMustReturnWindow = CInfoWindow::NewL(EMustReturnWindow, NULL, ESmallFont);
		STOREWINDOW(iMustReturnWindow);
		
		CInfoWindow* w = iMustReturnWindow;
		w->SetTextAlignment(ECenter);
		w->StringArray().AppendL(_L(""));
		w->StringArray().AppendL(_L("Exploring the sky in"));
		w->StringArray().AppendL(_L("'real mode' only works"));
		w->StringArray().AppendL(_L("from your home location"));
		w->StringArray().AppendL(_L(""));

		w->SetColours(KFcMustReturn, KBcMustReturn);
		w->AddButtonL(_L("Don't show this message again"), EMustReturnButton);
		w->Set(SkyRect(), ECentreEx);
		
		w->SetTemporary(ETrue);
		w->ActivateL();
		}
	}

void CQGazerAppUi::ReturnButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iReturnButton);
		return;
		}

	if (!iReturnButton)
		{
		iReturnButton = CInfoWindow::NewL(EReturnButton, NULL);
		STOREWINDOW(iReturnButton);
		}
	
		CInfoWindow* w = iReturnButton;
		w->StringArray().Reset();
		
		w->SetTextAlignment(ELeft);
		//TBldStr::Set(TChar(0x25C4));
		TBldStr::Set(_L("Return to your home"));
		TBldStr::Add(TChar(0x25BA));
		w->StringArray().AppendL(TBldStr::String());
		w->StringArray().AppendL(_L("location, current time"));
		//TBldStr::Set(gContext->CurrentLocation().City());
		//TBldStr::Comma();
		//w->StringArray().AppendL(TBldStr::String());
		//w->StringArray().AppendL(_L("current time"));
		w->SetTemporary(EFalse);
		
		w->SetColours(KFcGroundModeUser, KBcGroundModeUser);
		w->ActivateL();
		
	iReturnButton->Set(SkyRect(), EBottomRightEx /*ELeftEx*/);//causes redraw
	}

void CQGazerAppUi::CloseFlipButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iCloseFlipButton);
		return;
		}

	if (!iCloseFlipButton)
		{
		iCloseFlipButton = CInfoWindow::NewL(ECloseFlipButton, NULL);
		STOREWINDOW(iCloseFlipButton);
		CInfoWindow* w = iCloseFlipButton;	
		w->SetTextAlignment(ERight);

		w->StringArray().AppendL(_L("Flip should be closed"));
		w->SetTemporary(EFalse);
		
		//w->SetColours(KRgbWhite, KExploreSkyClr, KExploreSkyClr);
		w->SetColours(KFcCloseFlip, KBcCloseFlip);
		w->ActivateL();
		}
	iCloseFlipButton->Set(SkyRect(), EBottomRightEx /*ELeftEx*/);//causes redraw
	}

void CQGazerAppUi::CrossHairButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iCrossHairButton);
		return;
		}
	
	if (!iCrossHairButton)
		{
		iCrossHairButton = CInfoWindow::NewL(ECrossHairButton, NULL);
		STOREWINDOW(iCrossHairButton);
		iCrossHairButton->SetTextAlignment(ELeft);
		iCrossHairButton->SetThreeD(ETrue);
		}
	
		CInfoWindow* w = iCrossHairButton;	
		if (!gContext->CrossHairMode())
			w->SetBm(iCrossHairOnBm);
		else
			w->SetBm(iCrossHairOffBm);
		w->SetTemporary(EFalse);
		
		w->SetColours(KBcCrossHairButton, KBcCrossHairButton);
		ActivateIfReadyL(w);

	iCrossHairButton->Set(SkyRect(), ELeftEx /*ELeftEx*/);//causes redraw
	}

void CQGazerAppUi::ResetZoomButtonL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iResetZoomButton);
		return;
		}

	if (!iResetZoomButton)
		{
		iResetZoomButton = CInfoWindow::NewL(EResetZoomButton, NULL, ESmallFont);
		STOREWINDOW(iResetZoomButton);
		iResetZoomButton->SetTextAlignment(ECenter);
		}
	
	CInfoWindow* w = iResetZoomButton;		
	w->StringArray().Reset();
	w->StringArray().AppendL(_L("1:1"));
	w->SetTemporary(EFalse);
	
	w->SetColours(KFcResetZoom, KBcResetZoom);
	w->ActivateL();

	iResetZoomButton->Set(SkyRect(), ETopCentreEx /*ELeftEx*/);//causes redraw
	}


TPtrC KContexts[] = 
	{
	KGeneral_Information(),
	KGlobe_View(),
	KEarth_View(),
	KHome_Location(),
	KFind_Object(),
	KMain_Menu(),
	KStar_Count(),
	KVisit_Menu(),
	KDateTime_Input(),
	KLocation_Input(),
	KLearn_Page(),
	KArticle_Page(),
	KISS_Settings()
	};

void CQGazerAppUi::DoHelpL(TBool aIndex)
	{
	CArrayFixFlat<TCoeHelpContext>* array = new(ELeave)CArrayFixFlat<TCoeHelpContext>(1);
	CleanupStack::PushL(array);

	TInt c = 0;
	if (!aIndex)
		{
		c = gContext->InSpace() ? 1 : 2;
		if (iMenuWindow)
			{
			if ((iMenuWindow->CurrentPage() >= KFirstArticlePage)
					&& (iMenuWindow->CurrentPage() <= KLastArticlePage))
				c=10;
			else if (iMenuWindow->CurrentPage() == KIssSettingsPage)
				c = 12;
			else
				c = 5;
			}
		if (iListBoxWindow)
			c = 4;
		if (iSliderWindow)
			c = 6;
		if (iLocationInput)
			c = 3;
		if (iLDT)
			{
			if (iLDT->LocationActive())
				c = 9;
			else if (iLDT->DateTimeActive())
				c = 8;
			else
				c = 7;
			}
		if (iArticleWindow)
			c = 11;
		}
	
	array->AppendL(TCoeHelpContext(KUidQGazerHelp, KContexts[c]));

	HlpLauncher::LaunchHelpApplicationL(iEikonEnv->WsSession(), array);
	CleanupStack::Pop(array);
	}

void CQGazerAppUi::ArticleWindowL(const TSkyObject* aObject, TBool aFindButton)
	{
	if (!iArticleWindow)
		{
		iAllWindows->HideL(ETrue);
		iArticleWindow = CArticleWindow::NewL(SkyRect(), aObject, aFindButton);
		iArticleWindow->ActivateL();
		iArticleWindow->MakeVisible(ETrue);
		iArticleWindow->DrawNow();
		iAllWindows->QueueDeleteTemporaries();
		STOREWINDOW(iArticleWindow);
		}
	else
		{
		DELETEWINDOW(iArticleWindow);
		}
	}

void CQGazerAppUi::FindObject(const TSkyObject* aObject)
	{
	DELETEWINDOW(iArticleWindow);
	gContext->ClearChosenObject();
	
	THorizontal h;
	aObject->Horizontal(h);

	
	if (!gContext->InSpace())
		{
		if (h.iAltitude < 0)
			FlipModeL();
			
		gContext->SetChosenObject(aObject);
		ListBoxOkCancelL(EFindListBox, ETrue);
		}
	else
		{
		if (h.iAltitude > 0)
			{
			FlipModeL();
			gContext->SetChosenObject(aObject);
			if (gContext->FastOpenGL())
				{
				iDoFindAfterZoomEffect = ETrue;
				}
			else
				{
				ListBoxOkCancelL(EFindListBox, ETrue);				
				}
			}
		else
			{
			gContext->SetChosenObject(aObject);
			ListBoxOkCancelL(EFindListBox, ETrue);
			}
		}
	}

void CQGazerAppUi::DownloadBoxL(TBool aShow)
	{
	if (aShow)
		{
		if (!iDownloadWindow)
			{
			iDownloadWindow = CInfoWindow::NewL(EDownloadWindow, NULL, ESmallFont);
		    STOREWINDOW(iDownloadWindow);
			}


		iDownloadWindow->SetTextAlignment(ECenter);
		iDownloadWindow->StringArray().Reset();
		iDownloadWindow->AddButtonL(_L("     Cancel     "), EDownloadCancelButton);
		
		iDownloadWindow->StringArray().AppendL(_L(""));
		iDownloadWindow->StringArray().AppendL(_L("Checking connection..."));
		iDownloadWindow->StringArray().AppendL(_L(""));
				
		iDownloadWindow->Set(SkyRect(), ECentreEx);
		iDownloadWindow->SetColours(KFcDownload, KBcDownload);
	    iAllWindows->StartFlashingL();
	    iDownloadWindow->ActivateL();
		}
	else
		{
		DELETEWINDOW(iDownloadWindow);
		}
	}

void CQGazerAppUi::HandleDownloadL()
	{
	DownloadBoxL(ETrue);
	iCallback->CallMe(1);
	}

void CQGazerAppUi::HandleDownload2L()
	{
	
	//iDownload = CGetUrl::NewL(_L8("http://www.heliotropesoftware.com/downloads/astroller/satellites.txt"), *this);
	if (iDownload)
		{
		delete iDownload;
		iDownload = NULL;
		}
	iDownload = CGetUrl::NewL(_L8("http://www.heliotropesoftware.com/downloads/astroller/satellites.txt"), *this);
	}

void CQGazerAppUi::DownloadCancel()
	{
	delete iDownload;
	iDownload = NULL;
	DownloadBoxL(EFalse);
	}

void CQGazerAppUi::DownloadOk()
	{
	DownloadBoxL(EFalse);
	}


void CQGazerAppUi::DownloadStatusL(WebClientStatus aStatus)
	{
	iDownloadWindow->StringArray().Reset();
	iDownloadWindow->AddButtonL(_L("     Cancel     "), EDownloadCancelButton);
	iDownloadWindow->StringArray().AppendL(_L(""));
	
	TBool cancelButton = ETrue;
	TBool closeConnection = EFalse;
	TBool closeDialog = EFalse;
	
	switch (aStatus)
		{
		case EWebCancelled:
			closeConnection = ETrue;
			closeDialog = ETrue;
			break;
			
		case EWebConnecting:
			iDownloadWindow->StringArray().AppendL(_L("    Connecting    "));
			break;
		case EWebDownloading:
			iDownloadWindow->StringArray().AppendL(_L("    Downloading   "));
			break;
		case EWebError:
			iDownloadWindow->StringArray().AppendL(_L("      Failed      "));
			closeConnection = ETrue;
			break;
		case EWebFinished:
			iDownloadWindow->StringArray().AppendL(_L("     Finished     "));
			closeConnection = ETrue;
			gModel->InternalizeSatellitesL();
			MenuWindowL(KIssSettingsPage);
			cancelButton = EFalse;
			break;
		default:
			closeDialog = ETrue;

			break;

		}
	
	if (closeConnection)
		{
		iCallback->CallMe(2);
		}

	if (closeDialog)
		{
		DownloadBoxL(EFalse);
		}
	else
		{
		if (cancelButton)
			iDownloadWindow->AddButtonL(_L("  Cancel  "), EDownloadCancelButton);
		else
			iDownloadWindow->AddButtonL(_L("    Ok     "), EDownloadOkButton);
			
		
		iDownloadWindow->StringArray().AppendL(_L(""));
		iDownloadWindow->Set(SkyRect(), ECentreEx);
		}
	}

void CQGazerAppUi::Callback(TInt aId)
	{
	if (aId == 1)
		{
		HandleDownload2L();
		}
	else if (aId == 2)
		{
		delete iDownload;
		iDownload = NULL;
		}
	else
		{
		MenuWindowL(KIssSettingsPage);
		}
	}

void CQGazerAppUi::PleaseWaitWindowL(TBool aShow)
	{
	if (!aShow)
		{
		DELETEWINDOW(iPleaseWaitWindow);
		return;
		}
	
	if (!iPleaseWaitWindow)
		{
		iPleaseWaitWindow = CInfoWindow::NewL(EPleaseWaitWindow, NULL, ESmallFont);
		STOREWINDOW(iPleaseWaitWindow);
		
		CInfoWindow* w = iPleaseWaitWindow;
		w->SetTextAlignment(ECenter);
		w->StringArray().AppendL(_L("Please wait..."));

		w->SetColours(KRgbWhite, TRgb(46, 85, 158), KRgbWhite);

		w->Set(SkyRect(), ECentreEx);
		w->SetTemporary(ETrue);
		w->ActivateL();
		}
	}
