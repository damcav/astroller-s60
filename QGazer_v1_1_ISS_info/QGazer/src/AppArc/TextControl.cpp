/*
 * TextControl.cpp
 *
 *  Created on: 13-Apr-2010
 *      Author: Damien
 */


#include "TextControl.h"
#include "FloatingWindow.h"	//TODO MOVE THIS
#include "CommonTypes.h"

#include "colours.h"

#include <AknUtils.h> 

extern CFloatingWindowGroup* gGroup;
extern TColours gColours;


const CFont* CTextControl::NormalFont() const { return gGroup->iNormalFont; }
const CFont* CTextControl::SmallFont() const { return gGroup->iSmallFont; }
const CFont* CTextControl::TinyFont() const { return gGroup->iTinyFont; }
const CFont* CTextControl::BigFont() const { return gGroup->iBigFont; }

void CTextControl::SetFont(TInt aFontId) const
	{
	switch (aFontId)
		{
		case ENormalFont:
			iFont = NormalFont();
			break;
		case ESmallFont:
			iFont = SmallFont();
			break;
		case ETinyFont:
			iFont = TinyFont();
			break;
		case EBigFont:
			iFont = BigFont();
			break;
		default:
			User::Leave(KErrNotFound);
			User::Invariant();
		}
	
	iFontHeight = iFont->HeightInPixels();
	iFontDescent = iFont->DescentInPixels();
	}

void CTextControl::UseFont(CWindowGc& aGc, TInt aFontId) const
	{
	SetFont(aFontId);
	iGc = &aGc;
	Gc().UseFont(iFont);
	}


TRect CTextControl::TextRect(const CDesCArray* aLines) const
	{
	TInt longestLine = 0;
	
	for (TInt i=1; i<aLines->MdcaCount(); i++)
		{
		//if (aLines->MdcaPoint(i).Length() > aLines->MdcaPoint(longestLine).Length())
		if (TextWidth(aLines->MdcaPoint(i)) > TextWidth(aLines->MdcaPoint(longestLine)))
			longestLine = i;
		}

    TInt height = FontHeight() * aLines->Count();
    TInt width = TextWidth(aLines->MdcaPoint(longestLine));
    TSize size(width, height);
    TPoint pos(0,0);
    return TRect(pos, size);
	}

TBuf<128> iTwBuf;
TBuf<128> iMcBuf;

TInt CTextControl::ColourCodePos(const TDesC& aText) const
	{
	return aText.Locate('^');
	}

TInt CTextControl::TextWidth(const TDesC& aText) const 
	{
	TInt pos = ColourCodePos(aText);
	if (pos == KErrNotFound)
		return iFont->TextWidthInPixels(aText);
	
	iTwBuf = aText;
	while (pos != KErrNotFound)
		{
		iTwBuf.Delete(pos, 2);
		pos = ColourCodePos(iTwBuf);
		}
	return iFont->TextWidthInPixels(iTwBuf);
	}

void CTextControl::DrawTextMultiColour(const TPoint& aPoint, const TDesC& aText) const
	{
	iMcBuf = aText;
	TPoint p(aPoint);
	
	TInt pos = ColourCodePos(iMcBuf);
	while (pos != KErrNotFound)
		{
		TInt colourIndex = iMcBuf[pos+1] - '0';
		const TDesC& part(iMcBuf.Left(pos));
		DrawText(p, part);
		p += TSize(TextWidth(part), 0);
		if (iMcBuf.Length() - pos > 2)
			iMcBuf = iMcBuf.Mid(pos+2);
		else
			iMcBuf.Zero();
		Gc().SetPenColor(NM(gColours.GlobalColour(colourIndex)));
		pos = ColourCodePos(iMcBuf);
		}
	DrawText(p, iMcBuf);
	}

//this is ^1some text ^2 and this  
void CTextControl::DrawText(const TPoint& aPoint, const TDesC& aText) const
	{
	if (aText.Locate('^') != KErrNotFound)
		{
		DrawTextMultiColour(aPoint, aText);
		}
	else
		{	
		TInt h = iFontHeight - iFontDescent;
		TInt baseline = h;
		TPoint p(aPoint);
		p += TPoint(0, baseline);
		Gc().DrawText(aText, p);
		}
	}

void CTextControl::DrawText(TPoint aPoint, const CDesCArray* aLines) const
	{
	for (TInt i=0; i<aLines->MdcaCount(); i++)
		{
		DrawText(aPoint, aLines->MdcaPoint(i));
		aPoint += TPoint(0, FontHeight());
		}	
	}

void CTextControl::DrawText(const TRect& aRect, const CDesCArray* aLines, TAlign aAlign, TAlignEx aAlignY) const
	{
	TInt py = 0;
	
	if (aAlignY == ECentreEx)
		{
		py = aRect.Height() / 2;
		py -= ((aLines->MdcaCount() * FontHeight()) / 2);
		}
	
	for (TInt i=0; i<aLines->MdcaCount(); i++)
		{
		TPoint p(AlignX(aRect.Width(), aAlign, aLines->MdcaPoint(i)), py);
		p += aRect.iTl;
		DrawText(p, aLines->MdcaPoint(i));
		py += FontHeight();
		}	
	}


TInt CTextControl::AlignX(TInt aWidth, TAlign aAlign, const TDesC& aText) const
	{
	TInt px = 0;
	
	if (aAlign != ELeft)
		{
		TInt width = TextWidth(aText);
		if (aAlign == ECenter)
			px = aWidth/2 - width/2;
		else if (aAlign == ERight)
			px = aWidth - width;
		}
	return px;
	}

void CTextControl::DrawText(const TRect& aRect, const TDesC& aText, TAlign aAlign) const
	{
	TInt height = FontHeight();
	TInt py = aRect.Height()/2 - height/2;
	TInt px = AlignX(aRect.Width(), aAlign, aText);
	TPoint p(px, py);
    p += aRect.iTl;
    DrawText(p, aText);
	}


enum {ENoWord, EWord, ESpace};

class TWord
	{
public:
	TWord() : iNewLine(EFalse){}
	TPtrC iWord;
	TInt iWidth;
	TBool iNewLine;
	};

void CTextControl::MakeLines(TInt aWidth, const TDesC& aText, CDesCArray& aLines)
	{
	CArrayFixFlat<TPtrC>* wrappedArray = new(ELeave) CArrayFixFlat<TPtrC>(100);
	AknTextUtils::WrapToArrayL(aText, aWidth, *iFont, *wrappedArray);

    for (TInt i=0; i<wrappedArray->Count(); i++)
    	aLines.AppendL(wrappedArray->At(i));
    
    wrappedArray->Reset();
    delete wrappedArray;
	}


//This is a line of text. We want to break it up so that if fits.
