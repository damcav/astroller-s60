/*
 * Grid.cpp
 *
 *  Created on: 10-May-2010
 *      Author: Damien
 */

#include "Grid.h"
#include "CommonTypes.h"


TGrid::TGrid(const TSize& aSize)
	: iSize(aSize)
	{
	iHrBorder = iVtBorder = iRows = iColumns = iButVtBorder = iButHrBorder = 0;
	}

void TGrid::SetBorders(TInt aVertical, TInt aHorizontal)
	{
	iVtBorder = aVertical;
	iHrBorder = aHorizontal;
	}

void TGrid::SetCellBorders(TInt aVertical, TInt aHorizontal)
	{
	iButVtBorder = aVertical;
	iButHrBorder = aHorizontal;
	}

void TGrid::SetCellExtent(TInt aWidth, TInt aHeight)	//in cell numbers
	{
	iButCellExtent.iWidth = aWidth * iCellSize.iWidth;
	iButCellExtent.iHeight = aHeight * iCellSize.iHeight;
	}

TRect TGrid::Rect(TInt aX, TInt aY)
	{
	TPoint p(aX * iCellSize.iWidth, aY * iCellSize.iHeight);
	p += TPoint(iVtBorder, iHrBorder);
	TRect rect(p, iButCellExtent);
	rect.Shrink(iButVtBorder, iButHrBorder);
	return rect;
	}
	
TSize TGrid::SetDim(TInt aColumns, TInt aRows, TGridPolicy aPolicy)	//returns grow factor
	{
	iRows = aRows;
	iColumns = aColumns;
	TSize grow;
	
	TInt w = iSize.iWidth;

	w -= (iVtBorder * 2);

	TInt remainder = w % aColumns;
	//TDebug::Print(remainder);
	
	if (aPolicy == EShrink)
		{
		grow.iWidth = -remainder;
		w += grow.iWidth;
		}
	else if (aPolicy == EGrow)
		{
		grow.iWidth = (aColumns - remainder);
		w += grow.iWidth;
		}
	else
		{
		iVtBorder += (remainder / 2);
		}
			
	iCellSize.iWidth = w / aColumns;
	
//-----------------------------------
	
	TInt h = iSize.iHeight;
	h -= iHrBorder * 2;

	remainder = h % aRows;
	
	if (aPolicy == EShrink)
		{
		grow.iHeight = -remainder;
		h += grow.iHeight;
		}
	else if (aPolicy == EGrow)
		{
		grow.iHeight = (aRows - remainder);
		h += grow.iHeight;
		}
	else
		{
		iHrBorder += (remainder / 2);
		}
			
	iCellSize.iHeight = h / aRows;
	
	//TDebug::Print(iCellSize.iWidth, iCellSize.iHeight);
	return grow;
	}

