/*
 * ==============================================================================
 *  Name        : SimpleCubeApp.cpp
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */

// INCLUDE FILES
#include    <eikstart.h>
#include    "QGazerApp.h"
#include    "QGazerDocument.h"
#include	"QGazer.hlp.hrh"

CQGazerApp* gApp;

// ================= MEMBER FUNCTIONS =======================

// ---------------------------------------------------------
// CQGazerApp::AppDllUid()
// Returns application UID
// ---------------------------------------------------------
//
TUid CQGazerApp::AppDllUid() const
    {
    return KUidSimpleCube;
    }


// ---------------------------------------------------------
// CQGazerApp::CreateDocumentL()
// Creates CQGazerDocument object
// ---------------------------------------------------------
//
CApaDocument* CQGazerApp::CreateDocumentL()
    {
    gApp = this;
    return CQGazerDocument::NewL( *this );
    }


// ================= OTHER EXPORTED FUNCTIONS ==============


CApaApplication* NewApplication()
    {
    return new CQGazerApp;
    }

TInt E32Main()
    {
    return EikStart::RunApplication(NewApplication);
    }

const TDesC& CQGazerApp::FullResourcePath(const TDesC& aFileName)
	{
	iPath.Zero();
	iPath.Copy(AppFullName().Left(2));
	iPath.Append(_L("\\resource\\apps\\"));
	iPath.Append(aFileName);
	return iPath;
	}

const TDesC&  CQGazerApp::SetPrivatePath(RFs& aSession, TBool aWriteable)
	{
	TBuf< 1 > appDrive;
	appDrive.Copy(AppFullName().Left(1));
	aSession.SetSessionToPrivate( EDriveC );
	aSession.SessionPath(iPath);	//iPath now hold the full path of the private folder
									//this should always be C: for WINS
	
#if defined(__WINS__)
	aSession.CreatePrivatePath(EDriveC);
	//if (!aWriteable)
#else	
	iPath[0] = appDrive[0];
#endif
		

	aSession.SetSessionPath(iPath);
	return iPath;
	}
// End of File

