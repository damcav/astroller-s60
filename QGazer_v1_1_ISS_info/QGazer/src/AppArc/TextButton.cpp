/*
 * TextButton.cpp
 *
 *  Created on: 13-Apr-2010
 *      Author: Damien
 */

#include "TextButton.h"
#include "MButtonObserver.h"
#include "CommonTypes.h"

#include "colours.h"
extern TColours gColours;

#include "context.h"
extern Context* gContext;

#if(0)	
CQueueCallback* CQueueCallback::NewL(MQueueCallback& aObserver)
	{
	CQueueCallback* self = new (ELeave) CQueueCallback(aObserver);
	
	CActiveScheduler::Add(self);
	
	return self;	
	}


CQueueCallback::CQueueCallback(MQueueCallback& aObserver)
	: CActive(0), iObserver(aObserver)
	{

	}

void CQueueCallback::Queue()
	{
	if(!IsActive())
		{
		SetActive();
		TRequestStatus* status = &iStatus;   
		User::RequestComplete( status, KErrNone );
		}
	}

void CQueueCallback::RunL()
	{
	iObserver.QueueCallback();
	}

#endif
CTextButton* CTextButton::NewL(MButtonObserver& aObserver, TInt aId)
	{
	CTextButton* self = new (ELeave) CTextButton(aObserver, aId);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}


	
void CTextButton::ConstructL()
	{
	//SetColours(KRgbWhite, TRgb(0,0,64), KRgbWhite);
	SetColours(KFcStandardButton, KBcStandardButton);//DADA
	//iCb = CQueueCallback::NewL(*this);
	}

CTextButton::CTextButton(MButtonObserver& aObserver, TInt aId)
 : iObserver(aObserver), iId(aId)
	{
	iShadow = 3;
	iRound = 9;
	iCrossable = EFalse;
	iFontId = ETinyFont;
	iStyle = EButtonStyleNone;
	iHlColour = KBcButtonHighlight;
	iShadowColour = KRgbBlack;
	iShadowColour.SetAlpha(ALPHA);
	iAllowHighlighting = ETrue;
	}

void CTextButton::SetText(const TDesC& aString)
	{
	iText.Zero();
	iText.Append(aString);
	}

TPtrC KSuffixes[] = 
	{
		_S("th"),
		_S("st"),
		_S("nd"),
		_S("rd"),
		_S("th"),
		_S("th"),
		_S("th"),
		_S("th"),
		_S("th"),
		_S("th")
	};
		
		
void CTextButton::Draw(const TRect& aRect) const
	{
	
	CWindowGc& gc = SystemGc();
	gc.SetPenStyle(CGraphicsContext::ESolidPen);
	
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);

	gc.SetPenSize(TSize(1, 1));

	TRect rect(aRect);
	
	if (iShadow>-1)
		{
		gc.SetPenColor(NM(iShadowColour));
		gc.SetBrushColor(NM(iShadowColour));
		
		rect.Shrink(iShadow/2, iShadow/2);
		rect.Move(iShadow/2, iShadow/2);
		gc.DrawRoundRect(rect, TSize(iRound, iRound));

		rect.Move(-iShadow, -iShadow);

		gc.SetPenColor(NM(iBdColour));

		if (iHighlighted)
			gc.SetBrushColor(NM(iHlColour));
		else
			gc.SetBrushColor(NM(iBgColour));
		
		gc.DrawRoundRect(rect, TSize(iRound, iRound));
		}
#if(0)	
	if (iBm)
		{
		TRect bmRect = TRect(TPoint(0,0), iBm->SizeInPixels());
		bmRect.Move((rect.Width() - bmRect.Width()) / 2, 0);
		
		TPoint p = bmRect.iTl;
		p += rect.iTl;
		gc.BitBlt(p, iBm);
		
		if (iCrossed)
			{
			gc.SetPenColor(KRgbRed);
			gc.SetPenSize(TSize(2,2));
			TPoint cross = bmRect.iBr;
			cross += aRect.iTl;
			gc.MoveTo(cross);
			gc.DrawLineBy(TPoint(-16, -16));
			gc.MoveBy(TPoint(0, 16));
			gc.DrawLineBy(TPoint(16, -16));
			}
		
		rect.SetHeight(rect.Height() - bmRect.Height());
		rect.Move(0, bmRect.Height());
		}
#endif
	UseFont(gc, iFontId);
	
	if (iBm)
		{
		TRect bmRect = TRect(TPoint(0,0), iBm->SizeInPixels());
		bmRect.Move((rect.Width() - bmRect.Width()) / 2, 0);
		bmRect.Move(0, (rect.Height() - bmRect.Height()) / 2);
		
		TPoint p = bmRect.iTl;
		p += rect.iTl;
		gc.BitBlt(p, iBm);
		
		if (iCrossed)
			{
			gc.SetPenColor(KRgbRed);
			gc.SetPenSize(TSize(2,2));
			TPoint cross = bmRect.iBr;
			cross += aRect.iTl;
			gc.MoveTo(cross);
			gc.DrawLineBy(TPoint(-16, -16));
			gc.MoveBy(TPoint(0, 16));
			gc.DrawLineBy(TPoint(16, -16));
			}
		
		
		TRect textRect(rect);
		textRect.SetHeight(FontHeight()+FontDescent());
		textRect.Move(0, rect.Height() - textRect.Height()+6);
		rect = textRect;
		
		//rect.SetHeight(rect.Height() - bmRect.Height());
		//rect.Move(0, bmRect.Height());
		}

	if (iHighlighted)
		gc.SetPenColor(NM(KFcButtonHighlight));
	else
		gc.SetPenColor(NM(iFgColour));
	
	DrawText(rect, iText, ECenter);
	
	if (iStyle == EButtonStyleDate)
		{
		TInt x = TextWidth(iText) / 2;
		x += rect.Width()/2;
		TInt h = FontHeight()+FontDescent();
		TInt y = (rect.Height() - h) / 2;
		
		TPoint p(x, y);
		p += rect.iTl;
		
		gc.DiscardFont();
		UseFont(gc, ETinyFont);

		TInt n = 0;
		if ((iText.Compare(_L("13")) != 0) && 
				(iText.Compare(_L("12")) != 0) &&
				(iText.Compare(_L("11")) != 0))
				
			n = iText[iText.Length()-1] - '0';
		
		DrawText(p, KSuffixes[n]);
		}
		
	//iHighlighted = EFalse;
	}

CTextButton::~CTextButton()
	{
	//delete iCb;
	}

void CTextButton::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	switch (aPointerEvent.iType)
		{
		case TPointerEvent::EButton1Down:
			if (iCrossable)
				iCrossed = !iCrossed;
			iObserver.KeyPressedL(iId);
			iHighlighted = iAllowHighlighting;
			break;
		default:;
		}
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

void CTextButton::SetColours(const TRgb& aFg, const TRgb& aBg, const TRgb& aBd)
	{
	iFgColour = aFg;
	iBgColour = aBg;
	iBgColour.SetAlpha(ALPHA);
	iBdColour = aBd;
	iBdColour.SetAlpha(ALPHA);
	}

TInt CTextButton::Id() const
	{ 
	return iId;
	}

TInt CTextDoubleButton::Id() const
	{
	return (iId2 << 8) | iId;
	}

CTextDoubleButton* CTextDoubleButton::NewL(MButtonObserver& aObserver, TInt aId1, TInt aId2)
	{
	CTextDoubleButton* self = new (ELeave) CTextDoubleButton(aObserver, aId1, aId2);
	return self;
	}

CTextDoubleButton::CTextDoubleButton(MButtonObserver& aObserver, TInt aId1, TInt aId2)
 : CTextButton(aObserver, aId1), iId2(aId2)
	{
	}

void CTextDoubleButton::SetText2(const TDesC& aString)
	{
	iText2.Zero();
	iText2.Append(aString);
	}

void CTextDoubleButton::HandlePointerEventL(const TPointerEvent &aPointerEvent)
	{
	switch (aPointerEvent.iType)
		{
		case TPointerEvent::EButton1Down:
			{
			TRect rect(Rect());
			rect.SetWidth(rect.Width()/2);
			if (rect.Contains(aPointerEvent.iPosition))
				{
				iObserver.KeyPressedL(iId);
				iHighlighted = iAllowHighlighting;
				}
			else
				{
				iObserver.KeyPressedL(iId2);
				iHighlighted2 = iAllowHighlighting;
				}
			}
			break;
			
		default:;
		}
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

void CTextDoubleButton::Draw(const TRect& aRect) const
	{
	CWindowGc& gc = SystemGc();
	gc.SetPenStyle(CGraphicsContext::ESolidPen);
	
	gc.SetBrushStyle(CGraphicsContext::ESolidBrush);
	
	gc.SetPenSize(TSize(1, 1));

	TRect rect(aRect);
	
	//Draw the black shadow
	if (iShadow>-1)
		{
		gc.SetPenColor(NM(KRgbBlack));
		gc.SetBrushColor(NM(KRgbBlack));
		
		rect.Shrink(iShadow/2, iShadow/2);
		rect.Move(iShadow/2, iShadow/2);
		gc.DrawRoundRect(rect, TSize(iRound, iRound));

		rect.Move(-iShadow, -iShadow);
		}

	gc.SetPenColor(NM(iBdColour));
	gc.SetBrushColor(NM(iBgColour));
	
	TRgb leftColour;
	TRgb rightColour;
	TRgb leftPenColour;
	TRgb rightPenColour;
	
	if (iHighlighted)
		{
		leftColour = NM(KBcButtonHighlight);
		leftPenColour = NM(KFcButtonHighlight);
		}
	else
		{
		leftColour = NM(iBgColour);
		leftPenColour = NM(iFgColour);		
		}
	
	if (iHighlighted2)
		{
		rightColour = NM(KBcButtonHighlight);
		rightPenColour = NM(KFcButtonHighlight);
		}
	else
		{
		rightColour = NM(iBgColour2);
		rightPenColour = NM(iFgColour);			
		}
	
	//Draw the left side
	gc.SetPenColor(leftColour);
	gc.SetBrushColor(leftColour);
	TRect part(rect);
	part.SetWidth(rect.Width()/2);
	gc.SetClippingRect(part);
	gc.DrawRoundRect(rect, TSize(iRound, iRound));
	
	//Draw the right side
	gc.SetPenColor(rightColour);
	gc.SetBrushColor(rightColour);
	part.Move(rect.Width()/2, 0);
	gc.SetClippingRect(part);
	gc.DrawRoundRect(rect, TSize(iRound, iRound));
	
	gc.SetClippingRect(rect);
	
	//draw the halves in two colours
	UseFont(gc, iFontId);

	TBuf<64> buf;
	buf.Append(iText);
	
	if (iStyle == EButtonStyleTime)
		buf.Append(_L(" : "));
	
	buf.Append(iText2);

	if (iHighlighted || iHighlighted2)
		gc.SetPenColor(KFcCurrentItem);
	else
		gc.SetPenColor(iFgColour);
	
	DrawText(rect, buf, ECenter);

	gc.DiscardFont();
	}
