/*
 * ==============================================================================
 *  Name        : SimpleCubeDocument.cpp
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */


// INCLUDE FILES
#include "QGazerDocument.h"
#include "QGazerAppUi.h"
#include "Model.h"

// ================= MEMBER FUNCTIONS =======================

// -----------------------------------------------------------------------------
// CQGazerDocument::CQGazerDocument
//
// Just calls the base class constructor.
// -----------------------------------------------------------------------------
//

CQGazerDocument::CQGazerDocument(CEikApplication& aApp)
: CAknDocument(aApp)
    {
    }

// -----------------------------------------------------------------------------
// CQGazerDocument::~CQGazerDocument
//
// Empty destructor.
// -----------------------------------------------------------------------------
//

CQGazerDocument::~CQGazerDocument()
    {
    delete iModel;
    iModel = NULL;
    }

// -----------------------------------------------------------------------------
// CQGazerDocument::ConstructL
//
// Empty 2nd phase constructor.
// -----------------------------------------------------------------------------
//

void CQGazerDocument::ConstructL()
    {
    iModel = CSkyModel::NewL();
    }

// -----------------------------------------------------------------------------
// CQGazerDocument::NewL
//
// -----------------------------------------------------------------------------
//

CQGazerDocument* CQGazerDocument::NewL(
        CEikApplication& aApp)     // CQGazerApp reference
    {
    CQGazerDocument* self = new (ELeave) CQGazerDocument( aApp );
    CleanupStack::PushL( self );
    self->ConstructL();
    CleanupStack::Pop();

    return self;
    }

// ----------------------------------------------------
// CQGazerDocument::CreateAppUiL()
// Constructs and returns a CQGazerAppUi object.
// ----------------------------------------------------
//

CEikAppUi* CQGazerDocument::CreateAppUiL()
    {
    return new (ELeave) CQGazerAppUi;
    }

// End of File
