/*
 * CQueue.cpp
 *
 *  Created on: 02-Apr-2010
 *      Author: Damien
 */

#include "CQueue.h"
#include "CommonTypes.h"

void CQueue::Store(CFloatingWindow** aWindow)
	{
	TStoredWindow sw(aWindow);
	iWindows.Append(sw);
	}

void CQueue::QueueDeleteTemporaries(TBool aDeleteAll)
	{
	if (!IsActive())
		{
		TBool del(EFalse);
		for (TInt i=0; i<iWindows.Count(); i++)
			{
			if (aDeleteAll || iWindows[i].Window()->Temporary())
				{
				iWindows[i].Window()->MakeVisible(EFalse);
				iWindows[i].MarkForDelete();
				iWindows[i].Window()->SetDeletePending();
				del = ETrue;
				}
			}
		
		if (del)
			{
			SetActive();
			TRequestStatus* status = &iStatus;   
			User::RequestComplete( status, KErrNone );
			}
		}
	}

void CQueue::QueueDeleteAll()
	{
	QueueDeleteTemporaries(ETrue);
	}

	
void CQueue::Delete(CFloatingWindow** aWindow) 
	{
	for (TInt i=0; i<iWindows.Count(); i++)
		{
		if (iWindows[i].WindowPtr() == aWindow)
			{
			iWindows[i].Delete();
			iWindows.Remove(i);
			break;
			}
		}
	}

void CQueue::QueueDelete(CFloatingWindow** aWindow) 
	{
	for (TInt i=0; i<iWindows.Count(); i++)
		{
		if (iWindows[i].WindowPtr() == aWindow)
			{
			iWindows[i].Window()->MakeVisible(EFalse);
			iWindows[i].MarkForDelete();
			iWindows[i].Window()->SetDeletePending();
			break;
			}
		}
	
	if(!IsActive())
		{
		SetActive();
 		TRequestStatus* status = &iStatus;   
		User::RequestComplete( status, KErrNone );
		}
	}

CQueue::CQueue() : CActive(100)
	{
	iAlpha = 100;
	iAlphaDelta = -25;
	}

CQueue* CQueue::NewL()
	{
	CQueue* self = new (ELeave) CQueue();
	CActiveScheduler::Add(self);
	return self;
	}

void CQueue::RunL()
	{
	if (iStatus.Int() == KErrNone)
		{
again:		
//		for (TInt i=0; i<iWindows.Count(); i++)
	for (TInt i=iWindows.Count()-1; i>=0; i--)
			{
			if (iWindows[i].MarkedForDelete())
				{
				iWindows[i].Delete();
				iWindows.Remove(i);
				goto again;
				}
			}
		}
	}

void CQueue::DeleteAll()
	{
	for (TInt i=0; i<iWindows.Count(); i++)
		iWindows[i].Delete();
	}

CQueue::~CQueue()
	{		
	Cancel();
	DeleteAll();
	StopFlashing();
	iWindows.Close();
	}

void CQueue::HideL(TBool aTf)
	{
	for (TInt i=0; i<iWindows.Count(); i++)
		if (iWindows[i].Window()->HiddenByMenu())
			if (!iWindows[i].Window()->DeletePending())
				iWindows[i].Window()->ForegroundL(!aTf);
	}

void CQueue::ForegroundL(TBool aFg)
	{
	for (TInt i=0; i<iWindows.Count(); i++)
		iWindows[i].Window()->ForegroundL(aFg);
	
	if (aFg)
		StartFlashingL();
	else
		StopFlashing();
	}

void CQueue::StopFlashing()
	{
	delete iTimer;
	iTimer = NULL;
	}

void CQueue::StartFlashingL()
	{
	if (!iTimer)
		{
		iTimer = CPeriodic::NewL(EPriorityHigh);
		TTimeIntervalMicroSeconds32 one(200000);
		iTimer->Start(one, one, TCallBack(Callback, this));
		}
	}

TInt CQueue::Callback(TAny* aPtr )
	{
	CQueue* me=(CQueue*)aPtr;
	me->FlashingWindows();
	return 0;
	}

void CQueue::FlashingWindows()
	{
	TBool flashing(EFalse);
	
	iAlpha += iAlphaDelta;
	if (iAlpha > 100 || iAlpha < 0)
		{
		iAlphaDelta =- iAlphaDelta;
		iAlpha += iAlphaDelta*2;
		}
			
	for (TInt i=0; i<iWindows.Count(); i++)
		flashing |= iWindows[i].Window()->Flash(iAlpha);
	
	if (!flashing)
		StopFlashing();
	}

void CQueue::ActivateL()
	{
	for (TInt i=0; i<iWindows.Count(); i++)
		iWindows[i].Window()->ActivateL();
	}
