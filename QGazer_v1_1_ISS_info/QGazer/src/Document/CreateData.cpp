/*
 * CreateData.cpp
 *
 *  Created on: 16-Mar-2010
 *      Author: Damien
 */

#include <f32file.h>
#include <t32wld.h>
#include "CreateData.h"
#include "QGazerApp.h"
#include "CommonTypes.h"
#include <BAUTILS.H>

extern CQGazerApp* gApp;

TInt CInStream::OpenStreamL(const TDesC& aFileName, TBool aWriteable)
	{
	TBuf<64> fn;
	fn = aFileName;	
	fn.Append(_L(".dat"));

	User::LeaveIfError(iSession.Connect());
	gApp->SetPrivatePath(iSession, aWriteable);
	
	if (!BaflUtils::FileExists(iSession, fn))
		return KErrNotFound;
		
	
//	TBuf<128> sp;
//	iSession.SessionPath(sp);
//	User::InfoPrint(sp);

	TRAPD(err, iStream.Open(iSession, fn, EFileRead));
	
	return err;
	}

TInt CInStream::ReadIntL()
	{
	return iStream.ReadInt32L();
	}

TReal CInStream::ReadFloatL()
	{
	TInt32 r = ReadIntL();
	return (TReal(r) / 1000.0);
	}

const TDesC& CInStream::ReadStringL()
	{
	iStream >> iBuf;
	return iBuf;
	}

CInStream::CInStream()
	{
	}

CInStream::~CInStream()
	{
	iStream.Close();
	iSession.Close();
	}


#ifdef CREATEDATA
void CTextStream::ListCitiesL()
	{
	 RFs session;
	 User::LeaveIfError(session.Connect());
    RFile file;
	_LIT(KFileName,"c:\\nokiaWScities.txt");
	User::LeaveIfError(file.Create(session, KFileName, EFileWrite));
	TFileText ft;
	ft.Set(file);	
	
	RWorldServer ws;
	User::LeaveIfError(ws.Connect());
	TWorldId id;
	id.SetSelect(EWithinWorld);
	
	User::LeaveIfError(ws.FirstCity(id));
	
	TInt err = KErrNone;
	while (err != KErrNotFound)
		{
		TCityData data;
		User::LeaveIfError(ws.CityData(data, id));
		ft.Write(data.iCountry);
		ft.Write(data.iCity);
		TLatLong ll = data.iLatLong;
		TInt32 lon = -ll.MinutesLong();
		TBuf<32> n;
		n.AppendNum(lon);
		ft.Write(n);

		TInt32 lat = ll.MinutesLat();
		n.Zero();		
		n.AppendNum(lat);
		ft.Write(n);

		n.Zero();		
		n.AppendNum(data.iUniversalTimeOffset);
		ft.Write(n);

		err = ws.NextCity(id);
		}
	
	file.Close();
	session.Close();
	}


CTextStream::CTextStream()
	{
	}

CTextStream::~CTextStream()
	{
	iFile.Close();
	iSession.Close();
	}

void CTextStream::OpenStreamL(const TDesC& aFileName)
	{
	//ListCitiesL();
	TBuf<64> fn;
	fn = aFileName;
	fn.Append(_L(".txt"));
	
	User::LeaveIfError(iSession.Connect());
	gApp->SetPrivatePath(iSession);
	User::LeaveIfError(iFile.Open(iSession, fn, EFileRead));
	iFt.Set(iFile);
	}

void CTextStream::ReadLineL()
	{
	User::LeaveIfError(iFt.Read(iLine));
	}

const TDesC& CTextStream::ReadStringL()
	{
	ReadLineL();
	return iLine;
	}

TReal CTextStream::ReadFloatL()
	{
	TReal f = (TReal)ReadIntL();
	f /= 1000.0;
	return f;
	}

TInt CTextStream::ReadIntL()
	{
	ReadLineL();
	TInt sign = 1;
	TInt s = 0;
	TInt l = iLine.Length();
	
	if (iLine[0]=='-')
		{
		sign = -1;
		l--;
		s++;
		}
	
	_LIT(KDigits,"0123456789");
	TInt powers[] = {1,10,100,1000,10000,100000,1000000,10000000};
	TInt total = 0;
	for (TInt p = l-1; p>=0; p--)
		{
		TChar c(iLine[s++]);
		TInt v = KDigits().Locate(c);
		
		User::LeaveIfError(v);
		total += v * powers[p];
		}
	return total*sign;
	}


#endif

COutStream::COutStream()
	{
	}

COutStream::~COutStream()
	{
	iStream.Close();
	iSession.Close();
	}

TInt COutStream::OpenStreamL(const TDesC& aFileName)
	{
	User::LeaveIfError(iSession.Connect());
	
	gApp->SetPrivatePath(iSession, ETrue);
	iFn.Zero();
	iFn.Append(aFileName);
	iFn.Append(_L(".dat"));
	
	TInt err = iStream.Open(iSession, iFn, EFileWrite);
	if (err == KErrNotFound)
		err=iStream.Create(iSession, iFn, EFileWrite);

	return err;
	}

void COutStream::WriteIntL(TInt aInt)
	{
	iStream.WriteInt32L(aInt);
	}

void COutStream::WriteFloatL(TReal aReal) 
	{
	TInt32 r = TInt32(aReal * 1000.0);
	WriteIntL(r);
	}

void COutStream::WriteStringL(const TDesC& aString) 
	{
	iStream << aString;
	}



/*
 * TUInt32 lon1000, TUint32 lat1000, TInt16 fileIndex
 */










/*	
	TBuf<32> n;
	n.Append(_L(" = "));
	
	n.AppendNum(ToFloatL(), TRealFormat(10,3));//ToFloatL());
	User::InfoPrint(n);
*/	

