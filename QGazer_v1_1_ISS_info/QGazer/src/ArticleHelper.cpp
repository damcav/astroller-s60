#include <ImageConversion.h>
#include <W32STD.H> 

#include "ArticleHelper.h"


#include "QGazerapp.h"
extern CQGazerApp* gApp;


extern CSkyModel* gModel;


TBuf<64> CArticleHelper::iFn;
_LIT(KDash, "_");


CArticleBitmap* CArticleBitmap::NewL(MArticleBitmapObserver& aObserver, const TDesC& aFilename, const TSize& aNewSize)
	{
	CArticleBitmap* self = new(ELeave) CArticleBitmap(aObserver, aNewSize);
	CleanupStack::PushL(self);
	self->ConstructL(aFilename);
	CleanupStack::Pop(self);
	return self;
	}

CArticleBitmap* CArticleBitmap::NewL(MArticleBitmapObserver& aObserver, const TDesC& aFilename)
	{
	CArticleBitmap* self = new(ELeave) CArticleBitmap(aObserver);
	CleanupStack::PushL(self);
	self->ConstructL(aFilename);
	CleanupStack::Pop(self);
	return self;
	}

CArticleBitmap::CArticleBitmap(MArticleBitmapObserver& aObserver)
	: iObserver(aObserver), iNewSize(TSize(0,0)), CActive(0)
	{
	iThumbnail = EFalse;
	}

CArticleBitmap::CArticleBitmap(MArticleBitmapObserver& aObserver, const TSize& aNewSize)
	: iObserver(aObserver), iNewSize(aNewSize), CActive(0)
	{
	iThumbnail = ETrue;
	}

void CArticleBitmap::ConstructL(const TDesC& aFilename)
	{
	User::LeaveIfError(iFs.Connect());

	const TDesC& sPath = gApp->SetPrivatePath(iFs);
	iFullPath.Zero();
	iFullPath.Append(sPath);
	iFullPath.Append(aFilename);

	iDecoder = CImageDecoder::FileNewL(iFs, iFullPath);//, _L8("image/jpeg"));
	
	const TFrameInfo& info = iDecoder->FrameInfo();
	
	iBitmap = new (ELeave) CFbsBitmap();
	
	if(!iThumbnail)
		{
		User::LeaveIfError(iBitmap->Create(info.iOverallSizeInPixels, info.iFrameDisplayMode));
		}
	else
		{
		TInt rf = iDecoder->ReductionFactor(info.iOverallSizeInPixels, iNewSize);
		TSize thumb;
		iDecoder->ReducedSize(info.iOverallSizeInPixels, rf, thumb);
		User::LeaveIfError(iBitmap->Create(thumb, info.iFrameDisplayMode));
		}
	
	iDecoder->Convert(&iStatus, *iBitmap);
	CActiveScheduler::Add(this);
	SetActive();
	}

CArticleBitmap::~CArticleBitmap()
	{
	Cancel();
	iFs.Close();
	delete iBitmap;
	iBitmap = NULL;
	delete iDecoder;
	iDecoder = NULL;
	}

void CArticleBitmap::RunL()
	{
	iObserver.BitmapReady(iStatus.Int(), iBitmap);
	iBitmap = NULL;
	}

void CArticleBitmap::DoCancel()
	{
	iDecoder->Cancel();
	}




//----------------------------------------------------------------------
CArticleHelper* CArticleHelper::NewL(MArticleHelperObserver& aObserver)
	{
	CArticleHelper* self = new(ELeave) CArticleHelper(aObserver);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}

void CArticleHelper::ConstructL()
	{
	iCallback = new (ELeave) CCallback(*this);
	}

CCallback* iCallback;
CArticleHelper::CArticleHelper(MArticleHelperObserver& aObserver)
	: iObserver(aObserver)
	{
	}

void CArticleHelper::AllArticleObjects(RPointerArray<TSkyObject>& aArray)
	
	{
	if (gModel->SatellitesCount())
		aArray.Append(&(gModel->Satellite(0)));
		
	aArray.Append(&(gModel->Sun()));
	
	aArray.Append(&(gModel->Moon()));
	
	for (TInt i=0; i<gModel->PlanetCount(); i++)
		if (gModel->Planet(i).Article().Compare(KDash) == 0)
			aArray.Append(&(gModel->Planet(i)));
	
	for (TInt i=0; i<gModel->MessierCount(); i++)
			if (gModel->Messier(i).Article().Compare(KDash) == 0)
				aArray.Append(&(gModel->Messier(i)));
	}

const TDesC& CArticleHelper::ArticleFileName(const TDesC& aN, TBool aText)
	{
	iFn.Zero();
	iFn.Append(_L("a_"));

	TInt s=aN.Locate('(');
	if ( s != KErrNotFound)
		{//It's a Messier object
		TInt f=aN.Locate(')');
		if (f != KErrNotFound)
			{
			s++;
			iFn.Append(aN.Mid(s, f-s));
			}
		}
	else
		{
		iFn.Append(aN);
		}
	
	for(;;)
		{
		TInt l = iFn.Locate(TChar(32));
		if (l != KErrNotFound)
			iFn[l] = TChar('_');
		else
    		break;
		};    		

	if (aText)
		iFn.Append(_L(".txt"));
	else
		{
		iFn.Append(_L(".jpg"));
		}
	return iFn;
	}

void CArticleHelper::NextThumb()
	{
	
	delete iArticleBitmap;
	iArticleBitmap = NULL;
	
	if (iCurrentThumb < iAllThumbnailsObjects->Count())
		Thumbnail((*(iAllThumbnailsObjects))[iCurrentThumb++], iThumbnailSize);
	//else
	//	TDebug::Print(_L("Done"));
	}
			
void CArticleHelper::AllThumbnails(RPointerArray<TSkyObject>& aObjects, const TSize& aSize)
	{
	iAllThumbnailsObjects = &aObjects;
	iThumbnailSize = aSize;
	iCurrentThumb = 0;
	iGettingThumbs = ETrue;
	NextThumb();
	}
	
TInt CArticleHelper::Bitmap(const TSkyObject* aObject)
	{
	const TDesC& name = CArticleHelper::ArticleFileName(aObject->Name(), EFalse);
	TRAPD(err, iArticleBitmap = CArticleBitmap::NewL(*this, name));
	return err;
	}

TInt CArticleHelper::Thumbnail(const TSkyObject* aObject, const TSize& aSize)
	{
	const TDesC& name = CArticleHelper::ArticleFileName(aObject->Name(), EFalse);
	TRAPD(err, iArticleBitmap = CArticleBitmap::NewL(*this, name, aSize));
	return err;
	}

void CArticleHelper::BitmapReady(TInt aErr, CFbsBitmap* aBitmap)
	{
	CFbsBitmap* bm = aBitmap;
	if (aErr != KErrNone)
		{
		delete bm;
		bm = NULL;
		}
	
	iObserver.BitmapReady(bm);
	
	if (iGettingThumbs)
		iCallback->CallMe();
	}

void CArticleHelper::Callback(TInt /*aId*/)
	{
	if (iGettingThumbs)
		NextThumb();
	}

CArticleHelper::~CArticleHelper()
	{
	delete iArticleBitmap;
	iArticleBitmap = NULL;
	delete iCallback;
	iCallback = NULL;
	}

void CArticleHelper::LoadArticleL(const TSkyObject* aObject, RBuf& aArticle)
	{
	const TDesC& name = CArticleHelper::ArticleFileName(aObject->Name(), ETrue);
	
	RFs session;
	
	CleanupClosePushL(session);
	User::LeaveIfError(session.Connect());

	gApp->SetPrivatePath(session);
	
	RFile file;
	CleanupClosePushL(file);
	
	User::LeaveIfError(file.Open(session, name, EFileRead));
	
	TInt size(0);
	User::LeaveIfError(file.Size(size));
	
	HBufC8* buf = HBufC8::NewL(size);
	CleanupStack::PushL(buf);
	
	TUint8* chPtr=CONST_CAST(TUint8*,buf->Ptr());
	TPtr8 ptr(chPtr, size);

	User::LeaveIfError(file.Read(0, ptr, size));
		
	TUint16* p16 = (TUint16*)chPtr;
	TPtrC16 pp16(p16,size/2);
	
	aArticle.Create(pp16.Mid(1));		//WHY????
	
	CleanupStack::PopAndDestroy(3);
	}
