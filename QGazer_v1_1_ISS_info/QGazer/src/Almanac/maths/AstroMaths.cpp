
#include "astromaths.h"

void oprntf(char* s) {}
void oprntf(char* s, float f) {}
void printf(char*) {};
void print_all(double) {}
void put_coords(double, int, int) {}
void print_tz(double jd,short use,double jdb,double jde,char zabr) {}

double date_to_jd( const date_time& date)

//From Meeus' Astronomical Formulae for Calculators, p23
	{
	int y = 0;
	int m = 0;

	if(date.mo > 2) 
		{
		y = date.y;
		m = date.mo;
		}
	else
		{
		y = date.y - 1;
		m = date.mo + 12;
		}

	double jd = (long) (365.25 * y)
			+ (long) (30.6001 * (m + 1)) 
			+ date.d
			+ 1720994.5;

	jd += date.h / 24. + date.mn / 1440. + date.s / 86400.;

	if(date.y > 1582.1015) 
		{
		long A = (long) (y / 100.0);
		long B = 2 - A + (long) (A / 4.0);
		jd += B;
		}
	return jd;
	}

void jd_to_date(double jd, date_time& date)
{
	/* from Jean Meeus, Astronomical Formulae for Calculators,
	   published by Willman-Bell Inc.
           Avoids a copyrighted routine from Numerical Recipes.
           Tested and works properly from the beginning of the
           Gregorian calendar era (1583) to beyond 3000 AD. */

	double jdtmp;
	long alpha;
	long Z;
	long A, B, C, D, E;
	double F;
//	double x;   /* for day-of-week calculation */

	jdtmp = jd + 0.5;

	Z = (long) jdtmp;

    

	F = jdtmp - Z;

	if(Z < 2299161) A = Z;
	else {
		alpha = (long) ((Z - 1867216.25) / 36524.25);
		A = Z + 1 + alpha - (long) (alpha / 4);
	}

	B = A + 1524;
	C = ((B - 122.1) / 365.25);
	D =  (365.25 * C);\
	E =  ((B - D) / 30.6001);

	date.d = B - D - (long)(30.6001 * E);
	if(E < 13.5) date.mo = E - 1;
		else date.mo = E - 13;
	if(date.mo  > 2.5)  date.y = C - 4716;
		else date.y = C - 4715;

	date.h = F * 24.;  /* truncate */
        date.mn = (F - ((float) date.h)/24.) * 1440.;
        date.s = (F - ((float) date.h)/24. -
                        ((float) date.mn)/1440.) * 86400;

}

double adj_time(double x)
{
	while(x > 12.0)
		x = x - 24.0;

	while(x < -12.0)
		x = x + 24.0;
		
	return x;
}


double altit(double dec, double ha, double lat, double& az)
//Alt it returned
{
	dec /= DEG_IN_RADIAN;
	ha /= HRS_IN_RADIAN;
	lat /= DEG_IN_RADIAN;
	double cosdec = cos(dec); 
	double sindec = sin(dec);
	double cosha = cos(ha); 
	double sinha = sin(ha);
	double coslat = cos(lat); 
	double sinlat = sin(lat);

	double x = DEG_IN_RADIAN * asin(cosdec*cosha*coslat + sindec*sinlat);
	double y =  sindec*coslat - cosdec*cosha*sinlat;
	double z =  -1. * cosdec*sinha;
	az = atan2(z,y);

	az *= DEG_IN_RADIAN;
	while(az < 0.) az += 360.;
	while(az >= 360.) az -= 360.;

	return x;
}

void sidereal_time (double jd_high, double& gst)
{
	double t = (jd_high -  J2000) / 36525.0;
	double t2 = t * t;
	double t3 = t2 * t;

	double st =  0.0 - 6.2e-6 * t3 + 0.093104 * t2 + 67310.54841
	  + 8640184.812866 * t
	  + 3155760000.0 * t;

	gst = fmod ((st / 3600.0), 24.0);

	if (gst < 0.0)
		gst += 24.0;
}


