#include "almanacimpl.h"

#include "datetime.h"

#include "astromaths.h"
#include "planets.h"
#include "moon.h"
#include "CommonTypes.h"



AlmanacImpl::AlmanacImpl()
{
	iDateTimeSet = iLongLatSet = false;
}

double AlmanacImpl::CurrentJulianDateTime() const
	{
	return iJulianDateTime;
	}

bool AlmanacImpl::SetJulianDateTime(const date_time& aDate)
{
	iJulianDateTime = ::date_to_jd(aDate);

	iDateTimeSet = true;

	::sidereal_time(iJulianDateTime, iGreenwichSiderealTime);

	::planetaryOrbitalElements(iJulianDateTime);

	::planetxyz(3, iJulianDateTime, &iEarthX, &iEarthY, &iEarthZ);
	::eclrot(iJulianDateTime,  &iEarthX, &iEarthY, &iEarthZ);

	for (int i=0; i<10; i++)
	{
		if (i != 3)
		{
			double x, y, z;
			::planetxyz(i, iJulianDateTime, &x, &y, &z);
			::eclrot(iJulianDateTime, &x, &y, &z);
			::earthview(x, y, z, iEarthX, iEarthY, iEarthZ, &iRA[i], &iDec[i]);
			iRA[i] *= 15.0;
		}
	}

	SetLocalSiderealTime();
	return true;
}

double AlmanacImpl::GetJulianDateTime(const date_time& aDate)
	{
	return ::date_to_jd(aDate);
	}
/*
 -180 to 0 = WEST
 0 to 180 = EAST
 at GS = 0, RA = 0
 RA  = LONG - GS*15;
 ok
 RA  = 0
 if gs == 6
 long = -6
 so ra
 */
#
bool AlmanacImpl::SetLongLat(double aLongitude, double aLatitude)
{
	iLongLatSet = true;

	iLongitude = aLongitude;
	iLatitude = aLatitude;
	SetLocalSiderealTime();
	return true;
}

bool AlmanacImpl::SiderealTime(double& aSiderealTime)
{
	aSiderealTime = iGreenwichSiderealTime;
	return true;
}

bool AlmanacImpl::PlanetPosition(int aPlanetNumber, double& aRA, double& aDec)
{
	aRA = iRA[aPlanetNumber];
	aDec = iDec[aPlanetNumber];
	return true;
}

bool AlmanacImpl::MoonPosition(double& aRA, double& aDec, double& aDistance)
{
	::lpmoon(iJulianDateTime, iLatitude, iLocalSiderealTime, &aRA, &aDec, &aDistance);
	aRA *= 15.0;
	return true;
}

bool AlmanacImpl::ToHorizontal(double aRA, double aDec, double& aAzimuth, double& aAltitude)
{
	double ha = ::adj_time(iLocalSiderealTime - (aRA / 15.0));
	aAltitude = ::altit(aDec, ha, iLatitude, aAzimuth);
	return true;
}

void AlmanacImpl::SetLocalSiderealTime()
{
	if (iDateTimeSet && iLongLatSet)
	{
		iLocalSiderealTime = iGreenwichSiderealTime + iLongitude/15.0;
		if (iLocalSiderealTime > 24.0)
			iLocalSiderealTime -= 24.0;
		else if (iLocalSiderealTime < 0)
			iLocalSiderealTime += 24.0;
	}
}

bool AlmanacImpl::NextMoonPhase(int aPhase, date_time& aDate)
	{
//Find current lunation
	int lunation = int((iJulianDateTime - 2415020.5) / 29.5307);
	double jd=0;
	
	::flmoon(lunation, aPhase, jd);

	if (iJulianDateTime > jd)	//must be in the next lunation
		::flmoon(++lunation, aPhase, jd);
	
	::jd_to_date(jd, aDate);
	
	return true;
	}

bool AlmanacImpl::LastMoonPhase(int aPhase, date_time& aDate)
	{
	int lunation = int((iJulianDateTime - 2415020.5) / 29.5307);
	double jd=0;
	
	::flmoon(lunation, aPhase, jd);

	if (iJulianDateTime < jd)	//must be in the previous lunation
		::flmoon(--lunation, aPhase, jd);
	
	::jd_to_date(jd, aDate);
	return true;
	}



