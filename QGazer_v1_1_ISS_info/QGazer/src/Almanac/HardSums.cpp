/*
 *      This file, which is part of the project "QGazer", is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "HardSums.h"
#include "CommonTypes.h"
#include "context.h"
#include <math.h>
#include "Satellites.h"

#include "..\..\almanac\almanac.h"

extern Context* gContext;
CHardSums* gHardSums;

CHardSums::CHardSums()
	{
	}

CHardSums* CHardSums::NewL()
    {
    CHardSums* self = new (ELeave) CHardSums();
    CleanupStack::PushL( self );
    self->ConstructL();
    CleanupStack::Pop();
    return self;
    }

void CHardSums::ConstructL()
	{
	iAlmanac = Almanac::New();
	gHardSums = this;
	iSatMaths = CSatellites::NewL();
	}

CHardSums::~CHardSums()
{
	delete iAlmanac;
	delete iSatMaths;
}

double CHardSums::GetJulianDate(const TDateTime& aDateTime)
	{
	date_time dt;
	dt.d = aDateTime.Day()+1;
	dt.mo = aDateTime.Month()+1;
	dt.y = aDateTime.Year();
	dt.h = aDateTime.Hour();
	dt.mn = aDateTime.Minute();
	dt.s = aDateTime.Second();
	
	return iAlmanac->GetJulianDateTime(dt);	
	}

void CHardSums::SetJulianDate(const TDateTime& aDateTime)
	{
	date_time dt;
	dt.d = aDateTime.Day()+1;
	dt.mo = aDateTime.Month()+1;
	dt.y = aDateTime.Year();
	dt.h = aDateTime.Hour();
	dt.mn = aDateTime.Minute();
	dt.s = aDateTime.Second();
	
	iAlmanac->SetJulianDateTime(dt);	
	}

double CHardSums::CurrentJulianDateTime() const
	{
	return iAlmanac->CurrentJulianDateTime();
	}

void CHardSums::Reset(const TLocation& aLocation, const TDateTime aDateTime)
{
	iDateTime = aDateTime;
	
	iAlmanac->SetLongLat(aLocation.Longitude().DecDegrees(), aLocation.Latitude().DecDegrees());
	
	SetJulianDate(aDateTime);

	double t;
	iAlmanac->SiderealTime(t);
	iGreenwichSiderealTime = (GLfloat)t;
	
	::gContext->SetGreenwichSiderealTime(iGreenwichSiderealTime);
	::gContext->SetOriginLongitude(270.0 - iGreenwichSiderealTime * 15.0);
}

//aRa = decimal degrees
void CHardSums::ToHorizontal(const TEquatorial e, THorizontal& h) const
{
	iAlmanac->ToHorizontal(e.iRA, e.iDec, h.iAzimuth, h.iAltitude);
}

void CHardSums::PlanetPos(int aPlanet, TEquatorial& e) const
{
	iAlmanac->PlanetPosition(aPlanet, e.iRA, e.iDec);
}

void CHardSums::MoonPos(TEquatorial& e) const
{
	double distance;	//distance is ignored for now
	iAlmanac->MoonPosition(e.iRA, e.iDec, distance);
}

void CHardSums::ToDateTime(const date_time& aDt, TDateTime& aDateTime)
	{
	aDateTime.SetYear(aDt.y);
	aDateTime.SetMonth(TMonth(aDt.mo - 1));
	aDateTime.SetHour(aDt.h);
	aDateTime.SetDay(aDt.d - 1);
	aDateTime.SetMinute(aDt.mn);
	aDateTime.SetSecond(TInt(aDt.s));
	}

void CHardSums::NextMoonPhase(TInt aPhase, TDateTime& aDateTime)
	{
	date_time dt;
	iAlmanac->NextMoonPhase(aPhase, dt);
	ToDateTime(dt, aDateTime);
	}

void CHardSums::LastMoonPhase(TInt aPhase, TDateTime& aDateTime)
	{
	date_time dt;
	iAlmanac->LastMoonPhase(aPhase, dt);
	ToDateTime(dt, aDateTime);
	}

static const double DEG_TO_RAD = 0.017453292519943295769236907684886;
static const double EARTH_RADIUS_IN_METERS = 6372797.560856;

double CHardSums::ArcInRadians(const TLocation& from, const TLocation& to) 
	{
    double latitudeArc  = (from.Latitude().DecDegrees() - to.Latitude().DecDegrees()) * DEG_TO_RAD;
    double longitudeArc = (from.Longitude().DecDegrees() - to.Longitude().DecDegrees()) * DEG_TO_RAD;
    double latitudeH = sin(latitudeArc * 0.5);
    latitudeH *= latitudeH;
    double lontitudeH = sin(longitudeArc * 0.5);
    lontitudeH *= lontitudeH;
    double tmp = cos(from.Latitude().DecDegrees()*DEG_TO_RAD) * cos(to.Latitude().DecDegrees()*DEG_TO_RAD);
    return 2.0 * asin(sqrt(latitudeH + tmp*lontitudeH));
	}

double CHardSums::DistanceInMeters(const TLocation& from, const TLocation& to)
	{
    return EARTH_RADIUS_IN_METERS*ArcInRadians(from, to);
	}

TInt CHardSums::DistanceBetween(const TPoint& aPoint1, const TPoint& aPoint2)
	{
	TPoint delta(aPoint1 - aPoint2);
	return TInt(sqrt(delta.iX*delta.iX + delta.iY*delta.iY));
	}


TBool CHardSums::SunriseSunset(TBool aSunrise, TDateTime& aDateTime)
	{
	return SunriseSunset(aSunrise, aDateTime, iDateTime);
	}

void CHardSums::UtcToPhoneTime(TDateTime& aDt)
	{
	TInt mins = gContext->PhoneTzMins().Int();
	TTime t(aDt);
	t += TTimeIntervalMinutes(mins);
	aDt = t.DateTime();
	}

TBool CHardSums::SunriseSunset(TBool aSunrise, TDateTime& aDateTime, const TDateTime& aNow, TBool aPhoneTime)
	{
	int N = TTime(aNow).DayNoInYear();
		
	double lngHour = gContext->Location().Longitude().DecDegrees() / 15.0;

	double t = aSunrise ? N + ((6 - lngHour) / 24) : N + ((18 - lngHour) / 24);

	double zenith = 90.8333 / DEG_IN_RADIAN;
	double latitude = gContext->Location().Latitude().DecDegrees() / DEG_IN_RADIAN;
	
	double  M = (0.9856 * t) - 3.289;
	double L = M + (1.916 * sin(M/DEG_IN_RADIAN)) + (0.020 * sin(2 * M/DEG_IN_RADIAN)) + 282.634;
	while (L>360.0) L-=360.0;
	while (L<0.0) L+=360.0;
	
	double RA = atan(0.91764 * tan(L/DEG_IN_RADIAN)) * DEG_IN_RADIAN;
	while (RA>360.0) RA-=360.0;
	while (RA<0.0) RA+=360.0;

	int Lquadrant  = (int( L/90)) * 90;
	int RAquadrant = (int(RA/90)) * 90;
	RA = RA + (Lquadrant - RAquadrant);

	double sinDec = 0.39782 * sin(L/DEG_IN_RADIAN);
	double cosDec = cos(asin(sinDec));

	double cosH = (cos(zenith) - (sinDec * sin(latitude))) / (cosDec * cos(latitude));
	if (Abs(cosH)>1.0)
		return EFalse;
	
	double H = acos(cosH)*DEG_IN_RADIAN;
	if (aSunrise)
		H = 360.0 - H;
	
	H /= 15.0;
	
	//double T = H + aSun.iRA/15 - (0.06571 * t) - 6.622;
	double T = H + RA/15 - (0.06571 * t) - 6.622;
	
	double UT = T - lngHour;

	/* BOOBOO
	if (aPhoneTime)
		{
		TInt mins = gContext->PhoneTzMins().Int();
		UT += mins / 60;
		}
	*/
	
	if (UT>24.0) 
		UT -= 24.0;
	else if (UT<0.0) 
		UT += 24.0;

	
	TTime time(0);
	time += TTimeIntervalSeconds(UT * 60 * 60);
	
	aDateTime = time.DateTime();
	
	aDateTime.SetYear(aNow.Year());
	aDateTime.SetMonth(aNow.Month());
	aDateTime.SetDay(aNow.Day());
	
	
	if (aPhoneTime)
		UtcToPhoneTime(aDateTime);
	
	return ETrue;
	}


GLfloat CHardSums::AngleDiff(GLfloat a2, GLfloat a1)
	{
	GLfloat diff = a2 - a1;
	while (diff < -180) diff  += 360;
	while (diff > 180) diff -= 360;
	return Abs(diff);
	}
