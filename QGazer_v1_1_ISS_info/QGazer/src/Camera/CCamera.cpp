/*
 * TCamera.cpp
 *
 *  Created on: 22-Mar-2010
 *      Author: Damien
 */

#include "CCamera.h"
#include "Context.h"
#include <math.h>

const GLfloat KXBase = 15.0;
extern Context* gContext;

TCamera::TCamera()
	{
	iX = KXBase;
	iY = 0.0;
	SetZoom(1.0);
	}

TCamera::TCamera(const TLocation& aLocation)
	{
	SetZoom(1.0); 
	Set(aLocation);
	}


void TCamera::SetY(const GLfloat aY)
	{
	iY = aY;
	if (iY < 0.0)
		iY += 360.0;
	else if (iY > 360.0)
		iY -= 360.0;
	}

void TCamera::SetX(const GLfloat aX)
	{
	iX = aX;
	
	if (gContext->InSpace())
		{
		if (iX < 0.0)
			iX += 360.0;
		else if (iX > 360.0)
			iX -= 360.0;
		}
	else
		{
		if (iX < iXBase)
			iX = iXBase;
		else if (iX > 90.0)
			iX = 90.0;
		}
	}

void TCamera::Add(const TPoint& aDelta)
	{
	TReal32 dy = TReal32(aDelta.iX) / 4.0;
	TReal32 dx = TReal32(-aDelta.iY) / 4.0;
	
	if (!gContext->InSpace())
		{
		dy =- dy;
		dx =- dx;
		}

	
	SetY(iY + dy);
	SetX(iX + dx);
	//need to clamp globe mode to -90 (270) -> 90 
	//TDebug::Print(iX, iY);
	if (gContext->InSpace())
		{
		if (iX > 90.0 && iX < 180.0)
			iX = 90.0;
		else
			if (iX > 180.0 && iX < 270.0)
			iX = 270.0;
		}
	}

void TCamera::AddZoom(const GLfloat aDelta)
	{
	//User::InfoPrint(_L("zoom"));
	SetZoom(iZ + aDelta);
	}

void TCamera::SetZoom(const GLfloat aZoom)
	{
	iZ = aZoom;
	GLfloat xb = aZoom - 1.0;
	xb *= 4;
	xb += 1.0;
	iXBase = KXBase / xb;
	}

void TCamera::Set(const THorizontal& aHr)
{
	//0 (north -> 180)
	//90 (east) -> 270
	//180 (south) -> 0
	//270 (west) -> 90

	GLfloat y = aHr.iAzimuth + 180.0;
	if (y >= 360.0)
		y -= 360.0;

	SetY(y);
	SetX(aHr.iAltitude);
}

GLfloat TCamera::Bearing(const TCamera& aTo)
{
	return DoBearing(iX, iY,  aTo.X(), aTo.Y());
}

GLfloat TCamera::DoBearing(GLfloat lat1, GLfloat long1, GLfloat lat2, GLfloat long2)
	{
	/*
		//Convert input values to radians
		lat1 /= DEG_IN_RADIAN;
		long1 /= DEG_IN_RADIAN;
		lat2 /= DEG_IN_RADIAN;
		long2 /= DEG_IN_RADIAN;

		GLfloat deltaLong = long2 - long1;
		//deltaLong =- deltaLong;

		GLfloat y = sin(deltaLong) * cos(lat2);
		GLfloat x = cos(lat1) * sin(lat2) -
				sin(lat1) * cos(lat2) * cos(deltaLong);
		GLfloat bearing = atan2(y, x);
		bearing *= DEG_IN_RADIAN;
		
		bearing += 360;
		if (bearing>=360)
			bearing -= 360;
		
		return bearing;
*/
	//lat1 /= DEG_IN_RADIAN;
	//l//ong1 /= DEG_IN_RADIAN;
	//lat2 /= DEG_IN_RADIAN;
	//long2 /= DEG_IN_RADIAN;
	
	GLfloat deltaLong = long2 - long1;
	GLfloat deltaLat = lat2 - lat1;
	
	if (deltaLong > 180.0) //181 -> -179
		deltaLong = -(360-deltaLong);
	else if (deltaLong < -180) 	//-181 -> 179
		deltaLong = 360 + deltaLong;
	
	GLfloat angle = atan2(deltaLat, deltaLong);
	angle *= DEG_IN_RADIAN;
	angle = 360.0 - angle;
	angle += 90.0;
	
	if (angle > 360.0)
		angle -= 360.0;
	
	return angle;
	}

void TCamera::Set(const TEquatorial& aEq)
{
	//when sliders are zero, iRA(0) is at screen right
	//therefore, mid screen = iRA(90)
	//if we want to go to 270, we go to 0
	//269 -> 1
	//270 -> 0
	//0 -> 270
	//271 -> 359

	iX = -aEq.iDec;
	iY = 270.0 - aEq.iRA;
}

//iOriginLongitude = long when ySlider = 0, range 0->360
//as ySlider increases, long decreases
//therefore,	long = iOriginLongitude - ySlider
//				ySlider = iOriginLongitude - long
void TCamera::Set(const TLocation& aLocation)
	{
	GLfloat l = aLocation.Longitude().DecDegrees();
	if (l < 0)
		l = 360 + l;	//convert -180->180 to 0->360

	GLfloat y = gContext->OriginLongitude() - l;
	if (y < 0)
		y = 360 + y;

	GLfloat x = -aLocation.Latitude().DecDegrees();
	if (x < 0)
		x = 360 + x;

	iX = x;
	if (iX < 0.0)
		iX += 360.0;
	else if (iX > 360.0)
		iX -= 360.0;
//	SetX(x);//damn, being clamped to 90 when in earth mode
	SetY(y);
	}

void TCamera::Location(TLocation& aLocation) const
	{
	TReal lon = iY;
	lon = gContext->OriginLongitude() - lon;
	if (lon < -180.0)
		lon = (360.0 + lon);

	//0->90 = 0->-90 , 270->360 = 0->90
	TReal lat = iX;
	if (lat >= 180.0)
		lat = 360.0 - lat;
	else
		lat = -lat;

	aLocation.Set(lon, lat);
	}


TBool TCamera::CheckEarth(const TPoint& aPos, TLocation &aLocation)
{
	#define  DEG_IN_RADIAN     57.2957795130823
	const TVector& v = gContext->EarthPosAndSize();	// pos=x,y, radius = z
	float r = v.iZ;
	TPoint earthPos(TInt(v.iX), TInt(v.iY));
	TPoint delta(aPos - earthPos);
	float l = sqrt(delta.iX*delta.iX + delta.iY*delta.iY);//= delta.length();

	if (l >= r)
		return EFalse; //not on the earth!

	//Work out how many degrees 'around' the sphere we are.
	float degrees = acos(l / r) * DEG_IN_RADIAN;

	//From this, we can get our z coord
	float z = r * sin(degrees / DEG_IN_RADIAN);

	//And so we have our 3D coords
	TVector pos(delta.iX, delta.iY, z);

	//Rotate the coords according to the X rotation
	pos.RotateX(-iX);
	
	float latitude = acos(pos.y() / r) * DEG_IN_RADIAN;

	float cosLong, longitude;

	if (pos.iZ > 0)
	{
		cosLong = pos.iX / (r*sin(latitude/DEG_IN_RADIAN));
		longitude = acos(cosLong) * DEG_IN_RADIAN;
	}
	else
	{
		return EFalse;
	}

	//We have the relative longitude of the mouse pos
	//180 = left sphere, 90 = middle, 0 = right
	longitude = (90.0 - longitude);
	//Add the camera longitude
	
	TLocation loc;
	Location(loc);
	longitude += loc.Longitude().DecDegrees();

	latitude -= 90.0;

	if (longitude > 180.0)	//eg 181 becomes -179
		longitude = -(360.0 - longitude);

	aLocation.Set(longitude, latitude);
	return ETrue;
}


