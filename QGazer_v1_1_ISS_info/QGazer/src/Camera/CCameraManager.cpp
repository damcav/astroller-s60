/*
 * CCameraManager.cpp
 *
 *  Created on: 27-Mar-2010
 *      Author: Damien
 */

#include "CCameraManager.h"
#include "Context.h"
#include "HardSums.h"
#include "objects.h"
#include "MAppUiCb.h"
#include "Utils3d.h"


extern MAppUiCb* gAppUiCb;

extern Context* gContext;
extern CHardSums* gHardSums;

CCameraManager::CCameraManager()
	{
	}

CCameraManager::~CCameraManager()
	{
	// TODO Auto-generated destructor stub
	delete iTimer;
	iTimer = NULL;
	}

void CCameraManager::SetSpace(const TCamera& aCamera)
	{
	iSpace = aCamera;
	}

void CCameraManager::SetEarth(const TCamera& aCamera)
	{
	iEarth = aCamera;
	}

void CCameraManager::Save()
	{
	if (gContext->InSpace())
		iSpace = gContext->Camera();
	else
		iEarth = gContext->Camera();
	}

void CCameraManager::Restore()
	{
	if (gContext->InSpace())
		{
		gContext->Camera() = iSpace;
		}
	else
		gContext->Camera() = iEarth;
	}

void CCameraManager::StopEffectsL()
	{
	delete iTimer;
	iTimer = NULL;
	iMoveToRunning = EFalse;
#ifdef ZOOM_EFFECT
	gContext->SetZoomEffectRunning(EFalse);
#endif
	}

void CCameraManager::LaunchTimerL()
	{
	if (!iTimer)
		{
		iTimer = CPeriodic::NewL(EPriorityHigh);
	
		TInt timerFrequency = gContext->FastOpenGL() ? TIMER_FREQUENCY_FAST : TIMER_FREQUENCY_SLOW;
		TTimeIntervalMicroSeconds32 one(timerFrequency);
		iTimer->Start(one, one, TCallBack(Callback, this));
		}
	}

TInt CCameraManager::Callback(TAny* aPtr)
	{
	CCameraManager* me=(CCameraManager*)aPtr;
	me->DoCallbackL();
	return 0;
	}

void CCameraManager::DoCallbackL()
	{
#ifdef ZOOM_EFFECT
	if (gContext->ZoomEffectRunning())
		{
		if (!gAppUiCb->Zoom(iZoomingIn))
			{
			StopEffectsL();
			gAppUiCb->FindFinishedL(ETrue);
			}
		return;
		}
#endif
	
	if (iMoveToRunning)
		{
		if (gContext->SensorMode() && !gContext->UserLocationMode())
			{
			StopEffectsL();
			gAppUiCb->RefreshL();
			return;
			}
		
		TBool finished(EFalse);
		TTime now;
		now.HomeTime();
		GLfloat elapsed = GLfloat(now.MicroSecondsFrom(iMoveToStartTime).Int64());
		
		if (elapsed > iMoveToDuration)
			{
			elapsed = iMoveToDuration;
			finished = ETrue;
			}

		GLfloat distanceTravelled = iMoveToVelocity * elapsed;
		TVector here = iMoveToStartPos;;
		here += (iMoveToDelta * distanceTravelled);
		gContext->Camera().SetX(here.iX);
		gContext->Camera().SetY(here.iY);
		gAppUiCb->Redraw();
		if (finished)
			{
			gAppUiCb->FindFinishedL();
			StopEffectsL();
			gAppUiCb->RefreshL();
			}
		}
	}

void CCameraManager::StartObjectFoundEffect()

{
	TCamera toCamera;
	TChosenObject& chosen = gContext->ChosenObject();
	
	if (gContext->InSpace())
		toCamera.Set(chosen.iEq);
	else
		toCamera.Set(chosen.iHr);
	
	InitMoveToEffectL(toCamera);
	}

void CCameraManager::InitMoveToEffectL(const TCamera& aToCamera)
	{
	iMoveToStartPos = TVector(gContext->Camera().X(), gContext->Camera().Y(), 0);
	
	GLfloat dx = aToCamera.X() - iMoveToStartPos.iX;
	GLfloat dy = aToCamera.Y() - iMoveToStartPos.iY;

	//check if it's quicker to wrap around for y
	
	if (dy > 180.0)
		dy = -(360.0 - dy);
	else if (dy < -180.0)
		dy = 360.0 + dy;

	//... but only wrap around for x if we're in space
	if (gContext->InSpace())
		{
		if (dx > 180.0)
			dx = -(360.0 - dx);
		else if (dx < -180.0)
			dx = 360.0 + dx;
		}

	if (dx != 0 || dy != 0)
		{
		iMoveToDelta.Set(dx, dy, 0.0);
		iMoveToDuration = 2000000;
		iMoveToVelocity = iMoveToDelta.Magnitude() / iMoveToDuration; 
		iMoveToDelta.Normalize();
		iMoveToStartTime.HomeTime();
		iMoveToRunning = ETrue;
		LaunchTimerL();
		}
	}	

#ifdef ZOOM_EFFECT
void CCameraManager::StartZoomIn()
	{
	gContext->SetZoomEffectRunning(ETrue);
	iZoomingIn = ETrue;
	LaunchTimerL();
	}
void CCameraManager::StartZoomOut()
	{
	gContext->SetZoomEffectRunning(ETrue);
	iZoomingIn = EFalse;
	LaunchTimerL();
	}

void CCameraManager::StopZoom()
	{
	StopEffectsL();
	gAppUiCb->RefreshL();
	}
	
#endif
