#ifndef COMMONTYPES_CPP
#define COMMONTYPES_CPP

#include "CommonTypes.h"
#include "CreateData.h"
//#include "MathUtils.h"
#include <GLES/gl.h> // OpenGL ES header file
#include <AknGlobalNote.h>

#include "qgstream.h"
#include "context.h"
extern Context* gContext;

static int _debug_count = 0;

TBuf<512> TBldStr::iString;
TBuf<32> TBldStr::iNumString;

const TDesC& TBldStr::Num2(TInt aNum)
	{
	iString.Zero();
	if (aNum < 10)
		AddNum(0);
	AddNum(aNum);
	return iString;
	}

void TBldStr::AddNum2(TInt aNum)
	{
	if (aNum < 10)
		AddNum(0);
	AddNum(aNum);
	}

const TDesC& TBldStr::Num(TInt aNum)
	{
	iString.Zero();
	AddNum(aNum);
	return iString;
	}

void TBldStr::Set(TChar aChar)
	{
	iString.Zero();
	Add(aChar);
	}
	
void TBldStr::Set(const TDesC& aString)
	{
	iString.Zero();
	Add(aString);
	}

void TBldStr::AddNum(TInt aNumber)
	{
	iNumString.Zero();
	iNumString.AppendNum(aNumber);
	Add(iNumString);
	}

void TBldStr::AddNum(TReal aNumber)
	{
	iNumString.Zero();
	iNumString.AppendNum(aNumber, TRealFormat(10,2));
	Add(iNumString);
	}

void TBldStr::Add(const TDesC& aString)
	{
	iString.Append(aString);
	}

void TBldStr::Add(TChar aChar)
	{
	iString.Append(aChar);
	}

void TBldStr::Clear()
	{
	iString.Zero();
	}

void TBldStr::Space()
	{
	Add(_L(" "));
	}

void TBldStr::Comma()
	{
	Add(_L(", "));
	}

void TBldStr::Colon()
	{
	Add(_L(":"));
	}


void TBldStr::AddDate(TDateTime aT)
	{
	_LIT(KMonths,"JanFebMarAprMayJunJulAugSepOctNovDec");
	
	AddNum(aT.Day()+1);
	Space();
	Add(KMonths().Mid(aT.Month()*3,3));
	Space();
	AddNum(aT.Year());
	}
	
void TBldStr::AddTime(TDateTime aT, TBool aSecs)
	{
	if (aT.Hour() < 10)
		AddNum(0);
	AddNum(aT.Hour());
	Colon();
	if (aT.Minute() < 10)
			AddNum(0);
	AddNum(aT.Minute());
	
	if (aSecs)
		{
		Colon();
		if (aT.Second() < 10)
			AddNum(0);
		AddNum(aT.Second());
		}
	}

void TBldStr::AddDateTime(TDateTime aT, TBool aSecs)
	{
	AddDate(aT);
	Space();
	AddTime(aT, aSecs);
	}

const TDesC& TBldStr::String()
	{
	return iString;
	}

CFileLog* TDebug::iFileLog;

void TDebug::FileLog(TBool aTf)
	{
	iFileLog = NULL;
	if (aTf)
		{
		iFileLog = new (ELeave) CFileLog();
		iFileLog->StartL();
		}
	}

void TDebug::CloseFileLog()
	{
	delete iFileLog;
	iFileLog = NULL;
	}

void TDebug::NoteL(const TDesC& aNote)
	{
	CAknGlobalNote* globalNote = CAknGlobalNote::NewLC();
	globalNote->ShowNoteL( EAknGlobalTextNote , aNote );
	CleanupStack::PopAndDestroy();
	}

void TDebug::Count()
	{
	Print(_debug_count++);
	}

TInt TDebug::BreakAt(TInt aStop)
	{
	if (_debug_count++ == aStop)
		{
		TInt x;
		x++;
		return KErrNoMemory;
		}
	return KErrNone;
	}


void TDebug::Print(const TDesC& aDes)
	{
#if !defined(__WINS__) // *sigh*	
	NoteL(aDes);
#else	
	User::InfoPrint(aDes);
#endif
	}

void TDebug::Print(const TPoint& aPoint)
	{
	TBldStr::Clear();
	TBldStr::AddNum(aPoint.iX);
	TBldStr::Space();
	TBldStr::AddNum(aPoint.iY);
	Print(TBldStr::String());
	}

void TDebug::Print(const TRgb& aColour)
	{
	TBldStr::Clear();
	TBldStr::AddNum(aColour.Red());
	TBldStr::Space();
	TBldStr::AddNum(aColour.Green());
	TBldStr::Space();
	TBldStr::AddNum(aColour.Blue());
	Print(TBldStr::String());
	}

void TDebug::Print(TDateTime aDateTime)
	{
	TBldStr::Clear();
	TBldStr::AddNum(aDateTime.Year());
	TBldStr::Space();
	TBldStr::AddNum(aDateTime.Month()+1);
	TBldStr::Space();
	TBldStr::AddNum(aDateTime.Day()+1);
	TBldStr::Space();
	TBldStr::AddNum(aDateTime.Hour());
	TBldStr::Space();
	TBldStr::AddNum(aDateTime.Minute());
	Print(TBldStr::String());
	}

void TDebug::Print(TReal aNum)
	{	
	TBuf<32> n;
	n.AppendNum(aNum, TRealFormat(10,3));
	Print(n);
	}

void TDebug::Print(TReal aNum1, TReal aNum2)
	{	
	TBuf<64> n;
	n.AppendNum(aNum1, TRealFormat(10,3));
	n.Append(_L(" "));
	n.AppendNum(aNum2, TRealFormat(10,3));
	Print(n);
	}

void TDebug::Print(TReal aNum1, TReal aNum2, TReal aNum3)
	{	
	TBuf<64> n;
	n.AppendNum(aNum1, TRealFormat(10,3));
	n.Append(_L(" "));
	n.AppendNum(aNum2, TRealFormat(10,3));
	n.Append(_L(" "));
	n.AppendNum(aNum3, TRealFormat(10,3));

	Print(n);
	}

void TDebug::Print(const TRect& aRect)
	{
	TBuf<64> n;
	n.AppendNum(aRect.iTl.iX);
	n.Append(_L(" "));
	n.AppendNum(aRect.iTl.iY);
	n.Append(_L(" "));
	n.AppendNum(aRect.iBr.iX);
	n.Append(_L(" "));
	n.AppendNum(aRect.iBr.iY);
	Print(n);
	}

void TDebug::Print(TInt aNum)
	{
	TBuf<32> n;
	n.AppendNum(aNum);
	Print(n);
	}

void TEquatorial::InternalizeL(INSTREAM& aStream)
	{
	iRA = aStream.ReadFloatL();
	iDec = aStream.ReadFloatL();
	}

#ifdef CREATEDATA
void TEquatorial::ExternalizeL(OUTSTREAM& aStream)
	{
	aStream.WriteFloatL(iRA);
	aStream.WriteFloatL(iDec);
	}
#endif

/*
void TGeographic::Set(TInt aDeg, TInt aMin, TInt aSec, TBool aPositive)
{
	iDeg=aDeg;
	iMin=aMin;
	iSec=aSec,
	iPositive=aPositive;
	DegreesToDecimalDegrees();
}
*/

void TGeographic::Set(TReal aDecDegrees)
{
	iDecDegrees = aDecDegrees;
	DecimalDegreesToDegrees();
}

void TGeographic::SetHours(TReal aHours, TReal aMin, TReal aSec)
{
	Set(aHours, aMin, aSec);
	DegreesToDecimalDegrees();
	Set(DecDegrees() * 15.0);
	//qDebug() << "...." << decDegrees() * 15.0;
}

void TGeographic::Set(TReal aDeg, TReal aMin, TReal aSec)
{
	iDeg=aDeg;
	iMin=aMin;
	iSec=aSec,
	iPositive=aDeg >= 0.0;
	if (!iPositive)
		iDeg =- iDeg;
	DegreesToDecimalDegrees();
}


void TGeographic::DecimalDegreesToDegrees()
{
	iPositive = ETrue;

	TReal degrees = iDecDegrees;
	if (degrees < 0)
	{
		iPositive = EFalse;
		degrees = -degrees;
	}

	iDeg = (TInt)degrees;
	degrees -= iDeg;
	TReal temp = (degrees * 60);
	iMin = (TInt)temp;
	temp -= iMin;
	iSec = (TInt)(temp * 60.0);
}

void TGeographic::DegreesToDecimalDegrees()
{
	iDecDegrees = iDeg + iMin / 60.0 + iSec / 3600.0;
	if (!iPositive) //north / east
		iDecDegrees = -iDecDegrees;
}

TBuf<16> TGeographic::iBuf;

const TDesC& TGeographic::String() const
	{
	iBuf.Zero();
	iBuf.AppendNum(iDeg);
	iBuf.Append(_L("' "));
	iBuf.AppendNum(iMin);
	iBuf.Append(_L("\" "));
	return iBuf;
	}

TLocation&  TLocation::operator = (const TLocation& aOther)
    {
	if (this != &aOther) // protect against invalid self-assignment
		{
		Set(aOther.Country(), aOther.City());
		Set(aOther.Longitude(), aOther.Latitude());
		iTzOffset = aOther.iTzOffset;
		}
	return *this;
    }

void TLocation::Set(const TDesC& aCountry, const TDesC& aCity)
	{
	iCity.Close();
	iCity.CreateL(aCity);
	iCountry.Close();
	iCountry.CreateL(aCountry);
	}

TBuf<32> TLocation::iBuf;

const TDesC& TLocation::String() const
	{
	iBuf.Zero();
	iBuf.Append(iLongitude.String());
	if (iLongitude.Positive()) 	//-ve
		iBuf.Append(_L("E "));
	else
		iBuf.Append(_L("W "));
	
	iBuf.Append(iLatitude.String());
	if (iLatitude.Positive()) 	//-ve
		iBuf.Append(_L("N"));
	else
		iBuf.Append(_L("S"));
	return iBuf;
	}

TLocation::TLocation()
	{
	iLongitude.Set(666.0);
	iLatitude.Set(666.0);
	
	}

TBool TLocation::IsSet()
	{
	return iLongitude.DecDegrees() != 666.0;
	}

void TLocation::Set(const TGeographic& aLongitude, const TGeographic& aLatitude)
	{
	iLongitude = aLongitude;
	iLatitude = aLatitude;
	}

TLocation::TLocation(TReal32 aLongitude, TReal32 aLatitude)
{
	iLongitude.Set(aLongitude);
	iLatitude.Set(aLatitude);
}

void TLocation::Set(TReal32 aLongitude, TReal32 aLatitude)
	{
	iLongitude.Set(aLongitude);
	iLatitude.Set(aLatitude);
	}

void TLocation::Close()
	{
	iCountry.Close();
	iCity.Close();
	}

TLocation::~TLocation()
{
	Close();
}

void TLocation::InternalizeL(INSTREAM& aStream)
	{
	iCountry.Close();
	iCity.Close();
	
	iCountry.Create(aStream.ReadStringL());
	
	iCity.Create(aStream.ReadStringL());
	
	iLongitude.Set(aStream.ReadFloatL());
	iLatitude.Set(aStream.ReadFloatL());
	TReal32 tzOffset;
	tzOffset = aStream.ReadFloatL();
	//iTzOffset = aStream.ReadFloatL();
	
	//ASSERT(iTzOffset < 14.0);
	}

void TLocation::ExternalizeL(OUTSTREAM& aStream)
	{
	aStream.WriteStringL(iCountry);	
	aStream.WriteStringL(iCity);	
	aStream.WriteFloatL(iLongitude.DecDegrees());
	aStream.WriteFloatL(iLatitude.DecDegrees());
	aStream.WriteFloatL(iTzOffset);
	}


	
void TScreenCoords::Set(TInt aX, TInt aY, TInt aZ, TBool aOnScreen)
{
	iPoint.iX = aX;
	iPoint.iY = aY;
	iZ=aZ;
	iOnScreen = aOnScreen;
}

TColour::TColour(TInt aRed, TInt aGreen, TInt aBlue, TInt aAlpha)
	{
	Set(aRed, aGreen, aBlue, aAlpha);
	}

TColour::TColour(TUint32 aInt)
	{
	Set(aInt);
	}

TUint32 TColour::Int32() const
	{
	return iRed | (iGreen<<8) | (iBlue<<16); 
	}

void TColour::Set(TUint32 aInt)
	{
	iRed = aInt & 255;
	iGreen = (aInt>>8) & 255;
	iBlue = (aInt>>16) & 255;
	iAlpha = 255; 
	iLum = (iRed + iGreen + iBlue) / 3;
	Clamp();	
	}

void TColour::Set(TInt aRed, TInt aGreen, TInt aBlue, TInt aAlpha)
	{
	iRed = aRed;
	iGreen = aGreen;
	iBlue = aBlue;
	iAlpha = aAlpha;
	iLum = (aRed + aGreen + aBlue) / 3;
	Clamp();
	}

void TColour::SetClamped(GLfloat aRed, GLfloat aGreen, GLfloat aBlue, GLfloat aAlpha)
	{
	iRedC = aRed;
	iGreenC = aGreen;
	iBlueC = aBlue;
	iAlphaC = aAlpha;
	iLumC = (aRed + aGreen + aBlue) / 3.0;
	}

void TColour::Clamp()
	{
	iRedC = ((GLfloat)iRed) / 255.0;
	iGreenC = ((GLfloat)iGreen) / 255.0;
	iBlueC = ((GLfloat)iBlue) / 255.0;
	iAlphaC = ((GLfloat)iAlpha) / 255.0;
	iLumC = (iRedC + iGreenC + iBlueC) / 3.0;
	}

void TColour::Copy4(GLfloat* aPtr) const
	{
	if (gContext->NightMode())
		{
		*aPtr++ = iLumC;
		*aPtr++ = 0.0;
		*aPtr++ = 0.0;
		}
	else
		{
		*aPtr++ = iRedC;
		*aPtr++ = iGreenC;
		*aPtr++ = iBlueC;
		}
	*aPtr = iAlphaC;
	}

void TColour::GlColour() const
	{
	if (gContext->NightMode())
		glColor4f(iLumC, 0.0, 0.0, iAlphaC);
	else
		glColor4f(iRedC, iGreenC, iBlueC, iAlphaC);
	}

void TColour::GLClearColour() const
	{
	if (gContext->NightMode())
		glClearColor(iLumC, 0.0, 0.0, iAlphaC);
	else
		glClearColor(iRedC, iGreenC, iBlueC, iAlphaC);
	}

void TColourTransform::Set(const TColour& aFrom, const TColour& aTo)
{
	if (!gContext->NightMode())
		{
		iFrom.Set(aFrom.Red(), aFrom.Green(), aFrom.Blue());
		iTo.Set(aTo.Red(), aTo.Green(), aTo.Blue());
		}
	else
		{
		iFrom.Set(aFrom.Lum(), 0, 0);
		iTo.Set(aTo.Lum(), 0, 0);
		}
	iDelta = iTo - iFrom;
	iLength = iDelta.Magnitude();
	iDelta.Normalize();
}

const TColour& TColourTransform::Colour(GLfloat aPercentage)
{
	TVector v = iDelta * (iLength * (aPercentage/100.0));
	v += iFrom;
	iColour.Set(v.iX, v.iY, v.iZ);
	return iColour;
}

_LIT(KLogFileName,"c:\\QGlog.txt");

void CFileLog::StartL()
	{
	 iSession.Connect();
	
	User::LeaveIfError(iFile.Replace(iSession, KLogFileName, EFileWrite));
	iFile.Close();
	}
 CFileLog::~CFileLog()
	{
	iFile.Close();
	iSession.Close();
	}

void CFileLog::AddL(const TDesC& aText)
	{
	User::LeaveIfError(iFile.Open(iSession, KLogFileName, EFileWrite));
	TInt pos;
	iFile.Seek(TSeek(ESeekEnd), pos);
	TFileText t;
	t.Set(iFile);
	t.Write(aText);
	iFile.Close();
	}


#endif // COMMONTYPES_CPP
