#include "Callback.h"

CCallback::CCallback(MCallback& aListener)
	: CActive(-200), iListener(aListener)
	{
	CActiveScheduler::Add(this);
	}

void CCallback::RunL()
	{
	if (iStatus.Int() == KErrNone)
		iListener.Callback(iId);
	}

void CCallback::CallMe(TInt aId)
	{
	if (!IsActive())
		{
		iId = aId;
		SetActive();
	 	TRequestStatus* status = &iStatus;   
		User::RequestComplete( status, KErrNone );	
		}
	}

void CCallback::DoCancel()
	{
	}

CCallback::~CCallback()
	{
	Cancel();
	}
