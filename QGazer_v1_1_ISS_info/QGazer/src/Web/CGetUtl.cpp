#include "CGetUrl.h"
#include "commontypes.h"

#include "QGazerapp.h"
extern CQGazerApp* gApp;

CGetUrl* CGetUrl::NewL(const TDesC8& aUrl, MGetUrlObserver& aObs)
	{
	CGetUrl* self = new (ELeave) CGetUrl(aObs);
	CleanupStack::PushL(self);
	self->ConstructL(aUrl);
	CleanupStack::Pop(self);
	return self;
	}

CGetUrl::CGetUrl(MGetUrlObserver& aObs)
	: iObs(aObs)
	{
	}

void CGetUrl::ConstructL(const TDesC8& aUrl)
	{
	iUrl.CreateL(aUrl);
	TRAPD(err, iEngine = CWebClientEngine::NewL(*this));
	if (err == KErrNone)
		{
		TRAP(err, iEngine->IssueHTTPGetL(iUrl));
		}
    
	switch (err)
    	{
    	case KErrNone:
    		break;
    	case KErrNotFound:
    		iObs.DownloadStatusL(EWebNoConnection);
    		break;
    	case KErrCancel:
    		iObs.DownloadStatusL(EWebCancelled);
    		break;
    	default:;
			iObs.DownloadStatusL(EWebError);
    	};
	}

CGetUrl::~CGetUrl()
	{
	delete iEngine;
	iEngine = NULL;
	iUrl.Close();
	iBuffer.Close();
	}
			
void CGetUrl::ClientBodyReceived( const TDesC8& aBodyData )
	{
	iBuffer.ReAlloc(iBuffer.Length() + aBodyData.Length() * 2);
	for (TInt i=0; i< aBodyData.Length(); i++)
		{
		iBuffer.Append(aBodyData[i]);
		iBuffer.Append(TChar(0));
		}
	}

void CGetUrl::ClientEventL( WebClientStatus aStatus )
	{
	if (aStatus == EWebFinished)
		SaveL(_L("satellites.txt"));

	iObs.DownloadStatusL(aStatus);
	}

void CGetUrl::SaveL(const TDesC& aName)
	{
_LIT(KTemp,"temp");	
	 RFs session;
	 CleanupClosePushL(session);
	 User::LeaveIfError(session.Connect());
	 gApp->SetPrivatePath(session);
	
	 RFile file;
	 CleanupClosePushL(file);
	 
	 TInt err=file.Replace(session, KTemp, EFileWrite);
	 if (err != KErrNone)
		 User::LeaveIfError(file.Create(session, KTemp, EFileWrite));
	 
	 User::LeaveIfError(file.Write(iBuffer));
	
	 CleanupStack::PopAndDestroy(1);	//RFile
	 
	 session.Delete(aName);
	 session.Rename(KTemp, aName);
	 
	 CleanupStack::PopAndDestroy(1);	//fname, session, temp
	}
