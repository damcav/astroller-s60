#include "Satellites.h"
#include "CSat.h"
#include "HardSums.h"

#include "Context.h"

extern Context* gContext;
const TInt KCheckMinutes = 360;
extern CHardSums* gHardSums;

CSatellites* CSatellites::NewL()
    {
	CSatellites* self = new (ELeave) CSatellites();
    CleanupStack::PushL( self );
    self->ConstructL();
    CleanupStack::Pop();
    return self;
    }

void CSatellites::ConstructL()
	{
	iSatImpl = new CSat();
	iCallback = new CCallback(*this);
	}

CSatellites::~CSatellites()
	{
	delete iSatImpl;
	iSatImpl = NULL;
	delete iCallback;
	iCallback = NULL;
	}



void CSatellites::SatellitePos(TSatData& aSat, TInt aDayNumber)
	{
	char tle1[] = "Dummy"; 
	const char* tle2 = NULL;
	const char* tle3 = NULL;
	
	if (aDayNumber == -1)
		{
		TTime now;
		now.UniversalTime();
		aDayNumber = now.DayNoInYear();
		}
	
	aSat.iTle->TleChar(aDayNumber, &tle2, &tle3);
	
	if (aSat.iJd < 1.0)
		aSat.iJd = gHardSums->CurrentJulianDateTime();
		
	TBool changed = EFalse;

	if ((iOldTle2 != tle2)
			|| (iOldTle3 != tle3))
		{
		iOldTle2 = tle2;			
		iOldTle3 = tle3;
		iSatImpl->InitTle(tle1, tle2, tle3 );
		changed = ETrue;
		}

	if (changed || (aSat.iJd != iOldSatJd))
		{
		iOldSatJd = aSat.iJd;
		iSatImpl->SetTime(aSat.iJd);
		}
	
	if (aSat.iGetLongLat)
		{
		double lon = 0.0;
		double lat = 0.0;
		double alt = 0.0;
		iSatImpl->LonLatAlt(lon, lat, alt);
		aSat.iSatLoc.Set(lon, lat);
	
		aSat.iEq.iDec = lat;
		aSat.iEq.iRA = lon;	//TODO ADJUST FOR SIDEREAL TIME!!!!
		aSat.iEq.iRA  = gHardSums->GreenwichSiderealTime()*15 + lon;
		if (aSat.iEq.iRA  < 0)
			aSat.iEq.iRA  += 360.0;
		}
	
	if (aSat.iGetObsRaDec)
		{
		aSat.iEq.iRA = 0.0;
		aSat.iEq.iDec = 0.0;
		iSatImpl->ObsRaDec(
				aSat.iObsLoc.Longitude().DecDegrees(),
				aSat.iObsLoc.Latitude().DecDegrees(),
				aSat.iEq.iRA,
				aSat.iEq.iDec);
	
		}
	
	if (aSat.iGetObsAziAlt)
		{
		aSat.iVisible = iSatImpl->ObsAziAlt(
				aSat.iObsLoc.Longitude().DecDegrees(),
				aSat.iObsLoc.Latitude().DecDegrees(),
				aSat.iHr.iAzimuth,
				aSat.iHr.iAltitude);
		}
	
	if (aSat.iGetSunlit)
		{
		aSat.iSunlit = iSatImpl->Sunlit();
		}
	}


void TSatTle::TleChar(TInt aDayNumber, const char** aTle2, const char** aTle3) const
	{
	TInt c = iDayNumArray.Count();
	TInt indx = c-1; 
	
	for (TInt i=0; i<c; i++)
		if (iDayNumArray[i] == aDayNumber)
			{
			indx = i;
			
			break;
			}
	
	 *aTle2 = (const char*)iTle2Array[indx].Ptr(); 
	 *aTle3 = (const char*)iTle3Array[indx].Ptr();
 	}

void TSatTle::Close()
	{
	for (TInt i=0; i<iTle2Array.Count(); i++)
		iTle2Array[i].Close();
	iTle2Array.Close();
	
	for (TInt i=0; i<iTle3Array.Count(); i++)
		iTle3Array[i].Close();
	iTle3Array.Close();
	iDayNumArray.Close();
	}

//at this point we've already reading the name, now read all the data
#if(0)
ISS
#2011
#260
1 25544U 98067A   11259.14316919  .00016717  00000-0  10270-3 0  9009
2 25544  51.6378  62.9154 0011151 198.0648 162.0114 15.61132288 15114
#261
1 25544U 98067A   11259.14316919  .00016717  00000-0  10270-3 0  9009
2 25544  51.6378  62.9154 0011151 198.0648 162.0114 15.61132288 15114
#endif

void TSatTle::InternalizeL(TFileText& aFt, TDes& aLine)
	{
//next should be the year corresponding to the last entry
	User::LeaveIfError(aFt.Read(aLine));
	
	if (aLine[0] != TChar('#'))
		User::Leave(-1);
	if (aLine.Length() != 5)
		User::Leave(-1);

	iYear = ((aLine[1] - '0') * 1000);
	iYear += ((aLine[2] - '0') * 100);
	iYear += ((aLine[3] - '0') * 10);
	iYear += (aLine[4] - '0');
	
	//now get the days...
	while (InternalizeOneSetL(aFt, aLine));
	}

TBool TSatTle::InternalizeOneSetL(TFileText& aFt, TDes& aLine)
	{
	User::LeaveIfError(aFt.Read(aLine));
	
	if (aLine[0] == '^')
		return EFalse;
	
	if (aLine[0] != TChar('#'))
		User::Leave(-1);
	if (aLine.Length() != 4)
		User::Leave(-1);

	TInt dayNum = 0;
	dayNum = ((aLine[1] - '0') * 100);
	dayNum += ((aLine[2] - '0') * 10);
	dayNum += (aLine[3] - '0');

	if (dayNum == 999)
		return EFalse;
	
	iDayNumArray.Append(dayNum);
	
	RBuf8 tle2;
	tle2.Create(69);
	tle2.SetMax();
	User::LeaveIfError(aFt.Read(aLine));
	if (aLine.Length() >= 69)
		{
		for (TInt i=0; i<69; i++)
			tle2[i] = aLine[i];
		}
	
	RBuf8 tle3;	
	tle3.Create(69);
	tle3.SetMax();
	User::LeaveIfError(aFt.Read(aLine));
	if (aLine.Length() >= 69)
		{	
		for (TInt i=0; i<69; i++)
			tle3[i] = aLine[i];
		}
	
	iTle2Array.Append(tle2);
	iTle3Array.Append(tle3);
	
	return ETrue;
	}

TBool TSatTle::BeforeSatEpoch() const
	{
//iYear is the year of the last TLE entry. Since entries can wrap, the year of the first entry maybe the previous year	
	TInt year = iYear;
	TInt c = iDayNumArray.Count();
	if (iDayNumArray[c-1] < iDayNumArray[0])
		year--;
	
	TDateTime start;
	start.SetYear(year);
	start.SetMonth(EJanuary);
	start.SetDay(0);
	start.SetHour(12);

	TTime dt(start);
	dt += TTimeIntervalDays(iDayNumArray[0]);
	start = dt.DateTime();
	
	TTime now(gContext->UserTimeUtc());
	TBool before = now < dt;
	return before;
	}

TBool TSatTle::UpToDate(TDateTime& aValidFrom, TDateTime& aValidUntil) const
	{
	TInt c = iDayNumArray.Count();
	if (c < 2)
		return EFalse;

	TInt year = iYear;
	//if the final day < 1st day, the 1st day is in the previous year
	if (iDayNumArray[c-1] < iDayNumArray[0])
		year--;
	
	aValidFrom.SetYear(year);
	aValidFrom.SetMonth(EJanuary);
	aValidFrom.SetDay(0);
	aValidFrom.SetHour(12);
	TTime validFrom(aValidFrom);
	TInt addDays = iDayNumArray[0] - 1;
	validFrom += TTimeIntervalDays(addDays);

	aValidFrom = validFrom.DateTime();
	
	aValidUntil.SetYear(iYear);
	aValidUntil.SetMonth(EJanuary);
	aValidUntil.SetDay(0);
	aValidUntil.SetHour(12);
	TTime validUntil = TTime(aValidUntil);
	addDays = iDayNumArray[c-1] - 1;
	validUntil += TTimeIntervalDays(addDays);
	aValidUntil = validUntil.DateTime();

	TTime now;
	now.UniversalTime();
	
	return ( (now > validFrom) && (now < validUntil));
	}

TBool CSatellites::CheckSatInstant(TSatData& aSat, TTime& aCheck, TInt aDayNum)
	{
	aSat.iJd = gHardSums->GetJulianDate(aCheck.DateTime());

	aSat.iGetObsAziAlt = EFalse;
	aSat.iGetSunlit = ETrue;
	
	SatellitePos(aSat, aDayNum);
	
	if (aSat.iSunlit)
		{
		aSat.iGetSunlit = EFalse;
		aSat.iGetObsAziAlt = ETrue;
		SatellitePos(aSat, aDayNum);
		if (aSat.iHr.iAltitude > 10.0)
			return ETrue;
		}
	return EFalse;
	}

TBool CSatellites::CheckSatPeriod(TSatData& aSat, TTime& aCheck, TTimeIntervalMinutes aMinutes)
	{
	TInt dayNum = aCheck.DayNoInYear();
	
	for (TInt m=0; m < aMinutes.Int(); m++)
		{
		if (aCheck > iNow)
			{
			if (CheckSatInstant(aSat, aCheck, dayNum))
				{
				TInt safe = 0;
				do
					{
					aCheck -= TTimeIntervalSeconds(1);
					}
				while ( (++safe < 60) && (CheckSatInstant(aSat, aCheck, dayNum)));
				return ETrue;
				}
			}
		aCheck += TTimeIntervalMinutes(1);
		}
	return EFalse;
	}

void CSatellites::SatelliteNextVisible(TSatData* aSat, TDateTime* aVisible, TBool* aIsVisible, TRequestStatus* aStatus)
	{
	*aStatus = KRequestPending;
	
	iStatus = aStatus;
	iSat = aSat;
	iVisible = aVisible;
	iIsVisible = aIsVisible;
	
 	iCheckDay.UniversalTime();
 	iNow = iCheckDay;
 	iCheckDay -= TTimeIntervalDays(1);
 	
	*aIsVisible = EFalse;
	
	iCurrentDay = 0;
	iCallback->CallMe();
	}

void CSatellites::Callback(TInt /*aId*/)
{
	//check next 14 days
	
	//Get tonight's sunset	
	TDateTime setDT;
	TTime today = iCheckDay;	
	iCheckDay +=  TTimeIntervalDays(1);

	if (gHardSums->SunriseSunset(EFalse, setDT, today.DateTime(), EFalse))
		{
		//Get tomorrow's sunrise
		//
		//Since we're checking any place on earth, tomorrow's sunrise may be on the same UTC
		//day as tonight's sunset
//		TDebug::Print(setDT);
		//today +=  TTimeIntervalDays(1);	//move on one day
		TDateTime riseDT;		
		gHardSums->SunriseSunset(ETrue, riseDT, today.DateTime(), EFalse);
		if (TTime(riseDT) < TTime(setDT))
			{
			today +=  TTimeIntervalDays(1);	//move on one day
			gHardSums->SunriseSunset(ETrue, riseDT, today.DateTime(), EFalse);
			}
			
		if (1)
			{
			TTimeIntervalMinutes minutes;
			TTime(riseDT).MinutesFrom(TTime(setDT), minutes);
	
			TTime check(setDT);
			if (CheckSatPeriod(*iSat, check, minutes))
				{
				*iVisible = check.DateTime();
				*iIsVisible = ETrue;
				User::RequestComplete(iStatus, KErrNone);
				return;
				}
			}
		}

	if (iCurrentDay++ < 14)
		iCallback->CallMe();
	else
		User::RequestComplete(iStatus, KErrNone);	
	}

void CSatellites::CancelSatelliteNextVisible() 
	{
	iCallback->Cancel();
	User::RequestComplete(iStatus, KErrCancel);
	}
