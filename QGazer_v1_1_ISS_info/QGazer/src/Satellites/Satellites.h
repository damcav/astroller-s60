#ifndef __SATELLITES_H__
#define __SATELLITES_H__

#include <e32base.h>
#include <f32file.h>

#include "CommonTypes.h"
#include "Callback.h"

#ifdef SATELLITE

class CSat;

class TSatTle
	{
public:	
	void Close(); 
	void InternalizeL(TFileText& aFt, TDes& aLine);
	void TleChar(TInt aDayNumber, const char** aTle2, const char** aTle3) const;
	TBool UpToDate(TDateTime& aValidFrom, TDateTime& aValidUntil) const;
	TBool BeforeSatEpoch() const;
	
private:
	TBool InternalizeOneSetL(TFileText& aFt, TDes& aLine);

public:
	RArray<RBuf8> iTle2Array;
	RArray<RBuf8> iTle3Array;
	RArray<TInt> iDayNumArray;
	TInt iYear;
	};

class TSatData 
	{
public:		
	TSatData()
		{ 
		iJd = 0;
		iGetLongLat = iGetObsRaDec = iGetObsAziAlt = iGetSunlit = EFalse;
		iSunlit = iVisible = EFalse;
		iTle = NULL;
		}

		THorizontal iHr;
		TEquatorial iEq;
		TLocation iSatLoc;
		TLocation iObsLoc;
		double iJd;
		bool iGetLongLat;
		bool iGetSunlit;
		bool iGetObsRaDec;
		bool iGetObsAziAlt;
		bool iVisible;
		bool iSunlit;
		const TSatTle* iTle;
	};

class CSatellites : public CBase, public MCallback
	{
public:	
	static CSatellites* NewL();
	void ConstructL();
	~CSatellites();
	
	void SatellitePos(TSatData& aSat, TInt aDayNumber =  -1);
	void SatelliteNextVisible(TSatData* aSat, TDateTime* aVisible, TBool* aIsVisible, TRequestStatus* aStatus);
	void CancelSatelliteNextVisible();
	
protected:
	virtual void Callback(TInt aId = 0);
private:
	//CSatellites();
	TBool CheckSatPeriod(TSatData& aSat, TTime& aCheck, TTimeIntervalMinutes aMinutes);
	TBool CheckSatInstant(TSatData& aSat, TTime& aCheck, TInt aDayNum);
	
	const char* iOldTle1;
	const char* iOldTle2;
	const char* iOldTle3;
	double iOldSatJd;
	CSat* iSatImpl;
	CCallback* iCallback;
	
	TSatData* iSat;
	TDateTime* iVisible;
	TBool* iIsVisible;
	TTime iCheckDay;
	TTime iNow;
	TInt iCurrentDay;
	TRequestStatus* iStatus;
	};

#endif
#endif //__SATELLITES_H__
