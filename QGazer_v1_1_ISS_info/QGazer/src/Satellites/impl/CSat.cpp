/*
 * CSat.cpp
 *
 *  Created on: 15 Aug 2011
 *      Author: Damien
 */

#include "CSat.h"
#include "commontypes.h"

CSat::CSat()
	{
	iSat = 0;
	iAlreadyGotObs = false;	
	}

CSat::~CSat()
	{
	delete iSat;
	iSat = 0;
	}

bool CSat::InitTle(const char* aTle1, const char* aTle2, const char* aTle3)
	{
	if (iSat)
		{
		delete iSat;
		iSat = 0;
		}
	iSat = new CSGP4_SDP4(aTle1, aTle2, aTle3);
	iAlreadyGotObs = false;
	return true;
	};

void CSat::SetTime(double aJd)
	{
	//aJd = 2455789.6831597;;//aJd;
	if (iSat)
		iSat->SGP(aJd);
	iJd = aJd;
	iAlreadyGotObs = false;
	}

void CSat::LonLatAlt(double& aLon, double& aLat, double& aAlt)
	{
	if (!iSat)
		return;
	
	iSat->CalculateLatLonAlt(iJd);	//not needed if only doing az,elevation

	aLat = iSat->GetLat();
	aLon = iSat->GetLon();
	aAlt = iSat->GetAlt();

	if (aLat >= 180.)
		aLat = -(360. - aLat);

	if (aLon >= 180.)
		aLon = -(360. - aLon);	
	}

bool CSat::ObsAziAlt(double aUserLon, double aUserLat, double& aAzim, double& aAlt)
	{
	VECTOR user;
	user.y = aUserLon	/ (180./PI);
	user.x = aUserLat / (180./PI);
	user.z = 0;
	
	bool vis = iSat->CalculateObs(iSat->GetPos(), iSat->GetVel(), user, iJd);

	VECTOR obs = iSat->GetObserver();
	aAzim = obs.x * (180./PI);
	aAlt = obs.y * (180./PI);
	
	iAlreadyGotObs = true;
	
	return vis;
	}

void CSat::ObsRaDec(double aUserLon, double aUserLat, double& aRA, double& aDec)
	{
	VECTOR user;
	user.y =aUserLon	/ (180./PI);
	user.x = aUserLat / (180./PI);
	user.z = 0;

	iSat->CalculateRADec(iSat->GetPos(), iSat->GetVel(), user, iJd, iAlreadyGotObs);	//
	VECTOR rad = iSat->GetRADec();
	aRA = rad.x * (180./PI);
	aDec = rad.y * (180./PI);
	}

bool CSat::Sunlit()
	{
	VECTOR sun, moon;
	iSat->SunMoon(iJd, (double*)&sun, (double*)&moon);
	VECTOR sp = iSat->GetPos();
	return (iSat->sat_illum(sp, sun) == 'N');
	}

