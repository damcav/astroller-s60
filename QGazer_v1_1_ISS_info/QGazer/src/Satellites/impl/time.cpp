#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

//#include <afxwin.h>         // MFC core and standard components
//#include <afxext.h>         // MFC extensions
#ifndef _AFX_NO_AFXCMN_SUPPORT
//#include <afxcmn.h>			// MFC support for Windows 95 Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "math.h"
#include "sgpsdp.h" 

double CSGP4_SDP4::JulianDate (double st)
// Astronomical Formulae for Calculators, Jean Meeus, pages 23-25
// Calculate Julian Date of 0.0 Jan year
{
// input like : 99200.27104438 => 1999, 200.27104438 days.
/*	double dYear;
	int iYear = (int)(st/1000.0);
	st -= iYear * 1000;
	if (iYear < 50) iYear +=1999;
	else iYear +=1899;
old time ... */
// Modification to support Y2K }
// Valid 1957 through 2056 }
	double fYear, fDay;
	int iYear = (int)(st/1000.0);
	fDay = st - iYear * 1000.0; // = 99200,271 - 99000.0 = 200,271
	if (iYear < 57) iYear += 2000;
	else iYear += 1900;
	fYear = JulianDateOfYear(iYear);
	return fYear + fDay; 
}

double CSGP4_SDP4::JulianDateOfYear (int yr)
{
	long A,B;
	int iYear = yr - 1;
	double fYear;
	A = (long)(iYear/100.0);
	B = 2 - A + (long)(A/4.0);
	fYear = (long)(365.25 * iYear)
          + 428.0 + 1720994.5 + B;
	return fYear;
}


double CSGP4_SDP4::ThetaG(double jd)
{
// Reference:  The 1992 Astronomical Almanac, page B6. 
	double	UT,TU,GMST;
  UT	= modf(jd + 0.5, &jd);
  TU	= (jd - 2451545.0)/36525.0;
  GMST	= 24110.54841 + TU * (8640184.812866 + TU * (0.093104 - TU * 6.2E-6));
  GMST	= Modulus(GMST + secday*omega_E*UT,secday);
  return (2.0*PI*GMST/secday);
}





