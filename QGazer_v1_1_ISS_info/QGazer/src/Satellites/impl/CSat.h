/*
 * CSat.h
 *
 *  Created on: 15 Aug 2011
 *      Author: Damien
 */

#ifndef CSAT_H_
#define CSAT_H_

extern "C"
	{
	#include "Sgpsdp.h"
	}

class CSat
	{
public:
	CSat();
	virtual ~CSat();
	bool InitTle(const char* aTle1, const char* aTle2, const char* aTle3);
	void SetTime(double aJd);
	void LonLatAlt(double& aLon, double& aLat, double& aAlt);
	void ObsRaDec(double aUserLon, double aUserLat, double& aRA, double& aDec);
	bool ObsAziAlt(double aUserLon, double aUserLat, double& aAzim, double& aAlt);
	bool Sunlit();

private:
	CSGP4_SDP4* iSat;
	double iJd;
	bool iAlreadyGotObs;
	};

#endif /* CSAT_H_ */
