#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers


#include "math.h"
#include "sgpsdp.h" 
#include "vector.h" 



void CSGP4_SDP4::CalculateLatLonAlt(double jdTime)
{
	m_vLLA = CalculateLatLonAlt(m_vPOS, jdTime);
	m_bLatLonAlt = TRUE;
}

VECTOR CSGP4_SDP4::CalculateLatLonAlt(VECTOR vPOS, double time)
{
// Reference:  The 1992 Astronomical Almanac, page K12. 
	static VECTOR vLLA;
	double lat,lon,alt;
	double theta,r,e2,phi,c;
	double arg1, arg2;

	vLLA.x = vLLA.y = vLLA.z = vLLA.w = 0.0;
	lat = lon = alt = 0.0;
	theta = r = e2 = phi = c = 0.0;

//	theta = atan2(vPOS.y,vPOS.x);
	theta = AcTan(vPOS.y,vPOS.x);
	
	arg1 = ThetaG(time);
	arg1 = theta - arg1;
	arg2 = 2.0* PI;

//	lon = Modulus(theta - ThetaG(time),2.0*PI);
	lon = Modulus(arg1, arg2);

	r = sqrt(sqr(vPOS.x) + sqr(vPOS.y));
	e2 = f*(2.0 - f);
	lat = AcTan(vPOS.z,r);
	do	{
		phi = lat;
		c = 1.0/sqrt(1.0 - e2*sqr(sin(phi)));
		lat = AcTan( vPOS.z + xkmper*c*e2*sin(phi),r);
	}	while (fabs(lat - phi) > 1E-10);//1E-7); For speeding up calculation 7 digit
										//is exact enough (123.45
	alt = r/cos(lat) - xkmper*c;

	vLLA.x = lat*180.0/PI;   // radians
	vLLA.y = lon*180.0/PI;   // radians
	vLLA.z = alt;			// kilometers
	vLLA.w = theta*180.0/PI; // radians
	return vLLA;
}

#define R        6378.135
#define thetas 4.652418e-3  /* Sun's semidiameter as seen from Earth */

char CSGP4_SDP4::sat_illum(VECTOR sat, VECTOR sun)
	{
double rhoe, rhos, rs, thetae, theta, costheta;

 rhoe=sqrt(sat.x*sat.x+sat.y*sat.y+sat.z*sat.z);
 rs=sqrt(sun.x*sun.x+sun.y*sun.y+sun.z*sun.z);
 rhos=sqrt((sat.x-sun.x)*(sat.x-sun.x)+(sat.y-sun.y)*(sat.y-sun.y)+
	   (sat.z-sun.z)*(sat.z-sun.z));

 thetae=asin(R/rhoe);
 costheta=(sat.x*(sat.x-sun.x)+sat.y*(sat.y-sun.y)+sat.z*(sat.z-sun.z))
   /rhoe/rhos;
 theta=acos(costheta);


if(theta<thetae-thetas && costheta>0.)
   return 'U'; /* Umbral Eclipse */

 if(fabs(thetae-thetas)<theta && theta<thetae+thetas && costheta>0.)
   return 'P'; /* Penumbral Eclipse */

 /* The satellite is visible since it is over the horizon and the sun 
    has -6 deg of elevation */
// if(sat.el>0. && sun.el<-0.1047) 
 //  return 'V'; /* Visual Visible */

 /* The satellite is not visible sinec the sun is too high */
// if(sat.el>0. && sun.el>=-0.1047) 
//   return 'S'; /* in Sunlight */

 /* The satellite is illuminated but not in sigth */
// return 'I'; /* Illuminated */


 /* It should not to be difficult to find passeges aganist the sun (and moon) 
    disk, checking tha when el>0 az,el sat (better to do it with ra,dec?) 
    are no more than thetas from az,el from sun (or moon) */
 return 'N';
}

void CSGP4_SDP4::SunMoon( double T, double *Sun, double *Moon )
{
  /* locate the sun and moon in geocentric inertial coordinates
   input time in Julian days and get back sun and moon in meters.
  */

   /* sun ephemeris based on Slabinski's suneph subroutine 
   which is adapted from Newcomb's theory of the sun, A.P.A.E, 2nd ed.
   Vol 6 part 1 (Washington: Navy Dept., 1898), pp. 9-21 and
   described in Comsat Tech Memo CD-03-72.
   */

  static double ABERTN = 99.3664E-06;  /* constant of aberation in rad */
  static double T0 = 2415020.0;        /* epoch for this fourier series */
  double DT;
  double MEARTH, COSME, SINME, NURA;
  double LONSUN, COSL, SINL, RS, E, OB, SOB, COB;
  double LONPRT, NMOON; //MR, MRi LMMean, F, MM;
  //double cosmm, cosf, sinmm, sinf, sin2d; 
  //double cos2d, sin2f, cos2f, sin2mm, cos2mm; 
  //double SL, CL, S, cosd, sind, LATMN, LONGMN;
  double MVENUS, MMARS, MJUP, DMNSUN;
  //double myTrue[3];
  /* convert Julian time to ephemeris time wrt to epoch 1900 Jan 0.5 ET*/
  DT = (T - T0 + 6.31E-04 + 2.85E-08 * (T-2446066.5))/36525.0;
  /* compute earth mean anomaly, eqn 14 */
  MEARTH = fmod(6.25658358 + 628.30194572 * DT, M2PI );
  COSME = cos( MEARTH ); SINME = sin( MEARTH );
  /* Mean anomalies for perturbing bodies */
  MVENUS = fmod(3.70795199 + DT * 1021.32292286, M2PI);
  MMARS  = fmod(5.57772322 + DT *  334.05561740, M2PI);
  MJUP   = fmod(3.93187774 + DT *   52.96346477, M2PI);
  /* mean elongation of the moon from the sun, eqn 15 */
  DMNSUN = fmod(6.12152394 + DT * 7771.37719393, M2PI);
  /* celestial longitude of the mean asc node of the moon's orbit, eqn 16 */
  NMOON  = fmod(4.52360151 - DT *   33.757146246, M2PI);
  NURA = -83.55E-06 * sin( NMOON );   /* nutation of ra ?  eqn 25 */
  /* perturbation expressions are from Newcomb, Tables of the sun
     A.P.A.E. vol. 6, pp.14-18 (1898)*/
  LONPRT =  23.4553E-06 * cos( 5.220309 + MVENUS - MEARTH ) /*eqn 13 */
	  + 26.7908E-06 * cos(2.588556 + 2.0 * (MVENUS - MEARTH))
	  + 12.1058E-06 * cos(5.514251 + 2.0 * MVENUS - 3.0 * MEARTH)
	  +  9.9047E-06 * cos(6.001983 + 2.0 * (MEARTH - MMARS))
	  + 34.9454E-06 * cos(3.133418 + MEARTH - MJUP)
	  + 12.6051E-06 * cos(4.593997 - MJUP)
	  + 13.2402E-06 * cos(1.520967 + 2.0 * (MEARTH - MJUP))
	  + 31.0281E-06 * cos(4.035026 + DT * 0.352556)
	  + 31.2898E-06 * sin( DMNSUN ) + NURA - ABERTN;
  LONSUN = fmod(4.88162793 + DT * (628.33195099 + DT * 5.2844E-06), M2PI)
	   + SINME * (33495.79E-06 - 83.58E-06 * DT 
	   + COSME * (701.41E-06 - 3.50E-06 * DT + COSME * 20.44E-06))
	   + LONPRT;
  COSL = cos( LONSUN );
  SINL = sin( LONSUN );
  RS = AU * (1.0002806 - COSME * (1675.104E-05 - 4.180E-05 * DT
		       + COSME * 28.060E-05));
  /* true obliquity of the ecliptic, eqn 1 & 26 */
  OB = 0.40931970 - 2.27111E-04 * DT + 4.465E-05 * cos ( NMOON );
  COB = cos( OB ); SOB = sin( OB );
  E = NURA * COB;                   /* eqn of equinoxes, eqn 2 */
  /* convert to cartesian coordinates, eqns. 6-10 scaled to meters */
  Sun[0] = RS * (COSL + SINL * COB * E );  
  Sun[1] = RS * (SINL * COB - COSL * E );
  Sun[2] = RS * SINL * SOB; 

  return;
}

/*------------------------------------------------------------------------*/
