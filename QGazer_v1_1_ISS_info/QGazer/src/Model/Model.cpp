#include <AknGlobalNote.h> 

#include <f32file.h>

#include "model.h"
#include "objects.h"

#include "CreateData.h"

#include "context.h"
Context* gContext;
CSkyModel* gModel;


#include "QGazerapp.h"
extern CQGazerApp* gApp;

#include "MAppUiCb.h"

#include "HardSums.h"
extern CHardSums* gHardSums;

extern MAppUiCb* gAppUiCb;

#include "objects.inl"

const TUint KNumberOfStars = 2000;

CSkyModel* CSkyModel::NewL()
	{
	CSkyModel* self = new (ELeave) CSkyModel();
	CleanupStack::PushL(self);
    self->ConstructL();
	CleanupStack::Pop(self);    
    return self;
	}

_LIT(KStarsFile,"QGazerStars");
_LIT(KCitiesFile,"QGazerCities");

void CSkyModel::InitL()
	{
	SUSHI("CSkyModel::InitL() 1");
	
	INSTREAM* stream = new (ELeave) INSTREAM();
	CleanupStack::PushL(stream);
	stream->OpenStreamL(KStarsFile);
    InternalizeStarsL(*stream);
	CleanupStack::PopAndDestroy(stream);
		
	stream = new (ELeave) INSTREAM();
	CleanupStack::PushL(stream);
	stream->OpenStreamL(KCitiesFile);
    InternalizeCitiesL(*stream);
	CleanupStack::PopAndDestroy(stream);
	
#ifdef MESSIER
	_LIT(KMessierFile,"QGazerMessier");

	stream = new (ELeave) INSTREAM();
	CleanupStack::PushL(stream);
	stream->OpenStreamL(KMessierFile);
    InternalizeMessierL(*stream);
	CleanupStack::PopAndDestroy(stream);
	
	
	
	
	
	
#endif	

	SUSHI("CSkyModel::InitL() 1");
	
	for (TInt i=1; i<10; i++)
		{
		TSkyPlanet planet(i);	//planet 0 = the su	n!
		iPlanets.Append(planet);
		}
	
#ifdef SATELLITE
	TRAP_IGNORE(InternalizeSatellitesL());
#endif
	
	UpdateCoords();	//TODO MOVE!!!
	
#ifdef CREATEDATA
	OUTSTREAM* outStream = new (ELeave) OUTSTREAM();
	CleanupStack::PushL(outStream);
	outStream->OpenStreamL(KStarsFile);
    ExternalizeStarsL(*outStream);
	CleanupStack::PopAndDestroy(outStream);
	
	outStream = new (ELeave) OUTSTREAM();
	CleanupStack::PushL(outStream);
	outStream->OpenStreamL(KCitiesFile);
    ExternalizeCitiesL(*outStream);
	CleanupStack::PopAndDestroy(outStream);
	
#ifdef MESSIER
	outStream = new (ELeave) OUTSTREAM();
	CleanupStack::PushL(outStream);
	outStream->OpenStreamL(KMessierFile);
    ExternalizeMessierL(*outStream);
	CleanupStack::PopAndDestroy(outStream);
#endif	
#endif
	}

#ifdef SATELLITE
void CSkyModel::InternalizeSatellitesL()
	{
	for (TInt i=0; i<iSatellites.Count(); i++)
		iSatellites[i].Close();
	iSatellites.Close();
	
	RFs session;
	CleanupClosePushL(session);
	User::LeaveIfError(session.Connect());
	
	RFile file;
	CleanupClosePushL(file);
	
	const TDesC& sPath = gApp->SetPrivatePath(session);

	User::LeaveIfError(file.Open(session, _L("satellites.txt"), EFileRead));
	
	TBuf<3> line;
	TFileText t;
	t.Set(file);

	for (;;)
		{
		TSkySat sat;
		//CleanupClosePushL(sat);
		TRAPD(err, sat.InternalizeL(t));
		//CleanupStack::Pop(&sat);
		if (err != KErrNone)
			{
			sat.Close();
			break;
			}
		iSatellites.Append(sat);
		}
	CleanupStack::PopAndDestroy(2);
	}	
#endif

void CSkyModel::ConstructL()
	{
	gModel = this;

	iHardSums = CHardSums::NewL();
	gContext = Context::NewL();
	}
    
CSkyModel::CSkyModel()
{
}

CSkyModel::~CSkyModel()
{
	delete iHardSums;
	iHardSums = NULL;
	
	delete gContext;
	gContext = NULL;

	delete [] iStarPool;
	
	for (TInt i=0; i<iPlanets.Count(); i++)
		iPlanets[i].Close();
	iPlanets.Close();
	
	iSun.Close();

	for (TInt i=0; i<iConstellations.Count(); i++)
		iConstellations[i].Close();
	iConstellations.Close();
	delete[] TSkyConstellation::iVertices;
	
	for (TInt i=0; i<iLocations.Count(); i++)
		iLocations[i].Close();
	//iLocations.Close();
	
	for (TInt i=0; i<iNamedStars.Count(); i++)
		iNamedStars[i].Close();
	iNamedStars.Close();
	
	iMoon.Close();
	
	TSkyStar::iHipName.Close();
	
#ifdef MESSIER
	for (TInt i=0; i<iMessier.Count(); i++)
		iMessier[i].Close();
#endif
	
#ifdef SATELLITE
	for (TInt i=0; i<iSatellites.Count(); i++)
		iSatellites[i].Close();
	iSatellites.Close();
#endif
}


#ifdef CREATEDATA
void CSkyModel::ExternalizeCitiesL(OUTSTREAM& aStream)
{
	TUint32 count = iLocations.Count();
	aStream.WriteIntL(count);

	for (TUint32 i=0; i<count; i++)
		iLocations[i].ExternalizeL(aStream);
}
#endif

void CSkyModel::InternalizeCitiesL(INSTREAM& aStream)
	{
	//Locations--------------------------
	
	TUint32 count = aStream.ReadIntL();
	for (TUint32 i=0; i<count; i++)
		iLocations[i].InternalizeL(aStream);
	}

#ifdef MESSIER
void CSkyModel::InternalizeMessierL(INSTREAM& aStream)
	{
	TUint32 count = aStream.ReadIntL();
	for (TUint32 i=0; i<count; i++)
		iMessier[i].InternalizeL(aStream);

#ifdef CREATEDATA
	TDebug::Print((int)count);
	
	// SPECIAL CASE FOR THE HORSEHEAD NEBULA!!!
	TGeographic g;
	g.SetHours(5, 40, 59);
	iMessier[110].iEq.iRA = g.DecDegrees();
	g.Set(-2, 27, 30);
	iMessier[110].iEq.iDec = g.DecDegrees();
	iMessier[110].iName.Create(_L("Horsehead Nebula"));
	iMessier[110].iDist = 1500;
	iMessier[110].iType = 2;
	iMessier[110].iMagnitude = 4.0;
	TColour c(255,255,255);
	iMessier[110].iColour = c.Int32();	
#endif
	}

#ifdef CREATEDATA
void CSkyModel::ExternalizeMessierL(OUTSTREAM& aStream)
{
	TUint32 count = iMessier.Count();
	aStream.WriteIntL(count);

	for (TUint32 i=0; i<count; i++)
		iMessier[i].ExternalizeL(aStream);
}
#endif
#endif

#ifdef CREATEDATA
void CSkyModel::ExternalizeStarsL(OUTSTREAM& aStream)
{
	aStream.WriteFloatL(iMostRed);
	aStream.WriteFloatL(iMostBlue);

	TInt count = KNumberOfStars;
	aStream.WriteIntL(count);	

	TSkyStar* starAddr = iStarPool;
	for (TInt i=0; i<count; i++)
	{
		TSkyStar* star = iStarPool;
		star += i;
		star->ExternalizeL(aStream);
	}

	count = iConstellations.Count();
	aStream.WriteIntL(count);	
	for (TInt i=0; i<count; i++)
		iConstellations[i].ExternalizeL(aStream);

	count = iNamedStars.Count();
	aStream.WriteIntL(count);	
	for (TInt i=0; i<count; i++)
		iNamedStars[i].ExternalizeL(aStream);
	
}
#endif

void CSkyModel::InternalizeStarsL(INSTREAM& aStream)
	{
	TUint32 count = 0;

	//Stars--------------------------

	iMostRed = aStream.ReadFloatL();
	iMostBlue = aStream.ReadFloatL();
	
	count = aStream.ReadIntL();

	iStarPool = new (ELeave) TSkyStar[KNumberOfStars];
	
	TSkyStar* starAddr = iStarPool;
	TSkyStar dummyStar;
	for (TUint32 i=0; i<count; i++)
		{
		if (i < KNumberOfStars)
			{
			TSkyStar* star = new (starAddr++) TSkyStar;
			TReal bv = star->InternalizeL(aStream);
			//if (star->iParallax == 0)
			//	{
			//	TDebug::Print(TInt(star->iHipNum));
			//	User::After(2000000);
			//	}				
			star->Init(iMostRed, iMostBlue, bv);
			}
		else
			{
			dummyStar.InternalizeL(aStream);
			}
		}
	
	//Constellations--------------------------
	
	count = aStream.ReadIntL();
	TInt maxVertices = 0;
	for (TUint32 i=0; i<count; i++)
		{
		TSkyConstellation newCon;
		iConstellations.Append(newCon);
		TInt n = iConstellations[i].InternalizeL(aStream);
		if (n > maxVertices)
			maxVertices = n;
		}
	TSkyConstellation::iVertices = new (ELeave) GLfloat[maxVertices*3*2];

	count = aStream.ReadIntL();
	
	for (TUint32 i=0; i<count; i++)
		{
		TNamedStar namedStar;
		iNamedStars.Append(namedStar);
		TInt c = iNamedStars.Count()-1;
		iNamedStars[c].InternalizeL(aStream);
		
	//TODO! Sort this out in the data
		if (iNamedStars[c].iIndex >= KNumberOfStars)
			{
			iNamedStars[c].Close();
			iNamedStars.Remove(c);
			}
		else
			{
			TUint s = iNamedStars[c].iIndex;
			if ( s < KNumberOfStars)
				{
				TSkyStar* star = iStarPool;
				star += s;
				star->iNameIndex = c;
				}
			}
		}
	}

void CSkyModel::SetGL(CQGazer* aGL) const
	{
	TSkyObject::iGL = aGL;
	}

void CSkyModel::ObjectFoundL(const TSkyObject* aObject) const
	{
	/*
	//User::InfoPrint(aObject->Name());
	TChosenObject& found = gContext->ChosenObject();
	found.iObject = aObject;
	found.iEq = found.iObject->iEq;
	gHardSums->ToHorizontal(found.iEq, found.iHr);
	gAppUiCb->InfoBoxL(ETrue);
	iObjectQuery = EFalse;
	*/
	}



void CSkyModel::DrawGL() const
	{
	glDisable(GL_BLEND);
	
	if (gContext->ZoomEffectRunning())
		return;
	
	iMoon.Vector();
	TColour moonColour;
	
	iMoon.GetSkyColour(moonColour);	
	if (gContext->CrossHairMode())
		iMoon.AddScreenCoords();
	
	iSun.Vector();
	if (gContext->CrossHairMode())
		iSun.AddScreenCoords();
	
	if (gContext->ShowConstellations())
		for (TInt i=0; i<iConstellations.Count(); i++)
			iConstellations[i].DrawGL();
	
	TSkyPointObject::Reset();
//The stars will be drawn first as points, then pointsprites will be enabled
//if bright enough stars are drawn

	if (!gContext->InSpace())	//only draw the stars from the ground
		{	
		TSkyStar* star = iStarPool;
		
		//TInt visibleStars = TInt(KNumberOfStars * (gContext->VisibleStars() / 100.0));
		TReal a = KNumberOfStars / 10000.0;
		TInt visibleStars = TInt(a * gContext->VisibleStars() * gContext->VisibleStars());
		
		star += visibleStars;
		for (TInt i=visibleStars-1; i>=0; i--)
			{
			star--;
			star->DrawGL();
			if (gContext->CrossHairMode())
				star->AddScreenCoords();
			}
		}
	
	TSkyPointObject::Begin();
	
	for (TInt i=0; i<iPlanets.Count(); i++)
		if (i != 2)
			iPlanets[i].DrawGL();

	if (gContext->CrossHairMode())
		for (TInt i=0; i<iPlanets.Count(); i++)
			if (i != 2)
				{
				iPlanets[i].Vector();
				iPlanets[i].AddScreenCoords();
				}

#ifdef MESSIER
	for (TInt i=0; i<iMessier.Count(); i++)
		{
		iMessier[i].DrawGL();
		if (gContext->CrossHairMode())
			iMessier[i].AddScreenCoords();
		}
#endif
	
#ifdef SATELLITE
	if (SatellitesCount())
		{
		if (Iss().Show())
			{
			Iss().DrawGL();
			if (gContext->CrossHairMode())
				Iss().AddScreenCoords();
			}
		}
#endif
	
	iSun.DrawGL();

	TSkyPointObject::End();

	iMoon.DrawGL(moonColour);
	
	const TSkyObject* o = gContext->ChosenObject().iObject;
	if (o)
		o->HandleChosen();
	}

const TNamedStar& CSkyModel::NamedStar(TInt aIndex) const
	{
	return iNamedStars[aIndex];
	}

const TSkyStar& CSkyModel::NamedStarObject(TInt aIndex) const
	{
	const TSkyStar* s = iStarPool;
	const TNamedStar& n = iNamedStars[aIndex];
	ASSERT(n.iIndex < KNumberOfStars);
	ASSERT(n.iIndex >= 0);
	s += n.iIndex;
	return *s;
	}

const TSkyConstellation& CSkyModel::Constellation(TInt aIndex) const
	{
	return iConstellations[aIndex];
	}

const TSkyPlanet& CSkyModel::Planet(TInt aIndex) const
	{
	return iPlanets[aIndex];
	}

#ifdef MESSIER
const TSkyMessier& CSkyModel::Messier(TInt aIndex) const
	{
	return iMessier[aIndex];
	}
#endif



void CSkyModel::UpdateCoords()
	{
//	TLocation& loc = gContext->UserLocationMode() ? 
//			gContext->UserLocation() : gContext->CurrentLocation();

	gContext->SetSatelliteVisible(EFalse);
	const TLocation& loc = gContext->Location();
	
	//const TDateTime& dt = gContext->UserTimeMode() ?
	//		gContext->UserDateTime() : gContext->DateTime();
	
	const TDateTime& dt = gContext->Time();
	
	iHardSums->Reset(loc, dt);

	for (TInt i=0; i<iPlanets.Count(); i++)
		iPlanets[i].UpdateCoords();
	
	iMoon.UpdateCoords();
	iSun.UpdateCoords();
	
#ifdef SATELLITE
again:
	for (TInt i=0; i<iSatellites.Count(); i++)
		if (!iSatellites[i].UpdateCoords())
			{
			iSatellites[i].Close();
			iSatellites.Remove(i);
			goto again;
			}
		else
			{
			iSatellites[i].DetermineVisibility();	
			}
#endif
	

#ifdef STOREVECTORS
		iMoon.StoreVector();
		iMoon.DetermineVisibility();
		iSun.StoreVector();
		iSun.DetermineVisibility();
#ifdef SATELLITE
	for (TInt i=0; i<iSatellites.Count(); i++)
		iSatellites[i].StoreVector();
#endif
	
		//TODO! stars only need to be stored once
		TSkyStar* star = iStarPool;
		for (TInt i=0; i<KNumberOfStars; i++)
			{
			star->StoreVector();
			star++->DetermineVisibility();
			}
		
		for (TInt i=0; i<iPlanets.Count(); i++)
			if (i != 2)	//ignore Earth
				{
				iPlanets[i].StoreVector();
				iPlanets[i].DetermineVisibility();
				}
		
		for (TInt i=0; i<iConstellations.Count(); i++)
			{
			iConstellations[i].StoreVector();
			iConstellations[i].DetermineVisibility();
			}
#ifdef MESSIER		
		for (TInt i=0; i<iMessier.Count(); i++)
			{
			iMessier[i].StoreVector();
			iMessier[i].DetermineVisibility();
			}
#endif	
#endif
		
		TChosenObject& o = gContext->ChosenObject();
		if (o.iObject)
			{
			o.iEq = o.iObject->iEq;

			bool visible = o.iObject->Horizontal(o.iHr);
			if (!gContext->InSpace())
				{
				if (!visible)
					{
					//TDebug::Count();
					gAppUiCb->InfoBoxL(EFalse);
					}
				}
			}
	}

TInt CSkyModel::FindNearesetCity(TLocation& aLocation)
	{
	double shortest = 9999999;
	TInt shortestI(0);
	
	for (TInt i=0; i<iLocations.Count(); i++)
		{
		double d = iHardSums->DistanceInMeters(aLocation, iLocations[i]);
		if (d < shortest)
			{
			shortest = d;
			shortestI = i;
			}
		}
	const TLocation& loc = iLocations[shortestI];
	aLocation.Set(loc.Country(), loc.City());
	//aLocation = loc;
	return TInt(shortest);
	}


const TLocation& CSkyModel::Location(TInt aIndex) const
	{
	return iLocations[aIndex];
	}

//WHAT THE HELL IS THIS???

void CSkyModel::CreateNewLocationDataL()
	{
	//write the city text file
	RArray<TInt> fileIndexes;
	
	RFs session;
	User::LeaveIfError(session.Connect());
	
	RFile file;
	_LIT(KFileName,"c:\\cities.txt");
	User::LeaveIfError(file.Create(session, KFileName, EFileWrite));
	TFileText ft;
	ft.Set(file);
	
	TInt filePos = 0;
	
	for (TInt i=0; i<iLocations.Count(); i++)
		{
		ft.Write(iLocations[i].Country());
		ft.Write(iLocations[i].City());
		fileIndexes.Append(filePos);
		filePos += (iLocations[i].Country().Length() + iLocations[i].City().Length() + 2) * 2;
		
		}

	file.Close();
	session.Close();	
	
	COutStream* stream = new (ELeave) COutStream();
	TRAPD(err,stream->OpenStreamL(_L("citieslonglat")));
	TDebug::Print(err);
	
	for (TInt i=0; i<iLocations.Count(); i++)
		{
		TLocation& loc = iLocations[i];
		stream->WriteFloatL(loc.Longitude().DecDegrees());
		stream->WriteFloatL(loc.Latitude().DecDegrees());
	//	stream->WriteFloatL(loc.TzOffset());
		stream->WriteFloatL(0);
		stream->WriteIntL(fileIndexes[i]);
		}
	
	TDebug::Print(_L("done"));
	delete stream;
	
	fileIndexes.Reset();
	fileIndexes.Close();
	}
