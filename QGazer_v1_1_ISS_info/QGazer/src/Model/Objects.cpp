
#include <GLES/gl.h> // OpenGL ES header file


#include "objects.h"
#include "CreateData.h"
#include "QGazer.h"
#include "model.h"
#include "HardSums.h"
#include "Context.h"
#include "MAppUiCb.h"
#include "Satellites.h"

extern MAppUiCb* gAppUiCb;
extern Context* gContext;
extern CHardSums* gHardSums;
extern CSkyModel* gModel;

const TInt KNumSatData = 1;

CQGazer* TSkyObject::iGL;
RBuf TSkyStar::iHipName;
TVector TSkyObject::iVector;
GLfloat TSkyPointObject::iPointVertex[3];
TBool TSkyPointObject::iPointSprites;
GLfloat* TSkyConstellation::iVertices;


_LIT(KDash, "_");
_LIT(KNull, "");


#include "objects.inl"

void TNamedStar::InternalizeL(INSTREAM& aStream) 
{
	iName.Create(aStream.ReadStringL());
	iNotes.Create(aStream.ReadStringL());
	iIndex = aStream.ReadIntL();
}

#ifdef CREATEDATA
void TNamedStar::ExternalizeL(OUTSTREAM& aStream) 
{
	aStream.WriteStringL(iName);
	aStream.WriteStringL(iNotes);
	aStream.WriteIntL(iIndex);
}
#endif

void TNamedStar::Close()
	{
	iName.Close();
	iNotes.Close();
	}


TNamedStar::~TNamedStar()
	{
	Close();
	}

void TSkyObject::DescriptionL(CDesCArray& aLines) const
	{
	TBldStr::Set(_L("\""));
	TBldStr::Add(Name());
	TBldStr::Add(_L("\""));
	aLines.AppendL(TBldStr::String());
	}

void TSkyObject::DetermineVisibility()
	{
		THorizontal h;
		iVisible = Horizontal(h);
	}

TBool TSkyObject::Horizontal(THorizontal& aHor) const
	{
	gHardSums->ToHorizontal(iEq, aHor);
	return (aHor.iAltitude >= 0.0); 
	}

void TSkyObject::InternalizeL(INSTREAM& aStream)
	{
	iEq.InternalizeL(aStream);
	}

const TDesC& TSkyObject::Article() const
	{
	return KNull;
	}

#ifdef CREATEDATA
void TSkyObject::ExternalizeL(OUTSTREAM& aStream)
	{
	iEq.ExternalizeL(aStream);
	}
#endif

#ifdef STOREVECTORS
void TSkyObject::StoreVector() const
	{
	iGL->Vector(iEq, iStoredVector, 1.0);
	}
#endif




void TSkyObject::HandleChosen() const
	{
	TChosenObject& chosen = gContext->ChosenObject();
	TScreenCoords& sc = chosen.iScreen;
	TVector vect;
	Vector(vect);
	TSkyObject::iGL->ScreenCoords(vect, sc);

	if (sc.OnScreen())
		gAppUiCb->Label(sc.Point(), Name(), TLabelType(EChosen));

	TCamera temp;
	temp.Set(chosen.iHr);
	chosen.iBearing = gContext->Camera().Bearing(temp);
	}

TReal TSkyStar::InternalizeL(INSTREAM& aStream) 
{
	iMagnitude = aStream.ReadFloatL();
	//TReal hipNum = aStream.ReadFloatL();
	//iHipNum = TUint32(hipNum);
	iHipNum = aStream.ReadIntL();
	iParallax = aStream.ReadFloatL();
	TReal bv = aStream.ReadFloatL();
//#ifdef CREATEDATA	
	iBv = bv;		//not stored in RAM
//#endif
	TSkyObject::InternalizeL(aStream);
	return bv;
}

#ifdef CREATEDATA
void TSkyStar::ExternalizeL(OUTSTREAM& aStream)
{

	aStream.WriteFloatL(iMagnitude);
	
	//aStream.WriteFloatL(float(iHipNum));
	aStream.WriteIntL(iHipNum);
	
	aStream.WriteFloatL(iParallax);
	aStream.WriteFloatL(iBv); //********** TODO
	TSkyObject::ExternalizeL(aStream);
}
#endif

void TSkyObject::DescriptionCoordsL(CDesCArray& aLines) const
	{
//	if (gContext->InSpace())
		{
		TBldStr::Set(_L("RA/Dec: "));
		TBldStr::AddNum(iEq.iRA);
		TBldStr::Comma();
		TBldStr::AddNum(iEq.iDec);
		aLines.AppendL(TBldStr::String());
		}
//	else
		{
		THorizontal h;
		Horizontal(h);
		TBldStr::Set(_L("Az/Alt: "));
		TBldStr::AddNum(h.iAzimuth);
		TBldStr::Comma();
		TBldStr::AddNum(h.iAltitude);
		aLines.AppendL(TBldStr::String());
		}

	}

void TSkyPointObject::DescriptionL(CDesCArray& aLines) const
	{
	TSkyObject::DescriptionL(aLines);
	TSkyObject::DescriptionCoordsL(aLines);
	}

void TSkyPointObject::Begin()
	{
	iGL->StartPointSprites(&iPointVertex[0]);
	}


void TSkyPointObject::Reset()
	{
	iPointSprites = EFalse;
	iGL->StartPoints(&iPointVertex[0]);
	}


void TSkyPointObject::End()
	{
	iGL->EndPointSprites();
	}

TBool TSkyPointObject::CheckObjectQuery(const TRect& aTarget) const
	{
	return EFalse;
	}

void TSkyObject::AddScreenCoords() const
	{
	TScreenCoords sc;
	iGL->ScreenCoords(iVector, sc);
	if (sc.OnScreen())
		gAppUiCb->AddObjectOnScreen(sc, this, EFalse);
	}

void TSkySun::AddScreenCoords() const
	{
	TScreenCoords sc;
	iGL->ScreenCoords(iVector, sc);
	if (sc.OnScreen())
		gAppUiCb->AddObjectOnScreen(sc, this, ETrue);
	}

void TSkyMoon::AddScreenCoords() const
	{
	TScreenCoords sc;
	iGL->ScreenCoords(iVector, sc);
	if (sc.OnScreen())
		gAppUiCb->AddObjectOnScreen(sc, this, ETrue);
	}


void TSkyStar::DescriptionL(CDesCArray& aLines) const
	{
	
	TSkyPointObject::DescriptionL(aLines);
	if (iNameIndex != 9999)
		{
		TBldStr::Set(_L("HIP: "));
		TBldStr::AddNum((int)iHipNum);
		aLines.AppendL(TBldStr::String());
		}
	TBldStr::Set(_L("Mag: "));
	TBldStr::AddNum(iMagnitude);
	aLines.AppendL(TBldStr::String());
	TBldStr::Set(_L("B-V: "));
	TBldStr::AddNum(iBv);
	aLines.AppendL(TBldStr::String());
	TBldStr::Set(_L("Distance: "));
	float distance = (1000.0 / iParallax) * 3.26;
	TBldStr::AddNum(distance);
	TBldStr::Add(_L(" light years"));
	aLines.AppendL(TBldStr::String());
	TBldStr::Set(_L("Parallax: "));
	TBldStr::AddNum(iParallax/100.0);
	TBldStr::Add('"');
	aLines.AppendL(TBldStr::String());
	}

void TSkyStar::Init(TReal aMostRed, TReal aMostBlue, TReal aBV)
	{
	TColour c;
	if (aBV > 0)
		{
		int dc = (int)(aBV * (80.0 / aMostRed));
		c.Set(255, 255 - dc, 255 - dc*2);
		}
	else
		{
		int dc = (int)(aBV * (80.0 / aMostBlue));
		c.Set(255 - dc, 255 - dc, 255);
		}
	iColour = c.Int32();
	}

const RBuf& TSkyStar::Name() const
	{
	if (iNameIndex != 9999)
		return gModel->NamedStar(iNameIndex).iName;
	else
		{
		TBuf<64> n;
		n.Append(_L("HIP: "));
		n.AppendNum(iHipNum);
		iHipName.Close();
		iHipName.CreateL(n);
		return iHipName;
		}
	}

_LIT(KStar,"*");
const TDesC& TSkyStar::Article() const
{
	if (iNameIndex != 9999)
		{
		const TDesC& n = gModel->NamedStar(iNameIndex).iNotes;
		if (n.Length())
			return n;
		}
	return KStar; //no article
}

const TPtrC KPlanetNames[]=
	{
	_S("The Sun"),
	_S("Mercury"), 
	_S("Venus"),    
	_S("The Earth"),
	_S("Mars"),
	_S("Jupiter"),
	_S("Saturn"),
	_S("Uranus"),
	_S("Neptune"),
	_S("Pluto")
	};

void TSkyPlanet::DrawGL() const
	{
	TSkyPointObject::DrawGL();
	 
	if ((iNumber <= EUranus /*ie up to Saturn*/) && (gContext->ShowLabels()))
		{
		TScreenCoords sc;
		TSkyObject::iGL->ScreenCoords(iVector, sc);

		if (sc.OnScreen())
			{
			sc.Add(TPoint(4,4));
			gAppUiCb->Label(sc.Point(), Name(), TLabelType(EPlanetLabel));
			}
		}
	}

TSkyPlanet::TSkyPlanet(TInt aNumber) : iNumber(aNumber)
	{
	//hard coded mags. TODO: get hold of some maths to work out the real sizes
	GLfloat roughMags[10]={
		-27.0,	//ESun
		4.4,	//EMercury
		-3.5,	//EVenus
		0,		//EEarth
		-0.6,	//EMars
		-1.6,	//EJupiter
		1.16,	//ESaturn
		6.0,	//Uranus
		8.0,	//Neptune
		13.0};	//Pluto

	iName.Create(KPlanetNames[iNumber]);
	
	iMagnitude = roughMags[iNumber];
	
	TColour c(255, 255, 255);
	
	if (iNumber == 0)//Sun
		c.Set(255, 128, 80); //TODO alter this according to starfactor
	else if (iNumber == 4)//Mars
		c.Set(255, 128, 128);
	else if (iNumber == 2)//Venus
		c.Set(128, 128, 255);
	else
		c.Set(255, 255, 255);

	iColour = c.Int32();
	}

void TSkyPlanet::Close()
	{
	iName.Close();
	iArticle.Close();
	}

void TSkyPlanet::UpdateCoords()
	{
	if (iNumber != 3)
		{
		gHardSums->PlanetPos(iNumber, iEq);
		//gModel.Sums().ToHorizontal(iEq, iHr);
		}
	}

const TDesC& TSkyPlanet::Article() const
{
	if (iNumber == 3)
		return KNull;
	return KDash; //from file
}

const TDesC& TSkySun::Article() const
{
	return KDash; //from file
}

const TDesC& TSkyMoon::Article() const
{
	return KDash; //from file
}


TSkySun::TSkySun() : TSkyPlanet(0)
	{
	}

void TSkySun::DrawGL() const
	{
	Vector();

	GLfloat f = gContext->iStarFactor;
	TColour c;
	c.SetClamped(1.0, f / 2.0, f / 3.0);
	c.GlColour();
	
	GLfloat ps = gContext->InSpace() ? 30.0 : 40.0;
	glPointSize(ps);
	iPointVertex[0] = iVector.iX; iPointVertex[1] = iVector.iY; iPointVertex[2] = iVector.iZ;  
	glVertexPointer (3, GL_FLOAT , 0, &iPointVertex);
	glDrawArrays (GL_POINTS, 0, 1);
	}

void TSkySun::UpdateCoords()
	{
	TSkyPlanet::UpdateCoords();
	Horizontal(iHr);
	
	iSunsetOk = gHardSums->SunriseSunset(ETrue, iSunrise);
	iSunriseOk = gHardSums->SunriseSunset(EFalse, iSunset);
	}

TBool TSkySun::CheckObjectQuery(const TRect& aTarget) const
	{
	TScreenCoords sc;
	iGL->ScreenCoords(iVector, sc);
	TRect rect(aTarget);
	rect.Grow(rect.Width()*3, rect.Height()*3);
	if (sc.OnScreen())
		return rect.Contains(sc.Point());
	else
		return EFalse;
	}

void TSkySun::DescriptionL(CDesCArray& aLines) const
	{
	TSkyObject::DescriptionL(aLines);
	TSkyObject::DescriptionCoordsL(aLines);
	if (!gContext->UserLocationMode())
		{
		TBldStr::Set(_L("Rise: "));
		if (iSunriseOk)
			{
			TBldStr::AddNum2(iSunrise.Hour());
			TBldStr::Add(_L(":"));
			TBldStr::AddNum2(iSunrise.Minute());
			TBldStr::Add(_L(" (phone time)"));
			}
		else
			{
			TBldStr::Add(_L("-"));
			}
		aLines.AppendL(TBldStr::String());
		
		TBldStr::Set(_L("Set: "));
		if (iSunsetOk)
			{
			TBldStr::AddNum2(iSunset.Hour());
			TBldStr::Add(_L(":"));
			TBldStr::AddNum2(iSunset.Minute());
			TBldStr::Add(_L(" (phone time)"));
			}
		else
			{
			TBldStr::Add(_L("-"));
			}
		aLines.AppendL(TBldStr::String());
		}
	aLines.AppendL(_L("Don't look directly"));
	aLines.AppendL(_L("at the sun!"));
	}

TBool TSkyMoon::CheckObjectQuery(const TRect& aTarget) const
	{
	TScreenCoords sc;
	iGL->ScreenCoords(iVector, sc);
	TRect rect(aTarget);
	rect.Grow(rect.Width()*3, rect.Height()*3);
	if (sc.OnScreen())
		return rect.Contains(sc.Point());
	else
		return EFalse;
	}


TBool TSkyMoon::GetSkyColour(TColour& aColour) const
	{

		iGL->ScreenCoords(iVector, iScreen);
		if (iScreen.OnScreen())
		{
			unsigned char pixel[4];
			int y = iGL->Height() - iScreen.Point().iY;
			glReadPixels(iScreen.Point().iX, y , 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pixel);
			aColour.Set(pixel[0], pixel[1], pixel[2]);
			return ETrue;
		}
		return EFalse;
	}

void TSkyMoon::UpdateCoords()
	{
	gHardSums->MoonPos(iEq);
	Horizontal(iHr);

	gHardSums->LastMoonPhase(2, iLastFullMoon);
	gHardSums->NextMoonPhase(2, iNextFullMoon);
	}

TSkyMoon::TSkyMoon()
	{
	//iHasHitRect = true;
	_LIT(KMoon, "The Moon");
	iName.Create(KMoon());
	iRadius = 0.025;
	//iVectorLength = 0.35;
	}

void TSkyMoon::Close()
	{
	iName.Close();
	}

void TSkyMoon::DescriptionL(CDesCArray& aLines) const
	{
	TSkyObject::DescriptionL(aLines);
	TSkyObject::DescriptionCoordsL(aLines);
	aLines.AppendL(_L("Last full:"));
	aLines.AppendL(gContext->DoTimeString(TTime(iLastFullMoon), EUniversalTz));
	aLines.AppendL(_L("Next full:"));
	aLines.AppendL(gContext->DoTimeString(TTime(iNextFullMoon), EUniversalTz));
	}

void TSkyMoon::DrawGL(const TColour& aMoonColour) const
	{
	//return;
	Vector();
	//iGL->ScreenCoords(iVector, Name());

	if (!gContext->InSpace() && iScreen.OnScreen())
		{
		aMoonColour.GlColour();
		iGL->DrawCircle(iVector);
		}
	
	//turn the sun's light on
	TVector sunVect;
	gModel->Sun().Vector(sunVect);
	iGL->DirectionalLight(sunVect);

	glPushMatrix();
	glTranslatef(iVector.x(), iVector.y(), iVector.z());

//SPACE MODE		
	glRotatef(180 , 0.0, 0.0, 1.0);
	glRotatef(-90.0 - iEq.iRA, 0.0, 1.0, 0.0);

	iGL->DrawTheMoon();
	glPopMatrix();
	}

void TSkyConstellation::DescriptionL(CDesCArray& aLines) const
	{
	TSkyObject::DescriptionL(aLines);
	aLines.AppendL(iNotes);
	}

TInt TSkyConstellation::InternalizeL(INSTREAM& aStream)
{
	iName.Create(aStream.ReadStringL());
	iNotes.Create(aStream.ReadStringL());	

	TInt count = aStream.ReadIntL();

	TReal avDec = 0;
	TReal avRA = 0;
	
	for (TInt i = 0; i<count; i++)
	{
		TEquatorial e;
		e.InternalizeL(aStream);
		iEqList1.Append(e);	//ND
		avDec +=  e.iDec;
		avRA += e.iRA;
		
		e.InternalizeL(aStream);
		iEqList2.Append(e);	//ND
		avDec +=  e.iDec;
		avRA += e.iRA;
	}
	
		iEq.InternalizeL(aStream);
		
		if (iEq.iDec == -1.0)
			{
			iEq.iDec = avDec / (count*2);
			iEq.iRA = avRA / (count*2);
			}
	
	return count;
}
#ifdef CREATEDATA
void TSkyConstellation::ExternalizeL(OUTSTREAM& aStream)
{
	aStream.WriteStringL(iName);
	aStream.WriteStringL(iNotes);

	TInt count = iEqList1.Count();
	aStream.WriteIntL(count);
	
	TReal avDec = 0;
	TReal avRA = 0;
	
	for (TInt i = 0; i<count; i++)
	{
		iEqList1[i].ExternalizeL(aStream);
		iEqList2[i].ExternalizeL(aStream);
	}
	
	iEq.ExternalizeL(aStream);
}
#endif

void TSkyConstellation::Close()
	{
	iName.Close();
	iNotes.Close();
	iEqList1.Close();
	iEqList2.Close();
	}

TSkyConstellation::~TSkyConstellation()
	{
	Close();
	}

#ifdef MESSIER
void TSkyMessier::DrawGL() const
{
//don't show clusters 'cos the constituent stars should be being show instead
	if ((iType > EGlobularCluster) && iMagnitude < 6.0)
		TSkyPointObject::DrawGL();
}

void TSkyMessier::InternalizeL(INSTREAM& aStream) 
{
	iMagnitude = aStream.ReadFloatL();
	iDist = aStream.ReadFloatL();
	iName.Create(aStream.ReadStringL());
	iType = aStream.ReadIntL();
	TSkyObject::InternalizeL(aStream);
	
	TColour c(128,128,140);
	iColour = c.Int32();
}

void TSkyMessier::Close()
	{
	iName.Close();
	}

const TDesC& TSkyMessier::Article() const
{
	if (iName.Length() && iName[0] != 'M')
		return KDash;
	return KNull;
}

void TSkyMessier::DescriptionL(CDesCArray& aLines) const
{
const TPtrC KMessierType[]=
	{
	_S("Open cluster"),
	_S("Globular cluster"), 
	_S("Bright nebula"),    
	_S("Planetary Nebula"),
	_S("Galaxy"),
	_S("Asterism")
	};

	TSkyPointObject::DescriptionL(aLines);
	aLines.AppendL(KMessierType[iType]);
}

#ifdef CREATEDATA
void TSkyMessier::ExternalizeL(OUTSTREAM& aStream) 
{
	aStream.WriteFloatL(iMagnitude);
	aStream.WriteFloatL(iDist);
	aStream.WriteStringL(iName);
	aStream.WriteIntL(iType);
	TSkyObject::ExternalizeL(aStream);
}
#endif
#endif

#ifdef SATELLITE
void TSkySat::SetName(const TDesC& aName)
	{
	iName.Create(aName);
	}

const TDesC& TSkySat::Article() const
{
	return KDash; //from file
}

#if(0)
void TSkySat::SetTle1(const TDesC8& aLine)
	{
	iTle1.Create(aLine);
	}
	
void TSkySat::SetTle2(const TDesC8& aLine)
	{
	iTle2.Create(aLine);
	}
	
void TSkySat::SetTle3(const TDesC8& aLine)
	{
	iTle3.Create(aLine);
	}
#endif
TBool TSkySat::Horizontal(THorizontal& aHor) const
	{
	aHor = iHr;
	return (iHr.iAltitude > 0.0);
	}

TBool TSkySat::UpdateCoords()
	{
	iVisibilityIsCached = EFalse;
	
	TSatData sat;
	sat.iTle = &iTle;
	iAboveHorizon = false;
	
	
	sat.iGetLongLat = true;
	sat.iObsLoc = gContext->Location();
	sat.iGetSunlit =  true;
	
	//In space we derive the ra/dec from the lon,lat of the coords (this happens in the sat routines)
	//In ground mode we need to do the maths...
	if (!gContext->InSpace())
		sat.iGetObsRaDec = true;

	sat.iGetObsAziAlt = true;
	

	//sat.iJd = 2455824.70717593;

	gHardSums->SatMaths().SatellitePos(sat);
	iEq = sat.iEq;
	iHr = sat.iHr;
	iLocation = sat.iSatLoc;
	
	if (!gContext->InSpace())
		if (sat.iVisible)
			{
			gContext->SetSatelliteVisible(ETrue);	//flag this so we can speed up the timer!
			iAboveHorizon = true;
			}
	
	iSunlit = sat.iSunlit;
	
	if (iSunlit || gContext->InSpace())
		SetColour(TColour(255,255,255));
	else
		SetColour(TColour(0,0,0));
	
	return ETrue;
	}
	

TBool TSkySat::NextVisibleUtcCached(TDateTime& aVisible) const
	{
	aVisible = iWhenVisible;
	return iIsVisible;
	}

void TSkySat::NextVisibleUtc(TRequestStatus* aStatus) const
	{
	iVisSat.iObsLoc.Set(gContext->Location().Longitude(),
			gContext->Location().Latitude());		
		
	iVisSat.iTle = &iTle;
	iVisibilityIsCached = ETrue;

	gHardSums->SatMaths().SatelliteNextVisible(&iVisSat, &iWhenVisible, &iIsVisible, aStatus);
	}

void TSkySat::CancelNextVisibleUtc() const
	{
	gHardSums->SatMaths().CancelSatelliteNextVisible();
	}

void TSkySat::VisibilityText(CDesCArray& aLines) const
	{
	TDateTime nextVisible;

	aLines.AppendL(_L("^1Next visible from "));
	//TBldStr::Add(gContext->Location().City());
	
	TInt dist;
	const TLocation& loc = gContext->Location(dist);
	dist /= 1000;
	
	aLines.AppendL(loc.String());
	TBldStr::Set(loc.City());
	if (dist > 5)
		{
		TBldStr::Add(':');
		TBldStr::AddNum(dist);
		TBldStr::Add(_L("km"));
		}

	aLines.AppendL(TBldStr::String());
	
	if (NextVisibleUtcCached(nextVisible))
		{
		TDateTime homeTime(nextVisible);
		gHardSums->UtcToPhoneTime(homeTime);
		TBldStr::Set(_L("^2"));
		TBldStr::AddDateTime(homeTime, ETrue);
		aLines.AppendL(TBldStr::String());
		
		if (gContext->UserLocationMode())
			{
			TBldStr::Set(_L("^1"));
			TBldStr::AddDateTime(nextVisible, ETrue);
			TBldStr::Add(_L("(UTC)"));
			aLines.AppendL(TBldStr::String());
			}
		}
	else
		{
		aLines.AppendL(_L("^2Not within 14 days"));
		}
	}
void TSkySat::DescriptionL(CDesCArray& aLines) const
	{
	TSkyObject::DescriptionL(aLines);
	aLines.AppendL(iLocation.String());
	
	TBldStr::Set(_L("Az/Alt: "));
	TBldStr::AddNum(iHr.iAzimuth);
	TBldStr::Comma();
	TBldStr::AddNum(iHr.iAltitude);
	aLines.AppendL(TBldStr::String());
	
	if (iSunlit)
		aLines.AppendL(_L("Currently in sunlight"));
	else
		aLines.AppendL(_L("Currently in shadow"));
	
//	VisibilityText(aLines);

	aLines.AppendL(_L("To find out when"));
	aLines.AppendL(_L("next visible from"));
	aLines.AppendL(gContext->Location().City());
	aLines.AppendL(_L("tap the picture."));
	}

void TSkySat::Close()
	{
	iName.Close();
	iTle.Close();
	}

void TSkySat::DrawGL() const
	{
	if (!gContext->InSpace() && !iAboveHorizon)
		return;
	
	TSkyPointObject::DrawGL();
	
	TScreenCoords sc;
	TSkyObject::iGL->ScreenCoords(iVector, sc);
	if (sc.OnScreen() && gContext->ShowLabels())
		{
		sc.Add(TPoint(4,4));
		gAppUiCb->Label(sc.Point(), Name(), TLabelType(EPlanetLabel));
		}
	}

//_LIT8(K69, "012345678901234567890123456789012345678901234567890123456789012345678");
void TSkySat::InternalizeL(TFileText& aFt)
	{
	TBuf<80> line;
	User::LeaveIfError(aFt.Read(line));
	if (line.Compare(_L("^ISS")) != 0)
		User::Leave(-1);
	
	SetName(_L("ISS"));
	SetColour(TColour(255,0,0));
	SetMagnitude(-1.0);
	iTle.InternalizeL(aFt, line);
	}
	
void TSkySat::StoreVector() const
	{
	iGL->Vector(iEq, iStoredVector, 0.87);
	}

TBool TSkySat::Show() const
	{
	TBool show = EFalse;
	if (gContext->ShowIss())
		{
		if (gContext->UserTimeMode())
			{
			show = !BeforeSatEpoch();
			}
		else
			{
			show = ETrue;
			}
		}
	return show;
	}
#endif

