#include "Object3D.h"
#include <GLES/gl.h> // OpenGL ES header file
#include <e32std.h> // for CBase definition
#include "mathutils.h"
#include <math.h>
#include "utils3d.h"

#define M_PI		3.14159265358979323846

#ifdef DAMIEN_STICK2FLOAT
/*-----------------------------------------------------------------*/
// Constructor and destructor
//
CObject3D::CObject3D(unsigned int indexCount_, unsigned int vertexCount_) :
		indexCount(indexCount_),
		vertexCount(vertexCount_),
		indices(NULL),
		vertices(NULL),
		normals(NULL),
		texCoords(NULL)
	{
	// Allocate memory
	indices=   new (ELeave) GLushort[indexCount];
	vertices=  new (ELeave) GLfloat[vertexCount*3];
	normals=   new (ELeave) GLfloat[vertexCount*3];
	texCoords= new (ELeave) GLfloat[vertexCount*2];
	}

CObject3D::~CObject3D()
	{
	// Free memory
	delete indices;
	delete vertices;
	delete normals;
	delete texCoords;
	}

/*-----------------------------------------------------------------*/
// Draw object
//
void CObject3D::draw(void) const
	{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
	glVertexPoiglDrawElementsnter(3,GL_FLOAT, 0, vertices);
	glNormalPointer(GL_FLOAT, 0, normals);
	glTexCoordPointer(2,GL_FLOAT, 0, texCoords);
	
	// Draw stuff
	glDrawArrays(GL_POINTS, 0, vertexCount);
	(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, indices);
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

CObject3D* Utils::CreateSphereL(const GLfloat aRadius, const int aSlices, const int aStacks)
	{
	CObject3D* sphere=new (ELeave) CObject3D((aSlices*2 + (aStacks-3)*aSlices*2)*3, (aSlices+1)*aStacks);

	// Create sphere points
	int pos=0;
	int texPos=0;
	for (int j=0; j<aStacks; j++)
		{
		GLfloat rad=(j*M_PI)/(aStacks-1);
		GLfloat y=cos(rad);
		GLfloat r=sin(rad)*aRadius;		// The aRadius for this stack
		for (int i=0; i<aSlices+1; i++)
			{
			rad=(i*2*M_PI)/aSlices;
			GLfloat x=sin( rad );
			GLfloat z=cos( rad );
			
			sphere->vertices[pos+0]=x*r;
			sphere->vertices[pos+1]=y*aRadius;
			sphere->vertices[pos+2]=z*r;
			
			TVector normal(x*r,y*aRadius,z*r);
			normal.Normalize();
			sphere->normals[pos+0]=normal.x();
			sphere->normals[pos+1]=normal.y();
			sphere->normals[pos+2]=normal.z();
			
			sphere->texCoords[texPos+0]=1-(GLfloat)i/aSlices;
			sphere->texCoords[texPos+1]=1-(GLfloat)j/(aStacks-1);
			
			pos+=3;
			texPos+=2;
			}
		}
	
	// Create indices
	unsigned int indexPos=0;
	
	// Top
	pos=0;
	for (int i=0; i<aSlices; i++)
		{
		sphere->indices[indexPos+0]=pos;
		sphere->indices[indexPos+1]=pos+1+(aSlices+1);
		sphere->indices[indexPos+2]=pos+(aSlices+1);
		indexPos+=3;
		pos++;
		}
	pos++;
	
	// Middle
	for (int j=1; j<aStacks-2; j++)
		{
		for (int i=0; i<aSlices; i++)
			{
			sphere->indices[indexPos+0]=pos;
			sphere->indices[indexPos+1]=pos+1+(aSlices+1);
			sphere->indices[indexPos+2]=pos+(aSlices+1);
			indexPos+=3;
			
			sphere->indices[indexPos+0]=pos;
			sphere->indices[indexPos+1]=pos+1;
			sphere->indices[indexPos+2]=pos+1+(aSlices+1);
			indexPos+=3;
			pos++;
			}
		pos++;
		}	
	
	// Bottom
	for (int i=0; i<aSlices; i++)
		{
		sphere->indices[indexPos+0]=pos;
		sphere->indices[indexPos+1]=pos+1;
		sphere->indices[indexPos+2]=pos+(aSlices+1);
		indexPos+=3;
		pos++;
		}
	return sphere;
	}
#else

CObject3D* CObject3D::NewL(unsigned int indexCount_, unsigned int vertexCount_)
	{
	CObject3D* self = new (ELeave) CObject3D(indexCount_, vertexCount_);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}

void CObject3D::ConstructL()
	{
	indices=   new (ELeave) GLushort[indexCount];
	vertices=  new (ELeave) GLfixed[vertexCount*3];
	normals=   new (ELeave) GLfixed[vertexCount*3];
	texCoords= new (ELeave) GLfixed[vertexCount*2];
	}

CObject3D::CObject3D(unsigned int indexCount_, unsigned int vertexCount_) :
		indexCount(indexCount_),
		vertexCount(vertexCount_)
	{
	}

CObject3D::~CObject3D()
	{
	// Free memory
	delete indices;
	indices = NULL;
	delete vertices;
	vertices = NULL;
	delete normals;
	normals = NULL;
	delete texCoords;
	texCoords = NULL;
	}

/*-----------------------------------------------------------------*/
// Draw object
//

#define _d_first 2
#define _d_second 1
#define _d_third 0

void CObject3D::draw(void) const
	{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
	glVertexPointer(3,GL_FIXED, 0, vertices);
	glNormalPointer(GL_FIXED, 0, normals);
	glTexCoordPointer(2,GL_FIXED, 0, texCoords);
	
	// Draw stuff
	//glDrawArrays(GL_POINTS, 0, vertexCount);
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, indices);
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

CObject3D* Utils::CreateSphereL(const GLfloat aRadius, const int aSlices, const int aStacks)
	{
	CObject3D* sphere=CObject3D::NewL((aSlices*2 + (aStacks-3)*aSlices*2)*3, (aSlices+1)*aStacks);

	// Create sphere points
	int pos=0;
	int texPos=0;
	for (int j=0; j<aStacks; j++)
		{
		GLfloat rad=(j*M_PI)/(aStacks-1);
		GLfloat y=cos(rad);
		GLfloat r=sin(rad)*aRadius;		// The aRadius for this stack
		for (int i=0; i<aSlices+1; i++)
			{
			rad=(i*2*M_PI)/aSlices;
			GLfloat x=sin( rad );
			GLfloat z=cos( rad );
			
			GLfloat f = x*r;
			sphere->vertices[pos+0]=FLOAT_2_FIXED(f);
			f = y*aRadius;
			sphere->vertices[pos+1]=FLOAT_2_FIXED(f);
			f = z * r;
			sphere->vertices[pos+2]=FLOAT_2_FIXED(f);
			
			TVector normal(x*r,y*aRadius,z*r);
			normal.Normalize();
			sphere->normals[pos+0]=FLOAT_2_FIXED(normal.x());
			sphere->normals[pos+1]=FLOAT_2_FIXED(normal.y());
			sphere->normals[pos+2]=FLOAT_2_FIXED(normal.z());
			
			f = 1-(GLfloat)i/aSlices;
			sphere->texCoords[texPos+0]=FLOAT_2_FIXED(f);
			f = 1-(GLfloat)j/(aStacks-1);
			sphere->texCoords[texPos+1]=FLOAT_2_FIXED(f);
			
			pos+=3;
			texPos+=2;
			}
		}
	
	// Create indices
	unsigned int indexPos=0;
	
	// Top
	pos=0;
	for (int i=0; i<aSlices; i++)
		{
		sphere->indices[indexPos+_d_first]=pos;
		sphere->indices[indexPos+_d_second]=pos+1+(aSlices+1);
		sphere->indices[indexPos+_d_third]=pos+(aSlices+1);
		indexPos+=3;
		pos++;
		}
	pos++;
	
	// Middle
	for (int j=1; j<aStacks-2; j++)
		{
		for (int i=0; i<aSlices; i++)
			{
			sphere->indices[indexPos+_d_first]=pos;
			sphere->indices[indexPos+_d_second]=pos+1+(aSlices+1);
			sphere->indices[indexPos+_d_third]=pos+(aSlices+1);
			indexPos+=3;
			
			sphere->indices[indexPos+_d_first]=pos;
			sphere->indices[indexPos+_d_second]=pos+1;
			sphere->indices[indexPos+_d_third]=pos+1+(aSlices+1);
			indexPos+=3;
			pos++;
			}
		pos++;
		}	
	
	// Bottom
	for (int i=0; i<aSlices; i++)
		{
		sphere->indices[indexPos+_d_first]=pos;
		sphere->indices[indexPos+_d_second]=pos+1;
		sphere->indices[indexPos+_d_third]=pos+(aSlices+1);
		indexPos+=3;
		pos++;
		}
	return sphere;
	}
#endif
/*-----------------------------------------------------------------*/
// Creates sphere
//
