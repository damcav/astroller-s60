/*
 * ==============================================================================
 *  Name        : SimpleCube.cpp
 *  Part of     : OpenGLEx / SimpleCube
 *
 *  Copyright (c) 2004-2006 Nokia Corporation.
 *  This material, including documentation and any related
 *  computer programs, is protected by copyright controlled by
 *  Nokia Corporation.
 * ==============================================================================
 */


// INCLUDE FILES
#include <e32std.h>
#include <math.h>
#include "QGazer.h"
#include "mathutils.h"
#include "utils3d.h"
#include "glutils.h"
#include "DamProject.h"
#include "QGazerCb.h"
#include "CommonTypes.h"
#include "Model.h"
#include "context.h"
#include "TSkyColour.h"
#include "MAppUiCb.h"
#include "CommonTypes.h"

extern MAppUiCb* gAppUiCb;
extern Context* gContext;
#include "objects.inl"

CQGazer* CQGazer::NewL( TUint aWidth, TUint aHeight, const CSkyModel& aModel, MQGazerCb& aParent )
    {
    CQGazer* self = new (ELeave) CQGazer( aWidth, aHeight, aModel, aParent );
    CleanupStack::PushL( self );
    self->ConstructL();
    CleanupStack::Pop();
    return self;
    }

CQGazer::CQGazer( TUint aWidth, TUint aHeight, const CSkyModel& aModel, MQGazerCb& aParent ) :
    iWidth( aWidth ),
    iHeight( aHeight ),
    iParent(aParent),
    iModel(aModel)
    {
	iZoom = 1.0;
    }

CQGazer::~CQGazer()
    {
    delete iGlobe;
    delete iMoon;
    }

void CQGazer::ConstructL( void )
    {
    }

void CQGazer::OnStartLoadingTexturesL()
	{
	}

void CQGazer::OnEndLoadingTexturesL()
	{
	iReady = ETrue;
	iParent.GLReadyL();
	}
// -----------------------------------------------------------------------------
// CQGazer::AppInit
//
// Initializes OpenGL ES, sets the vertex and color arrays and pointers,
// and selects the shading mode.
// -----------------------------------------------------------------------------
//

const TReal KGlobeSize = 0.85;

void CQGazer::AppInitL( void )
    {
    // Initialize viewport and projection.
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
	SetScreenSize( iWidth, iHeight );

	glClearColor( 0.0f, 0.f, 0.f, 1.f );
    iGlobe = Utils::CreateSphereL(KGlobeSize, 32, 32);
    iMoon = Utils::CreateSphereL(0.04, 16, 16);
    
    InitCircle(0.04);
    
    
	// Construct a texture manager that uses the application's private
	// directory as the location for all textures.
    //GLfloat aspectRatio = (GLfloat)(iWidth) / (GLfloat)(iHeight);
    iTextureManager = CTextureManager::NewL ( iWidth, iHeight,
    		0,0,0,0,0,this);

    // Pushing the textures into the loading queue.
   _LIT(KGrassTexture, "grass2.jpg");
    iTextureManager->RequestToLoad(KGrassTexture, &iGrassTexture );
    _LIT(KGlobeTexture, "world.jpg");
    iTextureManager->RequestToLoad(KGlobeTexture, &iWorldTexture );
    
    _LIT(KMoonTexture, "moon.jpg");
    iTextureManager->RequestToLoad(KMoonTexture, &iMoonTexture );
    _LIT(KStarTexture, "star.jpg");
    iTextureManager->RequestToLoad(KStarTexture, &iStarTexture );
    _LIT(KArrowTexture, "arrow.jpg");
    iTextureManager->RequestToLoad(KArrowTexture, &iArrowTexture );

    
    //Start to load the textures.
    iTextureManager->DoLoadL();
    
	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	GLint glMaxRes;//OpenGL max texture resolution
	glGetIntegerv( GL_MAX_TEXTURE_SIZE, &glMaxRes );
	if (glMaxRes > 1024)
		gContext->SetFastOpenGL(ETrue);
    }

void CQGazer::DirectionalLight(const TVector& aVect)
	{
	if (gContext->InSpace() && gContext->Lights())
		return;
	
//	GLfloat LightAmbient[]= { 0.0f, 0.0f, 0.0f, 1.0f };
//	GLfloat LightAmbientSpace[]= { 1.0f, 1.0f, 1.0f, 1.0f };
//	GLfloat LightDiffuse[]= { 1.0f, 1.0f, 1.0f, 1.0f };
//	GLfloat LightAmbientR[]= { 0.5f, 0.0f, 0.0f, 1.0f };
//	GLfloat LightDiffuseR[]= { 1.0f, 0.0f, 0.0f, 1.0f };

	GLfloat LightAmbient[]= { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat LightAmbientSpace[]= { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat LightDiffuse[]= { 8.0f, 8.0f, 8.0f, 1.0f };
	GLfloat LightAmbientR[]= { 0.5f, 0.0f, 0.0f, 1.0f };
	GLfloat LightDiffuseR[]= { 1.0f, 0.0f, 0.0f, 1.0f };

	
	GLfloat LightPosition[]= { 0.0f, 0.0f, 0.0f, 0.0f };

	GLfloat mat_specular[] = {1,1,1,1};
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	GLfloat mat_shininess[] = {100};
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	//glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
	glShadeModel(GL_SMOOTH);
	
	LightPosition[0] = aVect.iX;
	LightPosition[1] = aVect.iY;
	LightPosition[2] = aVect.iZ;

	//-=----------------------------
	
	//int lastLight = GL_LIGHT0;//(gContext->InSpace()) ? GL_LIGHT7 : GL_LIGHT1;

	GLfloat c = 8.0;
	if (!gContext->InSpace())
		{
		if (!gContext->ShowAtmosphere())
			{
			c = 3.0;
			}
		else
			{
			if (iModel.Sun().iHr.iAltitude < 10.0)
				c = 2.0;
			else
				c = 0.75;
			}
		}
	
	LightDiffuse[0] = c;
	if (gContext->NightMode())
		c = 0;
	LightDiffuse[1] = LightDiffuse[2] = c;	
	
	TInt light = GL_LIGHT0;
		{
		glLightfv(light, GL_DIFFUSE, LightDiffuse);
		glLightfv(light, GL_SPECULAR, LightDiffuse);		
		if (!gContext->NightMode())
			{
			if (gContext->InSpace())
				glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightAmbientSpace);
			else
				glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightAmbient);
			}
		else
			{
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightAmbientR);
			}
		glLightfv(light, GL_POSITION, LightPosition);
		glEnable(light);
		}
	glEnable(GL_LIGHTING);
	}
// -----------------------------------------------------------------------------
// CQGazer::AppExit
//
// Release any allocations made in AppInit.
// -----------------------------------------------------------------------------
//

void CQGazer::AppExit( void )
    {
    delete iTextureManager;
    iTextureManager = NULL;
    }

// -----------------------------------------------------------------------------
// CQGazer::AppCycle
//
// Draws and animates the objects.
// The frame number determines the amount of rotation.
// -----------------------------------------------------------------------------
//

void CQGazer::ChangeRot(const TPoint& aDelta)
	{
	TInt dy = aDelta.iX / 4;
	iYRot += dy;
	if (iYRot < 0.0)
		iYRot += 360.0;
	else if (iYRot > 360.0)
		iYRot -= 360.0;
	
	TInt dx = -aDelta.iY / 4;
	iXRot += dx;
	if (iXRot < 0.0)
		iXRot += 360.0;
	else if (iXRot > 360.0)
		iXRot -= 360.0;
	}

void CQGazer::SeppoTest()
	{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
	
    //glTranslatef(0.0, 0.0, -10.0);
    glRotatef(-iXRot, 1.0, 0.0, 0.0);
    glRotatef(iYRot, 0,1,0);
    
    glDisable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    // Vertices
    const GLfloat vert[]=
        {
         0, 1, 4,
        -1, 0, 4,
         1, 0, 4,
        };
    
    // Normals per vertex
    const GLfloat norm[]=
        {
        0,0,1,
        0,0,1,
        0,0,1,
        };
    
    // Enable state and set pointers
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glVertexPointer(3, GL_FLOAT , 0, vert); 
    glNormalPointer(GL_FLOAT , 0, norm); 
    
    // Draw
    glDrawArrays(GL_TRIANGLES, 0, 3);
    
    // Disable states
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);	
	}




void CQGazer::DrawTheEarth()
	{
	glPushMatrix();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	TVector sunVect;
	iModel.Sun().Vector(sunVect);
	DirectionalLight(sunVect);
	
	if (!gContext->NightMode())
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	else
		glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
	
	glBindTexture( GL_TEXTURE_2D, iWorldTexture.iID );
	glEnable(GL_TEXTURE_2D);
	
	GLfloat time = ::gContext->GreenwichSiderealTime();

	glRotatef(180.0, 0.0, 0.0, 1.0);
	glRotatef(90.0-time*15.0, 0.0, 1.0, 0.0);	
		
	//glDisable(GL_CULL_FACE);
	
	glPointSize(1.0);
	iGlobe->draw();
//	sphere(0.5,32,32);

	glDisable(GL_LIGHTING);
	
	//glEnable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	}

void CQGazer::DrawTheMoon()
	{
	
	glEnable(GL_CULL_FACE);

	glBindTexture( GL_TEXTURE_2D, iMoonTexture.iID );
	glEnable(GL_TEXTURE_2D);
	glPointSize(1.0);
	glEnable(GL_SMOOTH);
	if (!gContext->NightMode())
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	else
		glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
	glDisable(GL_DEPTH_TEST);
	if (!gContext->InSpace())
		{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
		}
	iMoon->draw();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_CULL_FACE);
//    glCullFace(GL_BACK);
	}

void CQGazer::SetRotations()
	{
	if (gContext->InSpace())
		{
		iXRot = gContext->Camera().X();
		iYRot = gContext->Camera().Y();
		}
	else
		{
		///TLocation& loc = gContext->UserLocationMode() ? 
		//		gContext->UserLocation() : gContext->CurrentLocation();
		
		const TLocation& loc = gContext->Location();
		
		//Rotate Y by longitude
		float y = loc.Longitude().DecDegrees();
		if (y < 0)  //-1 == 359
			y = 360.0 + y;

		//adjust for RA = 0 being at right of screen
		iYRot = y - 90;

		  //adjust for sidereal time
		iYRot += gContext->GreenwichSiderealTime()*15.0;

		iYRot = -iYRot;

		//Rotate X by the latitude
		iXRot = loc.Latitude().DecDegrees();
		
		//Rotate Z by the direction we're looking in
		//If yDegrees = 0, we end up looking south
		iZRot = -gContext->Camera().Y();

		//Look up / down
		iXRot2 = 90.0 - gContext->Camera().X();
		}
	}

void CQGazer::AppCycle( TInt /*aFrame*/ )
    {
    if (!iReady)
    	{
    	glClearColor(0,0,0,0);
   		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   		glMatrixMode(GL_MODELVIEW);
   		glLoadIdentity();
   		return;
    	}
    

    
    
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	SetRotations();

	glLoadIdentity();

	glEnable(GL_DEPTH_TEST);

	if (gContext->InSpace())
		{
		GLfloat zoom = gContext->Camera().Zoom();	
		zoom *= 2;
		//TDebug::Print(zoom);
		
		glScalef(zoom, zoom, zoom);
		GetEarthRadiusL();
		}

	if (!gContext->InSpace())
		{
		glRotatef(iXRot2, 1.0, 0.0, 0.0);	//DAMEQ
		glRotatef(iZRot, 0.0, 0.0, 1.0);	//DAMEQ
		}
	
	glRotatef(-iXRot, 1.0, 0.0, 0.0);
	glRotatef(iYRot, 0,1,0);

	iDamProject = new (ELeave) gglMatrix();
								
	if (gContext->InSpace())
			DrawTheEarth();
		else
			Atmosphere();
	
	iModel.DrawGL();

	
	
	if (!gContext->InSpace())
		Horizon();
	/*
	glPointSize(10);
	const GLbyte KVertices []={	0,0,4}; 
	glEnableClientState (GL_VERTEX_ARRAY);
	glVertexPointer (3, GL_BYTE , 0, KVertices);	
	glDrawArrays (GL_POINTS, 0, 1);
	
	glPointSize(1);
	*/
	
//test damproject
	
	DrawArrow();

	

	delete iDamProject;
	iDamProject = NULL;
//---
	
    }

void CQGazer::ScreenCoords(const TVector& aVector, TScreenCoords& aScreen)
	{


	CPoint pix;
	iDamProject->World2Pixel(aVector.iX, aVector.iY, aVector.iZ, pix);
	
	TBool offScreen = ( pix.z > 1 || pix.x < 0 || pix.y < 0
					  || pix.x > iWidth || pix.y > iHeight);

	if (!offScreen)
		aScreen.Set(pix.x, iHeight - pix.y, pix.z, ETrue);
	else
		aScreen.Set(-1, -1, -1, EFalse);
	}
// -----------------------------------------------------------------------------
// CQGazer::SetScreenSize
// Reacts to the dynamic screen size change during execution of this program.
// -----------------------------------------------------------------------------
//
void CQGazer::SetScreenSize( TUint aWidth, TUint aHeight )
    {
    iWidth  = aWidth;
    iHeight = aHeight;

  // Reinitialize viewport and projection.
    glViewport( 0, 0, iWidth, iHeight );
    
    SetupView();
  }

void CQGazer::ZoomIn()
	{
	}

void CQGazer::ZoomOut()
	{
	}

void CQGazer::SetupView()
	{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (gContext->InSpace())
		{
		float xf=1.0;
		float yf=1.0;
		if (iWidth > iHeight)
			xf = (float)iWidth/(float)iHeight;
		else
			yf = (float)iHeight/(float)iWidth;

		// +10 to 0 is VISIBLE 		//---------------- POSITIVE Z IS VISIBLE
		GLfloat zoom = gContext->Camera().Zoom();
		glOrthof(-1*xf, 1*xf, -1*yf, +1*yf, -10.0, 0);
		}
	else
		{
//		GLfloat zoom = gContext->Camera().Zoom();
		GLfloat zoom=0.4 * gContext->Camera().Zoom(); 
		gluPerspective(60.0-50.f*zoom,(GLfloat)iWidth/(GLfloat)iHeight, 0.001f,100.0f);
		}
	
	// 0 to -10 is VISIBLE
	//glOrthof(-1*xf*zoom, 1*xf*zoom, -1*yf*zoom, +1*yf*zoom, 0, 10);
	//gluPerspective(45.0f,(GLfloat)iWidth/(GLfloat)iHeight, 0.1f,1000.0f);
	//gluPerspective(45.0f,(GLfloat)iWidth/(GLfloat)iHeight, 0.1f,10.0f);
	//glOrthof(-10,10, -10,10, 0,10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	}

const GLfloat DegInRad = DEG_IN_RADIAN;
void CQGazer::Vector(const TEquatorial& aEq, TVector& aVector, GLfloat aLength )
{
	GLfloat angle = aEq.iDec / DegInRad;
	GLfloat XX = (aLength * cos(angle));
	aVector.iY = (aLength * sin(angle));

	angle = aEq.iRA / DegInRad;
	aVector.iX = (XX * cos(angle));
	aVector.iZ = (XX * -sin(angle));
}


void CQGazer::BillBoard()
{
	glMultMatrixf(iModelViewBillboard);
}

void CQGazer::StartPoints(GLfloat* aVertices)
	{
	glDisableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glVertexPointer (3, GL_FLOAT , 0, aVertices);
	}

void CQGazer::StartPointSprites(GLfloat* aVertices)
	{
	glBindTexture(GL_TEXTURE_2D, iStarTexture.iID);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_POINT_SPRITE_OES);
	glTexEnvi(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
	glEnableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
	glVertexPointer (3, GL_FLOAT , 0, aVertices);
	}

void CQGazer::EndPointSprites()
	{
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glTexEnvi(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_FALSE);
	glDisable(GL_POINT_SPRITE_OES);
	}



const GLfloat CQGazer::iQt[] = {0,1, 1,0, 0,0, 0,1, 1,1, 1,0};

void CQGazer::BeginQuadTex(GLuint aTexture)
	{
	glBindTexture(GL_TEXTURE_2D, aTexture);
	glEnable(GL_TEXTURE_2D);
    glEnableClientState( GL_TEXTURE_COORD_ARRAY );
    glTexCoordPointer( 2, GL_FLOAT, 0, iQt );
	}

void CQGazer::EndTex()
	{
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	}

void CQGazer::DrawQuadCentred(GLfloat aWidth, GLfloat aHeight)
	{
	iQv[0] = -aWidth; iQv[1] = -aHeight; 
    iQv[2] = aWidth; iQv[3] = aHeight;
    iQv[4] = -aWidth; iQv[5] = aHeight;
    
    iQv[6] = -aWidth; iQv[7] = -aHeight;
    iQv[8] = aWidth; iQv[9] = -aHeight;
    iQv[10] = aWidth; iQv[11] = aHeight;
    
	glEnableClientState( GL_VERTEX_ARRAY );
    glVertexPointer( 2, GL_FLOAT, 0, iQv );
    glDrawArrays (GL_TRIANGLES, 0, 2*3);
	}

void CQGazer::DrawQuadDown(GLfloat aWidth, GLfloat aHeight)
	{
	iQv[0] = 0; iQv[1] = -aHeight; 
    iQv[2] = aWidth; iQv[3] = 0;
    iQv[4] = 0; iQv[5] = 0;
    
    iQv[6] = 0; iQv[7] = -aHeight;
    iQv[8] = aWidth; iQv[9] = -aHeight;
    iQv[10] = aWidth; iQv[11] = 0;
    
	glEnableClientState( GL_VERTEX_ARRAY );
    glVertexPointer( 2, GL_FLOAT, 0, iQv );
    glDrawArrays (GL_TRIANGLES, 0, 2*3);
	}





//const GLfloat CQGazer::iQc4[] = {1,0,0,1, 0,0,1,1, 0,0,1,1,   1,0,0,1,  1,0,0,1, 0,0,1,1};

void CQGazer::DrawQuadColour(GLfloat aWidth, GLfloat aHeight, const TColour& aTop, const TColour& aBottom)
	{
	
	aBottom.Copy4	(&iQcT[0 *4]);
	aBottom.Copy4	(&iQcT[3 *4]);
	aBottom.Copy4	(&iQcT[4 *4]);
	aTop.Copy4		(&iQcT[1 *4]);
	aTop.Copy4		(&iQcT[2 *4]);
	aTop.Copy4		(&iQcT[5 *4]);
	
	glEnableClientState( GL_COLOR_ARRAY );
	glColorPointer (4, GL_FLOAT, 0, (GLfloat*)iQcT);//iQc4);
	glShadeModel(GL_SMOOTH);
	
	DrawQuadCentred(aWidth, aHeight);
	
	glShadeModel(GL_FLAT);
	glDisableClientState( GL_COLOR_ARRAY );
	}

void CQGazer::DrawQuad3D(GLfloat aSize)
	{
	iQv3[1] = iQv3[4] = iQv3[7] = iQv3[10] = iQv3[13] = iQv3[16] = 0;  
	
	iQv3[0] = -aSize; iQv3[2] = -aSize; 
    iQv3[3] = aSize; iQv3[5] = aSize;
    iQv3[6] = -aSize; iQv3[8] = aSize;
    
    iQv3[9] = -aSize; iQv3[11] = -aSize;
    iQv3[12] = aSize; iQv3[14] = -aSize;
    iQv3[15] = aSize; iQv3[17] = aSize;
    
	glEnableClientState( GL_VERTEX_ARRAY );
    glVertexPointer( 3, GL_FLOAT, 0, iQv3 );
    glDrawArrays (GL_TRIANGLES, 0, 2*3);
	}

const GLfloat KGrassT[] = {0,9, 9,0, 0,0, 0,9, 9,9, 9,0};
/*
void CQGazer::Horizon()
	{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
	glColor4f(1,1,1,1);
	glTranslatef(0,0,-1);
	//glTranslatef(gContext->Camera().Y()/180,-0.5, 0);
//	glRotatef(-gContext->Camera().X(), 1.0, 0.0, 0.0);
//	glRotatef(gContext->Camera().Y(), 0.0, 1.0, 0.0);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	BeginQuadTex(iGrassTexture.iID);
	DrawQuad(6, 0.5);
//	glTranslatef(24,0,0);
//	DrawQuad(6, 0.5);
	
	EndTex();
	glPopMatrix();
	}
*/


void CQGazer::Horizon()
{

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	GLfloat w = 8.0 * (1.0 - gContext->iStarFactor) + 2.0;
	glColor4f(w, w, w, 1);

	glPushMatrix();
	glLoadIdentity();
	glRotatef(-gContext->Camera().X(), 1.0, 0.0, 0.0);
	glRotatef(gContext->Camera().Y(), 0.0, 1.0, 0.0);

	glEnable(GL_BLEND);//xxxx


	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);// GL_ONE_MINUS_SRC_ALPHA);


	glTranslatef(0,-2,0);

	glBindTexture(GL_TEXTURE_2D, iGrassTexture.iID);
	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );


   glEnableClientState( GL_TEXTURE_COORD_ARRAY );
   glTexCoordPointer( 2, GL_FLOAT, 0, KGrassT );
    glDisableClientState(GL_COLOR_ARRAY);

    GLfloat c=(1.0-gContext->iStarFactor);
    c *= 0.8;
    c += 0.2;
    if (gContext->NightMode())
        glColor4f(c,0,0,1);
    else
    	glColor4f(c,c,c,1);
    DrawQuad3D(300);

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

void CQGazer::Atmosphere()
	{
	if (!gContext->ShowAtmosphere())
		{
		glClearColor(0,0,0,0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		gContext->iStarFactor = 1.0;
		return;
		}
	glPushMatrix();
	glLoadIdentity();
	GLfloat height = 1.5 / 2;
	GLfloat width = 5.0 / 2;
	GLfloat gap = 0.15;

	GLfloat adjacent = 2.0;
	GLfloat opposite = adjacent * tan(gContext->Camera().X() / DEG_IN_RADIAN);
	
	glTranslatef(0, (-opposite)+height, -adjacent);

	iTop.Set(255,0,0);
	iBottom.Set(0,255,0);
	
	glShadeModel(GL_SMOOTH);

	TSkyColour sky;
	sky.Colours(iModel.Sun().iHr, iBottom, iTop, gContext->iStarFactor);
	//Dim the part of the sky that's opposite to the sun

	THorizontal s = iModel.Sun().iHr;
	if (s.iAltitude < 10.0 && s.iAltitude > -10.0)
		{
		GLfloat diff =  gContext->Camera().Y()- s.iAzimuth;
		while (diff < -180) diff  += 360;
		while (diff > 180) diff -= 360;
		diff = Abs(diff);
		diff = 180 - diff;

		if (diff > 80)	//MAGIC!
			{
			diff -= 80;
			int f = diff/2;
			TColourTransform c;	
			c.Set(iTop, TColour(1,1,1));
			iTop = c.Colour(f);
			c.Set(iBottom, TColour(1,1,1));
			iBottom = c.Colour(f);
			}
		}
	
	
	
	
	//TDebug::Print(gContext->iStarFactor, iModel.Sun().iHr.iAltitude);
	
	iTop.GLClearColour();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	DrawQuadColour(width, height, iTop, iBottom);
	
	glLoadIdentity();
	glTranslatef(0, (-opposite)-gap/2, -adjacent);

	iBottom.GlColour();
	DrawQuadCentred(2.5, gap/2);
	
	glPopMatrix();
}

void CQGazer::InitCircle(GLfloat aRadius)
	{
	TInt v=0;

	iCircleVertices[v++] = 0;
	iCircleVertices[v++] = 0;
	iCircleVertices[v++] = 0;
	
	for (GLfloat angle = 0.0; angle < 360.0; angle += 5.0 )
		{
		iCircleVertices[v++] = cos(angle / DegInRad) * aRadius;
		iCircleVertices[v++] = sin(angle / DegInRad) * aRadius;
		iCircleVertices[v++] = 0;
		}
	
	iCircleVertices[v++] = aRadius;
	iCircleVertices[v++] = 0;
	iCircleVertices[v++] = 0;
	}

void CQGazer::DrawCircle(const TVector& aVector)
	{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	
//Billboard	
	
	glTranslatef(aVector.iX, aVector.iY, aVector.iZ);
	
	glRotatef(-iYRot, 0,1,0);
    glRotatef(iXRot, 1.0, 0.0, 0.0);

	if (!gContext->InSpace())
		{
		glRotatef(-iZRot, 0.0, 0.0, 1.0);	//DAMEQ
			glRotatef(-iXRot2, 1.0, 0.0, 0.0);	//DAMEQ
		}

	glDisable(GL_DEPTH_TEST);
	glDisableClientState(GL_COLOR_ARRAY);
	glEnableClientState( GL_VERTEX_ARRAY );
    glVertexPointer( 3, GL_FLOAT, 0, iCircleVertices );
    glDrawArrays (GL_TRIANGLE_FAN, 0, 74);
    glPopMatrix();
    glEnable(GL_DEPTH_TEST);
	}

void CQGazer::DrawArrow()
	{
	//SCREENSHOTS!
	if (gContext->SensorMode() && !gContext->UserLocationMode())
		{
		TChosenObject& chosen = gContext->ChosenObject();
		if (chosen.iObject)
			{
			if (!gContext->ChosenObject().iScreen.OnScreen())
				{
				GLfloat b = gContext->ChosenObject().iBearing;
				glPushMatrix();
				glLoadIdentity();
				glRotatef(-b, 0, 0, 1);
				glTranslatef(0, 0, -1);
				glDisable(GL_DEPTH_TEST);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
				BeginQuadTex(iArrowTexture.iID);
				GLfloat zoom = gContext->Camera().Zoom();
				DrawQuadCentred(0.1 / zoom, 0.15 / zoom);
				EndTex();
				glDisable(GL_BLEND);
				glPopMatrix();
				}
			}
		}
	}
	
void CQGazer::GetEarthRadiusL()
{
	//TODO Optimise: this only needs to be done when the zoom changes
	if (!iGotRadius)
		{
		iGotRadius = ETrue;
		
		iDamProject = new (ELeave) gglMatrix();
	
		TVector v(KGlobeSize, 0, 0);
		TScreenCoords sphereEdge;
		ScreenCoords(v, sphereEdge);
	
		v.iX = iWidth/2;
		v.iY = iHeight/2;
		v.iZ = sphereEdge.Point().iX - iWidth/2;
		
		gContext->SetEarthPosAndSize(v);
	
		delete iDamProject;
		iDamProject = NULL;
		}
	}

/*
 	float a=300;
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);  glVertex3f(-a,0,-a);
	glTexCoord2f(9,0);  glVertex3f( a,0,-a);
	glTexCoord2f(9,9);  glVertex3f( a,0, a);
	glTexCoord2f(0,9);  glVertex3f(-a,0, a);
	glEnd(); 
 */
// End of File
