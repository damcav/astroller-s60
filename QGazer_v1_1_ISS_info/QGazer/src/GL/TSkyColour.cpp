/*
 * TSkyColour.cpp
 *
 *  Created on: 23-Mar-2010
 *      Author: Damien
 */

#include "TSkyColour.h"

const TColour KSky(65,172,252);
const TColour KSkyLight(170,251,253);
const TColour KSkyDark(2, 106, 183);
const TColour KGreyOrange(254,192,137);
const TColour KSunsetOrange(253,163,110);
const TColour KNightLight(2,1,40);
const TColour KNight(2,1,20);
const TColour KGrassLight(255,255,255);
const TColour KGrassDark(16,16,16);

//	TOP				BOTTOM						GRASS			STARFACTOR
//	sky				skyLight					grassLight		0.0
const float KStage1 = 12.5;	//	sky->skyDark	skyLight->greyOrange		grassLight->	0.0
const float KStage2 = 9.0;	//	skyDark->		greyOrange->sunsetOrange					0.0->
const float KStage3 = -1.0;	//		..night		sunsetOrange->nightLight
const float KStage4 = -10;	//	night			nightLight						..grassDark		..1.0

void TSkyColour::Colours(const THorizontal& aSun, TColour& aBottom, TColour& aTop, GLfloat& aStarFactor)
{
	const GLfloat& alt = aSun.iAltitude;
	
	if (alt > KStage1)
	{
		aBottom = KSkyLight;
		aTop = KSky;
		aStarFactor = 0.0;
		return;
	}

	if (alt <= KStage4)
	{
	
		aBottom = KNightLight;
		aTop = KNight;
		aStarFactor = 1.0;
		return;
	}

	float brLow(0), brHigh(0), trLow(0), trHigh(0);

	float range = KStage1 - KStage4;
	float s = alt -  KStage4;
	float percentage = (s / range) * 100.0;
	percentage = 100-percentage;
	
	if (	alt <= KStage1 && alt > KStage2)
	{
	
		iTransformBottom.Set(KSkyLight, KGreyOrange);
		brLow = KStage2; brHigh = KStage1;
		iTransformTop.Set(KSky, KSkyDark);
		trLow = KStage2; trHigh = KStage1;
		aStarFactor = 0;	//star factor starts at KStage 2
	}
	else if (alt <= KStage2 && alt > KStage3)
	{
		iTransformBottom.Set(KGreyOrange, KSunsetOrange);
		brLow = KStage3; brHigh = KStage2;
		iTransformTop.Set(KSkyDark, KNight);
		trLow = KStage4; trHigh = KStage2;
	}
	else if (alt <= KStage3 && alt > KStage4)
	{
		iTransformBottom.Set(KSunsetOrange, KNightLight);
		brLow = KStage4; brHigh = KStage3;
		iTransformTop.Set(KSkyDark, KNight);
		trLow = KStage4; trHigh = KStage2;
	}

	range = KStage2 - KStage4;
	s = alt - KStage4;
	if (s<=range)	//could get here during KStage 1...
	{
		percentage = (s / range);
		percentage *= 1.25;	//stars disappear quicker
		if (percentage>1.0)
			percentage = 1.0;
		aStarFactor = 1.0-percentage;
	}

	range = brHigh - brLow;
	s = alt - brLow;
	percentage = (s / range) * 100.0;
	aBottom = iTransformBottom.Colour(100.0 - percentage);

	range = trHigh - trLow;
	s = alt - trLow;
	percentage = (s / range) * 100.0;
	aTop = iTransformTop.Colour(100.0 - percentage);

#if(0)
	//Dim the part of the sky that's opposite to the sun
	int d = QG::context.camera().fieldOfView().distanceFromCentre(aSun.iAzimuth);
	if (d > 80)	//MAGIC!
	{
		d -= 80;
		int f = 100 + d/2;
		aBottom = aBottom.darker(f);
		aTop = aTop.darker(f);
	}
#endif
}

